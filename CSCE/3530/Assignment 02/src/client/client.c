/* ************************************************************************* *
 *                                                                           *
 * Copyright (c) 2015, Ryan Porterfield                                      *
 * All rights reserved.                                                      *
 *                                                                           *
 * Redistribution and use in source and binary forms, with or without        *
 * modification, are permitted provided that the following conditions        *
 * are met:                                                                  *
 *                                                                           *
 *     1. Redistributions of source code must retain the above copyright     *
 *     notice, this list of conditions and the following disclaimer.         *
 *                                                                           *
 *     2. Redistributions in binary form must reproduce the above copyright  *
 *     notice, this list of conditions and the following disclaimer in the   *
 *     documentation and/or other materials provided with the distribution.  *
 *                                                                           *
 *     3. Neither the name of the copyright holder nor the names of its      *
 *     contributors may be used to endorse or promote products derived from  *
 *     this software without specific prior written permission.              *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS   *
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED     *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A           *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT        *
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,    *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED  *
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR    *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF    *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING      *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS        *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.              *
 *                                                                           *
 * ************************************************************************* */

#include <iso646.h>
#include <netdb.h>
#include <signal.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

#define BUFFER_LEN 2048
#define HOST_PORT 17754
#define HOSTNAME "cse01.cse.unt.edu"

static bool running;
static int socketfd;

static int connect_to_host (const char const *hostname, int *sockfd);
static void get_input (char *buffer, size_t buffer_len);
static void run ();
static void sig_handler (int signo);

int
main (void)
{

	/*if (signal (SIGINT, sig_handler) == SIG_ERR) {
        fprintf(stderr, "ERROR: Can't set signal handler\n");
        return EXIT_FAILURE;
    }*/

    running = true;
    puts ("Press Ctrl-C to exit the program");
	run ();

	return EXIT_SUCCESS;
}


/**	@brief
 *
 *	@param hostname	The hostname of the server we're requesting a web page from.
 *	@param sockfd	The file descriptor for the connection to the server.
 *					Used for reading and writing to the server.
 */
int
connect_to_host (const char const *hostname, int *sockfd)
{
	int status;
    struct hostent *host;
    struct sockaddr_in serv_addr;

    if ((host = gethostbyname (hostname)) == NULL) {
        herror ("gethostbyname");
        return EXIT_FAILURE;
    }

    puts ("Resolved hostname, creating socket");

    memcpy (&(serv_addr.sin_addr.s_addr), host->h_addr, host->h_length);
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(HOST_PORT);

    *sockfd = socket (AF_INET, SOCK_STREAM, 0);
    status = connect (*sockfd, (struct sockaddr*) &serv_addr, sizeof (serv_addr));
    if (status < 0) {
        perror ("ERROR connecting");
    	return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}


void
get_input(char *buffer, size_t buffer_len)
{
	size_t in_len;
	memset (buffer, 0, buffer_len);
	puts ("Enter a URL");
	gets (buffer);
	in_len = strlen (buffer);
	if (buffer[in_len - 1] == '\n')
		buffer[in_len - 1] = '\0';
    printf ("Got input: \"%s\"\n", buffer);
}


/**
 *
 */
void
run ()
{
	char buffer[BUFFER_LEN];
    int *sockfd = &socketfd;

	while (running) {
        // Get hostname
        get_input (buffer, BUFFER_LEN);
        printf ("Writing data: \"%s\"\n", buffer);
        // Connect to server
        if (connect_to_host (HOSTNAME, sockfd) not_eq 0)
            return;
        // Write hostname
		write (*sockfd, buffer, strlen (buffer));
        // Read response
        memset (buffer, 0, BUFFER_LEN);
    	read (*sockfd, buffer, BUFFER_LEN);
    	puts ("\nGot response:");
    	puts (buffer);
        close (*sockfd);
	}
}


/** @brief          Handle signals from the kernel
 *
 */
void
sig_handler (int signo)
{
    if (signo not_eq SIGINT)
        return;
    puts ("Caught SIGINT, closing connections and exiting");
    running = false;
    close (socketfd);
}

/* ************************************************************************* *
 *                                    EOF                                    *
 * ************************************************************************* */