/* ************************************************************************* *
 *                                                                           *
 * Copyright (c) 2015, Ryan Porterfield                                      *
 * All rights reserved.                                                      *
 *                                                                           *
 * Redistribution and use in source and binary forms, with or without        *
 * modification, are permitted provided that the following conditions        *
 * are met:                                                                  *
 *                                                                           *
 *     1. Redistributions of source code must retain the above copyright     *
 *     notice, this list of conditions and the following disclaimer.         *
 *                                                                           *
 *     2. Redistributions in binary form must reproduce the above copyright  *
 *     notice, this list of conditions and the following disclaimer in the   *
 *     documentation and/or other materials provided with the distribution.  *
 *                                                                           *
 *     3. Neither the name of the copyright holder nor the names of its      *
 *     contributors may be used to endorse or promote products derived from  *
 *     this software without specific prior written permission.              *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS   *
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED     *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A           *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT        *
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,    *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED  *
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR    *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF    *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING      *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS        *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.              *
 *                                                                           *
 * ************************************************************************* */

#include <iso646.h>
#include <netdb.h>
#include <signal.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

#include "../tcpsegment.h"
#include "program.h"

#define BUFFER_LEN 2048
#define HOST_PORT 17754
#define HOSTNAME "cse01.cse.unt.edu"

static void close_connection (ProgramData data);
static int connect_to_host (const char const *hostname, int *sockfd);
static void get_input (char *buffer, size_t buffer_len);
static void open_connection (ProgramData data);
static bool readfile (FILE* file, char* buff, size_t len);
static void run (ProgramData data);
static void transmit_file (ProgramData data);

int
main (int argc, const char** argv)
{
    srand (time (NULL));

    ProgramData data = init_program(argc, argv);
    puts ("Press Ctrl-C to exit the program");
	run (data);

	return EXIT_SUCCESS;
}


void
close_connection (ProgramData data) 
{
    // Send FIN
    TCPSegment_t segment;
    init_segment (&segment, FIN_BIT, NULL);
    write_segment (&segment, data->sockfd);
    // Receive ACK
    read_segment (&segment, data->sockfd);
    // Receive FIN
    read_segment (&segment, data->sockfd);
    // Send ACK
    reply_segment (&segment, ACK_BIT, NULL);
    write_segment (&segment, data->sockfd);
}


/**	@brief
 *
 *	@param hostname	The hostname of the server we're requesting a web page from.
 *	@param sockfd	The file descriptor for the connection to the server.
 *					Used for reading and writing to the server.
 */
int
connect_to_host (const char const *hostname, int *sockfd)
{
	int status;
    struct hostent *host;
    struct sockaddr_in serv_addr;

    if ((host = gethostbyname (hostname)) == NULL) {
        herror ("gethostbyname");
        return EXIT_FAILURE;
    }

    puts ("Resolved hostname, creating socket");

    memcpy (&(serv_addr.sin_addr.s_addr), host->h_addr, host->h_length);
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(HOST_PORT);

    *sockfd = socket (AF_INET, SOCK_STREAM, 0);
    status = connect (*sockfd, (struct sockaddr*) &serv_addr, sizeof (serv_addr));
    if (status < 0) {
        perror ("ERROR connecting");
    	return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}


/** @brief      Left over from assignment 02.
 *
 */
void
get_input(char *buffer, size_t buffer_len)
{
	size_t in_len;
	memset (buffer, 0, buffer_len);
	puts ("Enter a URL");
	gets (buffer);
	in_len = strlen (buffer);
	if (buffer[in_len - 1] == '\n')
		buffer[in_len - 1] = '\0';
    printf ("Got input: \"%s\"\n", buffer);
}


/** @brief      Open a TCP connection with the server.
 *
 */
void
open_connection (ProgramData data)
{
    // Send SYN packet
    TCPSegment_t segment;
    init_segment (&segment, SYN_BIT, NULL);
    write_segment (&segment, data->sockfd);
    // Receive SYNACK
    read_segment (&segment, data->sockfd);
    // Send ACK
    reply_segment (&segment, ACK_BIT, NULL);
    write_segment (&segment, data->sockfd);
}


/** @brief Read a file into a buffer.
 *
 */
bool
readfile (FILE* file, char* buff, size_t len)
{
    for (int i = 0; (i < len); ++i) {
        char c = fgetc (file);
        buff[i] = c;
        if (c == EOF)
            return false;
    }
    return true;
}


/** @brief
 *
 */
void
run (ProgramData data)
{
    if (connect_to_host (HOSTNAME, &(data->sockfd)) not_eq 0)
        return;

    open_connection (data);
    transmit_file (data);
    close_connection (data);

    close (data->sockfd);
}


/** @brief
 *
 */
void
transmit_file (ProgramData data)
{
    char buff[DATA_SIZE];
    FILE* file = fopen (data->filename, "r");
    TCPSegment_t segment;

    readfile (file, buff, DATA_SIZE);
    init_segment (&segment, 0, buff);
    write_segment (&segment, data->sockfd);
    read_segment (&segment, data->sockfd);      // Receive ACK

    while (readfile (file, buff, DATA_SIZE)) {
        reply_segment (&segment, 0, buff);
        write_segment (&segment, data->sockfd);
        read_segment (&segment, data->sockfd);  // Receive ACK
    }

    fclose (file);
}


/* ************************************************************************* *
 *                                    EOF                                    *
 * ************************************************************************* */