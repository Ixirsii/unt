/* ************************************************************************* *
 *                                                                           *
 * Copyright (c) 2015, Ryan Porterfield                                      *
 * All rights reserved.                                                      *
 *                                                                           *
 * Redistribution and use in source and binary forms, with or without        *
 * modification, are permitted provided that the following conditions        *
 * are met:                                                                  *
 *                                                                           *
 *     1. Redistributions of source code must retain the above copyright     *
 *     notice, this list of conditions and the following disclaimer.         *
 *                                                                           *
 *     2. Redistributions in binary form must reproduce the above copyright  *
 *     notice, this list of conditions and the following disclaimer in the   *
 *     documentation and/or other materials provided with the distribution.  *
 *                                                                           *
 *     3. Neither the name of the copyright holder nor the names of its      *
 *     contributors may be used to endorse or promote products derived from  *
 *     this software without specific prior written permission.              *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS   *
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED     *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A           *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT        *
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,    *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED  *
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR    *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF    *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING      *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS        *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.              *
 *                                                                           *
 * ************************************************************************* */

#ifndef TCP_SEGMENT_H_
#define TCP_SEGMENT_H_

#include <stdbool.h>
#include <stdint.h>

#define ACK_BIT (1 << 11)
#define SYN_BIT (1 << 14)
#define FIN_BIT (1 << 15)

#define DATA_SIZE (128 / 32)

typedef struct TCPSegment {
    int16_t source_port;
    int16_t dest_port;
    uint32_t sequence;
    uint32_t acknowledgement;
    int16_t flags;
    int16_t recv_window;
    uint16_t checksum;
    int16_t urgent_data;
    int32_t options;
    char data[DATA_SIZE];
} __attribute__((packed)) TCPSegment_t;

void init_segment (TCPSegment_t* segment, int16_t flags, char* data);
bool is_valid (TCPSegment_t* segment);
void read_segment (TCPSegment_t* segment, int32_t sockfd);
void reply_segment (TCPSegment_t* segment, int16_t flags, char* data);
void write_segment (TCPSegment_t* segment, int32_t sockfd);

#endif /* TCP_SEGMENT_H_ */


/* ************************************************************************* *
 *                                    EOF                                    *
 * ************************************************************************* */