/* ************************************************************************* *
 *                                                                           *
 * Copyright (c) 2015, Ryan Porterfield                                      *
 * All rights reserved.                                                      *
 *                                                                           *
 * Redistribution and use in source and binary forms, with or without        *
 * modification, are permitted provided that the following conditions        *
 * are met:                                                                  *
 *                                                                           *
 *     1. Redistributions of source code must retain the above copyright     *
 *     notice, this list of conditions and the following disclaimer.         *
 *                                                                           *
 *     2. Redistributions in binary form must reproduce the above copyright  *
 *     notice, this list of conditions and the following disclaimer in the   *
 *     documentation and/or other materials provided with the distribution.  *
 *                                                                           *
 *     3. Neither the name of the copyright holder nor the names of its      *
 *     contributors may be used to endorse or promote products derived from  *
 *     this software without specific prior written permission.              *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS   *
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED     *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A           *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT        *
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,    *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED  *
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR    *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF    *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING      *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS        *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.              *
 *                                                                           *
 * ************************************************************************* */

#include "tcpsegment.h"
#include <iso646.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>

#define HOST_PORT 17754
#define OUT_FILE "segments.txt"

static uint32_t compute_checksum (TCPSegment_t* segment);
static void deserialize_segment (TCPSegment_t* segment);
static void serialize_segment (TCPSegment_t* segment);
static void write_segment_out (TCPSegment_t* segment);
static void write_segment_to_file (TCPSegment_t* segment, FILE* out_file);


uint32_t
compute_checksum (TCPSegment_t* segment)
{
    uint32_t cksum = 0, sum = segment->source_port;
    sum += segment->dest_port;
    sum += segment->sequence;
    sum += segment->acknowledgement;
    sum += segment->flags;
    sum += segment->recv_window;
    sum += segment->urgent_data;
    sum += segment->options;
    if (segment->data not_eq NULL) {
        for (int i = 0; i < DATA_SIZE; ++i)
            sum += segment->data[i];
    }
    
    cksum = sum >> 16;              // Fold once
    sum = sum & 0x0000FFFF; 
    sum = cksum + sum;

    cksum = sum >> 16;             // Fold once more
    sum = sum & 0x0000FFFF;
    cksum = cksum + sum;
    return cksum;
}


void
deserialize_segment (TCPSegment_t* segment)
{
	segment->source_port = ntohs (segment->source_port);
	segment->dest_port = ntohs (segment->dest_port);
	segment->sequence = ntohl (segment->sequence);
	segment->acknowledgement = ntohl (segment->acknowledgement);
	segment->flags = ntohs (segment->flags);
	segment->recv_window = ntohs (segment->recv_window);
	segment->checksum = ntohs (segment->checksum);
	segment->urgent_data = ntohs (segment->urgent_data);
	segment->options = ntohl (segment->options);
}


bool
is_valid (TCPSegment_t* segment)
{
    return segment->checksum == compute_checksum (segment);
}


void
init_segment (TCPSegment_t* segment, int16_t flags, char* data) {
    int16_t len = 0;
    segment->sequence = 0;
    if (data != NULL) {
        len = DATA_SIZE;
    }
    len <<= 12;
    segment->source_port = HOST_PORT;
    segment->dest_port = HOST_PORT;
    segment->acknowledgement = 0;
    segment->flags = len | flags;
    segment->recv_window = 0;
    segment->urgent_data = 0;
    segment->options = 0;
    if (data not_eq NULL)
        memcpy (segment->data, data, DATA_SIZE);
    segment->checksum = compute_checksum (segment);
}


void
read_segment (TCPSegment_t* segment, int32_t sockfd)
{
	int recv_len = read (sockfd, segment, sizeof (TCPSegment_t));
	if (recv_len < (sizeof (TCPSegment_t) - DATA_SIZE)) {
		fprintf(stderr, "ERROR: Didn't receive whole segment\n");
		/*return;*/
	}
	deserialize_segment (segment);
    write_segment_out (segment);
}


void
reply_segment (TCPSegment_t* segment, int16_t flags, char* data)
{
    int16_t len = 32 * (segment->flags >> 12);
    segment->sequence = segment->acknowledgement;
    segment->acknowledgement = segment->sequence + 1;
    segment->acknowledgement += len;
    
    if (data != NULL) {
        len = DATA_SIZE;
    }
    len <<= 12;
    segment->flags = len | flags;
    if (data not_eq NULL)
        memcpy (segment->data, data, DATA_SIZE);
    segment->checksum = compute_checksum (segment);
}


void
serialize_segment (TCPSegment_t* segment)
{
	segment->source_port = htons (segment->source_port);
	segment->dest_port = htons (segment->dest_port);
	segment->sequence = htonl (segment->sequence);
	segment->acknowledgement = htonl (segment->acknowledgement);
	segment->flags = htons (segment->flags);
	segment->recv_window = htons (segment->recv_window);
	segment->checksum = htons (segment->checksum);
	segment->urgent_data = htons (segment->urgent_data);
	segment->options = htonl (segment->options);
}


void
write_segment_out (TCPSegment_t* segment)
{
    write_segment_to_file (segment, stdout);
    FILE* out_file = fopen (OUT_FILE, "a");
    write_segment_to_file (segment, out_file);
    fclose (out_file);
}


void
write_segment_to_file (TCPSegment_t *segment, FILE* out_file)
{
    fprintf (out_file, "\t\t\t\tTCP SEGMENT:\n");
    fprintf (out_file, "Source port:\t\t%hd\n", segment->source_port);
    fprintf (out_file, "Dest port:\t\t%hd\n", segment->dest_port);
    fprintf (out_file, "Sequence:\t\t%u\n", segment->sequence);
    fprintf (out_file, "Acknowledgement:\t%u\n", segment->acknowledgement);
    fprintf (out_file, "Flags:\t\t\t%hd\n", segment->flags);
    fprintf (out_file, "Recv window:\t\t%hd\n", segment->recv_window);
    fprintf (out_file, "Checksum:\t\t%hu\n", segment->checksum);
    fprintf (out_file, "Urgent data:\t\t%hd\n", segment->urgent_data);
    fprintf (out_file, "Options:\t\t%hd\n", segment->options);
    fprintf (out_file, "\n");
}


void
write_segment (TCPSegment_t* segment, int sockfd)
{
    write_segment_out (segment);
	serialize_segment (segment);
	write (sockfd, segment, sizeof (TCPSegment_t));
}


/* ************************************************************************* *
 *                                    EOF                                    *
 * ************************************************************************* */