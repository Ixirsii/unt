/* ************************************************************************* *
 *                                                                           *
 * Copyright (c) 2015, Ryan Porterfield                                      *
 * All rights reserved.                                                      *
 *                                                                           *
 * Redistribution and use in source and binary forms, with or without        *
 * modification, are permitted provided that the following conditions        *
 * are met:                                                                  *
 *                                                                           *
 *     1. Redistributions of source code must retain the above copyright     *
 *     notice, this list of conditions and the following disclaimer.         *
 *                                                                           *
 *     2. Redistributions in binary form must reproduce the above copyright  *
 *     notice, this list of conditions and the following disclaimer in the   *
 *     documentation and/or other materials provided with the distribution.  *
 *                                                                           *
 *     3. Neither the name of the copyright holder nor the names of its      *
 *     contributors may be used to endorse or promote products derived from  *
 *     this software without specific prior written permission.              *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS   *
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED     *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A           *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT        *
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,    *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED  *
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR    *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF    *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING      *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS        *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.              *
 *                                                                           *
 * ************************************************************************* */

#include "server.h"
#include <iso646.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>
#include "../tcpsegment.h"

#define BLACKLIST_MSG "ERROR: This hostname has been blacklisted"

static void cache_webpage (ProgramData data, const char *hostname, char *buffer,
                           HTMLCache cache[CACHE_SIZE]);
static void close_connection (int sockfd, TCPSegment_t* segment);
static void fetch_webpage (ProgramData data, const char *hostname, int clientfd,
                           HTMLCache cache[CACHE_SIZE]);
static int find_empty_cache (HTMLCache cache[CACHE_SIZE]);
static HTMLCache * get_cached_webpage (ProgramData data, const char *hostname,
                                       HTMLCache cache[CACHE_SIZE]);
static void open_connection (int sockfd, TCPSegment_t* segment);
static void receive_file (int sockfd, TCPSegment_t* segment);


/** @brief          Add a webpage to the cache
 *
 */
void
cache_webpage (ProgramData data, const char *hostname, char *buffer,
               HTMLCache cache[CACHE_SIZE])
{
    log_msg ("Caching webpage");
    int index = find_empty_cache (cache);
    int oldest = (index < 4) ? index + 1 : 0;
    strcpy (cache[index].hostname, hostname);
    strcpy (cache[index].buffer, buffer);
    cache[index].oldest = false;
    cache[oldest].oldest = true;
}

void
close_connection (int sockfd, TCPSegment_t* segment)
{
    init_segment (segment, 0, NULL);
    log_msg ("Waiting for fin");
    read_segment (segment, sockfd);

    log_msg ("Writing ack");
    reply_segment (segment, ACK_BIT, NULL);
    write_segment (segment, sockfd);
    
    log_msg ("Writing fin");
    init_segment (segment, FIN_BIT, NULL);
    write_segment (segment, sockfd);
    
    log_msg ("Waiting for ack");
    read_segment (segment, sockfd);
}


/** @brief          pthread call back function that handles client connections.
 *
 */
void
connection_handler (ProgramData data)
{
    TCPSegment_t segment;
    open_connection (data->client_sock, &segment);
    receive_file (data->client_sock, &segment);
    close_connection (data->client_sock, &segment);
}


/** @brief          Request and fetch a webpage from a web server.
 *
 *  @param data     A structure continaing all data for this instance of the
 *                  programs execution.
 *  @param hostname The hostname of the server we're requesting a web page from.
 */
void
fetch_webpage (ProgramData data, const char *hostname, int clientfd, 
               HTMLCache cache[CACHE_SIZE])
{
    int sockfd;
    char buffer[BUFFER_LEN];
    memset (buffer, 0, BUFFER_LEN);
    log_msg ("Connecting to host");
    if (connect_to_host (hostname, &sockfd, data->verbose)) {
        close (sockfd);
        return;
    }
    log_msg ("Requesting webpage");
    if (http_request (hostname, sockfd)) {
        close (sockfd);
        return;
    }
    log_msg ("Sending webpage to client");
    if (http_response (sockfd, buffer, BUFFER_LEN)) {
        close (sockfd);
        return;
    }
    log_msg (buffer);
    //Send the message back to client
    cache_webpage (data, hostname, buffer, cache);
    write (clientfd, buffer, strlen (buffer));
}


/** @brief          Look for an empty spot in the cache
 *
 */
int
find_empty_cache (HTMLCache cache[CACHE_SIZE])
{
    int oldest = 0;
    for (int i = 0; i < CACHE_SIZE; ++i) {
        if (cache[i].hostname == NULL)
            return i;
        if (cache[i].oldest)
            oldest = i;
    }
    return oldest;
}


/** @brief          Look for a webpage in the cache
 *
 */
HTMLCache *
get_cached_webpage (ProgramData data, const char *hostname,
                    HTMLCache cache[CACHE_SIZE])
{
    for (int i = 0; i < CACHE_SIZE; ++i) {
        if (data->verbose)
            printf("Cache[%d] -> %s\n", i, cache[i].hostname);
        if (strcmp (cache[i].hostname, hostname) == 0)
            return &(cache[i]);
    }
    return NULL;
}


void
open_connection (int sockfd, TCPSegment_t* segment)
{
    log_msg ("Waiting for syn");
    read_segment (segment, sockfd);

    log_msg ("Writing synack");
    reply_segment (segment, SYN_BIT | ACK_BIT, NULL);
    write_segment (segment, sockfd);

    log_msg ("Waiting for ack");
    read_segment (segment, sockfd);
}


/** @brief          Open a port to listen for client connections.
 *
 *  @param socket   Socket descriptor for the socket.
 *  @param server   Server descriptor.
 */
void
open_port (ProgramData data)
{
    struct sockaddr_in server;
    if (data->verbose)
        printf ("Opening a connection on port %d\n", LISTEN_PORT);
    data->server_sock = socket (AF_INET, SOCK_STREAM, 0);
    if (data->server_sock < 0) {
        fprintf (stderr, "ERROR: Could not create socket\n");
        exit (EXIT_FAILURE);
    }
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = INADDR_ANY;
    server.sin_port = htons(LISTEN_PORT);
    int status = bind (data->server_sock, (struct sockaddr*) &server, 
        sizeof (server));
    if (status < 0) {
        perror ("ERROR: Could not bind port");
        exit (EXIT_FAILURE);
    }

    listen (data->server_sock, NUM_CONNECTIONS);
}


void
receive_file (int sockfd, TCPSegment_t* segment)
{
    while (not (segment->flags & FIN_BIT)) {
        log_msg ("Waiting for data");
        read_segment (segment, sockfd);

        log_msg ("Writing ack");
        reply_segment (segment, ACK_BIT, NULL);
        write_segment (segment, sockfd);
    }
}


/** @brief          Listens for connections from clients
 *
 */
void
wait_for_connect (ProgramData data)
{
    struct sockaddr_in client;
    size_t c = sizeof (struct sockaddr_in);
    log_msg ("Listening for a connection");
    data->client_sock = accept (data->server_sock,
                          (struct sockaddr *) &client,
                          (socklen_t*) &c);
    log_msg ("Client connected");
}


 /* ************************************************************************* *
  *                                    EOF                                    *
  * ************************************************************************* */