/* ************************************************************************* *
 *                                                                           *
 * Copyright (c) 2015, Ryan Porterfield                                      *
 * All rights reserved.                                                      *
 *                                                                           *
 * Redistribution and use in source and binary forms, with or without        *
 * modification, are permitted provided that the following conditions        *
 * are met:                                                                  *
 *                                                                           *
 *     1. Redistributions of source code must retain the above copyright     *
 *     notice, this list of conditions and the following disclaimer.         *
 *                                                                           *
 *     2. Redistributions in binary form must reproduce the above copyright  *
 *     notice, this list of conditions and the following disclaimer in the   *
 *     documentation and/or other materials provided with the distribution.  *
 *                                                                           *
 *     3. Neither the name of the copyright holder nor the names of its      *
 *     contributors may be used to endorse or promote products derived from  *
 *     this software without specific prior written permission.              *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS   *
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED     *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A           *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT        *
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,    *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED  *
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR    *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF    *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING      *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS        *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.              *
 *                                                                           *
 * ************************************************************************* */

#include "program.h"
#include <iso646.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

const char const *help[]         =   { "-h", "--help" };
const char const *verbose[]      =   { "-v", "--verbose" };
const char const *version[]      =   { "-V", "--version" };

static ExecStatus exec_is_valid (int argc, const char **argv, ProgramData *data);
static ProgramData init_data (const char *exec, int flags);
static ProgramData parse_args (int argc, const char **argv, const char *exec);
static void print_help (ProgramData data);
static int print_info (ProgramData data);
static void print_version (ProgramData data);


/** @brief          Clean up resources allocated to a ProgramData struct.
 *
 *  @param data     A structure continaing all data for this instance of the
 *                  programs execution.
 */
void
delete_data (ProgramData data)
{
    close (data->server_sock);
    free (data);
}


/** @brief          Checks if the program was started with correct arguments.
 *
 *  @details        The program is started correctly if a bash++ file name is
 *                  passed, or if the help or version flags are passed.
 *  @param argc     Number of command line arguments passed to the program.
 *  @param argv     Character pointer (string) array of command line arguments.
 *  @param data     A structure continaing all data for this instance of the
 *                  programs execution.
 *  @return         @link VALID @endlink if program execution should procede,
 *                  @link INVALID @endlink if the program should exit with an
 *                  error, or @link PRINT_INFO @endlink if the program should
 *                  exit successfully after printing either help or version.
 */
ExecStatus
exec_is_valid (int argc, const char **argv, ProgramData *data)
{
    const char *exec_name = &argv[0][2];
    *data = parse_args (argc, argv, exec_name);
    if (print_info (*data))
        return PRINT_INFO;
    return VALID;
}


/** @brief          Initializes a ProgramData struct for the program.
 *
 *  @details        Uses the flags passed to create a ProgramData structure
 *                  containing all relevant program data for the running
 *                  instance of the program.
 *  @param exec     The name of the executable
 *  @param i_name   The name of the input bash++ file
 *  @param flags    An integer whose bits are set based on command line
 *                  arguments passed at run time.
 *  @return         A new dynamically allocated instance of ProgramData
 */
ProgramData
init_data (const char *exec, int flags)
{
    ProgramData data = malloc (sizeof (struct ProgramData_s));
    /* Trick to initialize constant struct members */
    struct ProgramData_s init = {
        .running    = true,
        .exec       = exec,
        .help       = (flags & HELP_BIT) ? true : false,
        .version    = (flags & VERSION_BIT) ? true : false,
        .verbose    = (flags & VERBOSE_BIT) ? true : false
    };
    memcpy (data, &init, sizeof (init));
    return data;
}


/**
 *
 */
ProgramData
init_program (int argc, const char** argv)
{
    ProgramData data;
    ExecStatus stat = exec_is_valid (argc, argv, &data);
    if (INVALID == stat)
        exit(EXIT_FAILURE);
    else if (PRINT_INFO == stat)
        exit(EXIT_SUCCESS);
}


/** @brief          Parses command line arguments and sets internal flags
 *                  accordingly
 *
 *  @param argc     Number of command line arguments passed to the program.
 *  @param argv     Character pointer (string) array of command line arguments.
 *  @return         An integer variable with bits set according to flags
 *                  passed. Effectively a boolean array.
 */
ProgramData
parse_args (int argc, const char **argv, const char *exec)
{
    int flags = 0;
    for (int i = 1; i < argc; ++i) {
        const char *arg = argv[i];
        if (not strcmp (arg, help[0]) or not strcmp (arg, help[1]))
            flags |= HELP_BIT;
        else if (not strcmp (arg, verbose[0]) or not strcmp (arg, verbose[1]))
            flags |= VERBOSE_BIT;
        else if (not strcmp (arg, version[0]) or not strcmp (arg, version[1]))
            flags |= VERSION_BIT;
    }
    return init_data (exec, flags);
}


/** @brief          Print program usage and flag information.
 *
 *  @details        Print's program usage information when program called with
 *                  -h or --help. Presumably the program exits after this but
 *                  the function behavior does not require it.
 *  @param data     A structure continaing all data for this instance of the
 *                  programs execution.
 */
void
print_help (ProgramData data)
{
    printf ("Usage: %s [OPTION]\n", data->exec);
    printf ("Proxy server makes HTTP requests on behalf of a client.\n\n");
    printf ("  %s, %s\t\tshow this help message and exit\n", help[0],
            help[1]);
    printf ("  %s, %s\t\tshow debugging messages during execution\n",
            verbose[0], verbose[1]);
    printf ("  %s, %s\t\tshow program's version number and exit\n",
            version[0], version[1]);
}


/** @brief          Checks if help or version needs to be printed.
 *
 *  @details        If the program was called with (-h, --help) or 
 *                  (-v, --version) the appropriate print function is called
 *                  and the function returns true (1).
 *  @param data     A structure continaing all data for this instance of the
 *                  programs execution.
 *  @return         true (1) if -h, --help, -v, or --version was called or
 *                  false (0) if not.
 */
int
print_info (ProgramData data)
{
    if (data->help) {
        print_help (data);
        return 1;
    } else if (data->version) {
        print_version (data);
        return 1;
    }
    return 0;
}


/** @brief          Print program version number, copyright, and disclaimer.
 *
 *  @details        Print's program's version number, copyright line, and
 *                  disclaimer when program is called with -v or --version
 *                  Presumably the program exits after this but the function
 *                  behavior does not require it.
 *  @param data     A structure continaing all data for this instance of the
 *                  programs execution.
 */
void
print_version (ProgramData data)
{
    printf ("%s %s %s (%s)\n", data->exec, VERSION, BUILD_DATE,
            BUILD_STATUS);
    printf ("%s\n%s\n", COPYRIGHT, DISCLAIMER);
}


 /* ************************************************************************* *
  *                                    EOF                                    *
  * ************************************************************************* */