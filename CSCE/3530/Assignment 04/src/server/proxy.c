/* ************************************************************************* *
 *                                                                           *
 * Copyright (c) 2015, Ryan Porterfield                                      *
 * All rights reserved.                                                      *
 *                                                                           *
 * Redistribution and use in source and binary forms, with or without        *
 * modification, are permitted provided that the following conditions        *
 * are met:                                                                  *
 *                                                                           *
 *     1. Redistributions of source code must retain the above copyright     *
 *     notice, this list of conditions and the following disclaimer.         *
 *                                                                           *
 *     2. Redistributions in binary form must reproduce the above copyright  *
 *     notice, this list of conditions and the following disclaimer in the   *
 *     documentation and/or other materials provided with the distribution.  *
 *                                                                           *
 *     3. Neither the name of the copyright holder nor the names of its      *
 *     contributors may be used to endorse or promote products derived from  *
 *     this software without specific prior written permission.              *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS   *
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED     *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A           *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT        *
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,    *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED  *
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR    *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF    *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING      *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS        *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.              *
 *                                                                           *
 * ************************************************************************* */

#include "server.h"
#include <iso646.h>
#include <netdb.h> 
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

#define FORMAT "GET /index.html HTTP/1.0\r\nHost: %s\r\nUser-Agent: fetch.c\r\n\r\n"
#define HOST_PORT 80
#define SERVICE "http"


/**	@brief
 *
 *	@param hostname	The hostname of the server we're requesting a web page from.
 *	@param sockfd	The file descriptor for the connection to the server.
 *					Used for reading and writing to the server.
 */
int
connect_to_host (const char const *hostname, int *sockfd, bool verbose)
{
	int status;
    struct hostent *host;
    struct sockaddr_in serv_addr;

    if (verbose)
        printf ("Getting host \"%s\"\n", hostname);

    if ((host = gethostbyname (hostname)) == NULL) {
        herror ("gethostbyname");
        return EXIT_FAILURE;
    }

    if (verbose)
        puts ("Resolved hostname, creating socket");

    memcpy (&(serv_addr.sin_addr.s_addr), host->h_addr, host->h_length);
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(HOST_PORT);

    if (verbose)
        puts ("connecting to host");

    *sockfd = socket (AF_INET, SOCK_STREAM, 0);
    status = connect (*sockfd, (struct sockaddr*) &serv_addr, sizeof (serv_addr));
    if (status < 0) {
        perror ("ERROR connecting");
    	return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}


/**	@brief
 *
 *	@param sockfd	The file descriptor for the connection to the server.
 *					Used for reading and writing to the server.
 */
int
http_response (int sockfd, char *buffer, size_t len)
{
	int n;
    n = read (sockfd, buffer, len);
    if (n < 0) {
    	perror ("ERROR reading from socket");
    	return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}


/**	@brief
 *
 *	@param hostname	The hostname of the server we're requesting a web page from.
 *	@param sockfd	The file descriptor for the connection to the server.
 *					Used for reading and writing to the server.
 */
int
http_request (const char const *hostname, int sockfd)
{
	char *msg;
	int n;
	asprintf (&msg, FORMAT, hostname);
	n = write (sockfd, msg, strlen (msg));
    if (n < 0) {
        perror ("ERROR writing to socket");
        return EXIT_FAILURE;
 	}
 	return EXIT_SUCCESS;
}


 /* ************************************************************************* *
  *                                    EOF                                    *
  * ************************************************************************* */