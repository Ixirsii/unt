/* ************************************************************************* *
 *                                                                           *
 * Copyright (c) 2015, Ryan Porterfield                                      *
 * All rights reserved.                                                      *
 *                                                                           *
 * Redistribution and use in source and binary forms, with or without        *
 * modification, are permitted provided that the following conditions        *
 * are met:                                                                  *
 *                                                                           *
 *     1. Redistributions of source code must retain the above copyright     *
 *     notice, this list of conditions and the following disclaimer.         *
 *                                                                           *
 *     2. Redistributions in binary form must reproduce the above copyright  *
 *     notice, this list of conditions and the following disclaimer in the   *
 *     documentation and/or other materials provided with the distribution.  *
 *                                                                           *
 *     3. Neither the name of the copyright holder nor the names of its      *
 *     contributors may be used to endorse or promote products derived from  *
 *     this software without specific prior written permission.              *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS   *
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED     *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A           *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT        *
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,    *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED  *
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR    *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF    *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING      *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS        *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.              *
 *                                                                           *
 * ************************************************************************* */

#include "server.h"
#include <iso646.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>
#include "program.h"

#define __USE_XOPEN
#define _GNU_SOURCE
#define _XOPEN_SOURCE

#define BLACKLIST "blacklist.txt"

static bool running;

static int get_blacklist (Blacklist list[]);
static void parse_time (char *time_str, time_t *time);
static void run (ProgramData data, Blacklist list[]);
static void sig_handler (int signo);

ProgramData gdata;


int
main (int argc, const char **argv)
{
    Blacklist list[BLACKLIST_LEN];
    if (signal (SIGINT, sig_handler) == SIG_ERR) {
        fprintf(stderr, "ERROR: Can't set signal handler\n");
        return EXIT_FAILURE;
    }

    gdata = init_program (argc, argv);
    running = true;
    get_blacklist (list);
    open_port (gdata);
    run (gdata, list);
    close ((gdata)->client_sock);
    delete_data (gdata);
    return EXIT_SUCCESS;
}


int
get_blacklist (Blacklist list[])
{
    Blacklist *current = &list[0];
    FILE *blacklist = fopen (BLACKLIST, "r");
    if (blacklist == NULL) {
        perror ("Couldn't open blacklist");
        return EXIT_FAILURE;
    }

    int i = 0;
    while ((fscanf (blacklist, "%s", current->hostname) not_eq EOF) && 
            (i < BLACKLIST_LEN)) {
        char buffer[15];
        fscanf (blacklist, "%s", buffer);
        parse_time (buffer, &(current->start_time));
        fscanf (blacklist, "%s", buffer);
        parse_time (buffer, &(current->end_time));
        printf ("Blacklisting \"%s\" from %d to %d\n", current->hostname, 
            current->start_time, current->end_time);
        current = &list[++i];
    }
}


/** @brief          Check to see if a hostname is on the blacklist
 *
 */
bool
is_blacklisted (const char *hostname, Blacklist list[])
{
    log_msg ("Searching blacklist for host");
    for (int i = 0; i < BLACKLIST_LEN; ++i) {
        if (strcmp (list[i].hostname, hostname) == 0) {
            log_msg ("Found hostname on blacklist");
            time_t now = time (NULL);
            printf ("Current time: %d\n", now);
            return (now > list[i].start_time) && (now < list[i].end_time);
        }
    }
    return false;
}


void
log_msg (const char *msg)
{
    if (gdata->verbose)
        puts (msg);
}


void
parse_time (char *time_str, time_t *time)
{
    struct tm tm;
    strptime (time_str, "%Y%m%d%H%M%S", &tm);
    *time = mktime (&tm);
}


void
run (ProgramData data, Blacklist list[])
{
    wait_for_connect (data);
    connection_handler (data);
}


/** @brief          Handle signals from the kernel
 *
 */
void
sig_handler (int signo)
{
    if (signo not_eq SIGINT)
        return;
    puts ("Caught SIGINT, closing connections and exiting");
    running = false;
    gdata->running = false;
    close (gdata->server_sock);
}


/* ************************************************************************* *
 *                                    EOF                                    *
 * ************************************************************************* */