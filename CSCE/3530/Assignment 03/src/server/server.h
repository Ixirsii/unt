/* ************************************************************************* *
 *                                                                           *
 * Copyright (c) 2015, Ryan Porterfield                                      *
 * All rights reserved.                                                      *
 *                                                                           *
 * Redistribution and use in source and binary forms, with or without        *
 * modification, are permitted provided that the following conditions        *
 * are met:                                                                  *
 *                                                                           *
 *     1. Redistributions of source code must retain the above copyright     *
 *     notice, this list of conditions and the following disclaimer.         *
 *                                                                           *
 *     2. Redistributions in binary form must reproduce the above copyright  *
 *     notice, this list of conditions and the following disclaimer in the   *
 *     documentation and/or other materials provided with the distribution.  *
 *                                                                           *
 *     3. Neither the name of the copyright holder nor the names of its      *
 *     contributors may be used to endorse or promote products derived from  *
 *     this software without specific prior written permission.              *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS   *
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED     *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A           *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT        *
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,    *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED  *
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR    *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF    *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING      *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS        *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.              *
 *                                                                           *
 * ************************************************************************* */

#ifndef SERVER_H
#define SERVER_H

#include <stdbool.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>

#define SERVER_BUILD_DATE   "2015-09-19"
#define SERVER_BUILD_STATUS "Development"
#define SERVER_COPYRIGHT    "Copyright (C) 2015, Ryan Porterfield"
#define SERVER_DISCLAIMER   "This is free software; see the source for " \
                            "copying conditions.  There is NO\nwarranty; not " \
                            "even for MERCHANTABILITY or FITNESS FOR A " \
                            "PARTICULAR PURPOSE"
#define SERVER_VERSION      "0.0.1"
 
#define BLACKLIST_LEN       16
#define BUFFER_LEN          2048
#define CACHE_SIZE          5
#define LISTEN_PORT         17754
#define NAME_LEN            128
#define NUM_CONNECTIONS     1
#define HELP_BIT            (1 << 0)
#define VERBOSE_BIT         (1 << 1)
#define VERSION_BIT         (1 << 2)


typedef struct Blacklist_s {
    char        hostname[NAME_LEN];
    time_t      start_time;
    time_t      end_time;
} Blacklist;


/** @struct         HTMLCache
 *  @brief          Used to cache a webpage
 */
typedef struct HTMLCache_s {
    char        buffer[BUFFER_LEN]; /**< Text buffer stores the webpage */
    char        hostname[NAME_LEN]; /**< Hostname of the webpage */
    bool        oldest;             /**< Flag indicates if this cache is the
                                         oldest in a list */
} HTMLCache;


/** @struct         ProgramData
 *  @brief          Stores basic program data used by most functions.
 *
 */
typedef struct ProgramData_s {
    int         client_sock;
    int         server_sock;
    bool        running;
    const char  *exec;      /**< The name of the executable */
    const bool  help;       /**< Does the program usage need to be printed */
    const bool  version;    /**< Does the program version need to be printed */
    const bool  verbose;    /**< Is the program is in verbose mode */
} *ProgramData;


/** @enum           ExecStatus
 *  @brief          Possible program execution states
 *
 *  @details        Either the program was executed properly with a bas++
 *                  filename passed, or the program was executed with the help
 *                  flag or version flag passed, or the program was executed
 *                  incorrectly.
 */
typedef enum {
    VALID,      /**< The execution of the program is valid */
    INVALID,    /**< The execution of the program is invalid */
    PRINT_INFO  /**< Print some form of program info and exit successfully */
} ExecStatus;

bool        is_blacklisted      (const char *hostname, Blacklist list[]);
int         connect_to_host     (const char const *hostname, int *sockfd, 
                                 bool verbose);
void        connection_handler  (ProgramData data);
int         http_response       (int sockfd, char *buffer, size_t len);
int         http_request        (const char const *hostname, int sockfd);
void        log_msg             (const char *msg);
void        open_port           (ProgramData data);
void        wait_for_connect    (ProgramData data);


#endif /* SERVER_H */

 /* ************************************************************************* *
  *                                    EOF                                    *
  * ************************************************************************* */