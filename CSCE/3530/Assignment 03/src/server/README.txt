README

Compliation:
	To compile the program simply type `make`

Running:
	To run the server program execute `./server`
	The server will run in a loop until the user presses Ctl-C.

	The client program assumes that server will be running on cse01.cse.unt.edu
	To run the client program execute `./client` on any host.
	The client will prompt the user for a url, and when the proxy server
	returns a webpage the client will print the webpage to stdout. The client
	runs in a loop until the user presses Ctl-C.
