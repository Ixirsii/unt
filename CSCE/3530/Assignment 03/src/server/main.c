/* ************************************************************************* *
 *                                                                           *
 * Copyright (c) 2015, Ryan Porterfield                                      *
 * All rights reserved.                                                      *
 *                                                                           *
 * Redistribution and use in source and binary forms, with or without        *
 * modification, are permitted provided that the following conditions        *
 * are met:                                                                  *
 *                                                                           *
 *     1. Redistributions of source code must retain the above copyright     *
 *     notice, this list of conditions and the following disclaimer.         *
 *                                                                           *
 *     2. Redistributions in binary form must reproduce the above copyright  *
 *     notice, this list of conditions and the following disclaimer in the   *
 *     documentation and/or other materials provided with the distribution.  *
 *                                                                           *
 *     3. Neither the name of the copyright holder nor the names of its      *
 *     contributors may be used to endorse or promote products derived from  *
 *     this software without specific prior written permission.              *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS   *
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED     *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A           *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT        *
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,    *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED  *
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR    *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF    *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING      *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS        *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.              *
 *                                                                           *
 * ************************************************************************* */

#include "server.h"
#include <iso646.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>

#define __USE_XOPEN
#define _GNU_SOURCE
#define _XOPEN_SOURCE

#define BLACKLIST "blacklist.txt"

static bool running;

static void delete_data (ProgramData data);
static ExecStatus exec_is_valid (int argc, const char **argv, ProgramData *data);
static int get_blacklist ();
static ProgramData init_data (const char *exec, int flags);
static ProgramData parse_args (int argc, const char **argv, const char *exec);
static void parse_time (char *time_str, time_t *time);
static void print_help (ProgramData data);
static int print_info (ProgramData data);
static void print_version (ProgramData data);
static void run (ProgramData data, Blacklist list[]);
static void sig_handler (int signo);

const char const *help[]         =   { "-h", "--help" };
const char const *verbose[]      =   { "-v", "--verbose" };
const char const *version[]      =   { "-V", "--version" };
ProgramData gdata;


int
main (int argc, const char **argv)
{
    Blacklist list[BLACKLIST_LEN];
    ProgramData *data = &gdata;
    if (signal (SIGINT, sig_handler) == SIG_ERR) {
        fprintf(stderr, "ERROR: Can't set signal handler\n");
        return EXIT_FAILURE;
    }
    ExecStatus stat = exec_is_valid (argc, argv, data);
    if (INVALID == stat)
        return EXIT_FAILURE;
    else if (PRINT_INFO == stat)
        return EXIT_SUCCESS;
    if ((*data)->verbose) {
    }

    running = true;
    get_blacklist (list);
    open_port (*data);
    run (*data, list);
    close ((*data)->client_sock);
    delete_data (*data);
    return EXIT_SUCCESS;
}


/** @brief          Clean up resources allocated to a ProgramData struct.
 *
 *  @param data     A structure continaing all data for this instance of the
 *                  programs execution.
 */
void
delete_data (ProgramData data)
{
    close (data->server_sock);
    free (data);
}


/** @brief          Checks if the program was started with correct arguments.
 *
 *  @details        The program is started correctly if a bash++ file name is
 *                  passed, or if the help or version flags are passed.
 *  @param argc     Number of command line arguments passed to the program.
 *  @param argv     Character pointer (string) array of command line arguments.
 *  @param data     A structure continaing all data for this instance of the
 *                  programs execution.
 *  @return         @link VALID @endlink if program execution should procede,
 *                  @link INVALID @endlink if the program should exit with an
 *                  error, or @link PRINT_INFO @endlink if the program should
 *                  exit successfully after printing either help or version.
 */
ExecStatus
exec_is_valid (int argc, const char **argv, ProgramData *data)
{
    const char *exec_name = &argv[0][2];
    *data = parse_args (argc, argv, exec_name);
    if (print_info (*data))
        return PRINT_INFO;
    return VALID;
}


int
get_blacklist (Blacklist list[])
{
    Blacklist *current = &list[0];
    FILE *blacklist = fopen (BLACKLIST, "r");
    if (blacklist == NULL) {
        perror ("Couldn't open blacklist");
        return EXIT_FAILURE;
    }

    int i = 0;
    while ((fscanf (blacklist, "%s", current->hostname) not_eq EOF) && 
            (i < BLACKLIST_LEN)) {
        char buffer[15];
        fscanf (blacklist, "%s", buffer);
        parse_time (buffer, &(current->start_time));
        fscanf (blacklist, "%s", buffer);
        parse_time (buffer, &(current->end_time));
        printf ("Blacklisting \"%s\" from %d to %d\n", current->hostname, 
            current->start_time, current->end_time);
        current = &list[++i];
    }
}


/** @brief          Initializes a ProgramData struct for the program.
 *
 *  @details        Uses the flags passed to create a ProgramData structure
 *                  containing all relevant program data for the running
 *                  instance of the program.
 *  @param exec     The name of the executable
 *  @param i_name   The name of the input bash++ file
 *  @param flags    An integer whose bits are set based on command line
 *                  arguments passed at run time.
 *  @return         A new dynamically allocated instance of ProgramData
 */
ProgramData
init_data (const char *exec, int flags)
{
    ProgramData data = malloc (sizeof (struct ProgramData_s));
    /* Trick to initialize constant struct members */
    struct ProgramData_s init = {
        .running    = true,
        .exec       = exec,
        .help       = (flags & HELP_BIT) ? true : false,
        .version    = (flags & VERSION_BIT) ? true : false,
        .verbose    = (flags & VERBOSE_BIT) ? true : false
    };
    memcpy (data, &init, sizeof (init));
    return data;
}


/** @brief          Check to see if a hostname is on the blacklist
 *
 */
bool
is_blacklisted (const char *hostname, Blacklist list[])
{
    log_msg ("Searching blacklist for host");
    for (int i = 0; i < BLACKLIST_LEN; ++i) {
        if (strcmp (list[i].hostname, hostname) == 0) {
            log_msg ("Found hostname on blacklist");
            time_t now = time (NULL);
            printf ("Current time: %d\n", now);
            return (now > list[i].start_time) && (now < list[i].end_time);
        }
    }
    return false;
}


void
log_msg (const char *msg)
{
    if (gdata->verbose)
        puts (msg);
}


/** @brief          Parses command line arguments and sets internal flags
 *                  accordingly
 *
 *  @param argc     Number of command line arguments passed to the program.
 *  @param argv     Character pointer (string) array of command line arguments.
 *  @return         An integer variable with bits set according to flags
 *                  passed. Effectively a boolean array.
 */
ProgramData
parse_args (int argc, const char **argv, const char *exec)
{
    int flags = 0;
    for (int i = 1; i < argc; ++i) {
        const char *arg = argv[i];
        if (not strcmp (arg, help[0]) or not strcmp (arg, help[1]))
            flags |= HELP_BIT;
        else if (not strcmp (arg, verbose[0]) or not strcmp (arg, verbose[1]))
            flags |= VERBOSE_BIT;
        else if (not strcmp (arg, version[0]) or not strcmp (arg, version[1]))
            flags |= VERSION_BIT;
    }
    return init_data (exec, flags);
}


void parse_time (char *time_str, time_t *time)
{
    struct tm tm;
    strptime (time_str, "%Y%m%d%H%M%S", &tm);
    *time = mktime (&tm);
}


/** @brief          Print program usage and flag information.
 *
 *  @details        Print's program usage information when program called with
 *                  -h or --help. Presumably the program exits after this but
 *                  the function behavior does not require it.
 *  @param data     A structure continaing all data for this instance of the
 *                  programs execution.
 */
void
print_help (ProgramData data)
{
    printf ("Usage: %s [OPTION]\n", data->exec);
    printf ("Proxy server makes HTTP requests on behalf of a client.\n\n");
    printf ("  %s, %s\t\tshow this help message and exit\n", help[0],
            help[1]);
    printf ("  %s, %s\t\tshow debugging messages during execution\n",
            verbose[0], verbose[1]);
    printf ("  %s, %s\t\tshow program's version number and exit\n",
            version[0], version[1]);
}


/** @brief          Checks if help or version needs to be printed.
 *
 *  @details        If the program was called with (-h, --help) or 
 *                  (-v, --version) the appropriate print function is called
 *                  and the function returns true (1).
 *  @param data     A structure continaing all data for this instance of the
 *                  programs execution.
 *  @return         true (1) if -h, --help, -v, or --version was called or
 *                  false (0) if not.
 */
int
print_info (ProgramData data)
{
    if (data->help) {
        print_help (data);
        return 1;
    } else if (data->version) {
        print_version (data);
        return 1;
    }
    return 0;
}


/** @brief          Print program version number, copyright, and disclaimer.
 *
 *  @details        Print's program's version number, copyright line, and
 *                  disclaimer when program is called with -v or --version
 *                  Presumably the program exits after this but the function
 *                  behavior does not require it.
 *  @param data     A structure continaing all data for this instance of the
 *                  programs execution.
 */
void
print_version (ProgramData data)
{
    printf ("%s %s %s (%s)\n", data->exec, SERVER_VERSION, SERVER_BUILD_DATE,
            SERVER_BUILD_STATUS);
    printf ("%s\n%s\n", SERVER_COPYRIGHT, SERVER_DISCLAIMER);
}


void
run (ProgramData data, Blacklist list[])
{
    // HTMLCache cache[5];
    // memset (cache, 0, 5 * sizeof (struct HTMLCache_s));
    // while (running) {
    //     wait_for_connect (data);
    //     connection_handler (data);
    // }
    wait_for_connect (data);
    connection_handler (data);
}
/** @brief          Handle signals from the kernel
 *
 */
void
sig_handler (int signo)
{
    if (signo not_eq SIGINT)
        return;
    puts ("Caught SIGINT, closing connections and exiting");
    running = false;
    gdata->running = false;
    close (gdata->server_sock);
}


/* ************************************************************************* *
 *                                    EOF                                    *
 * ************************************************************************* */