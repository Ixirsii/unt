/* ************************************************************************* *
 *                                                                           *
 * Copyright (c) 2015, Ryan Porterfield                                      *
 * All rights reserved.                                                      *
 *                                                                           *
 * Redistribution and use in source and binary forms, with or without        *
 * modification, are permitted provided that the following conditions        *
 * are met:                                                                  *
 *                                                                           *
 *     1. Redistributions of source code must retain the above copyright     *
 *     notice, this list of conditions and the following disclaimer.         *
 *                                                                           *
 *     2. Redistributions in binary form must reproduce the above copyright  *
 *     notice, this list of conditions and the following disclaimer in the   *
 *     documentation and/or other materials provided with the distribution.  *
 *                                                                           *
 *     3. Neither the name of the copyright holder nor the names of its      *
 *     contributors may be used to endorse or promote products derived from  *
 *     this software without specific prior written permission.              *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS   *
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED     *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A           *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT        *
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,    *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED  *
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR    *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF    *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING      *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS        *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.              *
 *                                                                           *
 * ************************************************************************* */

#include "tcpsegment.h"
#include <iso646.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>

#define HOST_PORT 17754
#define OUT_FILE "segments.txt"

static int sequence = 0;

static int compute_checksum (TCPSegment_t* segment);
static void deserialize_segment (TCPSegment_t* segment);
static void serialize_segment (TCPSegment_t* segment);
static void write_segment_to_file (TCPSegment_t* segment, FILE* out_file);


int
compute_checksum (TCPSegment_t* segment)
{
    int checksum = segment->source_port;
    checksum = checksum ^ segment->dest_port;
    checksum = checksum ^ segment->sequence;
    checksum = checksum ^ segment->acknowledgement;
    checksum = checksum ^ segment->flags;
    checksum = checksum ^ segment->recv_window;
    checksum = checksum ^ segment->urgent_data;
    checksum = checksum ^ segment->options;
    return checksum;
}

void
deserialize_segment (TCPSegment_t* segment)
{
	segment->source_port = ntohs (segment->source_port);
	segment->dest_port = ntohs (segment->dest_port);
	segment->sequence = ntohl (segment->sequence);
	segment->acknowledgement = ntohl (segment->acknowledgement);
	segment->flags = ntohs (segment->flags);
	segment->recv_window = ntohs (segment->recv_window);
	segment->checksum = ntohs (segment->checksum);
	segment->urgent_data = ntohs (segment->urgent_data);
	segment->options = ntohl (segment->options);
}


bool
is_valid (TCPSegment_t* segment)
{
    return segment->checksum == compute_checksum (segment);
}


void
init_segment (TCPSegment_t* segment, short int flags) {
    sequence = rand ();
    segment->source_port = HOST_PORT;
    segment->dest_port = HOST_PORT;
    segment->sequence = sequence++;
    segment->acknowledgement = 0;
    segment->flags = flags;
    segment->recv_window = 0;
    segment->urgent_data = 0;
    segment->options = 0;
    segment->checksum = compute_checksum (segment);
}


void
read_segment (TCPSegment_t* segment, int sockfd)
{
	int recv_len = read (sockfd, segment, sizeof (TCPSegment_t));
	if (recv_len < sizeof (TCPSegment_t)) {
		fprintf(stderr, "ERROR: Didn't receive whole segment\n");
		return;
	}
	deserialize_segment (segment);
}


void
reply_segment (TCPSegment_t* segment, short int flags)
{
    segment->acknowledgement = segment->sequence + 1;
    segment->sequence = sequence++;
    segment->flags = flags;
    segment->checksum = compute_checksum (segment);
}


void
serialize_segment (TCPSegment_t* segment)
{
	segment->source_port = htons (segment->source_port);
	segment->dest_port = htons (segment->dest_port);
	segment->sequence = htonl (segment->sequence);
	segment->acknowledgement = htonl (segment->acknowledgement);
	segment->flags = htons (segment->flags);
	segment->recv_window = htons (segment->recv_window);
	segment->checksum = htons (segment->checksum);
	segment->urgent_data = htons (segment->urgent_data);
	segment->options = htonl (segment->options);
}


void
write_segment (TCPSegment_t* segment)
{
    write_segment_to_file (segment, stdout);
    FILE* out_file = fopen (OUT_FILE, "a");
    write_segment_to_file (segment, out_file);
    fclose (out_file);
}


void
write_segment_to_file (TCPSegment_t *segment, FILE* out_file)
{
    fprintf (out_file, "\t\t\t\tTCP SEGMENT:\n");
    fprintf (out_file, "Source port:\t\t%hd\n", segment->source_port);
    fprintf (out_file, "Dest port:\t\t%hd\n", segment->dest_port);
    fprintf (out_file, "Sequence:\t\t%hd\n", segment->sequence);
    fprintf (out_file, "Acknowledgement:\t%hd\n", segment->acknowledgement);
    fprintf (out_file, "Flags:\t\t\t%hd\n", segment->flags);
    fprintf (out_file, "Recv window:\t\t%hd\n", segment->recv_window);
    fprintf (out_file, "Checksum:\t\t%hd\n", segment->checksum);
    fprintf (out_file, "Urgent data:\t\t%hd\n", segment->urgent_data);
    fprintf (out_file, "Options:\t\t%hd\n", segment->options);
    fprintf (out_file, "\n");
}


void
write_segment_to_sock (TCPSegment_t* segment, int sockfd)
{
	serialize_segment (segment);
	write (sockfd, segment, sizeof (TCPSegment_t));
}


/* ************************************************************************* *
 *                                    EOF                                    *
 * ************************************************************************* */