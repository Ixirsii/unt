/*
 * /////////////////////////////////////////////////////////////////////
 * Group 12, authors: Alex Fatum, Sammy Sirak, Li Zhang, Luis Alvarez
 * /////////////////////////////////////////////////////////////////////
 * SuperBash.c
 * 
 * Copyright 2014 Alex Fatum <alex@Linux>, Sammy Sirak, Li Zhang, Luis Alvarez
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


int main(){
	char a[200];															//string from the file
	char *b;																//for finding the address of 'then'
	int c;																	//length of b
	int e;																	//length of a
	char *f;																//holds the address of 'repeat'
	char *equalsWhiteSpace;													//holds the address of '='

	char NewLine[41] ="while [ $repeatIndex1 -lt $";
	char Variable[11] = "!";								
	int position;														//finding the position in an array
	int count = 0;														//a counter
	int whiteSpace;														//counter for the whitespace loop
	int arrayCounter; 													//for white space around the '=' sign
	int derp = 0; 														//derpy counter for white space around the '=' sign
	FILE *fp;
		
    fp=fopen("output.txt","w");											//write to output.txt
    while(1){
        fgets(a,200,stdin);												//stdin can be changed to any file path
        if(feof(stdin)){												//foef return 1 then break
            break;
        }   
        b=strstr(a,"then");												//find the address of "then" if not return NULL
        if(b!=NULL){
            c=strlen(b);												//length of b
            e=strlen(a);												//length of a
            a[e-c-1]='\n';												//make then char which before "then" become "\n" 
        }
        
		f=strstr(a,"repeat");											
		if(f!=NULL){														
			if(a[7]=='$'){
				position = 8;
				// * Get Variable Name
				for(position=8;a[position]!=' ';position++)
					Variable[position-8]=a[position];			
			}
																		// *Error if $ for variable isn't detected
																		//	else printf("\nNo variable declared in line %d\n",linenum); 
																		// find "times" after variable in line and continue
			if(strstr(a,"times")==&a[position+=1]){
				strcat(NewLine,Variable);
				strcat(NewLine," ]");
				fprintf(fp,"$repeatIndex1=0\n");							
				fprintf(fp,"%s\n",NewLine);									
				fprintf(fp,"do\n");
				count = 0;
				while(count < 2){
					if(strstr(a,"repeat "))
						a[count] = '\0';
					count++;
				}						
			}
																		// Error if times wasn't found 
																		// else printf("\nError - Incorrect syntax - Missing \"times\"\n"); 
	}
	count = 0;															// Deletes the '{' bracket
	if(a[count] == '{'){
		a[count] = '\0';
		count++;
	}
		// ENDING
	if(strstr(a,"}")){ 
		fprintf(fp,"repeatIndex1=$[$repeatIndex1+1]\n");
		fprintf(fp,"done\n");
		strcpy(NewLine,"while [ $repeatIndex1 -lt $"); 					//reset NewLine
		strcpy(Variable,"!");                          					//reset Variable
																		// repeatflag = 0; * reset flag to 0 *
		count = 0;														// Deletes the '}' bracket
		while(count < 2){
			if(a[count] =='}'){
				a[count] = '\0';
			}
			count++;
		}
	}    																	
	
	count = 0;															// Deletes the '{' bracket
	if(a[count] == '='){
		a[count] = '\0';
	}
	
																		//Below Deletes white space around the '=' sign																		 
//	equalsWhiteSpace=strstr(a,"=");
//	if(equalsWhiteSpace!=NULL){
//		whiteSpace = strlen(a);
	if(strstr(a,"=")){
		while(arrayCounter < 200){
			if(a[arrayCounter] ==' '){
				derp = arrayCounter;
				while(derp < 200){
					a[derp] = a[derp+1];								//moving everything over 1
					derp++;
				}
			}
			arrayCounter++;
		}		
	}         
	fputs(a,fp);
    }
    fclose(fp);
    return (EXIT_SUCCESS);
}

