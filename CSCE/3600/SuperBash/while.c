/* Group Program - 1 | Luis Alvarez
 * Bash++ while loop
 * Doesn't read files or insert into files, just prints it out.
 * Doesn't change repeatIndex variable to change incase of a while loop in a while loop
 */

#include <stdio.h>
#include <string.h>

int main(){
   char Line[31] = "repeat $num times";
   char NewLine[41] ="while [ $repeatIndex1 -lt $";
   char Variable[11] = "!";
   int linenum = 1;
   int num;
   //int repeatflag = 0; // *Flag for repeat*
   int position;

// get Line
fgets(Line, sizeof(Line), stdin);

   if ( strstr(Line, "repeat") == &Line[0] ){
      // repeatflag = 1; * add flag for repeat *
      if ( Line[7] == '$') {
         position = 8;
         // * Get Variable Name
         for (position = 8; Line[position] != ' ';position++)
            Variable[position-8] = Line[position];
      }
      // *Error if $ for variable isn't detected
      else {printf("\nNo variable declared in line %d\n", linenum); return 0;}
      
      // find "times" after variable in line and continue
      if (strstr(Line,"times") == &Line[position+=1]){
         strcat(NewLine, Variable);
         strcat(NewLine, " ]");
         printf("$repeatIndex1=0\n");
         printf("%s\n",NewLine);
         printf("do\n");
      }
      // Error if times wasn't found 
      else {printf("\nError - Incorrect syntax - Missing \"times\"\n"); return 0;}
   }
   // ENDING
   if (strstr(Line, "}")) { // && repeatflag == 1 * We can add a flag to seperate if and repeat endings
      printf("repeatIndex1=$[$repeatIndex1+1]\n");
      printf("done\n");
      strcpy(NewLine, "while [ $repeatIndex1 -lt $"); //reset NewLine
      strcpy(Variable, "!");                          //reset Variable
      // repeatflag = 0; * reset flag to 0 *
   }
return 0;
}