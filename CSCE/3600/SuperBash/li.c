/* 
 * File:   main.c
 * Author: lizhang
 *
 * Created on September 25, 2014, 10:16 AM
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*
 * 
 */
char a[200];//string a
char buffy[200]; //buffy the buffer slayer
char *b;//string b
int c;
int e;


int main(int argc, char** argv) {
	//The below declarations are used in the while loop
	char Line[31] = "repeat $num times";
	char NewLine[41] ="while [ $repeatIndex1 -lt $";
	char Variable[11] = "!";
	int linenum = 1;
	int position;
	
    FILE *fp;
    fp=fopen("output.txt","w");//write to output.txt
    while(1){
        fgets(a,200,stdin);//stdin can be changed to any file path
        if(feof(stdin)){//foef return 1 then break
            break;
        }   
        b=strstr(a,"then");//find the address of "then" if not return NULL
        if(b!=NULL){
            c=strlen(b);//length of b
            e=strlen(a);//length of a
            a[e-c-1]='\n';//make then char which before "then" become "\n" 
        }
        
        fputs(a,fp);
		
		if ( strstr(Line, "repeat") == &Line[0] ){
			// repeatflag = 1; * add flag for repeat *
			if ( Line[7] == '$') {
				position = 8;
				// * Get Variable Name
				for (position = 8; Line[position] != ' ';position++)
					Variable[position-8] = Line[position];
		}
		// *Error if $ for variable isn't detected
		else {printf("\nNo variable declared in line %d\n", linenum); return 0;}
      
		// find "times" after variable in line and continue
		if (strstr(Line,"times") == &Line[position+=1]){
			strcat(NewLine, Variable);
			strcat(NewLine, " ]");
//			printf("$repeatIndex1=0\n");
//			printf("%s\n",NewLine);
//			printf("do\n");
		}
		// Error if times wasn't found 
		else {printf("\nError - Incorrect syntax - Missing \"times\"\n"); return 0;}
		}
		// ENDING
		if (strstr(Line, "}")) { // && repeatflag == 1 * We can add a flag to seperate if and repeat endings
			printf("repeatIndex1=$[$repeatIndex1+1]\n");
			printf("done\n");
			strcpy(NewLine, "while [ $repeatIndex1 -lt $"); //reset NewLine
			strcpy(Variable, "!");                          //reset Variable
			// repeatflag = 0; * reset flag to 0 *
		}    
     fputs(a,fp);
    }
    fclose(fp);
    return (EXIT_SUCCESS);
}

