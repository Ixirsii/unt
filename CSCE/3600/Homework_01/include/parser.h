/** *********************************************************************** **
 ** *********************************************************************** **
 **                                                                         **
 ** @file                                                                   **
 ** @brief      Provides command line argument parsing and help statements. **
 **                                                                         **
 ** @author     Ryan Porterfield <Ryan@RyanPorterfield.com>                 **
 ** @copyright  (2014) Ryan Porterfield                                     **
 ** @version    0.0.1:2014-09-16                                            **
 ** @license    BSD 3 Clause (See COPYING file)                             **
 **                                                                         **
 ** *********************************************************************** **
 ** *********************************************************************** **/


#ifndef MYGCC_PARSER_H
#define MYGCC_PARSER_H

#include <stdbool.h>
#include <stdlib.h>


void        cleanup         ();
_Bool       is_flag         (const char *arg);
int         parse_assembly  (int argc, const char **argv, int index);
int         parse_debug     (int argc, const char **argv, int index);
int         parse_flag      (int argc, const char **argv, int index);
int         parse_link      (int argc, const char **argv, int index);
int         parse_macro     (int argc, const char **argv, int index);
int         parse_object    (int argc, const char **argv, int index);
int         parse_optimize  (int argc, const char **argv, int index);
int         parse_out       (int argc, const char **argv, int index);
int         parse_to_out    (int argc, const char **argv, int index);
void        print_flags     ();


/** Function prototype for functions which parse flags */
typedef int (*parse_func) (int argc, const char **argv, int index);


typedef enum {
    SUCCESS,        /**< Parsing was a success */
    NOT_FLAG,       /**< The argument is not an optional flag */
    INVALID_FLAG,   /**< The flag is not supported */
    MALFORMED_FLAG  /**< The flag is malformed or missing information */
} parse_status_t;


/** @brief      Data type to hold info about possible command line arguments.
 *
 * @details     Data type assumes a command line argument will only be called
 *              once. A struct is defined for each possible 'flag' or argument
 *              and the struct can hold information about the options with that
 *              argument if it is set.
 */
typedef struct {
    /** How many strings are stored in @link info @endlink */
    int         info_len;
    /** Data strings relating to this flag */
    char        **info;
    /** The printf style format string to print help about the flag */
    char        *help;
    /** Function called to parse flags if they're present */
    parse_func  call;
    /** Was the flag called/set when the program was run */
    _Bool       is_set;
    /** The letter of the flag */
    const char  flag;
} flag_t;


#endif /* MYGCC_PARSER_H */


/** *********************************************************************** **
 **                                   EOF                                   **
 ** *********************************************************************** **/
