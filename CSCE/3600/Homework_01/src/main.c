/** *********************************************************************** **
 ** *********************************************************************** **
 **                                                                         **
 ** @file                                                                   **
 ** @brief      Provides main function to test the parser.                  **
 **                                                                         **
 ** @author     Ryan Porterfield <Ryan@RyanPorterfield.com>                 **
 ** @copyright  (2014) Ryan Porterfield                                     **
 ** @version    0.0.1:2014-09-16                                            **
 ** @license    BSD 3 Clause (See COPYING file)                             **
 **                                                                         **
 ** *********************************************************************** **
 ** *********************************************************************** **/


#include <stdio.h>
#include <parser.h>


int
main (int argc, const char **argv)
{
    for (int i = 0; i < argc; ++i) {
        parse_flag (argc, argv, i);
    }
    print_flags ();
    cleanup ();
    return EXIT_SUCCESS;
}


/** *********************************************************************** **
 **                                   EOF                                   **
 ** *********************************************************************** **/
