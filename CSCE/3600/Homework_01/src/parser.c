/** *********************************************************************** **
 ** *********************************************************************** **
 **                                                                         **
 ** @file                                                                   **
 ** @brief      Provides command line argument parsing and help statements. **
 **                                                                         **
 ** @author     Ryan Porterfield <Ryan@RyanPorterfield.com>                 **
 ** @copyright  (2014) Ryan Porterfield                                     **
 ** @version    0.0.1:2014-09-16                                            **
 ** @license    BSD 3 Clause (See COPYING file)                             **
 **                                                                         **
 ** *********************************************************************** **
 ** *********************************************************************** **/


#include <parser.h>
#include <iso646.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


/** Information for -o - Output filename */
flag_t out = {
    .info_len   = 2,
    .call       = parse_out,
    .is_set     = false,
    .flag       = 'o',
    .help       = "%s naming the executable %s\n"
};

/** Information for -O# where # is between 1 and 4 - Optimization */
flag_t optimize = {
    .info_len   = 2,
    .call       = parse_optimize,
    .is_set     = false,
    .flag       = 'O',
    .help       = "%s setting optimization level to %s\n"
};

/** Information for -g - Debugging symbols */
flag_t debug = {
    .info_len   = 1,
    .call       = parse_debug,
    .is_set     = false,
    .flag       = 'g',
    .help       = "%s to enable debugging\n"
};

/** Information for -l - Link library */
flag_t link = {
    .info_len   = 2,
    .call       = parse_link,
    .is_set     = false,
    .flag       = 'l',
    .help       = "%s linking to %s\n"
};

/** Information for -Dstring - Defines a macro */
flag_t macro = {
    .info_len   = 3,
    .call       = parse_macro,
    .is_set     = false,
    .flag       = 'D',
    .help       = "%s so compilation macro %s %s\n"
};

/** Information for -c - Create object files, don't link */
flag_t object = {
    .info_len   = 1,
    .call       = parse_object,
    .is_set     = false,
    .flag       = 'c',
    .help       = "%s creating object file\n"
};

/** Information for -S - Create assembly files, don't link */
flag_t assembly = {
    .info_len   = 2,
    .call       = parse_assembly,
    .is_set     = false,
    .flag       = 'S',
    .help       = "%s creating assembly file\n"
};

/** Information for -E - Only call preprocessor and print to stdout */
flag_t to_out = {
    .info_len   = 1,
    .call       = parse_to_out,
    .is_set     = false,
    .flag       = 'E',
    .help       = "%s calling the c preprocessor and sending output to stdout\n"
};

/** Iterable array of all supported flags */
flag_t *flags[] = { &out, &optimize, &debug, &link, &macro, &object, &assembly,
    &to_out };

/** Number of flag_t's in @link flags @endlink array. */
static const int FLAG_COUNT = sizeof (flags) / sizeof (flag_t*);


/** @brief      Clean up resources.
 *
 * @details     Free's @link flag_t.info @endlink and all the strings it points
 *              to if it was allocated.  We assume that any flag that
 *              @link flag_t.is_set @endlink was allocated and needs to be
 *              freed and any flag that wasn't set wasn't allocated.
 */
void
cleanup ()
{
    for (int i = 0; i < FLAG_COUNT; ++i) {
        flag_t *cur = flags[i];
        if (cur->is_set) {
            for (int j = 0; j < cur->info_len; ++j)
                free (cur->info[j]);
            free (cur->info);
        }
    }
}


/** @brief      Checks if the string passed is a flag/optional argument.
 *
 * @param arg   The string (presumable a command line argument) being checked.
 * @return      @c true if arg is an optional flag or @c false if it isn't.
 */
_Bool
is_flag (const char *arg)
{
    if (strlen (arg) > 1)
        return '-' == arg[0];
    return false;
}


/** @brief      Parse command line argument to see if it's an optional flag.
 *
 * @param argc  Total number of command line arguments passed.
 * @param argv  Character pointer array of all command line arguments.
 * @param index The index of the flag being parsed/checked.
 * @return      a code from @link parse_status_t @endlink
 */
int
parse_flag (int argc, const char **argv, int index)
{
    const char *argi = argv[index];
    /* Check if the argument starts with a '0' */
    if (not is_flag (argi))
        return NOT_FLAG;
    /* Find which flag the argument is */
    for (int i = 0; i < FLAG_COUNT; ++i) {
        if (flags[i]->flag == argi[1])
            return flags[i]->call (argc, argv, index);
    }
    /* No matching flag was found */
    return INVALID_FLAG;
}


/** @brief      Parse output flag.
 *
 * @param argc  Total number of command line arguments passed.
 * @param argv  Character pointer array of all command line arguments.
 * @param index The index of the flag being parsed/checked.
 * @return      a code from @link parse_status_t @endlink
 */
int
parse_out (int argc, const char **argv, int index)
{
    char *full, *name;
    int len = strlen (argv[index]);
    if (len > 2) {
        /* Flag is formatted as -o<outputname> */
        full = malloc (len * sizeof (char));
        name = malloc ((len - 2) * sizeof (char));
        strcpy (full, argv[index]);
        strcpy (name, &argv[index][2]);
    } else if (index < (argc - 1) and not is_flag (argv[index + 1])) {
        /* Flag is formatted as -o <outputname> */
        const char *name_ptr = argv[index + 1];
        full = malloc ((len + 1 + strlen (name_ptr)) * sizeof (char));
        name = malloc (strlen (name_ptr) * sizeof (char));
        strcpy (full, argv[index]);
        strcat (full, " ");
        strcat (full, name_ptr);
        strcpy (name, name_ptr);
    } else {
        return MALFORMED_FLAG;
    }
    out.is_set = true;
    out.info = malloc (out.info_len * sizeof (char*));
    out.info[0] = full;
    out.info[1] = name;
    return SUCCESS;
}


/** @brief      Parse optimize flag.
 *
 * @param argc  Total number of command line arguments passed.
 * @param argv  Character pointer array of all command line arguments.
 * @param index The index of the flag being parsed/checked.
 * @return      a code from @link parse_status_t @endlink
 */
int
parse_optimize (int argc, const char **argv, int index)
{
    char *full = malloc (4 * sizeof (char));
    char *level = malloc (2 * sizeof (char));
    int len = strlen (argv[index]);
    /* Flag not formatted in -O# format */
    if (len not_eq 3)
        return MALFORMED_FLAG;
    strcpy (full, argv[index]);
    strcpy (level, &argv[index][2]);
    optimize.is_set = true;
    optimize.info = malloc (optimize.info_len * sizeof (char*));
    optimize.info[0] = full;
    optimize.info[1] = level;
    return SUCCESS;
}


/** @brief      Parse debugging symbols flag.
 *
 * @param argc  Total number of command line arguments passed.
 * @param argv  Character pointer array of all command line arguments.
 * @param index The index of the flag being parsed/checked.
 * @return      a code from @link parse_status_t @endlink
 */
int
parse_debug (int argc, const char **argv, int index)
{
    char *full = malloc (3 * sizeof (char));
    int len = strlen (argv[index]);
    if (len not_eq 2)
        return MALFORMED_FLAG;
    strcpy (full, argv[index]);
    debug.info = malloc (debug.info_len * sizeof (char*));
    debug.is_set = true;
    debug.info[0] = full;
    return SUCCESS;
}


/** @brief      Parse link library flag.
 *
 * @param argc  Total number of command line arguments passed.
 * @param argv  Character pointer array of all command line arguments.
 * @param index The index of the flag being parsed/checked.
 * @return      a code from @link parse_status_t @endlink
 */
int
parse_link (int argc, const char **argv, int index)
{
    char *full = malloc (4 * sizeof (char));
    char *lib = malloc (5 * sizeof (char));
    char l;
    int len = strlen (argv[index]);
    /* Flag not formatted in -ll format */
    if (len not_eq 3 or index not_eq (argc - 1))
        return MALFORMED_FLAG;
    l = argv[index][2];
    if (not ('l' == l or 'm' == l))
        return MALFORMED_FLAG;
    switch (l) {
        case 'l':   strcpy (lib, "lex"); break;
        case 'm':   strcpy (lib, "math"); break;
    }
    strcpy (full, argv[index]);
    link.is_set = true;
    link.info = malloc (link.info_len * sizeof (char*));
    link.info[0] = full;
    link.info[1] = lib;
    return SUCCESS;
}


/** @brief      Parses long form macro flags, ie -Dsomemacrohere
 * 
 */
int
parse_long_macro (const char *arg, char **full, char **macro, char **val)
{
    static const char long_str[] = "is set to ";
    static const char short_str[] = "is set";
    int len = strlen (arg);
    *full = malloc (len * sizeof (char));
    char *v = strstr (arg, "=");
    if (NULL == v) {
        *macro = malloc ((strlen (arg) - 2) * sizeof (char));
        *val = malloc (strlen (long_str) * sizeof (char));
        strcpy (*val, long_str);
        strcpy (*macro, &arg[2]);
    } else {
        int v_len = strlen (v + 1);
        int m_len = len - (v_len + 3);
        *macro = malloc (m_len * sizeof (char));
        *val = malloc ((strlen (short_str) + v_len) * sizeof (char));
        memcpy (*macro, &arg[2], m_len);
        strcpy (*val, short_str);
        strcat (*val, v + 1);
    }
    strcpy (*full, arg);
    return SUCCESS;
}

/** @brief      Parse macro flag.
 *
 * @param argc  Total number of command line arguments passed.
 * @param argv  Character pointer array of all command line arguments.
 * @param index The index of the flag being parsed/checked.
 * @return      a code from @link parse_status_t @endlink
 */
int
parse_macro (int argc, const char **argv, int index)
{
    char *full, *var, *val;
    int len = strlen (argv[index]);
    if (not len > 2)
        return MALFORMED_FLAG;
    parse_long_macro (argv[index], &full, &var, &val);
    macro.is_set = true;
    macro.info = malloc (macro.info_len * sizeof (char*));
    macro.info[0] = full;
    macro.info[1] = var;
    macro.info[2] = val;
    return SUCCESS;
}


/** @brief      Parse object file flag.
 *
 * @param argc  Total number of command line arguments passed.
 * @param argv  Character pointer array of all command line arguments.
 * @param index The index of the flag being parsed/checked.
 * @return      a code from @link parse_status_t @endlink
 */
int
parse_object (int argc, const char **argv, int index)
{
    char *full = malloc (3 * sizeof (char));
    int len = strlen (argv[index]);
    if (len not_eq 2)
        return MALFORMED_FLAG;
    strcpy (full, argv[index]);
    object.info = malloc (debug.info_len * sizeof (char*));
    object.is_set = true;
    object.info[0] = full;
    return SUCCESS;
}


/** @brief      Parse assembly file flag.
 *
 * @param argc  Total number of command line arguments passed.
 * @param argv  Character pointer array of all command line arguments.
 * @param index The index of the flag being parsed/checked.
 * @return      a code from @link parse_status_t @endlink
 */
int
parse_assembly (int argc, const char **argv, int index)
{
    char *full = malloc (3 * sizeof (char));
    int len = strlen (argv[index]);
    if (len not_eq 2)
        return MALFORMED_FLAG;
    strcpy (full, argv[index]);
    assembly.info = malloc (debug.info_len * sizeof (char*));
    assembly.is_set = true;
    assembly.info[0] = full;
    return SUCCESS;
}


/** @brief      Parse preprocessor only flag.
 *
 * @param argc  Total number of command line arguments passed.
 * @param argv  Character pointer array of all command line arguments.
 * @param index The index of the flag being parsed/checked.
 * @return      a code from @link parse_status_t @endlink
 */
int
parse_to_out (int argc, const char **argv, int index)
{
    char *full = malloc (3 * sizeof (char));
    int len = strlen (argv[index]);
    if (len not_eq 2)
        return MALFORMED_FLAG;
    strcpy (full, argv[index]);
    to_out.info = malloc (debug.info_len * sizeof (char*));
    to_out.is_set = true;
    to_out.info[0] = full;
    return SUCCESS;
}


/**
 *
 */
void
print_flags ()
{
    if (out.is_set)
        printf (out.help, out.info[0], out.info[1]);
    if (optimize.is_set)
        printf (optimize.help, optimize.info[0], optimize.info[1]);
    if (debug.is_set)
        printf (debug.help, debug.info[0]);
    if (link.is_set)
        printf (link.help, link.info[0], link.info[1]);
    if (macro.is_set)
        printf (macro.help, macro.info[0], macro.info[1], macro.info[2]);
    if (object.is_set)
        printf (object.help, object.info[0], object.info[1]);
    if (assembly.is_set)
        printf (assembly.help, assembly.info[0], assembly.info[1]);
    if (to_out.is_set)
        printf (to_out.help, to_out.info[0]);
}


/** *********************************************************************** **
 **                                   EOF                                   **
 ** *********************************************************************** **/
