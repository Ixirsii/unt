/*
 * Copyright (c) 2016, Ryan Porterfield
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *     1. Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *     3. Neither the name of the copyright holder nor the names of its
 *     contributors may be used to endorse or promote products derived from
 *     this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <cstdlib>
#include <ciso646>
#include <iostream>
#include <pthread.h>
#include <sys/types.h>


pthread_mutex_t lock0, lock1;

void* process0(void*);
void* process1(void*);
void* process2(void*);


int main() {
    pthread_t id0, id1, id2;
    pthread_mutexattr_t attr;
    pthread_mutexattr_init(&attr);
    pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_NORMAL);
    if (pthread_mutex_init(&lock0, &attr) not_eq 0) {
        std::cerr << "Mutex 0 init failed" << std::endl;
        return EXIT_FAILURE;
    }
    if (pthread_mutex_init(&lock1, &attr) not_eq 0) {
        std::cerr << "Mutex 1 init failed" << std::endl;
        return EXIT_FAILURE;
    }

    /*std::cout << "PTHREAD_MUTEX_DEFAULT " << PTHREAD_MUTEX_DEFAULT << std::endl;
    std::cout << "PTHREAD_MUTEX_NORMAL " << PTHREAD_MUTEX_NORMAL << std::endl;
    std::cout << "PTHREAD_MUTEX_ERRORCHECK " << PTHREAD_MUTEX_ERRORCHECK << std::endl;
    std::cout << "PTHREAD_MUTEX_RECURSIVE " << PTHREAD_MUTEX_RECURSIVE << std::endl;
    std::cout << "Mutex type: " << type << std::endl;*/

    pthread_create(&id0, NULL, &process0, NULL);
    pthread_create(&id1, NULL, &process1, NULL);
    pthread_create(&id2, NULL, &process2, NULL);

    pthread_join(id0, NULL);
    pthread_join(id1, NULL);
    pthread_join(id2, NULL);
    pthread_mutexattr_destroy(&attr);
    pthread_mutex_destroy(&lock0);
    pthread_mutex_destroy(&lock1);

    return EXIT_SUCCESS;
}

void* process0(void*) {
    for (int i = 0; i < 2; ++i) {
        pthread_mutex_lock(&lock0);
        pthread_mutex_lock(&lock0);
        std::cout << "Process 0" << std::endl;
        pthread_mutex_unlock(&lock1);
        pthread_mutex_unlock(&lock0);
    }
    return NULL;
}

void* process1(void*) {
    for (int i = 0; i < 2; ++i) {
        pthread_mutex_lock(&lock1);
        pthread_mutex_lock(&lock0);
        std::cout << "Process 1" << std::endl;
        pthread_mutex_unlock(&lock0);
        pthread_mutex_unlock(&lock1);
    }
    return NULL;
}

void* process2(void*) {
    for (int i = 0; i < 2; ++i) {
        pthread_mutex_lock(&lock1);
        std::cout << "Process 2" << std::endl;
        pthread_mutex_unlock(&lock1);
    }
    return NULL;
}


/* ************************************************************************  *
 *                                    EOF                                    *
 * ************************************************************************  */