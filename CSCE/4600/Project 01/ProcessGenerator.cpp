
#include <iostream>
#include <cstdlib>
#define K_PROCESSES 50

#include "ProcessGenerator.h"

using namespace std;



ProcessGenerator::ProcessGenerator()
{
	PGen=NULL;
}

ProcessGenerator::~ProcessGenerator()
{
	remove();
}

void ProcessGenerator::distribute(int min_p)
{
	//iterator
	Ptable* iter = PGen;
    while (iter != NULL) {
        int min = processors[0].get_finish_time();
        int min_index = 0;
        for (int i = 1; i < min_p; ++i) {
            int c = processors[i].get_finish_time();
            if (c < min) {
                min = c;
                min_index = i;
            }
        }
        iter = processors[min_index].add(iter);
    }
}

void ProcessGenerator::remove()
{
    for (int i = 0; i < 4; ++i)
        processors[i].clear();
    PGen = NULL;
}

int ProcessGenerator::memsize () {
	int b;
	int p;
	int addmem=0;
	int upperlimit=100000;			//upper limit from memory size required
	int lowerlimit=1000;			//lower limit from memory size required
	int mean=20000;					// mean required from the total memory size from all processes

	/*execute triangular distribution*/
	if ((rand() % (upperlimit - lowerlimit)) > (mean - lowerlimit)) 
	{
		b= (rand() % (mean - lowerlimit)) + lowerlimit;
		addmem=addmem + b;
		return b;
	}
	else
	{
		p=  (rand() % (upperlimit - mean)) + mean;
		addmem=addmem + p;
	}
	return p;
}

int ProcessGenerator::cycleT ()
{
	 return rand() % (11000-1000)+1000;
}

void ProcessGenerator::P_Generator()
{

	int process_id= 1;
	int arrival_time= 0;
	int cycle= cycleT();
	int size_memory= memsize();
    Ptable* last = PGen;

	for (int i=0; i<K_PROCESSES; i++)
	{
        Ptable* generator = new Ptable(process_id, arrival_time, cycle, size_memory);
	
        if (PGen == NULL)
        {
            PGen = generator;
            last = generator;
        }
        else
        {
            last->next = generator;
            last = generator;
        }
		process_id=process_id+1;
		arrival_time=arrival_time+50;
		cycle=cycleT();
		size_memory=memsize();
	}
	printTable();
}

void ProcessGenerator::printTable()		//print the process generator
{
	cout<<"\nProcess\t\tArrival Time\tService Time\tMemory size";
	cout << endl;
	
	Ptable* temp= PGen;					//create a pointer
    int index = 0;
	while (temp != NULL && index < 50)					//while the pointer is not null
	{
        ++index;
		//cout << temp->cycle << endl;
		cout << temp->process_id <<"\t\t" << temp->arrival_time <<"\t\t"<< temp->cycle <<"\t\t" << temp->size_memory <<endl;		//print the info
				
			temp= temp->next;				//move the pointer to next
	}
	cout <<endl;

	
}

void ProcessGenerator::statistics()
{
	int avcycle=0; 	//to calculate the mean from the number of cycles
	int addcycle=0;
	Ptable *currentnode= PGen;
	
	
	int index=0;
	while(currentnode!=NULL)
	{
		addcycle+= currentnode->cycle;
		avcycle+= currentnode->size_memory;
		currentnode= currentnode->next;
		index++;
	}
	cout << "Total processes is " << index << endl;
	cout << "Mean cycle is " << (addcycle/index) << endl; //average cycle must be 6000 (1000-11000)
	cout << "Mean footprint is " << (avcycle/index) << endl; //average footprint must be 20KB-2000


}

void ProcessGenerator::FIFO(int num_p)
{
    // Generate processes
	P_Generator();
	
    // Maximum of 4 processors supported
    int min_p = (num_p < 4) ? num_p : 4;

    distribute(min_p);

    // Run FIFO on each processor
    for (int i = 0; i < min_p; ++i)
        processors[i].complete_job();
    int avg_wait = processors[0].get_avg_wait_time();
    int penalty = processors[0].get_penalty();
    // Calculate average wait time and total penalty
    for (int i = 1; i < min_p; ++i) {
        avg_wait = (avg_wait + processors[i].get_avg_wait_time()) / 2;
        penalty += processors[i].get_penalty();
    }

	cout << endl;
	cout << "Total average wait time for FIFO is: " << avg_wait << endl;
	cout << "Total penalty for context switch is: " << penalty << endl;
	cout << endl;
    remove();
}

void ProcessGenerator::SJF(int num_p)
{
	//cout << "Process Table " << endl;
	P_Generator();
	
    // Maximum of 4 processors supported
    int min_p = (num_p < 4) ? num_p : 4;

    sort();
    distribute(min_p);

    // Run SJF
    for (int i = 0; i < min_p; ++i)
        processors[i].complete_job();

    //printTable();
    int avg_wait = processors[0].get_avg_wait_time();
    int penalty = processors[0].get_penalty();
    // Calculate average wait time and total penalty
    for (int i = 1; i < min_p; ++i) {
        avg_wait = (avg_wait + processors[i].get_avg_wait_time()) / 2;
        penalty += processors[i].get_penalty();
    }

	cout << endl;
	cout << "Total average wait time for SJF is: " << avg_wait << endl;
	cout << "Total penalty for context switch is: " << penalty << endl;
	remove();
}

void ProcessGenerator::RR(int num_p)
{
	const int quantum=50;
	int array [K_PROCESSES];
	int array2 [K_PROCESSES];
	int array3 [K_PROCESSES];
	int j=0;
	int timet=0;
	int penalty=0;
	int temp=0;
	int counter=0;
	int sum_at=0;	//total sum of arriving time
	int per=0;
	P_Generator();
    // Maximum of 4 processors supported
    //int min_p = (num_p < 4) ? num_p : 4;
	Ptable *node1 = PGen;
	
    // Initialize arrays
	for (int i = 0; i < K_PROCESSES; i++) {
		array2[i]=node1->process_id;
		array[i] = node1->cycle;
		array3[i]=node1->arrival_time;
		
		node1=node1->next;
	}
	cout << "Round Robin Schedule" << endl;
	cout<<"\nProcess\t\tService Time\tTotal Time";
	cout << endl;
	while (j<K_PROCESSES) {
		for (int i=0; i<K_PROCESSES; i++) {
            // Don't run finished processes
			if (array[i] == 0)
                continue;
            // More or equal time left to the quantum
            if (array[i] >=quantum) {
                int var = timet;
                timet+=quantum;  //it is the total time
                int n= timet-var;
                penalty=penalty+10;
                array[i]=array[i]-quantum; //now is time left
                cout << array2[i] <<"\t\t" <<n<<"\t\t"<< timet <<"\t\t"<< temp<< endl;
                temp+=n+10;  //calculate the starting time for each process
                if (array[i] ==0)
                    j++;
                counter++;
            } else { // Job is shorter than quantum
                int var = timet;
                timet+=array[i];
                int n= timet-var;
                array[i]=0;
                penalty=penalty+10;
                
                cout << array2[i] <<"\t\t" <<n<<"\t\t"<< timet <<"\t\t"<< temp<< endl;
                temp+=n+10;
                per += temp -n -10; 
                sum_at += array3[i];
                j++;
            }
		}
	}
	cout <<endl;
	cout << "Total wait time for Round Robin is: " << ((per-sum_at)-(counter*quantum))/K_PROCESSES << endl;
	cout << "Total Penalty for context switch is: " << (penalty-10) << endl;
	cout << endl;
	
	remove();
	
}	

void ProcessGenerator::sort() {
    if (PGen == NULL)
        return;
	/* This section sorts the processes comparing the arrival time and the sum of the service time*/
	Ptable *currentnode = PGen;
	int min = currentnode->get_arrival_time();
	Ptable *node1 = PGen; //current
	Ptable *head3 = node1;
	Ptable *node2 = PGen;  //insertionPointer
	//node1=node1->get_next();		//pointer transversing
	//cout << node1->get_cycle() << endl;
	node1=head3;			//pointing to the head
	//cout << node1->get_cycle() << endl;
	
	int sum_st= min;		//declare the sum of service time equal to the minimun arrival time
					
	while (node2->get_arrival_time() != sum_st)
	{
			node2=node2->get_next();
	}
    int temp10= node1->get_cycle();
    int temp11= node1->get_process_id();
    int temp12= node1->get_arrival_time();
    int temp13= node1->get_size_memory();
        
    node1->cycle = node2->get_cycle();
    node1->process_id = node2->get_process_id();
    node1->arrival_time = node2->get_arrival_time();
    node1->size_memory = node2->size_memory;
                
    node2->cycle = temp10;
    node2->process_id = temp11;
    node2->arrival_time = temp12;
    node2->size_memory = temp13;
    sum_st+= node1->get_cycle();  //store new value of sum_st
    node2=node1->get_next();
	
	Ptable *current = PGen;
	Ptable *head = current;
	Ptable *insertionPointer = PGen;
	current = current->get_next();
	
    while (current !=NULL) {
        insertionPointer = head->get_next();
        while (insertionPointer != current) {
            if (insertionPointer->get_cycle() > current->get_cycle()
                    && insertionPointer->get_arrival_time() <sum_st) {
                sum_st+=insertionPointer->get_cycle();
                int temp= current->get_cycle();
                int temp1= current->get_process_id();
                int temp2= current->get_arrival_time();
                int temp3= current->size_memory;
                current->cycle = insertionPointer->get_cycle();
                current->process_id = insertionPointer->get_process_id();
                current->arrival_time = insertionPointer->get_arrival_time();
                current->size_memory = insertionPointer->size_memory;
                insertionPointer->cycle = temp;
                insertionPointer->process_id = temp1;
                insertionPointer->arrival_time = temp2;
                insertionPointer->size_memory = temp3;	
            } else {
                insertionPointer = insertionPointer->get_next();
            }
            
        }
        current = current->get_next();
    }
}

// EOF
