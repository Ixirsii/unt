
#include <string>
#include <cstring>
#include <stdlib.h>

#ifndef PTABLE_h
#define PTABLE_h

using namespace std;

class Ptable
{
	public:
	friend class ProcessGenerator;					
	Ptable(int pid, int at, int c, int fp);				//constructor

    int get_arrival_time();
    int get_cycle();
    int get_process_id();
    int get_size_memory();
    Ptable* get_next();
    void set_next(Ptable* next);
	
	private:
	int process_id;									//variable to store the process id
	int arrival_time;							//variable to store process arrival time
	int cycle;									//variable to store process burst time
	int size_memory;							//variable to store memory footprint
	Ptable* next;								//pointer to get access to link list
};
#endif
