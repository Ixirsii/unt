


#include <iostream>
#include <string>


#include "Ptable.h"

using namespace std;

Ptable::Ptable(int pid, int at, int c, int fp)
{
	process_id=pid;						//to store the process id
	arrival_time = at;
	cycle=c;						//to store burst time
	size_memory=fp;						//to store memory footprint
	next=NULL;							//pointer to get access to the information in link list
}

int Ptable::get_arrival_time()
{
    return arrival_time;
}

int Ptable::get_cycle()
{
    return cycle;
}

int Ptable::get_process_id()
{
    return process_id;
}

Ptable* Ptable::get_next()
{
    return next;
}

int Ptable::get_size_memory()
{
    return size_memory;
}

void Ptable::set_next(Ptable* next)
{
    this->next = next;
}
