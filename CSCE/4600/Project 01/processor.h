#ifndef PROCESSOR_H
#define PROCESSOR_H
#include "Ptable.h"


class Processor {
public:
    explicit Processor();
    virtual ~Processor();

    Ptable* add(Ptable* process);
    void clear();
	void complete_job();
    int get_avg_wait_time();
    int get_finish_time();
    int get_penalty();
    void print_table();
	void RR ();
    void quantum();

private:
    Ptable* processes;
    Ptable* last;
    int avg_wait;
    int penalty;

};


#endif // PROCESSOR_H
