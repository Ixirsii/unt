
#include <iostream>
#include <cstdlib>


#ifndef PROCESSGENERATOR_H
#define PROCESSGENERATOR_H
#include "Ptable.h"
#include "processor.h"


class ProcessGenerator
{
	public:
	ProcessGenerator();
	~ProcessGenerator();
	
	void P_Generator();
	void printTable();
	void statistics();
	void FIFO(int num_p);
	void SJF(int num_p);
	void RR (int num_p);
	void remove();
	
	//friend class scheduler;	
	
	private:
    const static int PENALTY = 10;
	Ptable* PGen;
    Processor processors[4];

	int cycleT();
    void distribute(int min_p);
	int memsize ();	
    void sort();
};
#endif
