
#include <iostream>
#include <string>


#include "ProcessGenerator.h"

using namespace std;
	
  	
	ProcessGenerator *table= new ProcessGenerator();
	//scheduler *type= new scheduler();
	string arg2, arg4;
	string command;	

/* Process generator funcions*/	
void process()
{
	table->P_Generator();
}
/*void print()									//print the information
{

	table->printTable();				//implement the print function


}*/
/*print the median of cycles and footprint*/
void print11()									//print the information
{
	table->statistics();				//implement the print function
}

void FIFO_scheduler()
{
	table->FIFO(1);
}	

void SJF_scheduler()
{
	table->SJF(1);
}

void RR_scheduler()
{
	table->RR(1);
}

// menu function to call the commands for the program
void menu()	
{
	do
	{
		cout << "<Scheduler Process Simulator> " << endl; //enter the prompt
		cout << "Enter the Following Options: " <<endl;
		cout << "FIFO " <<endl;
		cout << "SJF " << endl;
		cout << "RR " << endl;
		cout << "quit " << endl;
		cout << "schedule> ";
		cin >> command;							//enter the command to execute
		
		if(command== "process"){					//if command is insert
			process();							//implement insert function
		}
		else if (command=="median"){				//if command is print
			print11();							//implement the print function
		}
		else if (command=="FIFO"){				//if command is print
			FIFO_scheduler();							//implement the print function
		}
		else if (command=="multi-FIFO") {
			table->FIFO(4);
		}
		else if (command=="SJF"){				//if command is print
			SJF_scheduler();							//implement the print function
		}
		else if (command=="multi-SJF") {
			table->SJF(4);
		}
		else if (command=="RR"){				//if command is print
			RR_scheduler();							//implement the print function
		}
		
		else if(command!= "quit"){				//if the command is not in the menu
			cout << "Error!" << endl;			//print error
		}
	}
		while(command!= "quit");				//exit the program
}

int main ()
{
	menu();
	//process();
	return 0;

}
