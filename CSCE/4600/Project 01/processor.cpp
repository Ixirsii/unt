#include "processor.h"

#include <iostream>

#include "Ptable.h"


Processor::Processor() : avg_wait(0), penalty(0)
{

}

Processor::~Processor()
{
}

Ptable* Processor::add(Ptable* process)
{
    Ptable *next = process->get_next();
    if (processes == NULL) {
        processes = process;
        last = process;
    } else {
        last->set_next(process);
        last = process;
    }
    process->set_next(NULL);
    return next;
}

void Processor::clear()
{
    avg_wait = 0;
    penalty = 0;
	Ptable *temp;
	while (processes !=NULL)
	{
		temp = processes;
		processes = processes->get_next();
		delete temp;
	}
}

void Processor::complete_job()
{
	Ptable* temp1 = processes;
	Ptable* temp2 = processes;
	int addcycle1 = 0;
	int waittime = temp2->get_arrival_time();
	
	Ptable* iter = processes;					
	while (iter != NULL)					
	{
		addcycle1 += temp1->get_cycle();
		waittime = addcycle1 - temp2->get_arrival_time() - temp1->get_cycle() + penalty;
		avg_wait = (avg_wait + waittime) / 2;
        if (iter->get_next() != NULL)
            penalty = penalty + 10;

        iter = iter->get_next();
        temp1 = temp1->get_next();
        temp2 = temp2->get_next();
	}
    print_table();
}

int Processor::get_avg_wait_time()
{
    return avg_wait;
}

int Processor::get_finish_time()
{
    int time = 0;
    if (processes == NULL)
        return 0;
    Ptable* iter = processes;
    time += iter->get_cycle();
    while (iter != last) {
        iter = iter->get_next();
        time += iter->get_cycle();
    }
    return time;
}

int Processor::get_penalty()
{
    return penalty;
}

void Processor::print_table()
{
	std::cout << "Process\t\tArrival Time\tService Time\tMemory size";
    std::cout << std::endl;
	
	Ptable* iter = processes;
	while (iter != NULL)
	{
        std::cout << iter->get_process_id() << "\t\t";
        std::cout << iter->get_arrival_time() << "\t\t";
        std::cout << iter->get_cycle() << "\t\t";
        std::cout << iter->get_size_memory() << std::endl;
        iter = iter->get_next();
	}
    std::cout << "Average wait time: " << avg_wait << std::endl;
    std::cout << "Penalty for context switch is: " << penalty << std::endl;
    std::cout << std::endl;
}

void Processor::quantum()
{

}
