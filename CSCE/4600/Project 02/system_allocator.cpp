/*
 * Copyright (c) 2016, Erika Gutierrez Portillo, Ryan Porterfield, Shannon Hart
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *     1. Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *     3. Neither the name of the copyright holder nor the names of its
 *     contributors may be used to endorse or promote products derived from
 *     this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "system_allocator.h"
#include <ciso646>
#include <cstddef>
#include <cstdlib>
#include "allocator.h"
#include "process.h"

/*!
 *  \brief    Default constructor
 *  \param max_memory The maximum amount of memory available to the allocator
 *  to allocate to processes
 */
SystemAllocator::SystemAllocator(const size_t max_memory) noexcept
    : Allocator(max_memory) {
}

/*!
 *  \brief    Call malloc to allocate memory
 *  \param memory How much memory needs to be allocated
 *  \return   a pointer to the start of the memory block if allocation was
 *            successful, otherwise NULL
 */
void* SystemAllocator::allocate(const std::size_t memory) noexcept {
  if (not allocateMemory(memory))
    return NULL;
  return malloc(memory);
}

/*!
 *  \brief    Call free to deallocate (free) memory
 *  \param process A process with allocated memory that needs to be deallocated
 */
void SystemAllocator::deallocate(Process& process) noexcept {
  deallocateMemory(process.getMemorySize());
  free(process.getMemory());
}

/* ************************************************************************  *
 *                                    EOF                                    *
 * ************************************************************************  */
