/*
 * Copyright (c) 2016, Erika Gutierrez Portillo, Ryan Porterfield, Shannon Hart
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *     1. Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *     3. Neither the name of the copyright holder nor the names of its
 *     contributors may be used to endorse or promote products derived from
 *     this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "mem_scheduler.h"
#include <vector>
#include "allocator.h"
#include "process.h"
#include "processor.h"
#include "scheduler.h"

/*!
 *  \brief    Default constructor
 *
 *  Initialize Scheduler with 1 processor
 *
 *  \param allocator A reference to the Allocator instance to use for
 *            allocating memory to processes
 */
MemoryScheduler::MemoryScheduler(Allocator& allocator)
    : Scheduler(1), allocator_(allocator) {
}

/*! 
 *  \brief    Schedule each process with First-In-First-Out
 *
 *  Run through the list of processes, attempting to allocate memory for each
 *  and run it. If memory can not be allocated, add the process to the memory
 *  queue.
 */
void MemoryScheduler::run() noexcept {
  // We only need 1 processor for this project
  Processor& processor = getProcessor();
  ready_queue_ = schedule(ready_queue_, 0);

  while (not ready_queue_.empty() and not memory_queue_.empty()) {
    ready_queue_ = schedule(ready_queue_, processor.getCurrentTime());
    /*
     *  Any Processes in the memory queue have been waiting longer than the
     *  first process in the ready queue since processes in the memory queue
     *  were in the ready queue, and failed to run on its turn.
     */
    if (runFromMemQueue())
      continue;
    runFromReadyQueue();
  }
  printStatistics();
}

/*!
 *  \brief    Attempt to run a process on the memory queue
 *
 * Attempts to allocate memory for a process in the memory queue and run it.
 *
 *  \return   true if a process from the memory queue was run, otherwise false
 */
bool MemoryScheduler::runFromMemQueue() {
  Processor& processor = getProcessor();
  if (memory_queue_.empty())
    return false;
  std::vector<Process>::iterator iter = memory_queue_.begin();
  void* block = allocator_.allocate(iter->getMemorySize());
  if (block == NULL)
    return false;
  processor.run(std::move(*iter));
  memory_queue_.erase(iter);
  return true;
}

/*!
 *  \brief    Attempt to run a process on the ready queue
 *
 *  Attempts to allocate memory for a process in the memory queue and run it.
 *  If memory can't be allocated for the process, add it to the memory queue.
 *
 *  \return   true if a process from the ready queue was run, otherwise false
 */
bool MemoryScheduler::runFromReadyQueue() {
  Processor& processor = getProcessor();
  std::vector<Process>::iterator iter = ready_queue_.begin();
  void* block = allocator_.allocate(iter->getMemorySize());

  if (ready_queue_.empty())
    return false;

  if (block == NULL) {
    memory_queue_.push_back(std::move(*iter));
  } else {
    processor.run(std::move(*iter));
  }
  /* The value iter points to is in an undefined state, pop off the undefined
   * value. This is basically equivalent to a pop_front function */
  iter = ready_queue_.erase(iter);
  return true;
}

/* ************************************************************************* *
 *                                    EOF                                    *
 * ************************************************************************* */
