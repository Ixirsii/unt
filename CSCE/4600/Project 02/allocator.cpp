/*
 * Copyright (c) 2016, Erika Gutierrez Portillo, Ryan Porterfield, Shannon Hart
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *     1. Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *     3. Neither the name of the copyright holder nor the names of its
 *     contributors may be used to endorse or promote products derived from
 *     this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "allocator.h"
#include <ciso646>
#include <cstddef>

/*!
 *  \brief    Default constructor
 *  \param max_memory The maximum amount of memory available to the allocator
 *  to allocate to processes
 */
Allocator::Allocator(const std::size_t max_memory) noexcept 
    : max_memory_(max_memory) {
}

/*!
 *  \brief    Allocate a block of memory for a process
 *
 *  Checks if there is enough total memory available to allocate, and if there
 *  is adds the newly allocated memory to allocated_memory_.
 *
 *  \param memory How much memory needs to be allocated
 *  \return   true if there is enough memory to allocate, otherwise false
 */
bool Allocator::allocateMemory(const std::size_t memory) noexcept {
  if (not (allocated_memory_ + memory < max_memory_))
    return false;
  allocated_memory_ += memory;
  return true;
}

/*!
 *  \brief    Deallocate (free) a block of memory
 *
 *  Subtracts memory from allocated_memory_, increasing the total available
 *  free memory.
 *
 *  \param memory How much memory needs to be deallocated
 */
void Allocator::deallocateMemory(const std::size_t memory) noexcept {
  allocated_memory_ -= memory;
}

/* ************************************************************************  *
 *                                    EOF                                    *
 * ************************************************************************  */
