/*
 * Copyright (c) 2016, Erika Gutierrez Portillo, Ryan Porterfield
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *     1. Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *     3. Neither the name of the copyright holder nor the names of its
 *     contributors may be used to endorse or promote products derived from
 *     this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "scheduler.h"
#include <ciso646>
#include <iostream>
#include <memory>
#include <vector>

/*!
 *  \brief    Create a new scheduler
 *
 *  The scheduler automatically generates a randomized set of processes that
 *  can either be scheduled or analyzed.
 *
 *  \param    num_processors The number of processors to use for scheduling
 */
Scheduler::Scheduler(int num_processors) noexcept
    : processes(generate()), iter(processes.begin()),
      num_processors(num_processors) {
  printProcesses();
}

/*!
 *  \brief    Generate a new set of processes
 *  \return   a std::vector by value containing a set of randomized processes
 *            for scheduling.
 */
std::vector<Process> Scheduler::generate() const noexcept {
  std::vector<Process> processes;
  for (unsigned int i = 0; i < Process::PROCESSES; ++i) {
    Process p;
    processes.push_back(std::move(p));
  }
  return processes;
}

/*!
 *  \brief    Get the processor which finishes first
 *
 *  Since the scheduler doesn't actually run asynchronously, it just simulates
 *  it, we need to schedule the next process to the processor which completed
 *  first. To do this we return the processor witht eh minimum current time.
 *
 *  \return   the processor with the minimum current time
 */
Processor& Scheduler::getProcessor() {
  int min = processors[0].getCurrentTime();
  int min_index = 0;
  // Find the processor with the shortest time to completion
  for (int i = 1; i < num_processors; ++i) {
    int c = processors[i].getCurrentTime();
    if (c < min) {
      min = c;
      min_index = i;
    }
  }
  return processors[min_index];
}

/*!
 *  \brief    Print a Process to the console
 *  \param    process A const reference to a Process to be printed out
 */
void Scheduler::printProcess(const Process& process) const noexcept {
  std::cout << process.getProcessID() << "\t\t";
  std::cout << process.getArrivalTime() << "\t\t";
  std::cout << process.getRemainingTime() << "\t\t";
  std::cout << process.getMemorySize() << std::endl;
}

/*!
 *  \brief    Print the current list of processes
 */
void Scheduler::printProcesses() const noexcept {
  std::vector<Process>::const_iterator iter;
  std::cout << std::endl;
  std::cout << "Process\t\tArrival Time\tService Time\tMemory size";
  std::cout << std::endl;
  for (iter = processes.begin(); iter not_eq processes.end(); ++iter) {
    printProcess(*iter);
  }
  std::cout << std::endl;
  std::cout << std::endl;
}

/*! 
 *  \brief    Print the average wait time and penalty of the scheduler
 */
void Scheduler::printStatistics() const noexcept {
  unsigned int avg_wait = processors[0].getAvgWaitTime();
  unsigned int penalty = processors[0].getPenalty();
  for (int i = 0; i < num_processors; ++i)
    processors[i].print();
  for (int i = 1; i < num_processors; ++i) {
    avg_wait = (avg_wait + processors[i].getAvgWaitTime()) / 2;
    penalty += processors[i].getPenalty();
  }
  std::cout << std::endl;
  std::cout << "Total average wait time is: " << avg_wait << std::endl;
  std::cout << "Total penalty for context switch is: " << penalty << std::endl;
}

/*! 
 *  \brief    Check if there are still unscheduled processes
 *  \return   true if there are still unscheduled processes, otherwise false
 */
bool Scheduler::running() const noexcept {
  return not processes.empty();
}

/*! 
 *  \brief    Add processes to the ready queue
 *
 *  A process is ready when the system time is greater than or equal to the
 *  process's arrival time. When a process is ready it gets added to the ready
 *  queue so the scheduling algorithm can send it to a processor to be
 *  executed.
 *
 *  \param    ready_queue A reference to the ready queue being used by the
 *            processor
 *  \param    system_time The time of the system, used to determine if a
 *            process is ready to be executed (has arrived) or not
 *  \return   A reference to the ready_queue parameter that was passed in for
 *            convenience
 */
std::vector<Process>& Scheduler::schedule(std::vector<Process>& ready_queue,
    unsigned int system_time) {
  while (iter->getArrivalTime() >= system_time and 
      iter not_eq processes.end()) {
    ready_queue.push_back(std::move(*iter));
    iter = processes.erase(iter);
  }
  return ready_queue;
}

/*!
 *  \brief    Print advanced statistics about the generated processes
 *  
 *  Print advanced statistics about the generated processes so that the program
 *  can be analyzed.
 */
void Scheduler::statistics() noexcept {
  std::vector<Process>::iterator iter;
  int avcycle = 0;   //to calculate the mean from the number of cycles
  int addcycle = 0;

  for (iter = processes.begin(); iter not_eq processes.end(); ++iter) {
    addcycle += iter->getTotalBurstTime();
    avcycle += iter->getMemorySize();
  }
  std::cout << "Total processes is " << processes.size() << std::endl;
  //average cycle must be 6000 (1000-11000)
  std::cout << "Mean cycle is " << (addcycle / processes.size()) << std::endl;
  //average footprint must be 20KB-2000
  std::cout << "Mean footprint is " << (avcycle / processes.size()) << std::endl;
  processes.clear();
}

/* ************************************************************************* *
 *                                    EOF                                    *
 * ************************************************************************* */
