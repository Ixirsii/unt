/*
 * Copyright (c) 2016, Erika Gutierrez Portillo, Ryan Porterfield
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *     1. Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *     3. Neither the name of the copyright holder nor the names of its
 *     contributors may be used to endorse or promote products derived from
 *     this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <ciso646>
#include <cstdlib>
#include <iostream>
#include <string>
#include "fifo.h"
#include "rr.h"
#include "scheduler.h"
#include "sjf.h"

/*!
 *  \brief    Print the menu of commands
 */
void printMenu() {
  std::cout << "<Scheduler Process Simulator> " << std::endl;
  std::cout << "Enter the Following Options: " << std::endl;
  std::cout << "FIFO " << std::endl;
  std::cout << "SJF " << std::endl;
  std::cout << "RR " << std::endl;
  std::cout << "quit " << std::endl;
}

/*!
 *  \brief    Print the statistics of generated processes
 */
void printStatistics() {
  FIFOScheduler(1).statistics();
}

/*!
 *  \brief    Schedule processes with the FIFO scheduler
 */
void fifo(const int num_processors) {
  FIFOScheduler(num_processors).run();
}  

/*!
 *  \brief    Schedule processes with the SJF scheduler
 */
void sjf(const int num_processors) {
  SJFScheduler(num_processors).run();
}

/*!
 *  \brief    Schedule processes with the RR scheduler
 */
void rr(const int num_processors) {
  RRScheduler(num_processors).run();
}

/*!
 *  \brief    Main program loop
 *
 *  Run the program in a loop until the user enters the 'quit' function.
 *  Get the user's command, parse it, and execute the appropriate command or
 *  print an error message if the command is invalid.
 */
void menu()  {
  std::string command;
  do {
    printMenu();
    //enter the command to execute
    std::cout << "scheduler >>> ";
    std::cin >> command;
    
    if (command == "median")
      printStatistics();
    else if (command == "FIFO")
      fifo(1);
    else if (command == "multi-FIFO")
      fifo(4);
    else if (command == "SJF")
      sjf(1);
    else if (command == "multi-SJF")
      sjf(4);
    else if (command == "RR")
      rr(1);
    else if (command == "multi-RR")
      rr(4);
    else if(command not_eq "quit")
      std::cout << "Error!" << std::endl;
  }
    while(command not_eq "quit");
}

/*!
 *  \brief    Program executes from here
 */
int main() {
  menu();
  return EXIT_SUCCESS;
}

/* ************************************************************************* *
 *                                    EOF                                    *
 * ************************************************************************* */
