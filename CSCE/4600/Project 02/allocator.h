/*
 * Copyright (c) 2016, Erika Gutierrez Portillo, Ryan Porterfield, Shannon Hart
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *     1. Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *     3. Neither the name of the copyright holder nor the names of its
 *     contributors may be used to endorse or promote products derived from
 *     this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef ALLOCATOR_H_
#define ALLOCATOR_H_

#include <cstddef>
#include <vector>
#include "process.h"

/*! \class    Allocator allocator.h "allocator.h"
 *  \brief    Allocates memory for processes
 *
 *  Allocator is a base class which defines behavior for memory allocators. An
 *  allocator has a maximum memory size which determines how much memory can be
 *  allocated to processes globally. The base class also keeps track of how
 *  much memory has been allocated, although it leaves the actual allocation to
 *  its deriving classes through the pure virtual methods allocate and
 *  deallocate.
 *
 *  \sa       Allocator::allocate
 *  \sa       Allocator::deallocate
 */
class Allocator {
public:
  explicit Allocator(const std::size_t max_memory) noexcept;
  virtual ~Allocator() noexcept = default;

  static size_t getMemoryPercentage(const std::vector<Process>& processes,
                                    const float percentage) noexcept;
  /*!
   *  \brief    Allocate a block of memory for a process
   *  \param memory How much memory needs to be allocated
   *  \return   a pointer to the start of the memory block if allocation was
   *            successful, otherwise NULL
   */
  virtual void* allocate(const std::size_t memory) noexcept = 0;
  /*!
   *  \brief    Deallocate (free) memory
   *  \param process A process with allocated memory that needs to be
   *            deallocated
   */
  virtual void deallocate(Process& process) noexcept = 0;
  std::size_t getAllocatedMemory() const noexcept;
  std::size_t getMaxMemory() const noexcept;

protected:
  bool allocateMemory(const std::size_t memory) noexcept;
  void deallocateMemory(const std::size_t memory) noexcept;

private:
  std::size_t allocated_memory_;
  std::size_t max_memory_;
};

#endif // ALLOCATOR_H_

/* ************************************************************************  *
 *                                    EOF                                    *
 * ************************************************************************  */
