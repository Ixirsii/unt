/*
 * Copyright (c) 2016, Erika Gutierrez Portillo, Ryan Porterfield
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *     1. Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *     3. Neither the name of the copyright holder nor the names of its
 *     contributors may be used to endorse or promote products derived from
 *     this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "process.h"
#include <cstddef>
#include <iostream>
#include <string>

/*!
 *  \brief    Create a new process
 */
Process::Process()
    : process_id_(genProcessID()), arrival_time_(genArrivalTime()),
      burst_time_(genBurstTime()), memory_size_(genMemorySize()),
      remaining_time_(burst_time_) {
}

/*!
 *  \brief    Run the process
 *
 *  burst_time is subtracted from remaining_time until remaining_time is 0 and
 *  the process is marked as complete.
 *
 *  \param burst_time How long the process is run for
 *  \return   how much of the burst_time was unused. If the process does not
 *  complete this will be 0, but if the process completes before the burst_time
 *  is finished the remaining time will be returned.
 */
std::size_t Process::burst(const std::size_t burst_time) noexcept {
  if (burst_time > remaining_time_) {
    remaining_time_ = 0;
    return burst_time - remaining_time_;
  }
  remaining_time_ -= burst_time;
  return 0;
}

/*!
 *  \brief    Check if the process is complete
 *  \return   true if the remaining burst time is 0, otherwise false
 */
bool Process::complete() const noexcept {
  return remaining_time_ == 0;
}

/*!
 *  \brief    Generate the arrival time of the process
 *
 *  Processes arrive every 50 cycles, so the arrival time of this process
 *  should be 50 cycles after the last process, and each set of processes
 *  should start with an arrival time of 0.
 *
 *  \return   arrival (ready) time of the process
 */
std::size_t Process::genArrivalTime() const noexcept {
  static std::size_t counter = 0;
  static std::size_t arrival_time = 0;
  if (counter >= PROCESSES) {
    arrival_time = 0;
    counter = 0;
  }
  ++counter;
  std::size_t tmp = arrival_time;
  arrival_time += 50;
  return tmp;
}

/*!
 *  \brief    Generate the burst time of the process
 *
 *  Randomly select a burst time for the process in the range [1000, 11000].
 *
 *  \return   the burst time of the process
 */
std::size_t Process::genBurstTime() const noexcept {
   return rand() % (11000 - 1000) + 1000;
}

/*!
 *  \brief    Generate the memory size of the process
 *
 *  Randomly select a memory size for the process in the range [1000, 100000].
 *
 *  \return   the memory size of the process
 */
std::size_t Process::genMemorySize() const noexcept {
  // Random number
  int r;
  // upper limit from memory size required
  const static int upperlimit = 100000;
  // lower limit from memory size required
  const static int lowerlimit = 1000;
  // mean required from the total memory size from all processes
  const static int mean = 20000;

  /* execute triangular distribution */
  r = rand();
  if (r % (upperlimit - lowerlimit) > (mean - lowerlimit))
    return (r % (mean - lowerlimit)) + lowerlimit;
  else
    return (r % (upperlimit - mean)) + mean;
}

/*!
 *  \brief    Generate the process's unique ID
 *
 *  Process IDs are unique to each process, auto-incrementing by 1 for each
 *  consecutive process.
 *
 *  \return   the process's unique ID
 */
std::size_t Process::genProcessID() const noexcept {
  static std::size_t counter = 0;
  static std::size_t process_id = 0;
  if (counter >= PROCESSES) {
    process_id = 0;
    counter = 0;
  }
  ++counter;
  std::size_t tmp = process_id;
  ++process_id;
  return tmp;
}

/*!
 *  \brief    Get the time the process arrived in the queue
 *  \return   the time the process arrived in the queue
 */
std::size_t Process::getArrivalTime() const noexcept {
    return arrival_time_;
}

/*!
 *  \brief    Get memory footprint
 *  \return   memory footprint
 */
std::size_t Process::getMemorySize() const noexcept {
    return memory_size_;
}

/*!
 *  \brief    Get the process's unique identifier
 *  \return   the process's unique identifier
 */
std::size_t Process::getProcessID() const noexcept {
    return process_id_;
}

/*!
 *  \brief    Get the remaining burst time
 *
 *  This function returns how long until the process is complete. This is
 *  useful for Round Robin, or other scheduling algorithms which may not
 *  complete the process in a single round.
 *
 *  \return   the remaining burst time
 */
std::size_t Process::getRemainingTime() const noexcept {
  return remaining_time_;
}

/*!
 *  \brief    Get the total burst time
 *
 *  This function returns the total time the process takes to complete. This is
 *  useful for calculating turnaround time or wait time, and for scheduling
 *  algorithms such as shortest job first or first-in-first-out which run a
 *  single process to completion each round.
 *
 *  \return   the total burst time
 */
std::size_t Process::getTotalBurstTime() const noexcept {
    return burst_time_;
}

/* ************************************************************************  *
 *                                    EOF                                    *
 * ************************************************************************  */
