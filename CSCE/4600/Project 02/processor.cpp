/*
 * Copyright (c) 2016, Erika Gutierrez Portillo, Ryan Porterfield
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *     1. Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *     3. Neither the name of the copyright holder nor the names of its
 *     contributors may be used to endorse or promote products derived from
 *     this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "processor.h"
#include <ciso646>
#include <iostream>
#include <vector>
#include "process.h"

/*!
 *  \brief    Default constructor
 */
Processor::Processor() noexcept : avg_wait(0), current_time(0), penalty(0) {
}

/*!
 *  \brief    Complete a scheduled Process
 *
 *  The current time will be updated to the current time plus the process's
 *  burst time, and average wait time and penalty are updated accordingly
 *
 *  \param process A scheduled Process which will be "completed"
 */
void Processor::completeJob(const Process& process) noexcept {
  int wait_time;
  // No penalty for starting the first job
  if (current_time > 0) {
    penalty += Process::PENALTY;
    current_time += Process::PENALTY;
  }
  // Wait until the process arrives
  if (process.getArrivalTime() > current_time)
    current_time = process.getArrivalTime();
  wait_time = current_time - process.getArrivalTime();
  avg_wait = (avg_wait + wait_time) / 2;
  current_time += process.getTotalBurstTime();
  // Mark the process as complete - not necessary now but may be needed later
  //process.burst(process.getCycle());
}

/*!
 *  \brief    Get the average wait time
 *  \return   the average wait time
 */
unsigned int Processor::getAvgWaitTime() const noexcept {
  return avg_wait;
}

/*!
 *  \brief    Get the time it will take to finish all scheduled jobs
 *  \return   the time it will take to finish all scheduled jobs
 */
unsigned int Processor::getCurrentTime() const noexcept {
  return current_time;
}

/*!
 *  \brief    Get the total context switch penalty
 *  \return   the total context switch penalty
 */
unsigned int Processor::getPenalty() const noexcept {
  return penalty;
}

/*!
 *  \brief    Print the table of processes and the scheduling stats
 */
void Processor::print() const noexcept {
  std::cout << "Process\t\tArrival Time\tService Time\tMemory size";
  std::cout << std::endl;
  
  std::vector<Process>::const_iterator iter;
  for (iter = processes.begin(); iter not_eq processes.end(); ++iter) {
    std::cout << iter->getProcessID() << "\t\t";
    std::cout << iter->getArrivalTime() << "\t\t";
    std::cout << iter->getTotalBurstTime() << "\t\t";
    std::cout << iter->getMemorySize() << std::endl;
  }
  std::cout << "Processor average wait time: " << avg_wait << std::endl;
  std::cout << "Processor penalty for context switch is: " << penalty;
  std::cout << std::endl << std::endl;
}

/*!
 *  \brief    Run a process for a set amount of time (quantum)
 *
 *  Certain scheduling algorithms such as Round Robin run a process for a fixed
 *  amount of time (a quantum), then perform a context switch and run a
 *  different process, even if the first process did not complete.
 */
void Processor::quantum(Process& process) noexcept {
  int rtime = process.burst(QUANTUM);
  // No penalty for starting the first job
  if (current_time > 0) {
    penalty += Process::PENALTY;
    current_time += Process::PENALTY;
  }
  if (process.getArrivalTime() > current_time)
    current_time = process.getArrivalTime();
  if (process.complete()) {
    int wait_time = current_time - process.getArrivalTime();
    avg_wait = (avg_wait + wait_time) / 2;
  }
  current_time += QUANTUM - rtime;
}

/*!
 *  \brief    Runs a Process to the processor
 *
 *  A process is added to the list of processes scheduled for this processor,
 *  and completes the job.
 *
 *  \param process A right hand reference to a Process which will be moved into
 *            the processor. Ownership WILL be transfered to the processor
 *  \sa       Processor::completeJob
 */
void Processor::run(Process&& process) noexcept {
  processes.push_back(process);
  completeJob(processes.back());
}

/* ************************************************************************* *
 *                                    EOF                                    *
 * ************************************************************************* */
