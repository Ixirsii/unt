/*
 * Copyright (c) 2016, Erika Gutierrez Portillo, Ryan Porterfield
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *     1. Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *     3. Neither the name of the copyright holder nor the names of its
 *     contributors may be used to endorse or promote products derived from
 *     this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "fifo.h"
#include "scheduler.h"

/*!
 *  \brief    Create a new FIFOScheduler
 *  \param    num_processors The number of processors to use for scheduling
 */
FIFOScheduler::FIFOScheduler(const int num_processors)
    : Scheduler(num_processors) {
}

/*! 
 *  \brief    Schedule each process with First-In-First-Out
 *
 *  The initial ready_queue should only contain 1 process (the one that arrives
 *  at 0). Upon completion of the first process, the schedule function should
 *  add all processes that arrived during the scheduling of the first process
 *  so the function can keep iterating. This works because the burst time for
 *  each process is greater than the arrival time.
 */
void FIFOScheduler::run() noexcept {
  std::vector<Process> ready_queue;
  std::vector<Process>::iterator iter;
  ready_queue = schedule(ready_queue, 0);

  for (iter = ready_queue.begin(); iter not_eq ready_queue.end(); ) {
    Processor& processor = getProcessor();
    ready_queue = schedule(ready_queue, processor.getCurrentTime());
    processor.run(std::move(*iter));
    /* The value iter points to is in an undefined state, pop off the undefined
     * value. This is basically equivalent to a pop_front function */
    iter = ready_queue.erase(iter);
  }
  printStatistics();
}

/* ************************************************************************* *
 *                                    EOF                                    *
 * ************************************************************************* */
