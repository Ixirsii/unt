/*
 * Copyright (c) 2016, Ryan Porterfield
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *     1. Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *     3. Neither the name of the copyright holder nor the names of its
 *     contributors may be used to endorse or promote products derived from
 *     this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


#include <chrono>
#include <cmath>
#include <cstdlib>
#include <iostream>
#include <random>
#include <tuple>
#include <vector>


typedef std::tuple<unsigned int, unsigned int, unsigned int> process_t;
/*! \class
 * \brief Generates process tuples
 */
class Generator {
public:
    explicit Generator() 
            : generator_(std::chrono::system_clock::now().time_since_epoch().count()),
              cycle_dist_(6000, 1500),
              mem_dist_(20, 5) {
    }

    /*!
     * Generate a process
     */
    process_t generate_process() {
        static unsigned int counter = 0;
        return std::make_tuple(counter++, get_cycles(), get_memory());
    }

private:
    /*! Random number generator */
    std::default_random_engine generator_;
    /*! Distribution for cycles */
    std::normal_distribution<double> cycle_dist_;
    /*! Distribution for memory */
    std::normal_distribution<double> mem_dist_;

    /*!
     *
     */
    unsigned int get_cycles() {
        unsigned int rand = (unsigned int) round(cycle_dist_(generator_));
        if (rand < 1000)
            rand = 11000 - rand;
        else if (rand > 11000)
            rand = (rand % 10000) + 1000;
        return rand;
    }

    unsigned int get_memory() {
        unsigned int rand = (unsigned int) round(mem_dist_(generator_));
        if (rand < 1)
            rand = 100 - rand;
        else if (rand > 100)
            rand = (rand % 100) + 1;
        return rand;
    }
};


int main(void) {
    size_t k = 1000;
    std::vector<process_t> processes;
    Generator gen;

    for (size_t i = 0; i < k; ++i) {
        processes.push_back(gen.generate_process());
        std::cout << "Process: " << std::get<0>(processes[i]);
        std::cout << "\tCycles: " << std::get<1>(processes[i]);
        std::cout << "\tMemory: " << std::get<2>(processes[i]) << "K";
        std::cout << std::endl;
    }

    return EXIT_SUCCESS;
}


/* ************************************************************************  *
 *                                    EOF                                    *
 * ************************************************************************  */
