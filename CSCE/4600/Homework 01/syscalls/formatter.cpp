/* ************************************************************************* *
 *                                                                           *
 * Copyright (c) 2016, Ryan Porterfield                                      *
 * All rights reserved.                                                      *
 *                                                                           *
 * Redistribution and use in source and binary forms, with or without        *
 * modification, are permitted provided that the following conditions        *
 * are met:                                                                  *
 *                                                                           *
 *     1. Redistributions of source code must retain the above copyright     *
 *     notice, this list of conditions and the following disclaimer.         *
 *                                                                           *
 *     2. Redistributions in binary form must reproduce the above copyright  *
 *     notice, this list of conditions and the following disclaimer in the   *
 *     documentation and/or other materials provided with the distribution.  *
 *                                                                           *
 *     3. Neither the name of the copyright holder nor the names of its      *
 *     contributors may be used to endorse or promote products derived from  *
 *     this software without specific prior written permission.              *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS   *
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED     *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A           *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT        *
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,    *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED  *
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR    *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF    *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING      *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS        *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.              *
 *                                                                           *
 * ************************************************************************* */


#include <stdexcept>
#include "formatter.h"


/*! Map key for hex value of black */
const std::string Formatter::BLACK = "black";
/*! Map key for hex value of light black */
const std::string Formatter::LIGHT_BLACK = "light black";
/*! Map key for hex value of red */
const std::string Formatter::RED = "red";
/*! Map key for hex value of light red */
const std::string Formatter::LIGHT_RED = "light red";
/*! Map key for hex value of green */
const std::string Formatter::GREEN = "green";
/*! Map key for hex value of light green */
const std::string Formatter::LIGHT_GREEN = "light green";
/*! Map key for hex value of yellow */
const std::string Formatter::YELLOW = "yellow";
/*! Map key for hex value of light yellow */
const std::string Formatter::LIGHT_YELLOW = "light yellow";
/*! Map key for hex value of blue */
const std::string Formatter::BLUE = "blue";
/*! Map key for hex value of light blue */
const std::string Formatter::LIGHT_BLUE = "light blue";
/*! Map key for hex value of magenta */
const std::string Formatter::MAGENTA = "magenta";
/*! Map key for hex value of light magenta */
const std::string Formatter::LIGHT_MAGENTA = "light magenta";
/*! Map key for hex value of cyan */
const std::string Formatter::CYAN = "cyan";
/*! Map key for hex value of light cyan */
const std::string Formatter::LIGHT_CYAN = "light cyan";
/*! Map key for hex value of white */
const std::string Formatter::WHITE = "white";
/*! Map key for hex value of light white */
const std::string Formatter::LIGHT_WHITE = "light white";


/*!
 * \brief       Initialize Formatter
 */
Formatter::Formatter() {
    init_map();
}

/*!
 * \brief       Wrapper function for Formatter::color(Formatter::BLACK)
 * \return      a pango format string to change the foreground color
 * \sa          Formatter::color
 * \sa          Formatter::BLACK
 */
std::string Formatter::black() const noexcept {
    return color(BLACK);
}

/*!
 * \brief       Wrapper function for Formatter::color(Formatter::BLUE)
 * \return      a pango format string to change the foreground color
 * \sa          Formatter::color
 * \sa          Formatter::BLUE
 */
std::string Formatter::blue() const noexcept {
    return color(BLUE);
}

/*!
 * \brief       Get a pango format string to start a span
 * \param color_name Map key of the color of the foreground
 * \return      a pango format string to change the foreground color
 */
std::string Formatter::color(const std::string& color_name) const noexcept {
    std::string color_str = "#000000";
    try {
        color_str = color_map_.at(color_name);
    } catch (std::out_of_range& e) {
    }
    return "<span foreground=\"" + color_str + "\">";
}

/*!
 * \brief       Wrapper function for Formatter::color(Formatter::CYAN)
 * \return      a pango format string to change the foreground color
 * \sa          Formatter::color
 * \sa          Formatter::
 */
std::string Formatter::cyan() const noexcept {
    return color(CYAN);
}

/*!
 * \brief       Get a pango format string to end a span
 * \return      a pango format string to end a span
 */
std::string Formatter::end_color() const noexcept {
    return "</span>";
}

/*!
 * \brief       Wrapper function for Formatter::color(Formatter::GREEN)
 * \return      a pango format string to change the foreground color
 * \sa          Formatter::color
 * \sa          Formatter::GREEN
 */
std::string Formatter::green() const noexcept {
    return color(GREEN);
}

/*!
 * \brief       Initialize color_map_
 * \sa          Formatter::color_map_
 */
void Formatter::init_map() {
    color_map_.insert({
      {BLACK, "#373f4c"}, {LIGHT_BLACK, "#64626d"},
      {RED, "#aa3e64"}, {LIGHT_RED, "#f9afd2"},
      {GREEN, "#39acaa"}, {LIGHT_GREEN, "#96c7be"},
      {YELLOW, "#9a8e70"}, {LIGHT_YELLOW, "#f2ecba"},
      {BLUE, "#29529c"}, {LIGHT_BLUE, "#86b7df"},
      {MAGENTA, "#714161"}, {LIGHT_MAGENTA, "#af9098"},
      {CYAN, "#83cbbf"}, {LIGHT_CYAN, "#a7e8df"},
      {WHITE, "#c0c4c5"}, {LIGHT_WHITE, "#ffffff"},
    });
}

/*!
 * \brief       Wrapper function for Formatter::color(Formatter::LIGHT_BLACK)
 * \return      a pango format string to change the foreground color
 * \sa          Formatter::color
 * \sa          Formatter::LIGHT_BLACK
 */
std::string Formatter::light_black() const noexcept {
    return color(LIGHT_BLACK);
}

/*!
 * \brief       Wrapper function for Formatter::color(Formatter::LIGHT_BLUE)
 * \return      a pango format string to change the foreground color
 * \sa          Formatter::color
 * \sa          Formatter::LIGHT_BLUE
 */
std::string Formatter::light_blue() const noexcept {
    return color(LIGHT_BLUE);
}

/*!
 * \brief       Wrapper function for Formatter::color(Formatter::LIGHT_CYAN)
 * \return      a pango format string to change the foreground color
 * \sa          Formatter::color
 * \sa          Formatter::LIGHT_CYAN
 */
std::string Formatter::light_cyan() const noexcept {
    return color(LIGHT_CYAN);
}

/*!
 * \brief       Wrapper function for Formatter::color(Formatter::LIGHT_GREEN)
 * \return      a pango format string to change the foreground color
 * \sa          Formatter::color
 * \sa          Formatter::LIGHT_GREEN
 */
std::string Formatter::light_green() const noexcept {
    return color(LIGHT_GREEN);
}

/*!
 * \brief       Wrapper function for Formatter::color(Formatter::LIGHT_MAGENTA)
 * \return      a pango format string to change the foreground color
 * \sa          Formatter::color
 * \sa          Formatter::LIGHT_MAGENTA
 */
std::string Formatter::light_magenta() const noexcept {
    return color(LIGHT_MAGENTA);
}

/*!
 * \brief       Wrapper function for Formatter::color(Formatter::LIGHT_RED)
 * \return      a pango format string to change the foreground color
 * \sa          Formatter::color
 * \sa          Formatter::LIGHT_RED
 */
std::string Formatter::light_red() const noexcept {
    return color(LIGHT_RED);
}

/*!
 * \brief       Wrapper function for Formatter::color(Formatter::LIGHT_WHITE)
 * \return      a pango format string to change the foreground color
 * \sa          Formatter::color
 * \sa          Formatter::LIGHT_WHITE
 */
std::string Formatter::light_white() const noexcept {
    return color(LIGHT_WHITE);
}

/*!
 * \brief       Wrapper function for Formatter::color(Formatter::LIGHT_YELLOW)
 * \return      a pango format string to change the foreground color
 * \sa          Formatter::color
 * \sa          Formatter::LIGHT_YELLOW
 */
std::string Formatter::light_yellow() const noexcept {
    return color(LIGHT_YELLOW);
}

/*!
 * \brief       Wrapper function for Formatter::color(Formatter::MAGENTA)
 * \return      a pango format string to change the foreground color
 * \sa          Formatter::color
 * \sa          Formatter::MAGENTA
 */
std::string Formatter::magenta() const noexcept {
    return color(MAGENTA);
}

/*!
 * \brief       Wrapper function for Formatter::color(Formatter::RED)
 * \return      a pango format string to change the foreground color
 * \sa          Formatter::color
 * \sa          Formatter::RED
 */
std::string Formatter::red() const noexcept {
    return color(RED);
}

/*!
 * \brief       Wrapper function for Formatter::color(Formatter::WHITE)
 * \return      a pango format string to change the foreground color
 * \sa          Formatter::color
 * \sa          Formatter::WHITE
 */
std::string Formatter::white() const noexcept {
    return color(WHITE);
}

/*!
 * \brief       Wrapper function for Formatter::color(Formatter::YELLOW)
 * \return      a pango format string to change the foreground color
 * \sa          Formatter::color
 * \sa          Formatter::YELLOW
 */
std::string Formatter::yellow() const noexcept {
    return color(YELLOW);
}

/* ************************************************************************* *
 *                                    EOF                                    *
 * ************************************************************************* */
