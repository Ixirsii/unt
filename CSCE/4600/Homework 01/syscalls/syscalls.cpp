/*
 * Copyright (c) 2016, Ryan Porterfield
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *     1. Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *     3. Neither the name of the copyright holder nor the names of its
 *     contributors may be used to endorse or promote products derived from
 *     this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <chrono>
#include <csignal>
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <ctime>
#include <iostream>
#include <memory>
#include <string>
#include <thread>
#include <unordered_map>

#include <netdb.h>
#include <unistd.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>

#include "server.h"


bool connect_to_server(int& sockfd) noexcept;
void signal_handler(int signal);
pid_t start_server() noexcept;
void stop_server(pid_t pid) noexcept;

sig_atomic_t signaled = 0;


/*!
 * \brief
 *
 * \param argc
 * \param argv
 * \return
 */
int main(void)
{
    int status;
    pid_t pid;
    signal(SIGINT, signal_handler);
    /*
     * This should be done gracefully by sending commands to the server, not by
     * killing the server, but I need kill() for the homework requirements
     */
    pid = start_server();
    while (true) {
        if (signaled) {
            stop_server(pid);
            break;
        }
        sleep(1);    // Sleep for a second before checking again
    }
    wait(&status);
    return EXIT_SUCCESS;
}


/*!
 * \brief       Catch SIGINT signals
 *
 * Sets the \code sig_atomic_t signaled \endcode variable to 1 (true) so that
 * the main function will terminate.
 *
 * \param signal Signal caught
 */
void signal_handler(int signal) {
    std::cout << "Signaled - sig(" << signal << ")" << std::endl;
    signaled = 1;
}

/*!
 * \brief       Fork the current process and start the server
 * \return      process ID of the child process
 */
pid_t start_server() noexcept {
    pid_t pid;
    fork();
    pid = getpid();
    std::cout << "Starting server" << std::endl;
    std::cout << "PID: " << pid << std::endl;
    if (pid < 0) {
        std::cerr << "ERROR: Fork failed, exiting" << std::endl;
        exit(EXIT_FAILURE);
    } else if (pid == 0) {
        pid = getppid();
        std::cout << "Parent pid: " << pid << std::endl;
        Server server;
        server.run();
    }
    return pid;
}

/*!
 * \brief       Stop the server process
 * \param pid   Process ID of the child process
 */
void stop_server(const pid_t pid) noexcept {
    kill(pid, SIGTERM);
}


/* ************************************************************************  *
 *                                    EOF                                    *
 * ************************************************************************  */
