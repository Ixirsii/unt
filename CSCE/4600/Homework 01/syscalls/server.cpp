/* ************************************************************************* *
 *                                                                           *
 * Copyright (c) 2016, Ryan Porterfield                                      *
 * All rights reserved.                                                      *
 *                                                                           *
 * Redistribution and use in source and binary forms, with or without        *
 * modification, are permitted provided that the following conditions        *
 * are met:                                                                  *
 *                                                                           *
 *     1. Redistributions of source code must retain the above copyright     *
 *     notice, this list of conditions and the following disclaimer.         *
 *                                                                           *
 *     2. Redistributions in binary form must reproduce the above copyright  *
 *     notice, this list of conditions and the following disclaimer in the   *
 *     documentation and/or other materials provided with the distribution.  *
 *                                                                           *
 *     3. Neither the name of the copyright holder nor the names of its      *
 *     contributors may be used to endorse or promote products derived from  *
 *     this software without specific prior written permission.              *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS   *
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED     *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A           *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT        *
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,    *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED  *
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR    *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF    *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING      *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS        *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.              *
 *                                                                           *
 * ************************************************************************* */

#include "server.h"

#include <cstdlib>
#include <iostream>
#include <string>
#include <thread>

#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>

/*! Length of input buffer */
const size_t Server::BUFF_LEN = 10;
/*! Port the server listens on */
const int Server::PORT = 17926;
/*! How many connections the server can listen for at a time */
const int Server::NUM_CONNECTIONS = 1;
/*! Server command to pause the server */
const std::string Server::PAUSE = "pause";
/*! Server command to resume the server */
const std::string Server::RESUME = "resume";
/*! Server command to stop the server */
const std::string Server::STOP = "stop";



/*!
 * \brief       Default constructor
 */
Server::Server() : paused_(false), running_(true) {
    chdir();
    open_socket();
}

/*!
 *
 */
void Server::chdir() const noexcept {
    close(STDIN_FILENO);                /* Close stdin */
    close(STDOUT_FILENO);               /* Close stdout */
    close(STDERR_FILENO);               /* Close stderr */
    umask(0);                           /* Change file mask */
}

/*!
 *
 */
void Server::open_socket() noexcept {
    struct sockaddr_in server;
    socket_ = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_ < 0) {
        std::cerr << "ERROR: Could not create socket\n";
        exit(EXIT_FAILURE);
    }
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = INADDR_ANY;
    server.sin_port = htons(PORT);
    int status = bind(socket_, (struct sockaddr*) &server,
        sizeof(server));
    if (status < 0) {
        perror ("ERROR: Could not bind port");
        exit (EXIT_FAILURE);
    }
    listen (socket_, NUM_CONNECTIONS);
}

void Server::pause() noexcept {
    paused_ = true;
}

/*!
 *
 */
void Server::process(int client) noexcept {
    std::string buffer;
    buffer.reserve(BUFF_LEN);
    recv(client, (void*) buffer.data(), BUFF_LEN, 0);

    if (buffer == STOP)
        stop();
    else if (buffer == PAUSE)
        pause();
    else if (buffer == RESUME)
        resume();
}

void Server::resume() noexcept {
    paused_ = false;
}

/*!
 * \brief
 */
void Server::run() {
    std::thread update_thread(&Server::update, this);
    while (running_) {
        if (paused_) {
            sleep(1);
            continue;
        }
        int client = wait_for_connect();
        process(client);
    }
    update_thread.join();
}

void Server::stop() noexcept {
    running_ = false;
    paused_ = false;
}

void Server::update() noexcept {
    while (running_) {
        status_bar_.set_status();
        sleep(1);
    }
}

/*!
 * \brief       Listens for connections from clients
 * \return      file descriptor for the connected client socket
 */
int Server::wait_for_connect() const noexcept {
    struct sockaddr_in client;
    size_t c = sizeof(struct sockaddr_in);
    return accept(socket_, (struct sockaddr *) &client, (socklen_t*) &c);
}

/* ************************************************************************* *
 *                                    EOF                                    *
 * ************************************************************************* */
