/* ************************************************************************* *
 *                                                                           *
 * Copyright (c) 2016, Ryan Porterfield                                      *
 * All rights reserved.                                                      *
 *                                                                           *
 * Redistribution and use in source and binary forms, with or without        *
 * modification, are permitted provided that the following conditions        *
 * are met:                                                                  *
 *                                                                           *
 *     1. Redistributions of source code must retain the above copyright     *
 *     notice, this list of conditions and the following disclaimer.         *
 *                                                                           *
 *     2. Redistributions in binary form must reproduce the above copyright  *
 *     notice, this list of conditions and the following disclaimer in the   *
 *     documentation and/or other materials provided with the distribution.  *
 *                                                                           *
 *     3. Neither the name of the copyright holder nor the names of its      *
 *     contributors may be used to endorse or promote products derived from  *
 *     this software without specific prior written permission.              *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS   *
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED     *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A           *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT        *
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,    *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED  *
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR    *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF    *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING      *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS        *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.              *
 *                                                                           *
 * ************************************************************************* */


#ifndef STATUS_BAR_H_
#define STATUS_BAR_H_

#include <chrono>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <memory>
#include <string>

#include "sys/wait.h"

#include "formatter.h"


/*! \class
 * \brief       Builds the status bar output for xsetroot
 *
 * Calls xsetroot with some Pango formatted text to set the DWM status bar.
 */
class StatusBar {
public:
    /*!
     * \brief       Default constructor
     */
    explicit StatusBar() {
    }

    void set_status() {
        pid_t pid;
        std::string status = get_status();
        //std::cout << status << std::endl;

        pid = fork();
        if (pid < 0) {
            std::cerr << "ERROR: Fork failed, exiting" << std::endl;
            exit(EXIT_FAILURE);
        } else if (pid > 0) {
            int status;
            wait(&status);
        } else {
            execl("/usr/bin/xsetroot", "-name", status.data());
        }
    }
private:
    /*!  */
    Formatter formatter;

    /*!
     * \brief       Get the Pango formatted status bar text
     *
     * This program is incomplete because it is too much work to port my
     * entire status bar program over from python, so for now the status bar is
     * just the formatted time string. The full status bar program also
     * displays uptime and battery information.
     *
     * \return      the Pango formatted status bar text
     */
    std::string get_status() {
        return get_time();
    }

    /*!
     * \brief
     */
    std::string get_time() {
        using std::chrono::system_clock;
        static const std::string date_format = "%a %b %e, %Y";
        static const std::string time_format = "%H:%M:%S";
        // len(a) + len(b) + len(e) + len (Y) + # ' ' (& ',')
        char date_str[3 + 3 + 2 + 4 + 5];
        // len(H) + len(M) + len(S) + # ':'
        char time_str[2 + 2 + 2 + 3];
        memset(date_str, 0, 17);
        memset(time_str, 0, 9);

        system_clock::time_point now = system_clock::now();
        time_t now_tt = system_clock::to_time_t(now);
        strftime(date_str, 16, date_format.data(), localtime(&now_tt));
        strftime(time_str, 8, time_format.data(), localtime(&now_tt));
        return get_time_str(date_str, time_str);
    }

    /*!
     * \brief
     */
    std::string get_time_str(const char* date_str, const char* time_str) {
        std::string date_time = std::string(date_str) + " ";
        date_time += formatter.light_white() + std::string(time_str);
        date_time += formatter.end_color();
        return date_time;
    }

    /*!
     * \brief
     */
    std::string get_output(const std::string& cmd) {
        static const size_t len = 128;
        std::shared_ptr<FILE> pipe(popen(cmd.c_str(), "r"), pclose);
        if (not pipe)
            return "";
        char buffer[len];
        std::string output = "";

        while (not feof(pipe.get())) {
            if (fgets(buffer, len, pipe.get()) not_eq NULL)
                output += buffer;
        }
        return output;
    }

};


#endif // STATUS_BAR_H_

/* ************************************************************************* *
 *                                    EOF                                    *
 * ************************************************************************* */
