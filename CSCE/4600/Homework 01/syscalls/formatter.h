/* ************************************************************************* *
 *                                                                           *
 * Copyright (c) 2016, Ryan Porterfield                                      *
 * All rights reserved.                                                      *
 *                                                                           *
 * Redistribution and use in source and binary forms, with or without        *
 * modification, are permitted provided that the following conditions        *
 * are met:                                                                  *
 *                                                                           *
 *     1. Redistributions of source code must retain the above copyright     *
 *     notice, this list of conditions and the following disclaimer.         *
 *                                                                           *
 *     2. Redistributions in binary form must reproduce the above copyright  *
 *     notice, this list of conditions and the following disclaimer in the   *
 *     documentation and/or other materials provided with the distribution.  *
 *                                                                           *
 *     3. Neither the name of the copyright holder nor the names of its      *
 *     contributors may be used to endorse or promote products derived from  *
 *     this software without specific prior written permission.              *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS   *
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED     *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A           *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT        *
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,    *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED  *
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR    *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF    *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING      *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS        *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.              *
 *                                                                           *
 * ************************************************************************* */


#ifndef FORMATTER_H_
#define FORMATTER_H_

#include <string>
#include <unordered_map>


/*! \class
 * \brief       Format text for pango
 * \author      Ryan Porterfield
 * \since       2016-01-30
 */
class Formatter {
public:
    static const std::string BLACK;
    static const std::string LIGHT_BLACK;
    static const std::string RED;
    static const std::string LIGHT_RED;
    static const std::string GREEN;
    static const std::string LIGHT_GREEN;
    static const std::string YELLOW;
    static const std::string LIGHT_YELLOW;
    static const std::string BLUE;
    static const std::string LIGHT_BLUE;
    static const std::string MAGENTA;
    static const std::string LIGHT_MAGENTA;
    static const std::string CYAN;
    static const std::string LIGHT_CYAN;
    static const std::string WHITE;
    static const std::string LIGHT_WHITE;

    explicit Formatter();

    std::string black() const noexcept;
    std::string blue() const noexcept;
    std::string color(const std::string& color_name) const noexcept;
    std::string cyan() const noexcept;
    std::string end_color() const noexcept;
    std::string green() const noexcept;
    std::string light_black() const noexcept;
    std::string light_blue() const noexcept;
    std::string light_cyan() const noexcept;
    std::string light_green() const noexcept;
    std::string light_magenta() const noexcept;
    std::string light_red() const noexcept;
    std::string light_white() const noexcept;
    std::string light_yellow() const noexcept;
    std::string magenta() const noexcept;
    std::string red() const noexcept;
    std::string white() const noexcept;
    std::string yellow() const noexcept;

private:
    /*! Map color names to color codes */
    std::unordered_map<std::string, std::string> color_map_;

    void init_map();
};

#endif // FORMATTER_H_

/* ************************************************************************* *
 *                                    EOF                                    *
 * ************************************************************************* */
