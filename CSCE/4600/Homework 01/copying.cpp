/*
 * Copyright (c) 2016, Ryan Porterfield
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *     1. Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *     3. Neither the name of the copyright holder nor the names of its
 *     contributors may be used to endorse or promote products derived from
 *     this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <chrono>
#include <ciso646>
#include <cstdlib>
#include <cstdio>
#include <fstream>
#include <iostream>
#include <iterator>
#include <string>
#include <vector>

static const std::string VERSION = "1.0";

void printHelp(const std::string& exe) noexcept;
void printVersion(const std::string& exe) noexcept;

/*! \class
 * \brief       Parses command line arguments
 */
class ArgParser {
public:
    /*!
     * \brief       Parse command line arguments.
     * \param argc  Number of command line arguments. Also the length of argv
     * \param argv  C-string array of command line arguments
     */
    ArgParser(int argc, const char** argv) noexcept
        : clibFlag_(false), helpFlag_(false), streamFlag_(false), valid_(false),
          verboseFlag_(false), versionFlag_(false), error_(""),
          inputFileName_(""), outputFileName_("") {
        parse(argc, argv);
    }

    /*!
     * \brief       Default destructor
     */
	virtual ~ArgParser() {
    }

    /*!
     * \brief       Check if the C library option was passed as an argument
     * \return      true if the C library option was passed as an argument,
     *              otherwise false
     */
    bool clibFlag() const noexcept {
        return clibFlag_;
    }

    /*!
     * \brief       Get error message from the parser
     *
     * The parser keeps an internal error string that can be used for
     * debugging, this function returns an immutable reference to the internal
     * error string.
     *
     * \return      error message from the parser
     */
    const std::string& error() const noexcept {
        return error_;
    }

    /*!
     * \brief       Get the name of the executable
     * \return      The name of the executable
     */
    const std::string& executable() const noexcept {
        return executable_;
    }

    /*!
     * \brief       Check if the help flag was passed as an argument
     * \return      true if the help flag (-h or --help) was passed as a
     *              command line argument, otherwise false
     */
    bool helpFlag() const noexcept {
        return helpFlag_;
    }

    /*!
     * \brief       Get the name of the source (input) file
     * \return      the name of the source (input) file
     */
    const std::string& inputFileName() const noexcept {
        return inputFileName_;
    }

    /*!
     * \brief       Check if the program was executed with valid parameters
     *
     * In order for the program to function, it must be provided a source file
     * as well as a destination file. If a file is missing, too many files are
     * provided, or an unknown flag is set, the program execution is not valid.
     *
     * \return      true if the program was executed with valid parameters,
     *              otherwise false
     */
    bool isValid() const noexcept {
        return valid_;
    }

    /*!
     * \brief       Get the name of the destination (output) file
     * \return      the name of the destination (output) file
     */
    const std::string& outputFileName() const noexcept {
        return outputFileName_;
    }

    /*!
     * \brief       Check if the verbose flag was passed as an argument
     * \return      true if the verbose flag (-v --verbose) was passed as a
     *              command line argument
     */
    bool verboseFlag() const noexcept {
        return verboseFlag_;
    }

    /*!
     * \brief       Check if the version flag was passed as an argument
     * \return      true if the version flag (-V --version) was passed as a
     *              command line argument
     */
    bool versionFlag() const noexcept {
        return versionFlag_;
    }

private:
    static const std::string HELP_LONG;
    static const std::string HELP_SHORT;
    static const std::string MODE_CLIB;
    static const std::string MODE_STREAM;
    static const std::string VERBOSE_LONG;
    static const std::string VERBOSE_SHORT;
    static const std::string VERSION_LONG;
    static const std::string VERSION_SHORT;

    /*! Set to true if the program is using the CIODevice */
    bool clibFlag_;
    /*! Set to true if the help flag was passed on execution */
	bool helpFlag_;
    /*! Set to true if the program is using the StreamIODevice */
    bool streamFlag_;
    /*! Set to true if all the necessary arguments were passed on execution */
	bool valid_;
    /*! Set to true if the verbose flag was passed on execution */
	bool verboseFlag_;
    /*! Set to true if the version flag was passed on execution */
	bool versionFlag_;
    /*! Contains any error messages that occur during parsing */
	std::string error_;
    /*! Name of the program executable */
    std::string executable_;
    /*! Name of the source (input) file */
	std::string inputFileName_;
    /*! Name of the destination (output) file */
	std::string outputFileName_;

    /*!
     * \brief       Parse the command line arguments
     * \param argc  Number of command line arguments passed
     * \param argv  Command line arguments
     */
    void parse(int argc, const char** argv) noexcept {
        bool source_set = false, dest_set = false, erroneous_flags = false;
        executable_ = std::string(argv[0]).substr(2);
        for (int i = 1; i < argc; ++i) {
            if (argv[i] == HELP_LONG or argv[i] == HELP_SHORT) {
                helpFlag_ = true;
            } else if (argv[i] == VERBOSE_LONG or argv[i] == VERBOSE_SHORT) {
                verboseFlag_ = true;
            } else if (argv[i] == VERSION_LONG or argv[i] == VERSION_SHORT) {
                versionFlag_ = true;
            } else if (argv[i] == MODE_CLIB) {
                clibFlag_ = true;
            } else if (argv[i] == MODE_STREAM) {
                streamFlag_ = true;
            } else if (not source_set) {
                /*
                 * Because of evaluation order, this will always be set before
                 * dest_set
                 */
                inputFileName_ = argv[i];
                source_set = true;
            } else if (not dest_set) {
                outputFileName_ = argv[i];
                dest_set = true;
            } else {
                erroneous_flags = true;
            }
        }
        valid_ = source_set and dest_set and not erroneous_flags;
        valid_ = valid_ and not (clibFlag_ and streamFlag_);
    }

};


/*! GNU long option argument to print help and exit */
const std::string ArgParser::HELP_LONG = "--help";
/*! POSIX short option argument to print help and exit */
const std::string ArgParser::HELP_SHORT = "-h";
/*! Optional mode argument to run using the C library IODevice */
const std::string ArgParser::MODE_CLIB = "--c-lib";
/*! Optional mode argument to run using the C++ stream IODevice */
const std::string ArgParser::MODE_STREAM = "--cpp-stream";
/*! GNU long option argument to enable verbose output */
const std::string ArgParser::VERBOSE_LONG = "--verbose";
/*! POSIX short option argument to enable verbose output */
const std::string ArgParser::VERBOSE_SHORT = "-v";
/*! GNU long option argument to print the program version and exit */
const std::string ArgParser::VERSION_LONG = "--version";
/*! POSIX short option argument to print the program version and exit */
const std::string ArgParser::VERSION_SHORT = "-V";


/*! \class
 * \brief       Abstract class that defines behavior for file copiers
 *
 * The program needs 2 modes for file copying, one with C++ streams and
 * one with C fread/fwrite function calls. This base class defines behavior so
 * the copy classes can be used polymorphically.
 */
class IODevice {
public:
    /*!
     * \brief       Create an IODevice to copy a file
     * \param in_filename Name of the source file
     * \param out_filename Name of the destination file
     */
    explicit IODevice(std::string in_filename, std::string out_filename)
        : in_filename_(in_filename), out_filename_(out_filename) {
    }

    /*!
     * \brief       Default destructor so the compiler doesn't auto-generate one
     */
    virtual ~IODevice() {
    }

    /*!
     * \brief       Copy the source file to the destination file
     */
    virtual std::chrono::nanoseconds copy() const noexcept = 0;

protected:
    /*! Source file name */
    const std::string in_filename_;
    /*! Destination file name */
    const std::string out_filename_;
};


/*! \class
 * \brief IO stream to copy files using C++'s std::fstream library
 * \sa IODevice
 */
class StreamIODevice : public IODevice {
public:
    /*!
     * \brief       Create a StreamIODevice to copy a file
     * \param in_filename Name of the source file
     * \param out_filename Name of the destination file
     * \sa IODevice::IODevice
     */
    explicit StreamIODevice(std::string in_filename, std::string out_filename)
        : IODevice(in_filename, out_filename) {
    }

    /*!
     * \brief       Default destructor so the compiler doesn't auto-generate one
     */
    virtual ~StreamIODevice() {
    }

    virtual std::chrono::nanoseconds copy() const noexcept {
        using namespace std::chrono;
        nanoseconds avg(0);
        std::ifstream in(in_filename_, std::ifstream::binary);
        std::ofstream out(out_filename_, std::ofstream::binary);
        if (not in)
            return avg;

        char buff;
        while (in) {
            auto start = high_resolution_clock::now();
            // Read the input file
            in >> buff;
            // Write the output file
            out << buff;
            auto time = high_resolution_clock::now() - start;
            nanoseconds ms = duration_cast<nanoseconds>(time);
            if (avg == nanoseconds(0))
                avg = ms;
            avg = (avg + ms) / 2;
        }
        return avg;
    }
};


/*! \class
 * \brief       IO stream to copy files using the C library's fread and fwrite
 * \sa          IODevice
 */
class CIODevice : public IODevice {
public:
    /*!
     * \brief       Create a CIODevice to copy a file
     * \param in_filename Name of the source file
     * \param out_filename Name of the destination file
     * \sa IODevice::IODevice
     */
    explicit CIODevice(std::string in_filename, std::string out_filename)
        : IODevice(in_filename, out_filename) {
    }

    /*!
     * \brief       Default destructor so the compiler doesn't auto-generate one
     */
    virtual ~CIODevice() {
    }

    virtual std::chrono::nanoseconds copy() const noexcept {
        using namespace std::chrono;
        nanoseconds avg(0);
        FILE *in_file = fopen(in_filename_.c_str(), "r");
        if (in_file == NULL) {
            std::cerr << "ERROR: Can't open source file \"" << in_filename_;
            std::cerr << "\"" << std::endl;
            return avg;
        }

        FILE *out_file = fopen(out_filename_.c_str(), "w");
        char buffer;

        // Read source and write destination
        while (not feof(in_file)) {
            auto start = high_resolution_clock::now();
            fread(&buffer, sizeof(char), 1, in_file);
            fwrite(&buffer, sizeof(char), 1, out_file);
            auto time = high_resolution_clock::now() - start;
            nanoseconds ms = duration_cast<nanoseconds>(time);
            if (avg == nanoseconds(0))
                avg = ms;
            avg = (avg + ms) / 2;
        }
        // Clean up
        fclose(in_file);
        fclose(out_file);
        return avg;
    }
};


/*!
 * \brief       Run the program
 *
 * Parse the command line arguments and run the program in the appropriate
 * mode. The default mode is StreamIODevice, but the program can also be run
 * with CIODevice, print help text, or print version information.
 *
 * \param argc  Number of command line arguments
 * \param argv  Command line arguments
 * \return      Execution status
 * \sa          IODevice
 * \sa          StreamIODevice
 * \sa          CIODevice
 */
int main(int argc, const char** argv) {
    ArgParser parser(argc, argv);
	if (not parser.isValid()) {
		std::cout << parser.error() << std::endl;
		printHelp(parser.executable());
		return EXIT_FAILURE;
	} else if (parser.helpFlag()) {
		printHelp(parser.executable());
		return EXIT_SUCCESS;
	} else if (parser.versionFlag()) {
		printVersion(parser.executable());
		return EXIT_SUCCESS;
	}
    std::chrono::nanoseconds avg;
    if (parser.clibFlag())
        avg = CIODevice(parser.inputFileName(), parser.outputFileName()).copy();
    else
        avg = StreamIODevice(parser.inputFileName(), parser.outputFileName()).copy();
    std::cout << "Average read/write time: " << avg.count() << "ns" << std::endl;
    return EXIT_SUCCESS;
}

/*!
 * \brief       Print program help and usage information
 *
 * \param exe   The name of the program executable
 */
void printHelp(const std::string& exe) noexcept {
	std::cout << "Usage: ./" << exe;
    std::cout << " [MODE] [OPTION ...] <source file> <target file>" << std::endl;
	std::cout << "Copies source file to target file" << std::endl << std::endl;
	std::cout << "MODE" << std::endl;
	std::cout << "  If no mode is specified, --cpp-stream is assumed";
    std::cout << std::endl << std::endl;
	std::cout << "  --c-lib\t\t\tRun the program using fread and fwrite";
	std::cout << std::endl;
	std::cout << "  --cpp-stream\t\t\tRun the program using C++ streams";
	std::cout << std::endl << std::endl;
	std::cout << "OPTIONS" << std::endl << std::endl;
	std::cout << "  -h,  --help\t\t\tPrint program usage information";
	std::cout << std::endl;
	std::cout << "  -v,  --verbose\t\tRun the program with verbose output";
	std::cout << std::endl;
	std::cout << "  -V,  --version\t\tPrint program version and copyright";
	std::cout << std::endl;
}

/*!
 * \brief       Print program version and copyright information
 *
 * \param exe   The name of the program executable
 */
void printVersion(const std::string& exe) noexcept {
	std::cout << exe << " " << VERSION << std::endl;
	std::cout << "Copyright (c) 2016, Ryan Porterfield" << std::endl;
	std::cout << "License BSD 3 Clause ";
	std::cout << "<https://opensource.org/licenses/BSD-3-Clause>." << std::endl;
	std::cout << "This is free software: you are free to change and ";
	std::cout << "redistribute it." << std::endl;
	std::cout << "There is NO WARRANTY, to the extent permitted by law.";
	std::cout << std::endl;
}

/* ************************************************************************  *
 *                                    EOF                                    *
 * ************************************************************************  */
