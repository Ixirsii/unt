//include all libraries needed for the program
#include<stdio.h>
#include<stdlib.h>
#include<iostream>
#include<time.h>
#include<iomanip>
using namespace std;

//create a struct that will hold the 3-tuple data for each process
typedef struct
{
        int pid;//the pid for the process
        long cycles;//the number of cycles that the process takes to complete
        long memsize;//the size of the memory footprint of the process
}process;//name the struct process

void rr4(process *, int);

//enter the main function
int main()
{
	//create all variables needed for this program
	int k=0, var=0, newk=0, pcycle[10], pmem[10];
	long long sumc=0, summ=0;

	for(int i=0;i<10;i++)
		pcycle[i]=pmem[i]=0;
	
	//ask the user for k, the nuber of pseudo processes to create
	cout<<"\n\n\nPlease enter the number of processes you'd like to simulate: ";
	cin>>k;//read in k

	//create an array of processes with k elements
	process p[k];//struct process array with k elements
	newk=k;

	//seed the random function based off of time
	srand(time(NULL));

	//if k is odd
	if((k%2)==1)
	{
		p[k-1].cycles=600;//set the last element's (process's) item "cycles" to 6000, the mean
		++pcycle[(p[k-1].cycles/100)-1];
		newk-=1;
	}

	//set the pid and cycles item for each process. run loops k/2 times
	for(int i=0;i<(newk/2);i++)
	{
		//set the pid for the next 2 processes
		p[i].pid=i;
		p[newk-i-1].pid=newk-i-1;

		//create a random number and set var to that number
		var=(rand()%(500+1));
	
		//set the cycles for the next 2 processes
		p[i].cycles=600-var;
		p[newk-i-1].cycles=600+var;

		++pcycle[(p[newk-i-1].cycles/100-1)];
		++pcycle[(p[i].cycles/100-1)];
	}

	newk=k;

	//if k is not evenly divisible by 6, set the remainder processes to 20000, the mean amount for the memory footprint
	switch(k%6)
	{
		case 5:
			newk-=1;
			p[newk].memsize=20000;
			++pmem[1];
		case 4:
			newk-=1;
			p[newk].memsize=20000;
			++pmem[1];
		case 3:
			newk-=1;
			p[newk].memsize=20000;
			++pmem[1];
		case 2:
			newk-=1;
			p[newk].memsize=20000;
			++pmem[1];
		case 1:	
			newk-=1;
			p[newk].memsize=20000;
			++pmem[1];
			break;
		default:
			break;
	}		
	
	//set the memsize item for each process. run k/6 times.
	for(int i=0;i<(newk);i++)
	{
		//create a random number and set var to that number
		var=(rand()%(100000-20000+1));
	
		//set the memsize for the first of 6 processes
		p[i].memsize=var+20000;
		++pmem[(p[i].memsize/10000)];//
	
		//set the memsize for the next 5 processes
		for(int j=1;j<6;j++)
		{
			p[i+j].memsize=((120000-p[i].memsize)/5);
			++pmem[(p[i+j].memsize/10000)];
			
		}
			
		i+=5;
	}
/*	
	//print the title of the distribution
	cout<<"Distribution of the number of cycles the processes take:"<<endl;

	//print the distribution of the number of cycles the processes take
	for(int i=0;i<10;i++)
	{
		cout<<setw(5)<<(i+1)*1000<<"-"<<setw(5)<<(i+2)*1000<<": ";
		cout<<string(pcycle[i],'*')<<endl;
	}

	//print the title of the distribution
	cout<<endl<<endl<<"Distribution of the size of the memory footprints the processes make:"<<endl;;
	
	//print the distribution of the size of the memory footprints the processes take
	for(int i=0;i<10;i++)
	{
		cout<<setw(5)<<(i)*10000+1<<"-"<<setw(6)<<(i+1)*10000<<": ";
                cout<<string(pmem[i],'*')<<endl;
	}
*/

	cout<<"\n\n\n";

        //print out each processes tuple information
        for(int i=0;i<k;i++)
        {
                cout<<"Process "<<i<<": pid= "<<p[i].pid<<endl;
                cout<<"           # of cycles= "<<p[i].cycles<<endl;
                cout<<"           KB of memory= "<<p[i].memsize<<endl;

                sumc+=p[i].cycles;//create sum of cycles
                summ+=p[i].memsize;//create sum of memory footprints
        }

        //print out the mean for the cycles and memsize
        cout<<"Mean of number of cycles = (sum of all process's number of cycles)/(number of processes) = "<<"("<<sumc<<")/("<<k<<") = "<<sumc/k<<endl;
        cout<<"Mean of memory footprint sizes in KB = (sum of all process's memory footprints)/(number of processes) = "<<"("<<summ<<")/("<<k<<") = "<<summ/k<<endl;

	rr4(p, k);
	return 0;
}

void rr4(process *p, int k)
{
	int q=0;//amount of quantum used (max 50)
	int cs=10, cst1=0, cst2=0, cst3=0, cst4=0;//cs is the context switch penalty time, cst is the total cs penalty time
	int g=0, h=0, i=0, j=0;//used for increments in for loops
	int wait[k], twait1=0, twait2=0, twait3=0, twait4=0;//wait[k] holds the total wait time for each process, twait is the total wait time
	int y=0, z=0, p1=0, p2=0, p3=0, p4=0;//counters for the number of processes each processor runs
	int done=0, a=0;//=0 if all processes are finished

	for(i=0;i<k;i++)//initialize all wait to 0
		wait[i]=0;

	i=0;
	
	//execute the following code until flag a=-1
	while(a!=-1)
	{
		for(i=0;i<k;i++)//if there are any processes left to be executed, a=1. else a=-1 and loop will exit
		{	
			if(p[i].cycles>0)
			{
				a=1;
				break;
			}

			else
				a=-1;
		}

		//if there is at least one process left to execute
		if(a==1)
		{
			for(i=0;i<k;i+=8)//processor 1 execute every 8th process
			{	
				for(h=0;h<2;h++)//and 9th process
				{
					if((i+h)<k)//if i+h processes exist
					{
						if(p[i+h].cycles>0)//enter if the current process still needs to be executed
						{
							p[i+h].cycles=p[i+h].cycles-50;//subtract quantum time
							q=50;
	
							if(p[i+h].cycles<0)//if # of cycles left is negative, make it positive
							{
								q=-1*p[i+h].cycles;
								p[i+h].cycles=0;
							}
		
							wait[i+h]+=cs;//increment wait by a cs penalty 
				
							for(j=0;j<k;j+=8)//a process executed. add wait time to other processes waiting
							{
								for(g=0;g<2;g++)
								{
									if((j+g)<k)//if process j+g exists
									{
										if(p[j+g].cycles>0)//if the process still needs to be executed
										{
											if((i+h)!=(j+g))//do not add wait time to the currently executing process
											{						
											wait[j+g]=wait[j+g]+cs+q;//add wait time
											}	
										}
									}
								}
							}
			
							cst1+=cs;//increment total penalty 
						}	
					}
				}	
			}			
	
/*			for(i=0;i<k;i+=8)
				for(j=0;j<2;j++)
					if((i+j)<k)
						cout<<"wait["<<i+j<<"]: "<<wait[i+j]<<endl;
*/
			for(i=2;i<k;i+=8)//processor 2 execute every 8th process
                	{
        	                for(h=0;h<2;h++)//and 9th process
       	        	        {
					if((i+h)<k)//if i+h processes exist
					{
	               	        	        if(p[i+h].cycles>0)//enter if the current process still needs to be executed
        	               	        	{
	                        	                p[i+h].cycles=p[i+h].cycles-50;//subtract quantum time
       		                        	        q=50;
		
       			                                if(p[i+h].cycles<0)//if # of cycles left is negative, make it positive
               			                        {
							        q=-1*p[i+h].cycles;
								p[i+h].cycles=0;
							}

       			                                wait[i+h]+=cs;//increment wait by a cs penalty
	
       			                                for(j=2;j<k;j+=8)//a process executed. add wait time to other processes waiting
               			                        {
                       			                        for(g=0;g<2;g++)
									if((g+h)<k)//if process j+g exists
        	                       			                {
                	                       			                if(p[j+g].cycles>0)//if the process still needs to be executed
                        	                       			        {
	                        	                                	        if((i+h)!=(j+g))
       		                        	                        	                wait[j+g]=wait[j+g]+cs+q;//add wait time
	               		                	                        }
        	               		                	        }
                	               		        }

							cst2+=cs;//increment total penalty
	                	                }	
       		                	}
				}
               		}	
	
			for(i=4;i<k;i+=8)//processor 3 execute every 8th process
                        {
                                for(h=0;h<2;h++)//and 9th process
                                {
					if((i+h)<k)//if i+h processes exist
					{
                	                        if(p[i+h].cycles>0)//enter if the current process still needs to be executed
        	                                {
                	                                p[i+h].cycles=p[i+h].cycles-50;//subtract quantum time
        	                                        q=50;
	
                	                                if(p[i+h].cycles<0)//if # of cycles left is negative, make it positive
							{
        	                                                q=-1*p[i+h].cycles;
								p[i+h].cycles=0;
							}
	
        	                                        wait[i+h]+=cs;//increment wait by cs penalty
	
                        	                        for(j=4;j<k;j+=8)//a process executed. add wait time to other processes waiting
                	                                {
        	                                                for(g=0;g<2;g++)
									if((g+h)<k)//if process j+g exists
		                                                        {
                	                                        	        if(p[j+g].cycles>0)//if the process still needs to be executed
                        	                        	                {
                                	        	                        	if((i+h)!=(j+g))
                                		                                        	wait[j+g]=wait[j+g]+cs+q;//add wait time
                        	                	                        }
                	                                	        }
        	                                        }

							cst3+=cs;//increment total penalty
	                                        }
					}
                                }
                        }

			for(i=6;i<k;i+=8)//processor 4 execute every 8th process
                        {
                                for(h=0;h<2;h++)//and 9th process
                                {
					if((i+h)<k)//if i+h processes exist
					{
	                                        if(p[i+h].cycles>0)//enter if the current process still needs to be executed
        	                                {
	                                                p[i+h].cycles=p[i+h].cycles-50;//subtract quantum time
        	                                        q=50;

                	                                if(p[i+h].cycles<0)//if # of cycles left is negative, make it positive
                        	                                q=-1*p[i+h].cycles;

                                	                wait[i+h]+=cs;//increment wait by a cs penalty
	
        	                                        for(j=6;j<k;j+=8)//a process executed. add wait time to other processes waiting
                	                                {
                        	                                for(g=0;g<2;g++)
									if((g+j)<k)//if process j+g exists
        	                        	                        {
                	                        	                        if(p[j+g].cycles>0)//if the process still needs to be executed
                        	                        	                {
                                	                        		        if((i+h)!=(j+g))
                                        	                                	        wait[j+g]=wait[j+g]+cs+q;//add wait time
	                                        	                        }
        	                                        	        }
                	                                }

							cst4+=cs;//increment total penalty
                        	                }
					}
                                }
                        }
		}
		
		//if no more processes to be executed, a=-1 and loop exits
		else
			a=-1;
	
	}
	
	//decrement cst's by one cs because the last cs does not occur if no process is being executed after it
	cst1-=cs;
	cst2-=cs;
	cst3-=cs;
	cst4-=cs;

        //if cst's are negative (if there are no processes in that processor) make cst 0
	if(cst1<0)
		cst1=0;
        if(cst2<0)
                cst2=0;
        if(cst3<0)
                cst3=0;
        if(cst4<0)
                cst4=0;


	z=0;

        //adjust wait time for each process to account for arrival time
	for(i=0;i<k;i+=8)
		for(j=0;j<2;j++)
			if((i+j)<k)
			{
				wait[i+j]=wait[i+j]-cs-50*z;
				z++;
	
				twait1+=wait[i+j];
			}

	z=0;

        //adjust wait time for each process to account for arrival time
	for(i=2;i<k;i+=8)
                for(j=0;j<2;j++)
                        if((i+j)<k)
                        {
                                wait[i+j]=wait[i+j]-cs-50*z;
                                z++;

                                twait2+=wait[i+j];
                        }

        z=0;

        //adjust wait time for each process to account for arrival time
	for(i=4;i<k;i+=8)
                for(j=0;j<2;j++)
                        if((i+j)<k)
                        {
                                wait[i+j]=wait[i+j]-cs-50*z;
                                z++;

                                twait3+=wait[i+j];
                        }

        z=0;
	
        //adjust wait time for each process to account for arrival time
	for(i=6;i<k;i+=8)
                for(j=0;j<2;j++)
                        if((i+j)<k)
                        {
                                wait[i+j]=wait[i+j]-cs-50*z;
                                z++;

                                twait4+=wait[i+j];
                        }

       //set p's to the number of processes each processor executed.
	p1=p2=p3=p4=(k/8);
	p1*=2;
        p2*=2;
        p3*=2;
        p4*=2;

	z=k/8;
	z=(k-(z*8))%8;

	switch(z)
	{
		case 0:	
			break;
		case 1: p1+=1;
			break;
		case 2: p1+=2;
			break;
		case 3:	p1+=2;
			p2+=1;
			break;
		case 4:	p1+=2;
			p2+=2;
			break;
		case 5:	p1+=2;
			p2+=2;
			p3+=1;
			break;	
		case 6:	p1+=2;
			p2+=2;
			p3+=2;
			break;
		case 7:	p1+=2;
			p2+=2;
			p3+=2;
			p4+=1;
			break;
	}

        //print out the values for the number of processes each processor executed
	cout<<"\n\np1: "<<p1<<endl;
        cout<<"p2: "<<p2<<endl;
        cout<<"p3: "<<p3<<endl;
        cout<<"p4: "<<p4<<endl;

        //print out which processes where executed by which process
        cout<<endl<<"Processor 1 executed processes: "<<endl;

        for(i=0;i<k;i+=8)
		for(j=0;j<2;j++)
               	        cout<<i+j<<",";

        //print out which processes where executed by which process
        cout<<endl<<endl<<"Processor 2 executed processes: "<<endl;

        for(i=2;i<k;i+=8)
               for(j=0;j<2;j++)
               	        cout<<i+j<<",";

        //print out which processes where executed by which process
        cout<<endl<<endl<<"Processor 3 executed processes: "<<endl;

        for(i=4;i<k;i+=8)
               for(j=0;j<2;j++)
               	        cout<<i+j<<",";

        //print out which processes where executed by which process
        cout<<endl<<endl<<"Processor 4 executed processes: "<<endl;

        for(i=6;i<k;i+=8)
               for(j=0;j<2;j++)
               	        cout<<i+j<<",";

	cout<<endl<<endl<<"This list contains the total wait times for each process: "<<endl;

        //print out the total wait time for each process
	for(i=0;i<k;i++)
	{
		cout<<"Process "<<i<<": "<<wait[i]<<endl;
	}
	
        //print out the total cs penalty for each processor
        //print out the average wait time for each processor
	cout<<"\n\nTotal penalty time: "<<cst1<<endl;
	cout<<"Average wait time for 1: "<<(float)twait1/p1<<endl;	

        cout<<"\n\nTotal penalty time: "<<cst2<<endl;
        cout<<"Average wait time for 2: "<<(float)twait2/p2<<endl;

        cout<<"\n\nTotal penalty time: "<<cst3<<endl;
        cout<<"Average wait time for 3: "<<(float)twait3/p3<<endl;

        cout<<"\n\nTotal penalty time: "<<cst4<<endl;
        cout<<"Average wait time for 4: "<<(float)twait4/p4<<endl;

}
