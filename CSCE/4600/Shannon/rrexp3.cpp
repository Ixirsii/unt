//include all libraries needed for the program
#include<stdio.h>
#include<stdlib.h>
#include<iostream>
#include<time.h>
#include<iomanip>
using namespace std;

//create a struct that will hold the 3-tuple data for each process
typedef struct
{
        int pid;//the pid for the process
        long cycles;//the number of cycles that the process takes to complete
        long memsize;//the size of the memory footprint of the process
}process;//name the struct process

void rr(process *, int);

//enter the main function
int main()
{
	//create all variables needed for this program
	int k=0, var=0, newk=0, pcycle[10], pmem[10];
	long long sumc=0, summ=0;

	for(int i=0;i<10;i++)
		pcycle[i]=pmem[i]=0;
	
	//ask the user for k, the nuber of pseudo processes to create
	cout<<"\n\n\nPlease enter the number of processes you'd like to simulate: ";
	cin>>k;//read in k

	//create an array of processes with k elements
	process p[k];//struct process array with k elements
	newk=k;

	//seed the random function based off of time
	srand(time(NULL));

	//if k is odd
	if((k%2)==1)
	{
		p[k-1].cycles=600;//set the last element's (process's) item "cycles" to 6000, the mean
		++pcycle[(p[k-1].cycles/100)-1];
		newk-=1;
	}

	//set the pid and cycles item for each process. run loops k/2 times
	for(int i=0;i<(newk/2);i++)
	{
		//set the pid for the next 2 processes
		p[i].pid=i;
		p[newk-i-1].pid=newk-i-1;

		//create a random number and set var to that number
		var=(rand()%(500+1));
	
		//set the cycles for the next 2 processes
		p[i].cycles=600-var;
		p[newk-i-1].cycles=600+var;

		++pcycle[(p[newk-i-1].cycles/100-1)];
		++pcycle[(p[i].cycles/100-1)];
	}

	newk=k;

	//if k is not evenly divisible by 6, set the remainder processes to 20000, the mean amount for the memory footprint
	switch(k%6)
	{
		case 5:
			newk-=1;
			p[newk].memsize=20000;
			++pmem[1];
		case 4:
			newk-=1;
			p[newk].memsize=20000;
			++pmem[1];
		case 3:
			newk-=1;
			p[newk].memsize=20000;
			++pmem[1];
		case 2:
			newk-=1;
			p[newk].memsize=20000;
			++pmem[1];
		case 1:	
			newk-=1;
			p[newk].memsize=20000;
			++pmem[1];
			break;
		default:
			break;
	}		
	
	//set the memsize item for each process. run k/6 times.
	for(int i=0;i<(newk);i++)
	{
		//create a random number and set var to that number
		var=(rand()%(100000-20000+1));
	
		//set the memsize for the first of 6 processes
		p[i].memsize=var+20000;
		++pmem[(p[i].memsize/10000)];//
	
		//set the memsize for the next 5 processes
		for(int j=1;j<6;j++)
		{
			p[i+j].memsize=((120000-p[i].memsize)/5);
			++pmem[(p[i+j].memsize/10000)];
			
		}
			
		i+=5;
	}
/*	
	//print the title of the distribution
	cout<<"Distribution of the number of cycles the processes take:"<<endl;

	//print the distribution of the number of cycles the processes take
	for(int i=0;i<10;i++)
	{
		cout<<setw(5)<<(i+1)*1000<<"-"<<setw(5)<<(i+2)*1000<<": ";
		cout<<string(pcycle[i],'*')<<endl;
	}

	//print the title of the distribution
	cout<<endl<<endl<<"Distribution of the size of the memory footprints the processes make:"<<endl;;
	
	//print the distribution of the size of the memory footprints the processes take
	for(int i=0;i<10;i++)
	{
		cout<<setw(5)<<(i)*10000+1<<"-"<<setw(6)<<(i+1)*10000<<": ";
                cout<<string(pmem[i],'*')<<endl;
	}
*/
        cout<<"\n\n\n";

        //print out each processes tuple information
        for(int i=0;i<k;i++)
        {
                cout<<"Process "<<i<<": pid= "<<p[i].pid<<endl;
                cout<<"           # of cycles= "<<p[i].cycles<<endl;
                cout<<"           KB of memory= "<<p[i].memsize<<endl;

                sumc+=p[i].cycles;//create sum of cycles
                summ+=p[i].memsize;//create sum of memory footprints
        }

        //print out the mean for the cycles and memsize
        cout<<"Mean of number of cycles = (sum of all process's number of cycles)/(number of processes) = "<<"("<<sumc<<")/("<<k<<") = "<<sumc/k<<endl;
        cout<<"Mean of memory footprint sizes in KB = (sum of all process's memory footprints)/(number of processes) = "<<"("<<summ<<")/("<<k<<") = "<<summ/k<<endl;

	rr(p, k);
	return 0;
}

void rr(process *p, int k)
{
	int q=0;//amount of quantum used (max 50)
	int cs=10, cst=0;//cs is the context switch penalty time, cst is the total cs penalty time
	int i=0, j=0;//used for increments in for loops
	int wait[k], twait=0;//wait[k] holds the total wait time for each process, twait is the total wait time
	int done=0;//=0 if all processes are finished

	for(i=0;i<k;i++)//initialize all wait to 0
		wait[i]=0;

	i=0;

	//execute the following code until flag done<=k
	while(done<=k)
	{
		for(i=0;i<k;i++)//execute loop k times
		{
			if(p[i].cycles>0)//enter the following if the process still needs to be executed
			{
				p[i].cycles=p[i].cycles-50;//subtract quantum
				q=50;
	
				if(p[i].cycles<0)//if # of cycles left is negative, set q= to its positive
					q=-1*p[i].cycles;

				wait[i]+=cs;//increment wait by a cs penalty

				for(j=0;j<k;j++)//a process executed. add wait time to other processes waiting
				{
					if(p[j].cycles>0)//if process has more to execute, add wait 
					{
						if(i!=j)
							wait[j]=wait[j]+cs+q;//add wait time
					}
				}
				
				cst+=cs;//increment total penalty
				done=0;//reset done counter
			}
		}
		done++;//increment done
	}
	
        //decrement cst's by one cs because the last cs does not occur if no process is being executed after it
	cst-=cs;
	
        //adjust wait time for each process to account for arrival time
	for(i=0;i<k;i++)
	{
		wait[i]=wait[i]-cs-50*i;
		
		if(wait[i]<0)
			wait[i]=0;

		twait+=wait[i];
	}

        cout<<endl<<endl<<"This list contains the total wait times for each process: "<<endl;

        //print out the total wait time for each process
        for(i=0;i<k;i++)
        {
                cout<<"Process "<<i<<": "<<wait[i]<<endl;
        }

        //print out the total cs penalty and wait time
	cout<<"\n\nTotal penalty time: "<<cst<<endl;
	cout<<"Average wait time: "<<(float)twait/k<<endl;	
}
