//include all libraries needed for the program
#include<stdio.h>
#include<stdlib.h>
#include<iostream>
#include<time.h>
#include<iomanip>
using namespace std;

//create a struct that will hold the 3-tuple data for each process
typedef struct
{
        int pid;//the pid for the process
        long cycles;//the number of cycles that the process takes to complete
        long memsize;//the size of the memory footprint of the process
}process;//name the struct process

void sjf4(process *,int);

//enter the main function
int main()
{
	//create all variables needed for this program
	int k=0, var=0, newk=0, pcycle[10], pmem[10];
	long long sumc=0, summ=0;

	for(int i=0;i<10;i++)
		pcycle[i]=pmem[i]=0;
	
	//ask the user for k, the nuber of pseudo processes to create
	cout<<"\n\n\nPlease enter the number of processes you'd like to simulate: ";
	cin>>k;//read in k

	//create an array of processes with k elements
	process p[k];//struct process array with k elements
	newk=k;

	//seed the random function based off of time
	srand(time(NULL));

	//if k is odd
	if((k%2)==1)
	{
		p[k-1].cycles=6000;//set the last element's (process's) item "cycles" to 6000, the mean
		++pcycle[(p[k-1].cycles/1000)-1];
		newk-=1;
	}

	//set the pid and cycles item for each process. run loops k/2 times
	for(int i=0;i<(newk/2);i++)
	{
		//set the pid for the next 2 processes
		p[i].pid=i;
		p[newk-i-1].pid=newk-i-1;

		//create a random number and set var to that number
		var=(rand()%(5000+1));
	
		//set the cycles for the next 2 processes
		p[i].cycles=6000-var;
		p[newk-i-1].cycles=6000+var;

		++pcycle[(p[newk-i-1].cycles/1000-1)];
		++pcycle[(p[i].cycles/1000-1)];
	}

	newk=k;

	//if k is not evenly divisible by 6, set the remainder processes to 20000, the mean amount for the memory footprint
	switch(k%6)
	{
		case 5:
			newk-=1;
			p[newk].memsize=20000;
			++pmem[1];
		case 4:
			newk-=1;
			p[newk].memsize=20000;
			++pmem[1];
		case 3:
			newk-=1;
			p[newk].memsize=20000;
			++pmem[1];
		case 2:
			newk-=1;
			p[newk].memsize=20000;
			++pmem[1];
		case 1:	
			newk-=1;
			p[newk].memsize=20000;
			++pmem[1];
			break;
		default:
			break;
	}		
	
	//set the memsize item for each process. run k/6 times.
	for(int i=0;i<(newk);i++)
	{
		//create a random number and set var to that number
		var=(rand()%(100000-20000+1));
	
		//set the memsize for the first of 6 processes
		p[i].memsize=var+20000;
		++pmem[(p[i].memsize/10000)];//
	
		//set the memsize for the next 5 processes
		for(int j=1;j<6;j++)
		{
			p[i+j].memsize=((120000-p[i].memsize)/5);
			++pmem[(p[i+j].memsize/10000)];
			
		}
			
		i+=5;
	}
	
/*	//print the title of the distribution
	cout<<"Distribution of the number of cycles the processes take:"<<endl;

	//print the distribution of the number of cycles the processes take
	for(int i=0;i<10;i++)
	{
		cout<<setw(5)<<(i+1)*1000<<"-"<<setw(5)<<(i+2)*1000<<": ";
		cout<<string(pcycle[i],'*')<<endl;
	}

	//print the title of the distribution
	cout<<endl<<endl<<"Distribution of the size of the memory footprints the processes make:"<<endl;;
	
	//print the distribution of the size of the memory footprints the processes take
	for(int i=0;i<10;i++)
	{
		cout<<setw(5)<<(i)*10000+1<<"-"<<setw(6)<<(i+1)*10000<<": ";
                cout<<string(pmem[i],'*')<<endl;
	}
*/
	cout<<"\n\n\n";

        //print out each processes tuple information
        for(int i=0;i<k;i++)
        {
                cout<<"Process "<<i<<": pid= "<<p[i].pid<<endl;
                cout<<"           # of cycles= "<<p[i].cycles<<endl;
                cout<<"           KB of memory= "<<p[i].memsize<<endl;

                sumc+=p[i].cycles;//create sum of cycles
                summ+=p[i].memsize;//create sum of memory footprints
        }

        //print out the mean for the cycles and memsize
        cout<<"Mean of number of cycles = (sum of all process's number of cycles)/(number of processes) = "<<"("<<sumc<<")/("<<k<<") = "<<sumc/k<<endl;
        cout<<"Mean of memory footprint sizes in KB = (sum of all process's memory footprints)/(number of processes) = "<<"("<<summ<<")/("<<k<<") = "<<summ/k<<endl;

	sjf4(p, k);

	return 0;
}

void sjf4(process *p, int k)
{
	int tc1=0, tc2=0, tc3=0, tc4=0;//total number of cycles that have occured
	int cs=50, cst1=0, cst2=0, cst3=0, cst4=0;//cs is the context switch penalty time, cst is the total cs penalty time
	int temp=0;
	int chosen1=0, chosen2=0, chosen3=0, chosen4=0;//chosen=chosen process to go next
	int h=0, i=0, j=0, a=0;//used for increments in the for loops
	int wait[k], twait1=0, twait2=0, twait3=0, twait4=0;//wait[k] holds the total wait time for each process, twait is the total wait time
	int p1=0, p2=0, p3=0, p4=0;

	for(i=0;i<k;i++)//initialize wait to 0
		wait[i]=0;

	//execute the following code until flag a=-1
	while(a!=-1)
	{
		for(i=0;i<k;i++)//if there are any processes left to be executed, a=1. else a=-1 and loop will exit
		{
			if(p[i].cycles>0)
			{
				a=1;
				break;
			}	
			
			else				
				a=-1;
		}

                //if there is at least one process left to execute
		if(a==1)
		{
			if(tc1==0)//if no process has been executed yet
				chosen1=0;

                        else//if at least one process has been executed
                        {
                                temp=11000;//set temp to max+1 # of cycles

                                h=tc1/50;//h= max number of processes that have arrived at that time 

                                for(j=0;j<=h;j+=8)//processor 1 executes every 8th process
					for(i=0;i<2;i++)//and 9th process
	                                {
						if((i+j)<k)//if the i+j process exists
						{
        	                	                if(p[j+i].cycles<temp&&p[j+i].cycles>0)//enter if the current process still needs to be executed and find smallest job out of reachable proceses
                		                        {
								if(temp==11000)
									cst1+=cs;//increment wait by cs penalty 1 time per new process execution

                	        	                        temp=p[j+i].cycles;//set temp to new lowest process job
        	                        	                chosen1=j+i;//set chosen to lowest process job location
		                                        }
						}
        	                        }
                        }

			if(tc2==0)//if no process has been executed yet
                                chosen2=2;

                        else//if at least one process has been executed
                        {
                                temp=11000;//set temp to max+1 # of cycles

                                h=tc2/50;//h= max number of processes that have arrived at that time

                                for(j=2;j<=h;j+=8)//processor 1 executes every 8th process
					for(i=0;i<2;i++)//and 9th process
	                                {
						if((i+j)<k)//if the i+j process exists
						{
		                                        if(p[j+i].cycles<temp&&p[j+i].cycles>0)//enter if the current process still needs to be executed and find smallest job out of reachable proceses
        		                                {
								if(temp==11000)
									cst2+=cs;//increment wait by cs penalty 1 time per new process execution

                		                                temp=p[j+i].cycles;//set temp to new lowest process job
                        		                        chosen2=j+i;//set chosen to lowest process job location
                                		        }
						}
	                                }
                        }

                        if(tc3==0)//if no process has been executed yet
                                chosen3=4;

                        else//if at least one process has been executed
                        {
                                temp=11000;//set temp to max+1 # of cycles

                                h=tc3/50;//h= max number of processes that have arrived at that time

                                for(j=4;j<=h;j+=8)//processor 1 executes every 8th process
					for(i=0;i<2;i++)//and 9th process
	                                {
						if((i+j)<k)//if the i+j process exists
						{
        		                                if(p[j+i].cycles<temp&&p[j+i].cycles>0)//enter if the current process still needs to be executed and find smallest job out of reachable proceses
                		                        {
								if(temp==11000)
									cst3+=cs;//increment wait by cs penalty 1 time per new process execution

                        		                        temp=p[j+i].cycles;//set temp to new lowest process job
                                		                chosen3=j+i;//set chosen to lowest process job location
                                        		}
						}
	                                }
                        }

                        if(tc4==0)//if no process has been executed yet
                                chosen4=6;

			else//if at least one process has been executed
			{
				temp=11000;//set temp to max+1 # of cycles
	
				h=tc4/50;//h= max number of processes that have arrived at that time
	
				for(j=6;j<=h;j+=8)//processor 1 executes every 8th process
					for(i=0;i<2;i++)//and 9th process
					{
						if((i+j)<k)//if the i+j process exists
						{
							if(p[j+i].cycles<temp&&p[j+i].cycles>0)//enter if the current process still needs to be executed and find smallest job out of reachable proceses
							{
								if(temp==11000)
									cst4+=cs;//increment wait by cs penalty 1 time per new process execution

								temp=p[j].cycles;//set temp to new lowest process job
								chosen4=j+i;//set chosen to lowest process job location
							}
						}
					}
			}

			//add executed # of cycles and cs penalty to total # of cycles
			tc1+=p[chosen1].cycles+cs;
                        tc2+=p[chosen2].cycles+cs;
                        tc3+=p[chosen3].cycles+cs;
                        tc4+=p[chosen4].cycles+cs;

			//if current process needs to be executed
			if(p[chosen1].cycles>0)
				for(i=0;i<k;i+=8)
					for(j=0;j<2;j++)
						if((i+j)<k)
						{
							if((i+j)!=chosen1&&p[i+j].cycles>0)//add wait time to all other processes on processor1
								wait[i+j]=wait[i+j]+cs+p[chosen1].cycles;
						}

			//if current process needs to be executed
			if(p[chosen2].cycles>0)
	                        for(i=2;i<k;i+=8)
        	                        for(j=0;j<2;j++)
						if((i+j)<k)
	                	                {
        	                	                if((i+j)!=chosen2&&p[i+j].cycles>0)//add wait time to all other processes on processor2
                	                	                wait[i+j]=wait[i+j]+cs+p[chosen2].cycles;
	                        	        }
				
			//if current process needs to be executed
			if(p[chosen3].cycles>0)	
	                        for(i=4;i<k;i+=8)
		                       	for(j=0;j<2;j++)
						if((i+j)<k)
        	        	                {
                	        	                if((i+j)!=chosen3&&p[i+j].cycles>0)//add wait time to all other processes on processor3
                        	        	                wait[i+j]=wait[i+j]+cs+p[chosen3].cycles;
	                                	}
			
			//if current process needs to be executed
			if(p[chosen4].cycles>0)	
        	                for(i=6;i<k;i+=8)
	        	                for(j=0;j<2;j++)
						if((i+j)<k)
        	                	        {
                	                	        if((i+j)!=chosen4&&p[i+j].cycles>0)//add wait time to all other processes on processor4
	                        	                        wait[i+j]=wait[i+j]+cs+p[chosen4].cycles;
        	                        	}
	
			//set current process to 0. it has been executed to completion
			p[chosen1].cycles=0;
                        p[chosen2].cycles=0;
                       	p[chosen3].cycles=0;
                        p[chosen4].cycles=0;
		}

		//if there are no processes left to be executed, set a=-1 which will exit the loop
		else
			a=-1;
	}
	
        //if cst's are negative (if there are no processes in that processor) make cst 0
	if(cst1<0)
		cst1=0;

        if(cst2<0)
                cst2=0;

        if(cst3<0)
                cst3=0;

        if(cst4<0)
                cst4=0;
	
	h=0;

        //adjust wait time for each process to account for arrival time
	for(i=0;i<k;i+=8)
		for(j=0;j<2;j++)
			if((i+j)<k)
	       		{
		        	wait[i+j]=wait[i+j]-50*h;
				h++;

		                if(wait[i+j]<0)
              			        wait[i+j]=0;

		                twait1+=wait[i+j];
		        }

        h=0;

        //adjust wait time for each process to account for arrival time
        for(i=2;i<k;i+=8)
                for(j=0;j<2;j++)
                        if((i+j)<k)
                        {
                                wait[i+j]=wait[i+j]-50*h;
                                h++;

                                if(wait[i+j]<0)
                                        wait[i+j]=0;

                                twait2+=wait[i+j];
                        }

        h=0;

        //adjust wait time for each process to account for arrival time
        for(i=4;i<k;i+=8)
                for(j=0;j<2;j++)
                        if((i+j)<k)
                        {
                                wait[i+j]=wait[i+j]-50*h;
                                h++;

                                if(wait[i+j]<0)
                                        wait[i+j]=0;

                                twait3+=wait[i+j];
                        }

        h=0;

        //adjust wait time for each process to account for arrival time
        for(i=6;i<k;i+=8)
                for(j=0;j<2;j++)
                        if((i+j)<k)
                        {
                                wait[i+j]=wait[i+j]-50*h;
                                h++;

                                if(wait[i+j]<0)
                                        wait[i+j]=0;

                                twait4+=wait[i+j];
                        }

	cout<<endl;

       //set p's to the number of processes each processor executed.
	p1=p2=p3=p4=(k/8);
        p1*=2;
        p2*=2;
        p3*=2;
        p4*=2;

        h=k/8;
        h=(k-(h*8))%8;

        switch(h)
        {
                case 0:
                        break;
                case 1: p1+=1;
                        break;
                case 2: p1+=2;
                        break;
                case 3: p1+=2;
                        p2+=1;
                        break;
                case 4: p1+=2;
                        p2+=2;
                        break;
                case 5: p1+=2;
                        p2+=2;
                        p3+=1;
                        break;
                case 6: p1+=2;
                        p2+=2;
                        p3+=2;
                        break;
                case 7: p1+=2;
                        p2+=2;
                        p3+=2;
                        p4+=1;
			break;
        }

	//print out the values for the number of processes each processor executed
        cout<<"\n\np1: "<<p1<<endl;
        cout<<"p2: "<<p2<<endl;
        cout<<"p3: "<<p3<<endl;
        cout<<"p4: "<<p4<<endl;

        //print out which processes where executed by which process
        cout<<endl<<"Processor 1 executed processes: "<<endl;

        for(i=0;i<k;i+=8)
                for(j=0;j<2;j++)
                        cout<<i+j<<",";

        //print out which processes where executed by which process
        cout<<endl<<endl<<"Processor 2 executed processes: "<<endl;

        for(i=2;i<k;i+=8)
               for(j=0;j<2;j++)
                        cout<<i+j<<",";

        //print out which processes where executed by which process
        cout<<endl<<endl<<"Processor 3 executed processes: "<<endl;

        for(i=4;i<k;i+=8)
               for(j=0;j<2;j++)
                        cout<<i+j<<",";

        //print out which processes where executed by which process
        cout<<endl<<endl<<"Processor 4 executed processes: "<<endl;

        for(i=6;i<k;i+=8)
               for(j=0;j<2;j++)
                        cout<<i+j<<",";

	cout<<endl<<endl<<"This list contains the total wait times for each process: "<<endl;
	
	for(i=0;i<k;i++)
		cout<<"Process "<<i<<": "<<wait[i]<<endl;

        //print out the total cs penalty for each processor
        //print out the average wait time for each processor
	cout<<"\n\nTotal penalty time for processor 1: "<<cst1<<endl;
        cout<<"Average wait time for processor 1: "<<(float)twait1/p1<<endl;

	cout<<"\nTotal penalty time for processor 2: "<<cst2<<endl;
        cout<<"Average wait time for processor 2: "<<(float)twait2/p2<<endl;

	cout<<"\nTotal penalty time for processor 3: "<<cst3<<endl;
        cout<<"Average wait time for processor 3: "<<(float)twait3/p3<<endl;

	cout<<"\nTotal penalty time for processor 4: "<<cst4<<endl;
        cout<<"Average wait time for processor 4: "<<(float)twait4/p4<<endl;


}
