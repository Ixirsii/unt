/*
 * Copyright (c) 2016, Erika Gutierrez Portillo, Ryan Porterfield, Shannon Hart
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *     1. Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *     3. Neither the name of the copyright holder nor the names of its
 *     contributors may be used to endorse or promote products derived from
 *     this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SYSTEM_ALLOCATOR_H_
#define SYSTEM_ALLOCATOR_H_

#include <cstddef>
#include "allocator.h"
#include "process.h"

/*! \class    SystemAllocator system_allocator.h "system_allocator.h"
 *  \brief    Allocator wrapper for malloc and free
 *
 *  SystemAllocator uses calls to malloc and free to let the operating system
 *  allocate and deallocate memory for each process.
 */
class SystemAllocator : public Allocator {
public:
  explicit SystemAllocator(const size_t max_memory) noexcept;
  //virtual ~SystemAllocator() noexcept = default;

  virtual void* allocate(const std::size_t memory) noexcept;
  virtual void deallocate(void* ptr, std::size_t size) noexcept;

private:
  std::size_t allocated_memory_;

  bool allocateMemory(const std::size_t memory) noexcept;
  void deallocateMemory(const std::size_t memory) noexcept;
};

#endif // SYSTEM_ALLOCATOR_H_

/* ************************************************************************  *
 *                                    EOF                                    *
 * ************************************************************************  */
