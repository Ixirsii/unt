/*
 * Copyright (c) 2016, Erika Gutierrez Portillo, Ryan Porterfield, Shannon Hart
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *     1. Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *     3. Neither the name of the copyright holder nor the names of its
 *     contributors may be used to endorse or promote products derived from
 *     this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "block_allocator.h"
#include <cstddef>
#include <cstdlib>
#include <iostream>
#include "allocator.h"

/*!
 *  \brief    Default constructor
 *  \param max_memory The maximum amount of memory available to the allocator
 *  to allocate to processes
 */
BlockAllocator::BlockAllocator(const std::size_t max_memory) noexcept
    : Allocator(max_memory), block_(malloc(max_memory)) {
  // Insert a start-end pair corresponding to the entire block
  std::pair<void*, void*> p(block_, (char*) block_ + max_memory);
  holes_.insert(p);
}

/*!
 *  \brief    Default destructor, frees block_
 */
BlockAllocator::~BlockAllocator() noexcept {
  free(block_);
}

/*!
 *  \brief    Allocate a block of memory for a process
 *
 *  Uses the worst fit algorithm to find the largest block of free memory and
 *  allocates some of it.
 *
 *  \param memory How much memory needs to be allocated
 *  \return   a pointer to the start of the memory block if allocation was
 *            successful, otherwise NULL
 *  \sa       BlockAllocator::worstFit
 */
void* BlockAllocator::allocate(const std::size_t memory) noexcept {
  return my_malloc(memory);
}

/*!
 *  \brief    Deallocate (free) memory
 *
 *  If there is no available memory immediately before or after the block of
 *  memory being deallocated, then new_start and new_end will be equal to the
 *  start and end of the allocated memory of process, and a new hole will be
 *  opened. However if there is memory immediately before or after, new_start
 *  or new_end will be set to the start or end value of the free memory
 *  respectively, and the existing hole will grow.
 *
 *  \param process A process with allocated memory that needs to be
 *            deallocated
 */
void BlockAllocator::deallocate(void* ptr, std::size_t size)
    noexcept {
  my_free(ptr, size);
}

void BlockAllocator::my_free(void* ptr, std::size_t size) noexcept {
  void* block_end = (char*) ptr + size;
  void* new_end = block_end;
  void* new_start = ptr;
  std::unordered_map<void*, void*>::iterator iter;
  for (iter = holes_.begin(); iter not_eq holes_.end(); ) {
    bool flag = true;
    // If the hole is immediately after the freed block
    if (iter->first == (char*) new_end + 1) {
      new_end = iter->second;
      flag = false;
    }
    // If the hole is immediately before the freed block
    if (iter->second == (char*) new_start - 1) {
      new_start = iter->first;
      flag = false;
    }
    std::pair<void*, void*> p(new_start, new_end);
    holes_.insert(p);
    if (flag)
      ++iter;
    else
      iter = holes_.erase(iter);
  }
}

void* BlockAllocator::my_malloc(const std::size_t memory) noexcept {
  void* block_start = worstFit(memory);
  if (block_start == NULL)
    return NULL;
  void* block_end = (char*) block_start + memory + 1;
  void* hole_end = holes_.at(block_start);
  holes_.erase(block_start);
  std::pair<void*, void*> p(block_end, hole_end);
  holes_.insert(p);
  return block_start;
}

/*!
 *  \brief    Find the largest block of available memory which can be allocated
 *
 *  Search through the available memory looking for the largest hole that is
 *  larger than the memory that needs to be allocated. If a hole is large
 *  enough for the allocation then the largest block of available memory will
 *  be returned. However, if we just returned the largest block it might not be
 *  large enough for the allocation.
 *
 *  \param memory How much memory needs to be allocated
 *  \return   a pointer to the start of the memory block if a block large
 *            enough was found, otherwise NULL
 */
void* BlockAllocator::worstFit(std::size_t memory) {
  std::size_t worst_size = 0;
  void* worst_fit = NULL;
  std::unordered_map<void*, void*>::iterator iter;
  for (iter = holes_.begin(); iter not_eq holes_.end(); ++iter) {
    std::size_t size = (char*) (iter->second) - (char*) (iter->first);
    if ((size > memory) and (size > worst_size)) {
      worst_size = size;
      worst_fit = iter->first;
    }
  }
  //std::cout << "Found worst fit of <" << worst_fit << ", " << worst_size << ">";
  //std::cout << std::endl;
  return worst_fit;
}

/* ************************************************************************  *
 *                                    EOF                                    *
 * ************************************************************************  */
