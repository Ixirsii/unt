/*
 * Copyright (c) 2016, Erika Gutierrez Portillo, Ryan Porterfield
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *     1. Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *     3. Neither the name of the copyright holder nor the names of its
 *     contributors may be used to endorse or promote products derived from
 *     this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "processor.h"
#include <ciso646>
#include <iostream>
#include <vector>
#include "process.h"

/*!
 *  \brief    Default constructor
 */
Processor::Processor() noexcept : avg_wait_(0), current_time_(0), penalty_(0) {
}

/*!
 *  \brief    Get the average wait time
 *  \return   the average wait time
 */
unsigned int Processor::getAvgWaitTime() const noexcept {
  return avg_wait_;
}

/*!
 *  \brief    Get the time it will take to finish all scheduled jobs
 *  \return   the time it will take to finish all scheduled jobs
 */
unsigned int Processor::getCurrentTime() const noexcept {
  return current_time_;
}

/*!
 *  \brief    Get the total context switch penalty
 *  \return   the total context switch penalty
 */
unsigned int Processor::getPenalty() const noexcept {
  return penalty_;
}

/*!
 *  \brief    Run a process for a set amount of time (quantum)
 *
 *  Certain scheduling algorithms such as Round Robin run a process for a fixed
 *  amount of time (a quantum), then perform a context switch and run a
 *  different process, even if the first process did not complete.
 */
void Processor::quantum(Process& process) noexcept {
  int rtime = process.burst(QUANTUM);
  // No penalty for starting the first job
  if (current_time_ > 0) {
    penalty_ += Process::PENALTY;
    current_time_ += Process::PENALTY;
  }
  if (process.getArrivalTime() > current_time_)
    current_time_ = process.getArrivalTime();
  if (process.complete()) {
    int wait_time = current_time_ - process.getArrivalTime();
    avg_wait_ = (avg_wait_ + wait_time) / 2;
  }
  current_time_ += QUANTUM - rtime;
}

/*!
 *  \brief    Run a scheduled Process
 *
 *  The current time will be updated to the current time plus the process's
 *  burst time, and average wait time and penalty are updated accordingly
 *
 *  \param process A scheduled Process which will be "completed"
 */
void Processor::run(Process& process) noexcept {
  int wait_time;
  // No penalty for starting the first job
  if (current_time_ > 0) {
    penalty_ += Process::PENALTY;
    current_time_ += Process::PENALTY;
  }
  // Wait until the process arrives
  if (process.getArrivalTime() > current_time_)
    current_time_ = process.getArrivalTime();
  wait_time = current_time_ - process.getArrivalTime();
  process.setWaitTime(wait_time);
  avg_wait_ = (avg_wait_ + wait_time) / 2;
  current_time_ += process.getTotalBurstTime();
  // Mark the process as complete - not necessary now but may be needed later
  process.burst(process.getTotalBurstTime());
}

/* ************************************************************************* *
 *                                    EOF                                    *
 * ************************************************************************* */
