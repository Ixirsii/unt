/*
 * Copyright (c) 2016, Erika Gutierrez Portillo, Ryan Porterfield
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *     1. Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *     3. Neither the name of the copyright holder nor the names of its
 *     contributors may be used to endorse or promote products derived from
 *     this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <chrono>
#include <ciso646>
#include <cstdlib>
#include <iostream>
#include <string>
#include "block_allocator.h"
#include "fifo.h"
#include "process.h"
#include "rr.h"
#include "scheduler.h"
#include "sjf.h"
#include "system_allocator.h"

bool contains(const std::string& str, const std::string& end);
void customMemory(const float mem_percent);
bool endsWith(const std::string& str, const std::string& end);
void fifo(const int num_processors);
void menu();
void parseMemory(const std::string command);
void parseSchedule(const std::string command);
void printMenu();
void printMemoryMenu();
void printSchedulerMenu();
void printStatistics();
void sjf(const int num_processors);
bool startsWith(const std::string& str, const std::string& start);
void systemMemory(const float mem_percent);
void rr(const int num_processors);

/*!
 *  \brief    Check if a string ends with another string
 *  \param str The base string that may or may not end with the substring
 *  \param end The substring that may or may not be the end of the base string
 *  \return   true if str ends with end, otherwise false
 */
bool contains(const std::string& str, const std::string& substr) {
  std::size_t found = str.find(substr);
  return found not_eq std::string::npos;
}

/*!
 *  \brief    Run a set of processes with the custom memory scheduler
 *  \param mem_percent The percentage of total required memory that is
 *            available to the system. NOTE that if mem_percent is 1 (100%)
 *            then the total allocated memory will be 20MiB instead of a strict
 *            100% of needed memory.
 */
void customMemory(const float mem_percent) {
  using namespace std::chrono;
  // This is 20MiB
  // MiB (mebibyte) is the IEC replacement for a megabyte
  std::size_t memory = 20 * (1 << 20);
  std::vector<Process> processes = Scheduler::generate(Process::PROCESSES);
  if (mem_percent not_eq 1.0)
    memory = Process::getMemoryPercentage(processes, mem_percent);
  std::cout << "Maximum memory: " << memory << std::endl;
  BlockAllocator allocator(memory);
  // Time scheduling
  auto start = high_resolution_clock::now();
  FIFOScheduler(1, allocator, processes).run();
  auto time = high_resolution_clock::now() - start;
  nanoseconds ms = duration_cast<nanoseconds>(time);
  std::cout << "Time taken: " << ms.count() << "ns" << std::endl << std::endl;
}

/*!
 *  \brief    Check if a string ends with another string
 *  \param str The base string that may or may not end with the substring
 *  \param end The substring that may or may not be the end of the base string
 *  \return   true if str ends with end, otherwise false
 */
bool endsWith(const std::string& str, const std::string& end) {
  if (end.length() > str.length())
    return false;
  int cmp = str.compare(str.length() - end.length(), end.length(), end);
  return 0 == cmp;
}

/*!
 *  \brief    Schedule processes with the FIFO scheduler
 */
void fifo(const int num_processors) {
  std::size_t memory = 20 * (1 << 20);
  SystemAllocator allocator(memory);
  FIFOScheduler(num_processors, allocator).run();
}

/*!
 *  \brief    Main program loop
 *
 *  Run the program in a loop until the user enters the 'quit' function.
 *  Get the user's command, parse it, and execute the appropriate command or
 *  print an error message if the command is invalid.
 */
void menu()  {
  std::string command;
  do {
    printMenu();
    //enter the command to execute
    std::cout << "scheduler >>> ";
    std::getline(std::cin, command);

    if (command == "median")
      printStatistics();
    else if (contains(command, "memory"))
      parseMemory(command);
    else if (contains(command, "scheduler"))
      parseSchedule(command);
    else if (command not_eq "quit")
      std::cout << "Error!" << std::endl;
  }
    while (command not_eq "quit");
}

/*!
 *  \brief    Parse memory manager commands
 *  \param command The command passed by the user
 */
void parseMemory(const std::string command) {
  float mem_percent = 1.0;
  if (endsWith(command, "50"))
    mem_percent = 0.5;
  else if (endsWith(command, "10"))
    mem_percent = 0.1;
  if (contains(command, "system"))
    systemMemory(mem_percent);
  else if (contains(command, "custom"))
    customMemory(mem_percent);
}

/*!
 *  \brief    Parse memory scheduler commands
 *  \param command The command passed by the user
 */
void parseSchedule(const std::string command) {
  if (endsWith(command, "FIFO"))
    fifo(1);
  else if (endsWith(command, "multi FIFO"))
    fifo(4);
  else if (endsWith(command, "SJF"))
    sjf(1);
  else if (endsWith(command, "multi SJF"))
    sjf(4);
  else if (endsWith(command, "RR"))
    rr(1);
  else if (endsWith(command, "multi RR"))
    rr(4);
}

/*!
 *  \brief    Print the menu of commands
 */
void printMenu() {
  std::cout << "<Scheduler Process Simulator> " << std::endl;
  std::cout << "Enter the Following Options: " << std::endl;
  //printSchedulerMenu();
  printMemoryMenu();
  std::cout << "quit" << std::endl;
}

/*!
 *  \brief    Print the menu of commands for the memory allocator
 */
void printMemoryMenu() {
  std::cout << "custom memory" << std::endl;
  std::cout << "custom memory 50" << std::endl;
  std::cout << "custom memory 10" << std::endl;
  std::cout << "system memory" << std::endl;
  std::cout << "system memory 50" << std::endl;
  std::cout << "system memory 10" << std::endl;
}

/*!
 *  \brief    Print the menu of commands for the process scheduler
 */
void printSchedulerMenu() {
  std::cout << "FIFO scheduler" << std::endl;
  std::cout << "multi FIFO scheduler" << std::endl;
  std::cout << "SJF scheduler" << std::endl;
  std::cout << "multi SJF scheduler" << std::endl;
  std::cout << "RR scheduler" << std::endl;
  std::cout << "multi RR scheduler" << std::endl;
}

/*!
 *  \brief    Print the statistics of generated processes
 */
void printStatistics() {
  std::size_t memory = 20 * (1 << 20);
  SystemAllocator allocator(memory);
  FIFOScheduler(1, allocator).statistics();
}

/*!
 *  \brief    Schedule processes with the RR scheduler
 */
void rr(const int num_processors) {
  std::size_t memory = 20 * (1 << 20);
  SystemAllocator allocator(memory);
  RRScheduler(num_processors, allocator).run();
}

/*!
 *  \brief    Schedule processes with the SJF scheduler
 */
void sjf(const int num_processors) {
  std::size_t memory = 20 * (1 << 20);
  SystemAllocator allocator(memory);
  SJFScheduler(num_processors, allocator).run();
}

/*!
 *  \brief    Check if a string starts with another string
 *  \param str The base string that may or may not start with the substring
 *  \param start The substring that may or may not be the start of the base
 *            string
 *  \return   true if str starts with start, otherwise false
 */
bool startsWith(const std::string& str, const std::string& start) {
  if (start.length() > str.length())
    return false;
  int cmp = str.compare(0, start.length(), start);
  return 0 == cmp;
}

/*!
 *  \brief
 *  \param mem_percent The percentage of total required memory that is
 *            available to the system. NOTE that if mem_percent is 1 (100%)
 *            then the total allocated memory will be 20MiB instead of a strict
 *            100% of needed memory.
 */
void systemMemory(const float mem_percent) {
  using namespace std::chrono;
  // This is 20MiB
  // MiB (mebibyte) is the IEC replacement for a megabyte
  std::size_t memory = 20 * (1 << 20);
  std::vector<Process> processes = Scheduler::generate(Process::PROCESSES);
  if (mem_percent not_eq 1.0)
    memory = Process::getMemoryPercentage(processes, mem_percent);
  std::cout << "Maximum memory: " << memory << std::endl;
  SystemAllocator allocator(memory);
  // Time scheduling
  auto start = high_resolution_clock::now();
  FIFOScheduler(1, allocator, processes).run();
  auto time = high_resolution_clock::now() - start;
  nanoseconds ms = duration_cast<nanoseconds>(time);
  std::cout << "Time taken: " << ms.count() << "ns" << std::endl << std::endl;
}

/*!
 *  \brief    Program executes from here
 */
int main() {
  menu();
  return EXIT_SUCCESS;
}

/* ************************************************************************* *
 *                                    EOF                                    *
 * ************************************************************************* */
