/*
 * Copyright (c) 2016, Erika Gutierrez Portillo, Ryan Porterfield
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *     1. Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *     3. Neither the name of the copyright holder nor the names of its
 *     contributors may be used to endorse or promote products derived from
 *     this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "sjf.h"
#include <algorithm>
#include <ciso646>
#include <cstddef>
#include <iostream>
#include <vector>
#include "allocator.h"
#include "process.h"

/*! \struct   Compare
 *  \brief    Comparison data structure used to sort Processes
 *
 *  Data structure with just an operator() function used by std::sort to sort
 *  the ready queue
 */
struct Compare {
  /*!
   *  \brief  Check if a Process's burst time is less than another Process
   *  \param  p1  The first Process to be compared with p2
   *  \param  p2  The second Process to be compared with p1
   *  \return true if p1's remaining burst time is less than p2's
   */
  bool operator()(const Process* p1, const Process* p2) {
    return p1->getRemainingTime() <= p2->getRemainingTime();
  }
};

/*!
 *  \brief    Create a new SJFScheduler
 *  \param    num_processors The number of processors to use for scheduling
 */
SJFScheduler::SJFScheduler(const std::size_t num_processors,
                           Allocator& allocator) noexcept 
    : Scheduler(num_processors, allocator) {
}

/*!
 *  \brief    Schedule each process with Shortest Job First
 */
void SJFScheduler::run() noexcept {
  std::vector<Process*> ready_queue;
  std::vector<Process*>::iterator iter;
  ready_queue = schedule(ready_queue, 0);
  // Run SJF
  for (iter = ready_queue.begin(); iter not_eq ready_queue.end(); ) {
    Processor& processor = getProcessor();
    if (running()) {
      ready_queue = schedule(ready_queue, processor.getCurrentTime());
      std::sort(ready_queue.begin(), ready_queue.end(), Compare());
    }
    processor.run(**iter);
    getAllocator().deallocate((*iter)->getMemory(), (*iter)->getMemorySize());
    /* When we're done with a process, we don't want it to remain in the queue
     * because it might end up getting sorted after a new process with a lower
     * burst time and getting run again */
    iter = ready_queue.erase(iter);
  }
  printStatistics();
}


/* ************************************************************************* *
 *                                    EOF                                    *
 * ************************************************************************* */
