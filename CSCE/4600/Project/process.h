/*
 * Copyright (c) 2016, Erika Gutierrez Portillo, Ryan Porterfield
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *     1. Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *     3. Neither the name of the copyright holder nor the names of its
 *     contributors may be used to endorse or promote products derived from
 *     this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef PROCESS_H_
#define PROCESS_H_

#include <cstddef>
#include <cstring>
#include <random>
#include <string>
#include <stdlib.h>
#include <vector>

class Process {
public:
  /*! Context switch penalty time */
  static const std::size_t PENALTY = 10;
  /*! Number of processes to generate */
  const static std::size_t PROCESSES = 64;

  explicit Process();
  /*explicit Process(const Process& copy) = default;
  explicit Process(Process&& move) noexcept = default;
  virtual ~Process() noexcept = default;
  Process& operator=(const Process& copy) = default;
  Process& operator=(Process&& move) noexcept = default;*/

  std::size_t burst(const std::size_t burst_time) noexcept;
  bool complete() const noexcept;
  std::size_t getArrivalTime() const noexcept;
  void* getMemory() noexcept;
  static std::size_t getMemoryPercentage(const std::vector<Process>& processes,
                                         const float percentage) noexcept;
  std::size_t getMemorySize() const noexcept;
  std::size_t getProcessID() const noexcept;
  std::size_t getRemainingTime() const noexcept;
  std::size_t getTotalBurstTime() const noexcept;
  std::size_t getWaitTime() const noexcept;
  void setMemory(void* memory) noexcept;
  void setWaitTime(std::size_t wait_time) noexcept;

private:
  /*! Random number generator */
  std::default_random_engine generator_;
  /*! Distribution for cycles */
  std::normal_distribution<double> cycle_dist_;
  /*! Distribution for memory */
  std::normal_distribution<double> mem_dist_;
  /*! Process id */
  std::size_t process_id_;
  /*! Arrival time */
  std::size_t arrival_time_;
  /*! Total burst time */
  std::size_t burst_time_;
  /*! Memory footprint */
  std::size_t memory_size_;
  /*! Remaining burst time */
  std::size_t remaining_time_;
  /*! Remaining burst time */
  std::size_t wait_time_;
  /*!  */
  void* memory_;

  std::size_t genArrivalTime() const noexcept;
  std::size_t genBurstTime() noexcept;
  std::size_t genMemorySize() noexcept;
  std::size_t genProcessID() const noexcept;
};

#endif // PROCESS_H_

/* ************************************************************************  *
 *                                    EOF                                    *
 * ************************************************************************  */
