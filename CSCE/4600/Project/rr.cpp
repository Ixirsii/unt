/*
 * Copyright (c) 2016, Erika Gutierrez Portillo, Ryan Porterfield
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *     1. Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *     3. Neither the name of the copyright holder nor the names of its
 *     contributors may be used to endorse or promote products derived from
 *     this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "rr.h"
#include <ciso646>
#include <cstddef>
#include <iostream>
#include <vector>
#include "allocator.h"
#include "process.h"

/*! 
 *  \brief    Create a new RRScheduler
 *  \param    num_processors The number of processors to use for scheduling
 */
RRScheduler::RRScheduler(const std::size_t num_processors,
                         Allocator& allocator) noexcept
    : Scheduler(num_processors, allocator) {
}

/*! 
 *  \brief    Schedule each process with Round Robin
 *
 *  The initial ready_queue should only contain 1 process (the one that arrives
 *  at 0). The process is run for the quantum time, which conveniently is
 *  enough time for the next process to arrive and start.
 *  Each process is run in turn for the quantum amount of time, and when a
 *  process is complete it is removed from the ready queue. This allows the
 *  loop to run until the ready queue is empty
 */
void RRScheduler::run() noexcept {
  std::vector<Process*> ready_queue;
  std::vector<Process*>::iterator iter;
  ready_queue = schedule(ready_queue, 0);
  iter = ready_queue.begin();

  while (not ready_queue.empty()) {
    Processor& processor = getProcessor();
    printProcess(**iter);
    processor.quantum(**iter);
    if (running())
      ready_queue = schedule(ready_queue, processor.getCurrentTime());
    if ((*iter)->complete()) {
      getAllocator().deallocate((*iter)->getMemory(), (*iter)->getMemorySize());
      iter = ready_queue.erase(iter);
    }
    else
      ++iter;
    if (iter == ready_queue.end())
      iter = ready_queue.begin();
  }
  printStatistics();
}

/* ************************************************************************* *
 *                                    EOF                                    *
 * ************************************************************************* */
