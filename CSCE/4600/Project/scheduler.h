/*
 * Copyright (c) 2016, Erika Gutierrez Portillo, Ryan Porterfield
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *     1. Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *     3. Neither the name of the copyright holder nor the names of its
 *     contributors may be used to endorse or promote products derived from
 *     this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef SCHEDULER_H_
#define SCHEDULER_H_

#include <cstddef>
#include <vector>
#include "allocator.h"
#include "process.h"
#include "processor.h"

class Scheduler {
public:
  explicit Scheduler(const std::size_t num_processors,
                     Allocator& allocator) noexcept;
  explicit Scheduler(const std::size_t num_processors,
                     Allocator& allocator,
                     std::vector<Process> processes) noexcept;

  static std::vector<Process> generate(std::size_t num_processes) noexcept;
  void printProcesses() const noexcept;
  virtual void run() noexcept = 0;
  bool running() const noexcept;
  void statistics() noexcept;

protected:
  Allocator& getAllocator() noexcept;
  Processor& getProcessor() noexcept;
  void printProcess(const Process& process) const noexcept;
  void printStatistics() const noexcept;
  std::vector<Process*>& schedule(std::vector<Process*>& ready_queue,
                                 std::size_t system_time);
                        
private:
  /*! Allocator for process memory */
  Allocator& allocator_;
  /*! List of generated Processes */
  std::vector<Process> processes_;
  /*! Queue of processes waiting for available memory */
  std::vector<Process*> memory_queue_;
  /*! Iterator for processes */
  std::vector<Process>::iterator iter_;
  /*! Array of Processors */
  Processor processors_[4];
  /*! How many CPUs/Processors are being used */
  std::size_t num_processors_;

  std::vector<Process*>& scheduleFromMemQueue(
      std::vector<Process*>& ready_queue) noexcept;
  std::vector<Process*>& scheduleFromQueue(
      std::vector<Process*>& ready_queue, std::size_t system_time) noexcept;
};

#endif // SCHEDULER_H_

/* ************************************************************************* *
 *                                    EOF                                    *
 * ************************************************************************* */
