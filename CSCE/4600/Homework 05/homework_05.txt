1)  a.  The set is safe
    b.  P1 can be safely granted 1 unit of resource 1
    c.  P2 can be safely granted 6 unit of resource 3
    d.  P2 can be safely granted 2 unit of resource 2

2)
  a.  Hole 1: 0Mb left, Hole 2: 6Mb left, Hole 3: 10Mb left
  b.  Hole 1: 5Mb left, Hole 2: 5Mb left, Hole 3: 6Mb left
  c.  Hole 1: 0Mb left, Hole 2: 6Mb left, Hole 3: 10Mb left
  d.  Hole 1: 5Mb left, Hole 2: 5Mb left, Hole 3: 6Mb left

3)
  a.  The advantage of compaction is that it keeps all the blocks in memory
      which is faster than reading/writing to disk. However the disadvantage
      is that moving things around in memory can be slow.
  b.  The advantage of swapping is that it can help keep more memory clear for
      new processes, but recovering a block that has been moved to disk is slow.

4)  a.  1, 1, 2, 2, 1, 4, 2, 3, 3, 5, 5, 4
    b.  1, 1, 1, 1, 1, 2, 1, 2, 2, 3, 3, 2
    c.  1, 1, 1, 1, 1, 2, 1, 1, 1, 2, 2, 2

5)  a.  Largest working set = 3, Ex. {y, u, x}
    b.  Smalles working set = 1 Ex. {x, x, x}