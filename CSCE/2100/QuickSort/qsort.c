/** ************************************************************************ **
 ** ************************************************************************ **
 **                                                                          **
 ** /brief      A simple quick sort implementation using lists.              **
 **                                                                          **
 ** /details    Test results:                                                **
 **                 qsort [9, 2, 5, 1, 7, 4, 6, 0, 8, 3]                     **
 **                 [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]                           **
 **                 --                                                       **
 **                 qsort [15, 23, 8, 67, 93, 2, 54, 80, 22, 47]             **
 **                 [2, 8, 15, 22, 23, 47, 54, 67, 80, 93]                   **
 **                 --                                                       **
 **                 qsort [5, 6, 2, 5, 7, 9, 0, 13, 22, 1]                   **
 **                 [0, 1, 2, 5, 5, 6, 7, 9, 13, 22]                         **
 **                 --                                                       **
 **                 qsort [100, 2, 310, 5742, 16]                            **
 **                 [2, 16, 100, 310, 5742]                                  **
 **                 --                                                       **
 **                 qsort [510, 2264, 16, 32, 980, 427]                      **
 **                 [16, 32, 427, 510, 980, 2264]                            **
 **                                                                          **
 ** /author     Ryan Porterfield <RyanPorterfield@my.unt.edu>                **
 ** /date       2014-02-12                                                   **
 **                                                                          **
 ** ************************************************************************ **
 ** ************************************************************************ **/

#include <iso646.h>
#include <stdio.h>
#include <stdlib.h>

typedef struct node_s {
    int             element;
    struct node_s   *next;
} *list_t;

/* ************************** Function Prototypes *************************** */
/**
 * Append an existing element to a list.
 */
void    AppendNode  (list_t *list, struct node_s *next);

/**
 * Create a new list from user input.
 */
list_t  MakeList    ();

/**
 * Merge two lists together.
 */
list_t  Merge       (list_t lesser, struct node_s *current, list_t greater);

/**
 * Print every element in a list recursively.
 */
void    PrintList   (list_t list);

/**
 * Quick sort a list of integers.
 */
list_t  QSort       (list_t list);

/**
 * Split a list of integers.
 */
void    Split       (list_t list, list_t *lesser, list_t *greater);

/* ***************************** Main Function ****************************** */
int
main(int argc, const char **argv)
{
    list_t list;
    printf ("Enter integers separated by spaces and ended by Ctrl-D\n");
    list = MakeList ();
    list = QSort (list);
    printf ("Sorted list:\n");
    PrintList (list);
    return EXIT_SUCCESS;
}

/* ************************** Function Definitions ************************** */

/*
 * Checks to see if list is null. If yes then list is pointed to next. If no,
 * then we iterate to the end of the list and point last->next to next.
 */
void
AppendNode (list_t *list, struct node_s *next)
{
    if (*list == NULL) {
        *list = next;
        (*list)->next = NULL;
        return;
    }

    list_t iter = *list;
    for (; iter->next not_eq NULL; iter = iter->next) {}
    iter->next = next;
    next->next = NULL;
}

/*
 * Read a new integer in from stdin until EOF is caught, creating a new node in
 * the list for every entry.
 */
list_t
MakeList()
{
    int     x;
    list_t  pNewCell;

    if (scanf ("\%d", &x) == EOF)
        return NULL;
    else {
        pNewCell =  malloc (sizeof (struct node_s));
        pNewCell->next = MakeList ();
        pNewCell->element = x;
        return pNewCell;
    }
}

/*
 * Assuming that current is never NULL, check if lesser or greater is NULL, and
 * safely append the list accordingly.
 */
list_t
Merge (list_t lesser, struct node_s *current, list_t greater)
{
    list_t iter = lesser;
    if (greater not_eq NULL)
        current->next = greater;
    if (lesser not_eq NULL) {
        for (; iter->next not_eq NULL; iter = iter->next) {}
        iter->next = current;
    } else
        lesser = current;
    return lesser;
}

/*
 * Iterate through the list printing every element.
 */
void
PrintList(list_t list)
{
    while (list not_eq NULL) {
        printf("\%d\n", list->element);
        list = list->next;
    }
}

/*
 * Split the list into lesser and greater lists using the first element in the
 * list as the sentinel value. Do this recursively until all lists have been
 * split then merge the lists back together in sorted order.
 */
list_t
QSort(list_t list)
{
    if (list == NULL or list->next == NULL)
        return list;
    list_t lesser = NULL, greater = NULL;
    Split (list, &lesser, &greater);
    return Merge (QSort (lesser), list, QSort (greater));
}

/*
 * Split a list into lesser and greater lists using the first element as the
 * sentinel value.
 */
void
Split (list_t list, list_t *lesser, list_t *greater)
{
    /* */
    int current = list->element;
    list_t iter = list->next, next;
    list->next = NULL;
    while (iter not_eq NULL) {
        next = iter->next;
        if (iter->element <= current)
            AppendNode (lesser, iter);
        else
            AppendNode (greater, iter);
        iter = next;
    }
}

/* EOF */
