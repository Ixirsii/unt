Homework 02
===========

Due Wednesday, February 12 @ 1200
---------------------------------

- Write a C program equivalent to qsort.hs
- Test it with 5 different sequences of integers

You can use either arrays or lists to implement sequences.
Feel free to reuse the list implementation in mergesort.c
