Assignment 06
=============

**Due Wednesday, April 9 @ 12:00**

Using C++:
- Use classes and virtual methods to organize the animals in a small zoo.
  The animals are:
  - lions,
  - parrots,
  - alligators,
  - penguins,
  - elephants,
  - cobras,
  - zebras,
  - hawks,
  - turkeys,
  - rabbits
- Use at least 3 properties the animals might share and demonstrate overriding
  of their getter and setter methods.
- Write 10 examples of method calls asking the animals to perform a task to
  which the animals would answer either "Doing it!" or "Sorry, I cannot do it".
