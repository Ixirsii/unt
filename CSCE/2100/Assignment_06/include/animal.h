/*  ************************************************************************ **
 ** ************************************************************************ **
 **                                                                          **
 ** Defines animal classes and types for assignment 06                       **
 ** Copyright (C) 2014 Ryan Porterfield                                      **
 **                                                                          **
 ** This program is free software; you can redistribute it and/or            **
 ** modify it under the terms of the GNU General Public License              **
 ** as published by the Free Software Foundation; either version 2           **
 ** of the License, or (at your option) any later version.                   **
 **                                                                          **
 ** This program is distributed in the hope that it will be useful,          **
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of           **
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            **
 ** GNU General Public License for more details.                             **
 **                                                                          **
 ** You should have received a copy of the GNU General Public License        **
 ** along with this program; if not, write to the                            **
 ** Free Software Foundation, Inc.,                                          **
 ** 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.            **
 **                                                                          **
 ** ************************************************************************ **
 ** ************************************************************************  */


/** ************************************************************************ **
 ** ************************************************************************ **
 **                                                                          **
 ** @brief          Provides basic implementation for all animals.           **
 **                                                                          **
 ** @author         Ryan Porterfield <RyanPorterfield@my.unt.edu>            **
 ** @date           2014-04-02                                               **
 ** @version        0.0.1                                                    **
 ** @copyright      GNU General Public License v2                            **
 ** @file                                                                    **
 **                                                                          **
 ** ************************************************************************ **
 ** ************************************************************************ **/


#include <string>


#ifndef ANIMAL_H
#define ANIMAL_H

#define DOES    "Doing it!"
#define DOESNT  "Sorry, I can't do that."

enum DietType { CARNIVORE, HERBAVORE, OMNIVORE, };


/**
 * /brief           Simple animal class that implements basic animal behavior.
 *
 * /details         Base class in an animal inheritance tree that provides the
 *                  basic implementation and defines all necessary
 *                  implementaion for specific animal classes.
 */
class Animal {
public:
    /**
     * /brief       Construct a new animal.
     *
     * /param name  The animals name.
     */
    Animal                              (bool can_fly, bool can_walk,
                                         bool can_swim, std::string name,
                                         DietType diet);
    /**
     * /brief       Destroy the animal.
     */
    ~Animal                             ();
    /**
     * /brief       Have the animal bite.
     *
     * /return      "Doing it!" if the animal can do it, otherwise "Sorry, I
     *              cannot do it."
     */
    virtual std::string     bite        ()  = 0;
    /**
     * /brief       Have the animal try to jump.
     *
     * /return      "Doing it!" if the animal can do it, otherwise "Sorry, I
     *              cannot do it."
     */
    virtual std::string     jump        ()  = 0;
    /**
     * /brief       The animal will "make noise."
     *
     * /return      The noise the animal makes.
     */
    virtual std::string     make_noise  ()  = 0;
    /**
     * /brief       Have the animal try to preen.
     *
     * /return      "Doing it!" if the animal can do it, otherwise "Sorry, I
     *              cannot do it."
     */
    virtual std::string     preen       ()  = 0;
    /**
     * /brief       Have the animal try to eat some grass.
     *
     * /return      "Doing it!" if the animal can do it, otherwise "Sorry, I
     *              cannot do it."
     */
    std::string             eat_grass   ();
    /**
     * /brief       Have the animal try to eat a mouse.
     *
     * /return      "Doing it!" if the animal can do it, otherwise "Sorry, I
     *              cannot do it."
     */
    std::string             eat_mouse   ();
    /**
     * /brief       Have the animal try to fly.
     *
     * /return      "Doing it!" if the animal can do it, otherwise "Sorry, I
     *              cannot do it."
     */
    std::string             fly         ();
    /**
     * /brief       Get the animals name.
     *
     * /return      The animals name.
     */
    std::string             get_name    ();
    /**
     * /brief
     *
     * /return      "Doing it!" if the animal can do it, otherwise "Sorry, I
     *              cannot do it."
     */
    std::string             sleep       ();
    /**
     * /brief
     *
     * /return      "Doing it!" if the animal can do it, otherwise "Sorry, I
     *              cannot do it."
     */
    std::string             swim        ();
    /**
     * /brief
     *
     * /return      "Doing it!" if the animal can do it, otherwise "Sorry, I
     *              cannot do it."
     */
    std::string             walk        ();

protected:
    /**
     * /brief       Changes what kinds of food the animal can eat.
     *
     * /param diet  The animals new diet.
     */
    void                    change_diet (DietType diet);

private:
    /**
     * /brief       /c true if the animal can fly, otherwise /c false.
     */
    const bool              can_fly_;
    /**
     * /brief       /c true if the animal can swim, otherwise /c false
     */
    const bool              can_swim_;
    /**
     * /brief       /c true if the animal can walk, otherwise /c false
     */
    const bool              can_walk_;
    /**
     * /brief       The animals name.
     */
    std::string             name_;
    /**
     * /brief       What kind of food the animal eats.
     */
    DietType                diet_;
};


#endif // ANIMAL_H


/*  ************************************************************************ **
 **                                    EOF                                   **
 ** ************************************************************************ **/
