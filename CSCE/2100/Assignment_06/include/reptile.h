/*  ************************************************************************ **
 ** ************************************************************************ **
 **                                                                          **
 ** Defines animal classes and types for assignment 06                       **
 ** Copyright (C) 2014 Ryan Porterfield                                      **
 **                                                                          **
 ** This program is free software; you can redistribute it and/or            **
 ** modify it under the terms of the GNU General Public License              **
 ** as published by the Free Software Foundation; either version 2           **
 ** of the License, or (at your option) any later version.                   **
 **                                                                          **
 ** This program is distributed in the hope that it will be useful,          **
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of           **
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            **
 ** GNU General Public License for more details.                             **
 **                                                                          **
 ** You should have received a copy of the GNU General Public License        **
 ** along with this program; if not, write to the                            **
 ** Free Software Foundation, Inc.,                                          **
 ** 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.            **
 **                                                                          **
 ** ************************************************************************ **
 ** ************************************************************************  */

/** ************************************************************************ **
 ** ************************************************************************ **
 **                                                                          **
 ** @brief          Provides implementation for all reptiles.                **
 **                                                                          **
 ** @author         Ryan Porterfield <RyanPorterfield@my.unt.edu>            **
 ** @date           2014-04-02                                               **
 ** @version        0.0.1                                                    **
 ** @copyright      GNU General Public License v2                            **
 ** @file                                                                    **
 **                                                                          **
 ** ************************************************************************ **
 ** ************************************************************************ **/


#include <animal.h>

#include <string>


#ifndef REPTILE_H
#define REPTILE_H


class Reptile : public Animal
{
public:
    Reptile                                 (bool can_swim, bool can_walk,
                                             std::string name, DietType diet,
                                             bool has_scales);
    ~Reptile                                ();

    virtual std::string     bite            ()  = 0;
    virtual std::string     jump            ()  = 0;
    virtual std::string     make_noise      ()  = 0;
    virtual std::string     preen           ()  = 0;

    bool                    has_scales      ();

private:
    bool                    has_scales_;
};


#endif // REPTILE_H


/*  ************************************************************************ **
 **                                    EOF                                   **
 ** ************************************************************************ **/
