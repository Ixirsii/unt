/*  ************************************************************************ **
 ** ************************************************************************ **
 **                                                                          **
 ** Defines concrete classes and types for assignment 06                     **
 ** Copyright (C) 2014 Ryan Porterfield                                      **
 **                                                                          **
 ** This program is free software; you can redistribute it and/or            **
 ** modify it under the terms of the GNU General Public License              **
 ** as published by the Free Software Foundation; either version 2           **
 ** of the License, or (at your option) any later version.                   **
 **                                                                          **
 ** This program is distributed in the hope that it will be useful,          **
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of           **
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            **
 ** GNU General Public License for more details.                             **
 **                                                                          **
 ** You should have received a copy of the GNU General Public License        **
 ** along with this program; if not, write to the                            **
 ** Free Software Foundation, Inc.,                                          **
 ** 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.            **
 **                                                                          **
 ** ************************************************************************ **
 ** ************************************************************************  */

/** ************************************************************************ **
 ** ************************************************************************ **
 **                                                                          **
 ** @brief          Provides implementation for all concrete animals.        **
 **                                                                          **
 ** @author         Ryan Porterfield <RyanPorterfield@my.unt.edu>            **
 ** @date           2014-04-02                                               **
 ** @version        0.0.1                                                    **
 ** @copyright      GNU General Public License v2                            **
 ** @file                                                                    **
 **                                                                          **
 ** ************************************************************************ **
 ** ************************************************************************ **/


#include <string>

#include <foul.h>
#include <mammal.h>
#include <reptile.h>


#ifndef ANIMALS_H
#define ANIMALS_H


/** ************************************************************************ **
 **                                   Foul                                   **
 ** ************************************************************************ **/

class Hawk: public Foul
{
public:
    Hawk                                    (std::string name);
    ~Hawk                                   ();

    virtual std::string     bite            ();
    virtual std::string     jump            ();
    virtual std::string     make_noise      ();
    virtual std::string     preen           ();
};


class Parrot: public Foul
{
public:
    Parrot                                  (std::string name);
    ~Parrot                                 ();

    virtual std::string     bite            ();
    virtual std::string     jump            ();
    virtual std::string     make_noise      ();
    virtual std::string     preen           ();
};


class Penguin: public Foul
{
public:
    Penguin                                  (std::string name);
    ~Penguin                                 ();

    virtual std::string     bite            ();
    virtual std::string     jump            ();
    virtual std::string     make_noise      ();
    virtual std::string     preen           ();
};


class Turkey: public Foul
{
public:
    Turkey                                  (std::string name);
    ~Turkey                                 ();

    virtual std::string     bite            ();
    virtual std::string     jump            ();
    virtual std::string     make_noise      ();
    virtual std::string     preen           ();
};


/** ************************************************************************ **
 **                                  Mammal                                  **
 ** ************************************************************************ **/

class Elephant: public Mammal
{
public:
    Elephant                                (std::string name);
    ~Elephant                               ();

    virtual std::string     bite            ();
    virtual std::string     jump            ();
    virtual std::string     make_noise      ();
    virtual std::string     preen           ();
};


class Lion: public Mammal
{
public:
    Lion                                    (std::string name);
    ~Lion                                   ();

    virtual std::string     bite            ();
    virtual std::string     jump            ();
    virtual std::string     make_noise      ();
    virtual std::string     preen           ();
};


class Rabbit: public Mammal
{
public:
    Rabbit                                  (std::string name);
    ~Rabbit                                 ();

    virtual std::string     bite            ();
    virtual std::string     jump            ();
    virtual std::string     make_noise      ();
    virtual std::string     preen           ();
};


class Zebra: public Mammal
{
public:
    Zebra                                   (std::string name);
    ~Zebra                                  ();

    virtual std::string     bite            ();
    virtual std::string     jump            ();
    virtual std::string     make_noise      ();
    virtual std::string     preen           ();
};


/** ************************************************************************ **
 **                                 Reptiles                                 **
 ** ************************************************************************ **/

class Alligator : public Reptile
{
public:
    Alligator                               (std::string name);
    ~Alligator                              ();

    virtual std::string     bite            ();
    virtual std::string     jump            ();
    virtual std::string     make_noise      ();
    virtual std::string     preen           ();
};


class Cobra : public Reptile
{
public:
    Cobra                                   (std::string name);
    ~Cobra                                  ();

    virtual std::string     bite            ();
    virtual std::string     jump            ();
    virtual std::string     make_noise      ();
    virtual std::string     preen           ();
};


#endif // ANIMALS_H


/*  ************************************************************************ **
 **                                    EOF                                   **
 ** ************************************************************************ **/
