/*  ************************************************************************ **
 ** ************************************************************************ **
 **                                                                          **
 ** Generic fould implementation for assignment 06                           **
 ** Copyright (C) 2014 Ryan Porterfield                                      **
 **                                                                          **
 ** This program is free software; you can redistribute it and/or            **
 ** modify it under the terms of the GNU General Public License              **
 ** as published by the Free Software Foundation; either version 2           **
 ** of the License, or (at your option) any later version.                   **
 **                                                                          **
 ** This program is distributed in the hope that it will be useful,          **
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of           **
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            **
 ** GNU General Public License for more details.                             **
 **                                                                          **
 ** You should have received a copy of the GNU General Public License        **
 ** along with this program; if not, write to the                            **
 ** Free Software Foundation, Inc.,                                          **
 ** 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.            **
 **                                                                          **
 ** ************************************************************************ **
 ** ************************************************************************  */


#include "foul.h"

#include <string>


/*
 * Construct a new bird/foul.
 */
Foul::Foul (bool can_fly, bool can_swim, std::string name, DietType diet, 
            double wingspan)
    : Animal (can_fly, true, can_swim, name, diet),
      wingspan_ (wingspan)
{

}

/*
 * Destroy a bird/foul.
 */
Foul::~Foul () {}

/*
 * Return the bird's wingspan.
 */
double
Foul::get_wingspan ()
{
    return wingspan_;
}


/*  ************************************************************************ **
 **                                    EOF                                   **
 ** ************************************************************************ **/
