/** ************************************************************************ **
 ** ************************************************************************ **
 **                                                                          **
 ** Generic animal implementation for assignment 06                          **
 ** Copyright (C) 2014 Ryan Porterfield                                      **
 **                                                                          **
 ** This program is free software; you can redistribute it and/or            **
 ** modify it under the terms of the GNU General Public License              **
 ** as published by the Free Software Foundation; either version 2           **
 ** of the License, or (at your option) any later version.                   **
 **                                                                          **
 ** This program is distributed in the hope that it will be useful,          **
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of           **
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            **
 ** GNU General Public License for more details.                             **
 **                                                                          **
 ** You should have received a copy of the GNU General Public License        **
 ** along with this program; if not, write to the                            **
 ** Free Software Foundation, Inc.,                                          **
 ** 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.            **
 **                                                                          **
 ** ************************************************************************ **
 ** ************************************************************************ **/


#include "animal.h"

#include <iso646.h>
#include <string>


/*
 * Construct a new animal.
 * Initialize the animals name, and whether it can fly, swim, or walk.
 */
Animal::Animal (bool can_fly, bool can_swim, bool can_walk,
                std::string name, DietType diet)
    : can_fly_(can_fly),
      can_swim_(can_swim),
      can_walk_(can_walk),
      name_(name),
      diet_(diet)
{}

/*
 * Default destructor.
 */
Animal::~Animal () {}

/*
 * Change what type of food the animal can eat.
 */
void
Animal::change_diet (DietType diet)
{
    diet_ = diet;
}

/*
 * Have the animal eat some grass.
 */
std::string
Animal::eat_grass ()
{
    return (diet_ not_eq CARNIVORE) ? DOES : DOESNT;

}

/*
 * Have the animal eat a small rodent
 */
std::string
Animal::eat_mouse ()
{
    return (diet_ not_eq HERBAVORE) ? DOES : DOESNT;
}

/*
 * Try to have the animal fly.
 */
std::string
Animal::fly ()
{
    return can_fly_ ? DOES : DOESNT;
}

/*
 * Return the animal's name.
 */
std::string
Animal::get_name ()
{
    return name_;
}

/*
 * Have the animal sleep.
 */
std::string
Animal::sleep ()
{
    return DOES;
}

/*
 * Try to have the animal swim.
 */
std::string
Animal::swim ()
{
    return can_swim_ ? DOES : DOESNT;
}

/*
 * Try to have the animal walk.
 */
std::string
Animal::walk ()
{
    return can_walk_ ? DOES : DOESNT;
}


/*  ************************************************************************ **
 **                                    EOF                                   **
 ** ************************************************************************ **/
