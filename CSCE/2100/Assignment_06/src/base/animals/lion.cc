/*  ************************************************************************ **
 ** ************************************************************************ **
 **                                                                          **
 ** Defines lion behavior for assignment 06                                  **
 ** Copyright (C) 2014 Ryan Porterfield                                      **
 **                                                                          **
 ** This program is free software; you can redistribute it and/or            **
 ** modify it under the terms of the GNU General Public License              **
 ** as published by the Free Software Foundation; either version 2           **
 ** of the License, or (at your option) any later version.                   **
 **                                                                          **
 ** This program is distributed in the hope that it will be useful,          **
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of           **
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            **
 ** GNU General Public License for more details.                             **
 **                                                                          **
 ** You should have received a copy of the GNU General Public License        **
 ** along with this program; if not, write to the                            **
 ** Free Software Foundation, Inc.,                                          **
 ** 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.            **
 **                                                                          **
 ** ************************************************************************ **
 ** ************************************************************************  */


#include <animals.h>

#include <string>

#include <animal.h>


Lion::Lion (std::string name)
    : Mammal (true, name, CARNIVORE, 2)
{

}

Lion::~Lion () {}

std::string
Lion::bite ()
{
    return DOES;
}

std::string
Lion::jump ()
{
    return DOES;
}

std::string
Lion::make_noise ()
{
    return "ROAR";
}

std::string
Lion::preen ()
{
    return DOESNT;
}


/*  ************************************************************************ **
 **                                    EOF                                   **
 ** ************************************************************************ **/


