/*  ************************************************************************ **
 ** ************************************************************************ **
 **                                                                          **
 ** Defines parrot behavior for assignment 06                                **
 ** Copyright (C) 2014 Ryan Porterfield                                      **
 **                                                                          **
 ** This program is free software; you can redistribute it and/or            **
 ** modify it under the terms of the GNU General Public License              **
 ** as published by the Free Software Foundation; either version 2           **
 ** of the License, or (at your option) any later version.                   **
 **                                                                          **
 ** This program is distributed in the hope that it will be useful,          **
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of           **
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            **
 ** GNU General Public License for more details.                             **
 **                                                                          **
 ** You should have received a copy of the GNU General Public License        **
 ** along with this program; if not, write to the                            **
 ** Free Software Foundation, Inc.,                                          **
 ** 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.            **
 **                                                                          **
 ** ************************************************************************ **
 ** ************************************************************************  */


#include <animals.h>

#include <string>

#include <animal.h>


Parrot::Parrot (std::string name)
    : Foul (true, false , name, HERBAVORE, 50)
{

}

Parrot::~Parrot () {}

std::string
Parrot::bite ()
{
    return DOES;
}

std::string
Parrot::jump ()
{
    return DOESNT;
}

std::string
Parrot::make_noise ()
{
    return "AWK! HELLO!";
}

std::string
Parrot::preen ()
{
    return DOES;
}


/*  ************************************************************************ **
 **                                    EOF                                   **
 ** ************************************************************************ **/




 

