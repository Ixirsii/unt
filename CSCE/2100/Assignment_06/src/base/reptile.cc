/*  ************************************************************************ **
 ** ************************************************************************ **
 **                                                                          **
 ** Defines animal classes and types for assignment 06                       **
 ** Copyright (C) 2014 Ryan Porterfield                                      **
 **                                                                          **
 ** This program is free software; you can redistribute it and/or            **
 ** modify it under the terms of the GNU General Public License              **
 ** as published by the Free Software Foundation; either version 2           **
 ** of the License, or (at your option) any later version.                   **
 **                                                                          **
 ** This program is distributed in the hope that it will be useful,          **
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of           **
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            **
 ** GNU General Public License for more details.                             **
 **                                                                          **
 ** You should have received a copy of the GNU General Public License        **
 ** along with this program; if not, write to the                            **
 ** Free Software Foundation, Inc.,                                          **
 ** 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.            **
 **                                                                          **
 ** ************************************************************************ **
 ** ************************************************************************  */


#include "reptile.h"

#include <string>


/*
 * Reptile constructor.
 */
Reptile::Reptile (bool can_swim, bool can_walk, std::string name, DietType diet,
                  bool has_scales)
    : Animal (false, can_swim, can_walk, name, diet),
      has_scales_ (has_scales)
{
    
}

/*
 * Reptile destructor.
 */
Reptile::~Reptile () {}

/*
 * Return true if the reptile has scales. If the reptile does not have scales,
 * it must have skin.
 */
bool
Reptile::has_scales () {
    return has_scales_;
}


/*  ************************************************************************ **
 **                                    EOF                                   **
 ** ************************************************************************ **/
