/*  ************************************************************************ **
 ** ************************************************************************ **
 **                                                                          **
 ** Defines concrete classes and types for assignment 06                     **
 ** Copyright (C) 2014 Ryan Porterfield                                      **
 **                                                                          **
 ** This program is free software; you can redistribute it and/or            **
 ** modify it under the terms of the GNU General Public License              **
 ** as published by the Free Software Foundation; either version 2           **
 ** of the License, or (at your option) any later version.                   **
 **                                                                          **
 ** This program is distributed in the hope that it will be useful,          **
 ** but WITHOUT ANY WARRANTY; without even the implied warranty of           **
 ** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            **
 ** GNU General Public License for more details.                             **
 **                                                                          **
 ** You should have received a copy of the GNU General Public License        **
 ** along with this program; if not, write to the                            **
 ** Free Software Foundation, Inc.,                                          **
 ** 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.            **
 **                                                                          **
 ** ************************************************************************ **
 ** ************************************************************************  */


#include <iso646.h>
#include <stdlib.h>

#include <iostream>
#include <string>

#include <animal.h>
#include <animals.h>


// Print a menu to select an animal
int     animal_menu     ();
// Animal meta-test functions
void    see_alligator   ();
void    see_cobra       ();
void    see_elephant    ();
void    see_hawk        ();
void    see_lion        ();
void    see_parrot      ();
void    see_penguin     ();
void    see_rabbit      ();
void    see_turkey      ();
void    see_zebra       ();
// Tests
void    bite_test       (Animal *a);
void    eat_grass_test  (Animal *a);
void    eat_mouse_test  (Animal *a);
void    fly_test        (Animal *a);
void    jump_test       (Animal *a);
void    make_noise_test (Animal *a);
void    preen_test      (Animal *a);
void    sleep_test      (Animal *a);
void    swim_test       (Animal *a);
void    walk_test       (Animal *a);
// Meta-test function
void    command         (Animal *a, std::string test);
void    test            (Animal *a);


int
main ()
{
    int animal = 0;
    do {
    animal = animal_menu ();
        switch (animal) {
            case 1:     see_alligator (); break;
            case 2:     see_cobra (); break;
            case 3:     see_elephant (); break;
            case 4:     see_hawk (); break;
            case 5:     see_lion (); break;
            case 6:     see_parrot (); break;
            case 7:     see_penguin (); break;
            case 8:     see_rabbit (); break;
            case 9:     see_turkey (); break;
            case 10:    see_zebra (); break;
            default:    break;
        }
    } while (animal not_eq 0);
    return EXIT_SUCCESS;
}

int
animal_menu ()
{
    int flag = -1;
    do {
        std::cout << "Which animal would you like to see?" << std::endl;
        std::cout << "0)\tQuit" << std::endl;
        std::cout << "1)\tAlligator" << std::endl;
        std::cout << "2)\tCobra" << std::endl;
        std::cout << "3)\tElephant" << std::endl;
        std::cout << "4)\tHawk" << std::endl;
        std::cout << "5)\tLion" << std::endl;
        std::cout << "6)\tParrot" << std::endl;
        std::cout << "7)\tPenguin" << std::endl;
        std::cout << "8)\tRabbit" << std::endl;
        std::cout << "9)\tTurkey" << std::endl;
        std::cout << "10)\tZebra" << std::endl;
        std::cout << "Enter a number 0-10: " << std::endl;
        std::cin >> flag;
    } while (flag < 0 or flag > 10);
    return flag;
}

void
bite_test (Animal *a)
{
    command (a, "bite");
    std::cout << a->bite () << std::endl;
}

void
eat_grass_test (Animal *a)
{
    command (a, "eat grass");
    std::cout << a->eat_grass () << std::endl;
}

void
eat_mouse_test (Animal *a)
{
    command (a, "eat a mouse");
    std::cout << a->eat_mouse () << std::endl;
}

void
jump_test (Animal *a)
{
    command (a, "jump");
    std::cout << a->jump () << std::endl;
}

void
make_noise_test (Animal *a)
{
    command (a, "make noise");
    std::cout << a->make_noise () << std::endl;
}

void
preen_test (Animal *a)
{
    command (a, "preen");
    std::cout << a->preen () << std::endl;
}

void
fly_test (Animal *a)
{
    command (a, "fly");
    std::cout << a->fly () << std::endl;
}

void
sleep_test (Animal *a)
{
    command (a, "sleep");
    std::cout << a->sleep () << std::endl;
}

void
swim_test (Animal *a)
{
    command (a, "swim");
    std::cout << a->swim () << std::endl;
}

void
walk_test (Animal *a)
{
    command (a, "walk");
    std::cout << a->walk () << std::endl;
}


void
see_alligator ()
{
    static Alligator alligator ("Jessie");
    std::cout << "You are looking at the alligator named ";
    std::cout << alligator.get_name () << "." << std::endl << std::endl;
    test (&alligator);
}

void
see_cobra ()
{
    static Cobra cobra ("Commander");
    std::cout << "You are looking at the cobra named ";
    std::cout << cobra.get_name () << "." << std::endl << std::endl;
    test (&cobra);
}

void
see_elephant ()
{
    static Elephant elephant ("David");
    std::cout << "You are looking at the elephant named ";
    std::cout << elephant.get_name () << "." << std::endl << std::endl;
    test (&elephant);
}

void
see_hawk ()
{
    static Hawk hawk ("Ryan");
    std::cout << "You are looking at the hawk named ";
    std::cout << hawk.get_name () << "." << std::endl << std::endl;
    test (&hawk);
}

void
see_lion ()
{
    static Lion lion ("Alex");
    std::cout << "You are looking at the lion named ";
    std::cout << lion.get_name () << "." << std::endl << std::endl;
    test (&lion);
}

void
see_parrot ()
{
    static Parrot parrot ("Blu");
    std::cout << "You are looking at the parrot named ";
    std::cout << parrot.get_name () << "." << std::endl << std::endl;
    test (&parrot);
}

void
see_penguin ()
{
    static Penguin penguin ("Happy Feet");
    std::cout << "You are looking at the penguin named ";
    std::cout << penguin.get_name () << "." << std::endl << std::endl;
    test (&penguin);
}

void
see_rabbit ()
{
    static Rabbit rabbit ("Thumper");
    std::cout << "You are looking at the rabbit named ";
    std::cout << rabbit.get_name () << "." << std::endl << std::endl;
    test (&rabbit);
}

void
see_turkey ()
{
    static Turkey turkey ("Dinner");
    std::cout << "You are looking at the trukey named ";
    std::cout << turkey.get_name () << "." << std::endl << std::endl;
    test (&turkey);
}

void
see_zebra ()
{
    static Zebra zebra ("Marty");
    std::cout << "You are looking at the zebra named ";
    std::cout << zebra.get_name () << "." << std::endl << std::endl;
    test (&zebra);
}

void
command (Animal *a, std::string test)
{
    std::cout << std::endl << "You: " << a->get_name () << ", " << test << ".";
    std::cout << std:: endl <<a->get_name () << ": ";
}

void
test (Animal *a)
{
    bite_test       (a);
    eat_grass_test  (a);
    eat_mouse_test  (a);
    fly_test        (a);
    jump_test       (a);
    make_noise_test (a);
    preen_test      (a);
    sleep_test      (a);
    swim_test       (a);
    walk_test       (a);
    std::cout << std::endl;
}

/*  ************************************************************************ **
 **                                    EOF                                   **
 ** ************************************************************************ **/
