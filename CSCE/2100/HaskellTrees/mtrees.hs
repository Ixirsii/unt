{-- ----------------------------------------------------------------------- --
 -- ----------------------------------------------------------------------- --
 --                         2013-03-24 Notes & Code                         --
 -- ----------------------------------------------------------------------- --
 -- ----------------------------------------------------------------------- --}

{-
 - Ordered trees of "polymorphic" type a b
 - (All Leaf labels should have the same type)
 - (All Branch labels should have the same type)
 - Leaf and Branch could have different types
 -}

data Tree a b = Leaf a | Branch b [Tree a b] deriving (Eq, Read, Show)

e1 = Branch "+" [Leaf 10, Leaf 20, Branch "*" [Leaf 2, Leaf 3, Leaf 4], Leaf 50]

e2 = Branch "*" [e1, e1, e1]

e3 = Branch "+" [Leaf 0, e1, e2, Leaf 100]

e4 = Branch "*" [e1, Cranch "+" [Branch "+" [], Branch "*"}}]]

-- Preorder visit:
-- Start at top (parent nodes) of tree


-- Postorder visit:
-- Start at the bottom (child nodes) of tree


-- Infix visit:
-- Start in the middle of the tree

