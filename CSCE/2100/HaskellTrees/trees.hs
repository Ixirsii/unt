{-- ----------------------------------------------------------------------- --
 -- ----------------------------------------------------------------------- --
 --                                                                         --
 -- Simple ordered tree implementation in Haskell                           --
 -- Copyright (C) 2014 Ryan Porterfield                                     --
 --                                                                         --
 -- This program is free software; you can redistribute it and/or           --
 -- modify it under the terms of the GNU General Public License             --
 -- as published by the Free Software Foundation; either version 2          --
 -- of the License, or (at your option) any later version.                  --
 --                                                                         --
 -- This program is distributed in the hope that it will be useful,         --
 -- but WITHOUT ANY WARRANTY; without even the implied warranty of          --
 -- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           --
 -- GNU General Public License for more details.                            --
 --                                                                         --
 -- You should have received a copy of the GNU General Public License       --
 -- along with this program; if not, write to the                           --
 -- Free Software Foundation, Inc.,                                         --
 -- 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.           --
 --                                                                         --
 -- ----------------------------------------------------------------------- --
 -- ----------------------------------------------------------------------- --}


-- Optional: Define concatonation function

{-
data Tree = T [Tree] deriving (Eq, Read, Show)

e1                  =   T [T [], T []]

e2                  =   T [e1, e1, e1]

e3                  =   T [e1, e2]


nbLeaves (T [])     =   1
nbLeaves (T cs)     =   sum (map nbLeaves cs)

nbNodes (T [])      =   1
nbNodes (T cs)      =   1 + sum (map nbNodes cs)

nbIntNodes (T [])   =   0
nbIntNodes (T cs)   =   1 + sum (map nbIntNodes cs)
-}


data Tree a = T a [Tree a] deriving (Eq, Read, Show)

e1                  =   T "hello" [T "bye" [], T "soon" []]

e2                  =   T "words" [e1, e1, e1]

e3                  =   T "more" [e1, e2]

i1                  =   T 10 [T 9 [], T 20 []]

nbLeaves (T _ [])   =   1
nbLeaves (T _ cs)   =   sum (map nbLeaves cs)

nbNodes (T _ [])    =   1
nbNodes (T _ cs)    =   1 + sum (map nbNodes cs)

nbIntNodes (T _ []) =   0
nbIntNodes (T _ cs) =   1 + sum (map nbIntNodes cs)

leavesOf (T l [])   =   [l]
leavesOf (T _ cs)   =   concat (map leavesOf cs)

{-- ----------------------------------------------------------------------- --
 -- ----------------------------------------------------------------------- --
 --                                                                         --
 --                                   EOF                                   --
 --                                                                         --
 -- ----------------------------------------------------------------------- --
 -- ----------------------------------------------------------------------- --}
