{-
 - Simple ordered tree implementation in Haskell
 - Copyright (C) 2014 Ryan Porterfield
 -
 - This program is free software; you can redistribute it and/or
 - modify it under the terms of the GNU General Public License
 - as published by the Free Software Foundation; either version 2
 - of the License, or (at your option) any later version.
 -
 - This program is distributed in the hope that it will be useful,
 - but WITHOUT ANY WARRANTY; without even the implied warranty of
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 - GNU General Public License for more details.
 -
 - You should have received a copy of the GNU General Public License
 - along with this program; if not, write to the
 - Free Software Foundation, Inc.,
 - 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 -}

data Ops        =   Num Int | Plus Ops Ops | Mul Ops Ops
        deriving (Eq, Read, Show)

t1              =   Plus
                    (Mul (Num 3) (Num 4))
                    (Plus ( Mul (Num 2) (Num 5))
                        (Num 100)
                    )

eval :: Ops -> Int
eval (Num n)    =   n
eval (Mul x y)  =   (eval x) * (eval y)
eval (Plus x y) =   (eval x) + (eval y)

{-- ----------------------------------------------------------------------- --
 -- ----------------------------------------------------------------------- --
 --                                                                         --
 --                                   EOF                                   --
 --                                                                         --
 -- ----------------------------------------------------------------------- --
 -- ----------------------------------------------------------------------- --}
