{-- ----------------------------------------------------------------------- --
 -- ----------------------------------------------------------------------- --
 --                                                                         --
 -- Author: Ryan Porterfield <RyanPorterfield@my.unt.edu>                   --
 -- Date:   2014-04-01                                                      --
 --                                                                         --
 -- TESTS:                                                                  --
 --                                                                         --
 -- > taut1 tautXor                                                         --
 -- True                                                                    --
 -- > taut1 tautNot2                                                        --
 -- True                                                                    --
 -- > taut1 tautf                                                           --
 -- True                                                                    --
 --                                                                         --
 -- > sat1 satis1                                                           --
 -- True                                                                    --
 -- > sat2 satis2                                                           --
 -- True                                                                    --
 -- > sat2 satis3                                                           --
 -- True                                                                    --
 --                                                                         --
 -- > unsat1 contXor                                                        --
 -- True                                                                    --
 -- > unsat1 contNor                                                        --
 -- True                                                                    --
 -- > unsat2 contf                                                          --
 -- True                                                                    --
 -- ----------------------------------------------------------------------- --
 -- ----------------------------------------------------------------------- --}

{-- ----------------------------------------------------------------------- --
 --                           Provided Functions                            --
 -- ----------------------------------------------------------------------- --}

-- definition of basic gates
data Prop = T | F |
   Not Prop |
   And Prop Prop |
   Or Prop Prop 
   deriving (Eq,Read,Show)

-- truth tables of basic gates
   
tt (Not F) = T
tt (Not T) = F

tt (And F F) = F
tt (And F T) = F
tt (And T F) = F
tt (And T T) = T

tt (Or F F) = F
tt (Or F T) = T
tt (Or T F) = T
tt (Or T T) = T

-- giving the tt of a derived gate
xor' F F = F
xor' F T = T
xor' T F = T
xor' T T = F

-- building the derived gate from Not, And, Or
xor x y = eval (And (Or x y) (Not (And x y)))

-- evaluating expressions made of logic gates

eval T = T
eval F = F
eval (Not x) = tt (Not (eval x))  
eval (And x y) = tt (And (eval x) (eval y))
eval (Or x y) = tt (Or (eval x) (eval y))

ite c t e = eval (Or (And c t) (And (Not c) e))
   
truthTable1 f = [(x,f x)|x<-[F,T]]

tt1 f = mapM_ print (truthTable1 f)
   
truthTable2 f = [((x,y),f x y)|x<-[F,T],y<-[F,T]]

tt2 f = mapM_ print (truthTable2 f)

truthTable3 f = [((x, y, z),f x y z)|x<-[F,T],y<-[F,T],z<-[F,T]]

tt3 f = mapM_ print (truthTable3 f)

or' x y = eval (Or x y)
and' x y = eval (And x y)
not' x = eval (Not x)

impl x y = eval (Or (Not x) y)

eq x y = eval (And (impl x y) (impl y x))

deMorgan1 x y = eq (Not (And x y)) (Or (Not x) (Not y))
deMorgan2 x y = eq (Not (Or x y)) (And (Not x) (Not y))


-- tautologies, satisfiable and unsatisfiable formulas

taut1 f = all (==T) [f x|x<-[F,T]]

taut2 f = all (==T) [f x y|x<-[F,T],y<-[F,T]]

sat1 f = any (==T) [f x|x<-[F,T]]

sat2 f = any (==T) [f x y|x<-[F,T],y<-[F,T]]

unsat1 f = not (sat1 f)

unsat2 f = not (sat2 f)

-- examples of tautologies: de Morgan1,2
-- examples of satisfiable formulas: xor, impl, ite


{-- ----------------------------------------------------------------------- --
 --                         User defined functions                          --
 -- ----------------------------------------------------------------------- --}

nand' :: Prop -> Prop -> Prop
nand' x y   = eval (Not (And x y))

nor' :: Prop -> Prop -> Prop
nor' x y    = eval (Not (Or x y))

-- Tautologies
-- x XOR Not x
tautXor :: Prop -> Prop
tautXor x   = eval (xor x (Not x))

-- Double negation
tautNot2 :: Prop -> Prop
tautNot2 x  = eval (eq x (Not (Not x)))

-- Distributive Property
tautf :: Prop -> Prop
tautf x     = eval (Not (eq x (Not x)))

-- Satisfiable formulas
satis1 :: Prop -> Prop
satis1 x    = eval (impl x (Not x))

satis2 :: Prop -> Prop -> Prop
satis2 x y  = eval (ite x y (Not x))

satis3 :: Prop -> Prop -> Prop
satis3 x y  = eval (impl (ite x y T) x)

-- Unsatisfiable formulas
-- x XORed with itself
contXor :: Prop -> Prop
contXor x   = eval (xor x x)

-- X NANDed with its inverse
contNor :: Prop -> Prop
contNor x   = eval (nor' x (Not x))

-- 
contf :: Prop -> Prop -> Prop
contf x y   = eval (eq (And x y) (nand' x y))

{-- ----------------------------------------------------------------------- --
 --                                 EOF                                     --
 -- ----------------------------------------------------------------------- --}
