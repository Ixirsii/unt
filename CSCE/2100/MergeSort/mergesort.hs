{-
 - Simple merge-sort implementation in Haskell
 - Copyright (C) 2014 Ryan Porterfield
 -
 - This program is free software; you can redistribute it and/or
 - modify it under the terms of the GNU General Public License
 - as published by the Free Software Foundation; either version 2
 - of the License, or (at your option) any later version.
 -
 - This program is distributed in the hope that it will be useful,
 - but WITHOUT ANY WARRANTY; without even the implied warranty of
 - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 - GNU General Public License for more details.
 -
 - You should have received a copy of the GNU General Public License
 - along with this program; if not, write to the
 - Free Software Foundation, Inc.,
 - 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 -}

{-
 - Usage:
 - > mergeSort [3, 1, 4, 5, 0, 1, 1, 91, 1, 0]
 - [0, 0, 1, 1, 1, 1, 3, 4, 5, 91]
 -}

-- Recursive sorting function
mergeSort []        = []
mergeSort [x]       = [x]
mergeSort xs        =  merge (mergeSort as) (mergeSort bs) where
                        (as, bs) = split xs

-- Recursive list-splitting function
split []            = ([], [])
split [x]           = ([x], [])
split (x:y:zs)      = (x:xs, y:ys) where
                        (xs,ys) = split zs

-- Recursive list-merging function
merge [] ys         = ys
merge xs []         = xs
merge (x:xs) (y:ys) | x <= y =
                        x : merge xs (y:ys)
merge xs (y:ys)     = y : merge xs ys
