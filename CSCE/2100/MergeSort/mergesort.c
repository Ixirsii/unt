#include <stdio.h>
#include <stdlib.h>

typedef struct node {
    void        *e;
    size_t      e_size;
    struct node *next;
} node_t, *list;

list Merge(list list1, list list2);
list Split(list list);
list MergeSort(list list);
list MakeList(void *, size_t);
void PrintList(list list);

int
main (int argc, const char **argv)
{
    return EXIT_SUCCESS;
}

list
MakeList (void *e, size_t e_size)
{
    list l;
    l = malloc (sizeof (list));
    l->e = e;
    l->e_size = e_size;
    l->next = NULL;
}
