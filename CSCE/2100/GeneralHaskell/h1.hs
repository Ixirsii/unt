module Peano where

data Nat = Zero | S Nat deriving (Read,Show)

eq :: Nat -> Nat -> Bool
eq Zero Zero  = True
eq Zero (S _) = False
eq (S _) Zero = False
eq (S x) (S y) = eq x y

int2nat :: Integer -> Nat
int2nat 0 = Zero
int2nat n | n>0 = S (int2nat (n-1))

nat2int :: Nat -> Integer
nat2int Zero = 0
nat2int (S n) = 1+(nat2int n)

add :: Nat -> Nat -> Nat
add Zero n = n
add (S x) y = S (add x y)

mul :: Nat -> Nat -> Nat
mul Zero _ = Zero
mul (S x) y = add y (mul x y)

-- assertions

bij1 :: Integer -> Bool
bij1 i = nat2int (int2nat i) == i

bij2 :: Nat -> Bool
bij2 n = eq (int2nat (nat2int n)) n
 

{- Assignment 1 due: Feb 3 at noon

Define the following operations using Peano arithmetic
and the data type Nat in Haskell.

difference a-b assuming a>=b
comparison (<,>)
power

Write 5 tests for each, using numbers larger than 100 and verify
using nat2int and int2nat that your numbers work the same way as
the usual natural numbers.

-}

{- Subtraction -}

sub :: Nat -> Nat -> Nat
sub x Zero      = x
sub (S x) (S y) = sub x y

{- Comparison -}

-- Less Than
lt :: Nat -> Nat -> Bool
lt Zero Zero    = False
lt _ Zero       = False
lt Zero _       = True
lt (S x) (S y)  = lt x y

-- Greater Than
gt :: Nat -> Nat -> Bool
gt Zero Zero    = False
gt _ Zero       = True
gt Zero _       = False
gt (S x) (S y)  = gt x y

{- Power -}

-- 0^0 = 1 so we match anything to the power 0 first
pow :: Nat -> Nat -> Nat
pow _ Zero      = (int2nat 1)
pow Zero _      = (int2nat 0)
pow x (S y)     = (mul x (pow x y))


-- Subtraction tests
test_sub = do
        putStr "100 - 0 = "
        print (nat2int (sub (int2nat 100) (int2nat 0)))
        putStr "200 - 50 = "
        print (nat2int (sub (int2nat 200) (int2nat 50)))
        putStr "400 - 150 = "
        print (nat2int (sub (int2nat 400) (int2nat 150)))
        putStr "400 - 100 = "
        print (nat2int (sub (int2nat 100) (int2nat 100)))
        putStr "250 - 249 = "
        print (nat2int (sub (int2nat 250) (int2nat 249)))

-- Less than tests
test_lt = do
        putStr "100 < 1000? "
        print ((int2nat 100) `lt` (int2nat 1000))
        putStr "100 < 100? "
        print ((int2nat 100) `lt` (int2nat 100))
        putStr "1000 < 100? "
        print ((int2nat 1000) `lt` (int2nat 100))
        putStr "250 < 249? "
        print ((int2nat 250) `lt` (int2nat 249))
        putStr "249 < 250? "
        print ((int2nat 249) `lt` (int2nat 250))

-- Greater than tests
test_gt = do
        putStr "100 > 1000? "
        print ((int2nat 100) `gt` (int2nat 1000))
        putStr "100 > 100? "
        print ((int2nat 100) `gt` (int2nat 100))
        putStr "1000 > 100? "
        print ((int2nat 1000) `gt` (int2nat 100))
        putStr "250 > 249? "
        print ((int2nat 250) `gt` (int2nat 249))
        putStr "249 > 250? "
        print ((int2nat 249) `gt` (int2nat 250))

-- Exponential tests
test_pow = do
        putStr "0^0 = "
        print (nat2int ((int2nat 0) `pow` (int2nat 0)))
        putStr "0^4 = "
        print (nat2int ((int2nat 0) `pow` (int2nat 4)))
        putStr "4^0 = "
        print (nat2int ((int2nat 2) `pow` (int2nat 0)))
        putStr "2^10 = "
        print (nat2int ((int2nat 2) `pow` (int2nat 10)))
        putStr "10^3 = "
        print (nat2int ((int2nat 10) `pow` (int2nat 3)))

-- Run tests for homework
main :: IO ()
main = do
        test_sub
        test_lt
        test_gt
        test_pow

-- EOF
