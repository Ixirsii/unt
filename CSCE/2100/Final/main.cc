/** *********************************************************************** **
 ** *********************************************************************** **
 **                                                                         **
 ** @brief      Program provided in class final to test output of classes   **
 **                                                                         **
 ** @author     Ryan Porterfield                                            **
 ** @license    BSD 3 Clause                                                **
 ** @file                                                                   **
 **                                                                         **
 ** *********************************************************************** **
 ** *********************************************************************** **/

#include "animal.h"

int
main (void)
{
    Mammal *m = new Mammal ("Bruno");
    Dog *d1 = new Dog (m->get_name());
    Dog d2(d1->get_name());
    Bird *b = new Bird ("Tweety");
    Bird *p1 = (Bird *) (new Penguin (b->get_name ()));
    Penguin *p2 = new Penguin (p1->get_name());

    m->action();
    d1->action();
    d2.action();
    b->action();
    p1->action();
    p2->action();
    return 0;
}
