/** *********************************************************************** **
 ** *********************************************************************** **
 **                                                                         **
 ** @brief      Program provided in class final to test output of classes   **
 **                                                                         **
 ** @author     Ryan Porterfield                                            **
 ** @license    BSD 3 Clause                                                **
 ** @file                                                                   **
 **                                                                         **
 ** *********************************************************************** **
 ** *********************************************************************** **/


#include <iostream>
#include <string>

#ifndef ANIMAL_H
#define ANIMAL_H


class Animal
{
public:
    Animal  (std::string name) : name_(name)    {}
    ~Animal ()                                  {}
    std::string         get_name    ()          { return name_; }
    virtual void        action      ()          = 0;

private:
    const std::string   name_;
};


class Mammal : public Animal
{
public:
    Mammal  (std::string name) : Animal (name)  {}
    ~Mammal ()                                  {}
    virtual void        action      ()          { std::cout << get_name () << " says Bye!" << std::endl; }
};


class Dog : public Mammal
{
public:
    Dog     (std::string name) : Mammal (name)  {}
    ~Dog    ()                                  {}
    virtual void        action      ()          { std::cout << get_name () << " says Good Morning!" << std::endl; }
};


class Bird: public Animal
{
public:
    Bird    (std::string name) : Animal (name)  {}
    ~Bird   ()                                  {}
    void                action      ()          { std::cout << get_name() << " says Hello!" << std::endl; }
};


class Penguin : public Mammal
{
public:
    Penguin (std::string name) : Mammal (name)  {}
    ~Penguin()                                  {}
    void                action      ()          { std::cout << get_name() << " says Good Night!" << std::endl; }
};


#endif // ANIMAL_H

/** *********************************************************************** **
 **                                   EOF                                   **
 ** *********************************************************************** **/
