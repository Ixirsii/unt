Assignment 05
=============

+ Due Wed April 2 at noon.

+ Programming language to be used: Haskell.

+ e1, e2, e3 and e4 to be syntactically compliant with your new data type for
sum and product expressions.

Requirements
------------
Using the data type MTree in file mtrees.hs:

1.  Write an arithmetic evaluator  of expression trees of sums and products. 
    Note that each sum (marked with label "+") or product (marked with labels
    "*") acts upon a subtree that can have 0 or more operands.
    A sum with 0 operands should return 0 and a product with 0 operands should
    return 1.

2.  Test your code on examples e1, e2, e3 and e4 and paste the output as a
    haskell comment in your source file.

3.  The data type MTree accepts anything as labels for the operations.
    Define a more precise data type such that only sums and products can be
    entered as operators. Modify examples

