module MTrees where

-- ordered trees of "polymorphic" type a b
-- (all Leaf labels should have the same type
-- (all Branch labels should have the same type)
-- but the two could be different

data Ops        = Num | Plus | Mul deriving (Eq, Read, Show)

data Tree a b   = Leaf a | Branch b [Tree a b] deriving (Eq, Read, Show)


e1              = Branch Plus [
                    Leaf 10, Leaf 20, Branch Mul [
                        Leaf 2, Leaf 3, Leaf 4
                    ], Leaf 50
                ]

e2              = Branch Mul [
                    e1, e1, e1
                ]

e3              = Branch Plus [
                    Leaf 0, e1, e2, Leaf 100
                ]

e4              = Branch Mul [
                    e1, Branch Plus [
                        Branch Plus [], Branch Mul []
                    ]
                ]

e5              = Branch Mul [Leaf 10, Leaf 20]

-- see assignment5.txt for assignment using this file
 
