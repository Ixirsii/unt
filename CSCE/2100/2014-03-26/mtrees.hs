module MTrees where

e1 = Branch "+" [Leaf 10, Leaf 20, Branch "*" [Leaf , Leaf , Branch []]]

-- See assignment5.txt for assignment using this file

height (Leaf _) = 1
height (Branch _ []) = 1
height (Branch _ ts) = 1 + maximum (map height ts)

myMax [x] = x
myMax (x:xs) = max2 x (myMax xs) where
    max2 x y = 
