-- import Data.Tree
data Tree a = Node [Tree a] deriving (Eq, Show, Read)

e1 = Node 1 [
        Node 2 [], Node 3 [
            Node 4 [], Node 5 [
                Node 6, [], Node 7 []
            ]
        ],
        Node 8 [
            Node 9 []
        ]
    ]

-- Neat 2D drawing of a forest.
drawTree :: Tree String -> String
drawTree = unlines . draw

drawForest = unlines . map drawTree

--draw :: Tree a -> String
draw (Node x ts0) = x : drawSubTrees ts0 where
    drawSubTrees [t] = "|" : shift "`- " "   " (draw t)
    drawSubTrees (t:ts) = "|" : shift "+- " "|  " (draw t) ++ drawSubTrees ts 

    shift first other = zipWith (++) (first : repeat other)

showTree t = putStrLn (drawTree t)

go = showTree e1
