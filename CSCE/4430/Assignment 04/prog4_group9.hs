{-
  Ryan Porterfield
  Group 09
  CSCE 4430 Fall 2016
  Assignment 04

  Copyright (c) 2016, Ryan Porterfield
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

      1. Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
      2. Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
      3. Neither the name of the copyright holder nor the names of its
      contributors may be used to endorse or promote products derived from
      this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
  IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
  TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
  PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
  HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
  TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-}

{-
  The number of leaves in a tree is always 1 more than the number of branches.

  In the first base case there are 0 branches and 1 leaf.
  In the second base case there is 1 branch and 2 leaves.
  In the third base case there is a root branch with 1 leaf and another branch.
  The child branch has 2 leaves for a total of 2 branches and 3 leaves.

  Every branch of a btree can be considered the root of a smaller btree, and
  every branch much eventually terminate in one of the base cases. As such, all
  btrees will have 1 more leaf than branch.
-}

-- |Simple binary tree structure which holds no data
data BTree = Leaf | Branch BTree BTree deriving (Eq, Show, Read)

{- |
  Generate all btrees of size n.
  A tree of size 1 is just a leaf. A tree of size 2 or more is a branch with
  subtrees of size L and R where L is 1..n-1 and r is n - l
-}
genTrees :: Int -> [BTree]
genTrees n | n <= 0 = []
genTrees 1 = [Leaf]
genTrees n =
    [Branch l r | i <- [1 .. n - 1], l <- genTrees i, r <- genTrees (n - i)]

-- |Count the number of internal nodes (branches) in a btree
countBranches :: BTree -> Int
countBranches Leaf = 0
countBranches (Branch left right) = 1 + (countBranches left) + (countBranches right)

-- |Count all the leaves in a btree
countLeaves :: BTree -> Int
countLeaves Leaf = 1
countLeaves (Branch left right) = (countLeaves left) + (countLeaves right)

{- -------------------------------------------------------------------------- -
 -                                    EOF                                     -
 - -------------------------------------------------------------------------- -}
