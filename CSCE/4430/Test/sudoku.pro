% Sudoku for kids in pure Prolog
% author: Paul Tarau, Sept 2009
% license: free to all

/*
- go.

true.
*/

c:-['sudoku.pro'].

s4x4([
 [
    [S11,S12, S13,S14],
    [S21,S22, S23,S24],

    [S31,S32, S33,S34],
    [S41,S42, S43,S44]
 ],
 [
    [S11,S21, S31,S41],
    [S12,S22, S32,S42],

    [S13,S23, S33,S43],
    [S14,S24, S34,S44]
 ],
 [
    [S11,S12, S21,S22],
    [S13,S14, S23,S24],

    [S31,S32, S41,S42],
    [S33,S34, S43,S44]
 ]
]):-
 % here you can add clues like S11=3, S41=2, etc.
 % tested with SWI-Prolog - uses maplist/2
 true.

% this generates the 288 puzzles
% and solves one if given clues
sudoku(Xss):-
 s4x4(Xsss),
 Xsss=[Xss|_],
 maplist(maplist(permute([1,2,3,4])),Xsss).

permute([],[]).
permute([X|Xs],Zs):-
  writeln(Zs),
  permute(Xs,Ys),
  ins(X,Ys,Zs).

ins(X,Xs,[X|Xs]).
ins(X,[Y|Xs],[Y|Ys]):-ins(X,Xs,Ys).

% test
go:-sudoku(Xss),
   nl,member(Xs,Xss),
   write(Xs),nl,fail
 ; nl.

/*
[
  [
    [_G261,_G264,_G267,_G270],
    [_G276,_G279,_G282,_G285],
    [_G291,_G294,_G297,_G300],
    [_G306,_G309,_G312,_G315]
  ],
  [
    [_G261,_G276,_G291,_G306],
    [_G264,_G279,_G294,_G309],
    [_G267,_G282,_G297,_G312],
    [_G270,_G285,_G300,_G315]
  ],
  [
    [_G261,_G264,_G276,_G279],
    [_G267,_G270,_G282,_G285],
    [_G291,_G294,_G306,_G309],
    [_G297,_G300,_G312,_G315]
  ]
]

[_G261,_G264,_G267,_G270],
[_G276,_G279,_G282,_G285],
[_G291,_G294,_G297,_G300],
[_G306,_G309,_G312,_G315]]
 */
