c:-['family.pro'].

/*
?- aunt_of('Joe','Helen').
false.

?- aunt_of('Joe','Miranda').
true.

?- uncle_of('Joe','Dave').
true 

?- uncle_of('Marie','Dave').
true .

?- uncle_of('Marie','Bill').
false.

?- uncle_of('Marie','Mike').
false.

?- ancestor_of('Joe','Bill').
true .

?- ancestor_of('Joe','Helen').
true .

?- ancestor_of('Joe','Marie').
false.

?- ancestor_of('Joe','Mike').
true .

?- ancestor_of('Joe','Miranda').
false.

?- ancestor_of('Joe','Vanessa').
true .
*/

% facts

male('Joe').
male('Bill').
male('Paul').
male('Dave').

female('Marie').
female('Helen').
female('Miranda').
female('Vanessa').

parent_of('Joe','Helen').
parent_of('Joe','Bill').
parent_of('Marie','Helen').
parent_of('Marie','Bill').
parent_of('Bill','Mike').
parent_of('Mike','Vanessa').
parent_of('Helen','Adam').
parent_of('Dave','Adam').
parent_of('Paul','Miranda').
parent_of('Miranda','Mike').


% rules
sibling_of(X,S):-parent_of(X,P),parent_of(S,P),X\==S.

brother_of(X,B):-sibling_of(X,B),male(B).

sister_of(X,B):-sibling_of(X,B),female(B).

mother_of(X,M):-parent_of(X,M),female(M).

father_of(X,M):-parent_of(X,M),male(M).

gp_of(X,GP):-parent_of(X,P),parent_of(P,GP).

ancestor_of(X,A):-
  parent_of(X,A);
  (parent_of(X,P),
   ancestor_of(P,A)).

aunt_of(X,A):-
  gp_of(X,GP),
  parent_of(A, GP),
  not(parent_of(X,A)),
  female(A).

cousin_of(X,C):-
  gp_of(X,GP),
  gp_of(C,GP),
  X\==C,
  not(sibling_of(X,C)).

uncle_of(X,U):-
  gp_of(X,GP),
  parent_of(U, GP),
  not(parent_of(X,U)),
  male(U).

