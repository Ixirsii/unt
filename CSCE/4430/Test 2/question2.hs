{-
  Ryan Porterfield
  CSCE 4430 Fall 2016
  Test 2
-}

import System.IO.Error

-- append :: [String] -> String -> [String]
-- append xs "" = xs
-- append [] y = [y]
-- append (x:xs) y = x : append xs y

-- first = [1, 2, 3]

-- f :: [Int] -> [String]
-- f = map show

-- worm = append (f first) "worm"
-- eel = f [1, 2, 3, 1, 2, 3, 1, 2, 3]
-- snake = append (f [3, 2, 1]) "snake"
-- whale = f [1..100]

worm = 1 : 2 : 3 : worm
eel = [1, 2, 3, 1, 2, 3, 1, 2, 3]
snake = 3 : 2 : 1 : snake
whale = [1..100]

match :: [Integer] -> [Integer] -> Bool
match [] [] = True
match _ [] = True
match [] _ = False
match (x : xs) (y : ys)
    | x == y    = match xs ys
    | otherwise = False

which :: [Integer] -> String
which [] = ""
which list
    | match list eel        = "eel"
    | match list whale      = "whale"
    | match list [1, 2, 3]  = "worm"
    | match list [3, 2, 1]  = "snake"
    | otherwise             = "none"

{- -------------------------------------------------------------------------- -
 -                                    EOF                                     -
 - -------------------------------------------------------------------------- -}