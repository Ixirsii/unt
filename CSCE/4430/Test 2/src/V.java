public class V {
	final private int id; 			// int defining the position of the vertex in an array of vertices
	final private String label; 	// meaningful to humans name given to this node
	private int[] incoming; 		// set of ids of incoming edges
	private int[] outgoing; 		// set of ids of outgoing edges
	double rank; 					// value in 0..1 computed by a ranking algorithm

	/**
	 * @param args Program arguments
	 */
	public static void main(String[] args) {
		V[] vs = {
			new V(0, "a", new int[] {3}, new int[] {}, 0.033),
			new V(1, "b", new int[] {2, 3, 4, 5, 6, 7, 8}, new int[] {2}, 0.384),
			new V(2, "c", new int[] {1}, new int[] {1}, 0.343),
			new V(3, "d", new int[] {4}, new int[] {0, 1}, 0.039),
			new V(4, "e", new int[] {5, 6, 7, 8, 9, 10}, new int[] {1, 3, 5}, 0.081),
			new V(5, "f", new int[] {4}, new int[] {1, 4}, 0.039),
			new V(6, "g", new int[] {}, new int[] {1, 4}, 0.016),
			new V(7, "h", new int[] {}, new int[] {1, 4}, 0.016),
			new V(8, "i", new int[] {}, new int[] {1, 4}, 0.016),
			new V(9, "j", new int[] {}, new int[] {4}, 0.016),
			new V(10, "k", new int[] {}, new int[] {4}, 0.016)
		};
		
		for (V v : vs)
			System.out.println(v.toString());
	}

	/**
	 * Create a new Vertex.
	 * 
	 * @param id		Vertex ID
	 * @param label		Vertex name
	 * @param incoming	Incoming links
	 * @param outgoing	Outgoing links
	 * @param rank		Vertex rank
	 */
	public V(int id, String label, int[] incoming, int[] outgoing, double rank) {
		this.id = id;
		this.label = label;
		this.incoming = incoming;
		this.outgoing = outgoing;
		this.rank = rank;
	}
	
	/**
	 * Puts an integer array to a string for printing.
	 * 
	 * @param array Integer array
	 * @param name Name of the array
	 * @return a string representation of an integer array
	 */
	private String arrayToString(int[] array, String name) {
		String str = "\t" + name + ": {\n\t\t";
		for (int in : array) {
			str += in + ", ";
		}
		str += "\n\t}\n";
		return str;
	}
	
	/**
	 * @see java.lang.Object#toString()
	 * @return a human readable representation of the vertex 
	 */
	@Override
	public String toString() {
		String str = id + ": " + label + "(" + rank + ")\n";
		str += arrayToString(incoming, "incoming");
		str += arrayToString(outgoing, "outgoing");
		return str;
	}
	
	/*
	 * Program output:
	 * 
	 * 0: a(0.033)
	 * 		 * incoming: {
	 * 		3, 
	 * 	}
	 * 	 * 	outgoing: {
	 * 		
	 * 	}
	 * 
	 * 1: b(0.384)
	 * 	incoming: {
	 * 		2, 3, 4, 5, 6, 7, 8, 
	 * 	}
	 * 	outgoing: {
	 * 		2, 
	 * 	}
	 * 
	 * 2: c(0.343)
	 * 	incoming: {
	 * 		1, 
	 * 	}
	 * 	outgoing: {
	 * 		1, 
	 * 	}
	 * 
	 * 3: d(0.039)
	 * 	incoming: {
	 * 		4, 
	 * 	}
	 * 	outgoing: {
	 * 		0, 1, 
	 * 	}
	 * 
	 * 4: e(0.081)
	 * 	incoming: {
	 * 		5, 6, 7, 8, 9, 10, 
	 * 	}
	 * 	outgoing: {
	 * 		1, 3, 5, 
	 * 	}
	 * 
	 * 5: f(0.039)
	 * 	incoming: {
	 * 		4, 
	 * 	}
	 * 	outgoing: {
	 * 		1, 4, 
	 * 	}
	 * 
	 * 6: g(0.016)
	 * 	incoming: {
	 * 		
	 * 	}
	 * 	outgoing: {
	 * 		1, 4, 
	 * 	}
	 * 
	 * 7: h(0.016)
	 * 	incoming: {
	 * 		
	 * 	}
	 * 	outgoing: {
	 * 		1, 4, 
	 * 	}
	 * 
	 * 8: i(0.016)
	 * 	incoming: {
	 * 		
	 * 	}
	 * 	outgoing: {
	 * 		1, 4, 
	 * 	}
	 * 
	 * 9: j(0.016)
	 * 	incoming: {
	 * 		
	 * 	}
	 * 	outgoing: {
	 * 		4, 
	 * 	}
	 * 
	 * 10: k(0.016)
	 * 	incoming: {
	 * 		
	 * 	}
	 * 	outgoing: {
	 * 		4, 
	 * 	}
	 * 
	 * In Swift:
	 * 		- you don't have to have a main() method, so all the executable code can go in the top level
	 *  	- the function for console output is print(), not System.out.println()
	 *  	- Arrays can be appended with +=, so you could append incoming & outgoing nodes to the arrays
	 *  		instead of having to pass them to the constructor 
	 */
}