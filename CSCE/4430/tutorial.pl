likes(ryan, brittney).
likes(brittney, ryan).
likes(josh, brittney).

dating(X, Y) :- likes(X, Y), likes(Y, X).
friendship(X, Y) :- likes(X, Y); likes(Y, X).
