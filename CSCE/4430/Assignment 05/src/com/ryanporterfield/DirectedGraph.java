/* ************************************************************************** *
 * ************************************************************************** *
 *                                                                            *
 * Copyright (c) 2016, Ryan Porterfield                                       *
 * All rights reserved.                                                       *
 *                                                                            *
 * Redistribution and use in source and binary forms, with or without         *
 * modification, are permitted provided that the following conditions         *
 * are met:                                                                   *
 *                                                                            *
 *     1. Redistributions of source code must retain the above copyright      *
 *     notice, this list of conditions and the following disclaimer.          *
 *                                                                            *
 *     2. Redistributions in binary form must reproduce the above copyright   *
 *     notice, this list of conditions and the following disclaimer in the    *
 *     documentation and/or other materials provided with the distribution.   *
 *                                                                            *
 *     3. Neither the name of the copyright holder nor the names of its       *
 *     contributors may be used to endorse or promote products derived from   *
 *     this software without specific prior written permission.               *
 *                                                                            *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS    *
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED      *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A            *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT         *
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED   *
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR     *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF     *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING       *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS         *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.               *
 *                                                                            *
 * ************************************************************************** *
 * ************************************************************************** */

/**
 * @file
 * @author Ryan Porterfield
 * @copyright BSD 3-Clause
 * @since 2016-11-22
 */
package com.ryanporterfield;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

/**
 * @class DirectedGraph
 * 
 * A graph with directional edges, but without weighted edges.
 */
public class DirectedGraph<E> {
	/** Map of nodes to lists of connected nodes (edges). */
	final private HashMap<E, ArrayList<E>> adjacencyList;
	
	/**
	 * Construct an empty graph with the default backing HashMap.
	 */
	public DirectedGraph() {
		adjacencyList = new HashMap<>();
	}
	
	/**
	 * Add a node to the graph with no outgoing edges.
	 * 
	 * @param elem Node to be added to the graph
	 * @return the initial list of edges from the node
	 */
	public boolean add(E elem) {
		return add(elem, new ArrayList<E>());
	}
	
	/**
	 * Add a node to the graph with some initial outgoing edges.
	 * 
	 * @param elem Node to be added to the graph
	 * @param edges List of outgoing edges from the node
	 * @return the initial list of edges from the node
	 */
	public boolean add(E elem, ArrayList<E> edges) {
		if (contains(elem))
			return false;
		return adjacencyList.put(elem, edges) == null;
	}
	
	/**
	 * Add an edge to two existing nodes in the graph.
	 * 
	 * @param first Node with the outgoing edge
	 * @param second Node with the incoming edge
	 * @return @c true if the edge was successfully added, otherwise false.
	 */
	public boolean addEdge(E first, E second) {
		if (!adjacencyList.containsKey(first) || !adjacencyList.containsKey(second))
			return false;
		if (adjacencyList.get(first).contains(second))
			return false;
		return adjacencyList.get(first).add(second);
	}
	
	/**
	 * Check if the graph contains a node.
	 * 
	 * @param elem The element being checked
	 * @return true if the graph contains the specified element
	 */
	public boolean contains(E elem) {
		return adjacencyList.containsKey(elem);
	}
	
	/**
	 * Get the number of edges in the graph.
	 * 
	 * @return the number of edges in the graph
	 */
	public int edges() {
		int count = 0;
		for (E elem : adjacencyList.keySet())
			count += adjacencyList.get(elem).size();
		return count;
	}
	
	/**
	 * Dumb hack to make sure that the nodes in the mapped ArrayList are the
	 * same as the keys in adjacencyList.
	 * 
	 * @param elem Element equal to the element being retrieved from the graph
	 * @return An element equal to parameter elem which is already stored in the graph
	 */
	public E get(E elem) {
		for (E e : adjacencyList.keySet()) {
			if (e.equals(elem))
				return e;
		}
		return null;
	}
	/**
	 * Get the elements connected to an element.
	 * 
	 * @param elem
	 * @return a list of elements with edges to elem
	 * @throws IllegalArgumentException
	 */
	public final ArrayList<E> getEdges(E elem) {
		return adjacencyList.get(elem);
	}
	
	public void printGraph() {
		for (E node : adjacencyList.keySet()) {
			System.out.print(node + " -> {\n\t");
			for (E adj : adjacencyList.get(node))
				System.out.print(adj + ", ");
			System.out.println("\n}");
		}
	}
	
	/**
	 * Removes a node from the graph, if it is present. If the graph does not
	 * contain the node, it is unchanged. Returns true if this graph contained
	 * the specified node (or equivalently, if this graph changed as a result
	 * of the call).
	 * 
	 * @param elem Node to be removed from the graph, if present
	 * @return true if this graph contained the specified node
	 */
	public ArrayList<E> remove(E elem) {
		for (E e : adjacencyList.keySet())
			adjacencyList.get(e).remove(elem);
		return adjacencyList.remove(elem);
	}
	
	/**
	 * Removes an edge from the graph, if it is present. If the graph does not
	 * contain the edge, it is unchanged. Returns true if this graph contained
	 * the specified edge (or equivalently, if this graph changed as a result
	 * of the call).
	 * 
	 * @param first Outgoing node of the edge
	 * @param second Incoming node of the edge
	 * @return true if this graph contained the specified edge
	 */
	public boolean removeEdge(E first, E second) {
		return adjacencyList.get(first).remove(second);
	}
	
	/**
	 * @return the set of all vertexes in the graph
	 */
	public Set<E> vertexSet() {
		return adjacencyList.keySet();
	}
	
	/**
	 * Get the number of vertices in the graph.
	 * 
	 * @return the number of vertices in the graph
	 */
	public int vertexCount() {
		return adjacencyList.keySet().size();
	}

}
