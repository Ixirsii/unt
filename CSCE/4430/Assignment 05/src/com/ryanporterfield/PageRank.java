/*
 * Copyright (c) 2016, Ryan Porterfield
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *     1. Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *     3. Neither the name of the copyright holder nor the names of its
 *     contributors may be used to endorse or promote products derived from
 *     this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * @file
 * @author Ryan Porterfield
 * @copyright BSD 3-Clause
 * @since 2016-11-30
 */
package com.ryanporterfield;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * 
 */
public class PageRank {
	static final public double DAMPING_FACTOR = 0.85;
	static final public int DEFAULT_ITERATIONS = 50;
	static final public int DEFAULT_THREAD_COUNT = 2;
	
	/** Number of times to run the algorithm on the data set. */
	final private int iterations;
	/** Number of threads to use for updating page ranks. */
	final private int threadCount;
	/** Directed graph whose nodes are web pages and edges are links to other pages. */
	final private DirectedGraph<Site> graph;
	
	/**
	 * 
	 * @param graph Directed graph whose nodes are web pages and edges are links to other pages.
	 */
	public PageRank(DirectedGraph<Site> graph) {
		this(graph, DEFAULT_ITERATIONS, DEFAULT_THREAD_COUNT);
	}
	
	public PageRank(DirectedGraph<Site> graph, int iterations) {
		this(graph, iterations, DEFAULT_THREAD_COUNT);
	}
	
	/**
	 * 
	 * @param graph Directed graph whose nodes are web pages and edges are links to other pages.
	 * @param iterations Number of times to run the algorithm on the data set.
	 * @param threadCount Number of threads to use for updating page ranks.
	 */
	public PageRank(DirectedGraph<Site> graph, int iterations, int threadCount) {
		this.graph = graph;
		this.iterations = iterations;
		this.threadCount = threadCount;
	}
	
	private void init() {
		double initialRank = 1 / (double) graph.vertexCount();
		for (Site site : graph.vertexSet())
			site.setRank(initialRank);
	}
	
	/**
	 * 
	 */
	public void run() {
		init();
		ArrayList<List<Site>> subSets = getSubLists();
		try {
			for (int i = 0; i < iterations; ++i) {
				ExecutorService executor = Executors.newFixedThreadPool(threadCount);
				for (int j = 0; j < threadCount; ++j) {
					Worker w = new Worker(subSets.get(j));
					executor.execute(w);
				}
				executor.shutdown();
				executor.awaitTermination(5, TimeUnit.SECONDS);
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
	}
	
	public ArrayList<List<Site>> getSubLists() {
		ArrayList<Site> allSites = new ArrayList<>(graph.vertexSet());
		ArrayList<List<Site>> subSets = new ArrayList<>();
		int subLength = allSites.size() / threadCount;
		for (int i = 0; i < threadCount - 1; ++i) {
			subSets.add(allSites.subList(i * subLength, (i + 1) * subLength));
		}
		// Hack to make sure ever element gets added, even if subLength isn't a
		// multiple of allSites.size()
		subSets.add(allSites.subList((threadCount - 1) * subLength, allSites.size()));
		return subSets;
	}
	
	private class Worker implements Runnable {
		final private List<Site> vertices;
		
		public Worker(List<Site> vertices) {
			this.vertices = vertices;
		}
		
		private ArrayList<Site> getIncomingLinks(Site site) {
			ArrayList<Site> incoming = new ArrayList<>();
			for (Site s: graph.vertexSet()) {
				ArrayList<Site> outgoing = graph.getEdges(s);
				if (outgoing.contains(site))
					incoming.add(s);
			}
			return incoming;
		}
		
		@Override
		public void run() {
			for (Site site : vertices) {
				ArrayList<Site> incoming = getIncomingLinks(site);
				site.setRank(updateRank(incoming));
			}
		}
		
		/**
		 * 
		 * @param incomingLinks
		 */
		public double updateRank(ArrayList<Site> incomingLinks) {
			double newRank = 0;
			for (Site incomingLink : incomingLinks) {
				newRank += incomingLink.sendRank(graph.getEdges(incomingLink).size());
			}
			return ((1 - DAMPING_FACTOR) / graph.vertexCount()) + (DAMPING_FACTOR * newRank);
		}
	}

}
