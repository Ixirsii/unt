/* ************************************************************************** *
 * ************************************************************************** *
 *                                                                            *
 * Copyright (c) 2016, Ryan Porterfield                                       *
 * All rights reserved.                                                       *
 *                                                                            *
 * Redistribution and use in source and binary forms, with or without         *
 * modification, are permitted provided that the following conditions         *
 * are met:                                                                   *
 *                                                                            *
 *     1. Redistributions of source code must retain the above copyright      *
 *     notice, this list of conditions and the following disclaimer.          *
 *                                                                            *
 *     2. Redistributions in binary form must reproduce the above copyright   *
 *     notice, this list of conditions and the following disclaimer in the    *
 *     documentation and/or other materials provided with the distribution.   *
 *                                                                            *
 *     3. Neither the name of the copyright holder nor the names of its       *
 *     contributors may be used to endorse or promote products derived from   *
 *     this software without specific prior written permission.               *
 *                                                                            *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS    *
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED      *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A            *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT         *
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED   *
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR     *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF     *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING       *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS         *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.               *
 *                                                                            *
 * ************************************************************************** *
 * ************************************************************************** */

/**
 * @file
 * @author Ryan Porterfield
 * @copyright BSD 3-Clause
 * @since 2016-11-21
 */
package com.ryanporterfield;

/**
 * @class
 */
public final class Site {
	final private String name;
	
	/**  */
	volatile private double rank;
	
	public Site(String name) {
		this(name, 0);
	}
	
	/**
	 * 
	 * @param name
	 * @param rank
	 */
	public Site(String name, double rank) {
		this.name = name;
		this.rank = rank;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Site other = (Site) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	
	/**
	 * @return the name of the site
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * 
	 * @return
	 */
	public synchronized double getRank() {
		return rank;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}
	
	/**
	 * 
	 * @param outgoingLinks
	 * @return
	 */
	public final double sendRank(int outgoingLinks) {
		return rank / (double) outgoingLinks;
	}
	
	/**
	 * 
	 * @param rank
	 */
	public synchronized void setRank(double rank) {
		this.rank = rank;
	}
	
	@Override
	public String toString() {
		return name + " : " + rank;
	}
}