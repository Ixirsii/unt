/*
 * Copyright (c) 2016, Ryan Porterfield
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *     1. Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *     2. Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *     3. Neither the name of the copyright holder nor the names of its
 *     contributors may be used to endorse or promote products derived from
 *     this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * @file
 * @author Ryan Porterfield
 * @copyright BSD 3-Clause
 * @since 2016-11-29
 */
package com.ryanporterfield;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Builds a graph of sites and links from an input file.
 */
public class GraphBuilder {
	final private DirectedGraph<Site> graph;
	final private String graphFile;
	final private Stop stop;
	
	/**
	 * Create a new GraphBuilder to generate a graph.
	 * 
	 * @param stop List of stop words which are not links
	 * @param graphFile File name of the input file to build the graph from
	 */
	public GraphBuilder(Stop stop, String graphFile) {
		this.graph = new DirectedGraph<>();
		this.graphFile = graphFile;
		this.stop = stop;
		readGraph();
	}
	
	/**
	 * @return the graph generated by the builder
	 */
	public DirectedGraph<Site> graph() {
		return graph;
	}
	
	/**
	 * Get a BufferedReader from the file name.
	 * 
	 * @param graphFile File name of the input file to build the graph from
	 * @return a BufferedReader which can read in the input file
	 * @throws IOException
	 */
	private BufferedReader openGraphFile(String graphFile) throws IOException {
		FileInputStream instream = new FileInputStream(graphFile);
		return new BufferedReader(new InputStreamReader(new DataInputStream(instream)));
	}
	
	/**
	 * Read the input file and build the graph.
	 */
	private void readGraph() {
		try {
			BufferedReader reader = openGraphFile(graphFile);
			String line;
			while ((line = reader.readLine()) != null)
				readLine(line);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Read a line from the input file and add the nodes and links.
	 * 
	 * @param line A non-null line from the input file
	 */
	private void readLine(String line) {
		line = line.replaceAll("[^a-zA-Z ]", "").toLowerCase();
		Site site = new Site(line);
		if (!graph.contains(site))
			graph.add(site);
		else
			site = graph.get(site);
		String[] words = line.split("\\s+");
		for (String word : words) {
			if (stop.isStop(word))
				continue;
			Site tmp = new Site(word);
			if (!graph.contains(tmp))
				graph.add(tmp);
			else
				tmp = graph.get(tmp);
			graph.addEdge(site, tmp);
			graph.addEdge(tmp, site);
		}
	}

}
