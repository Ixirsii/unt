/* ************************************************************************** *
 * ************************************************************************** *
 *                                                                            *
 * Copyright (c) 2016, Ryan Porterfield                                       *
 * All rights reserved.                                                       *
 *                                                                            *
 * Redistribution and use in source and binary forms, with or without         *
 * modification, are permitted provided that the following conditions         *
 * are met:                                                                   *
 *                                                                            *
 *     1. Redistributions of source code must retain the above copyright      *
 *     notice, this list of conditions and the following disclaimer.          *
 *                                                                            *
 *     2. Redistributions in binary form must reproduce the above copyright   *
 *     notice, this list of conditions and the following disclaimer in the    *
 *     documentation and/or other materials provided with the distribution.   *
 *                                                                            *
 *     3. Neither the name of the copyright holder nor the names of its       *
 *     contributors may be used to endorse or promote products derived from   *
 *     this software without specific prior written permission.               *
 *                                                                            *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS    *
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED      *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A            *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT         *
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED   *
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR     *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF     *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING       *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS         *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.               *
 *                                                                            *
 * ************************************************************************** *
 * ************************************************************************** */

/**
 * @file
 * @author Ryan Porterfield
 * @copyright BSD 3-Clause
 * @since 2016-11-21
 */
package com.ryanporterfield;

import java.util.HashSet;
import java.util.Set;

/*
 * Program output:
 * 
 * tasso : 0.004646972743506632
 * bunker : 0.0025977044613662505
 * ash : 0.0025895344394016194
 * claws : 0.00248012481972906
 * klaus : 0.004544978087795909
 * came : 0.0030569746507376587
 * hendricks : 0.011394011075409575
 * said : 0.0045037628198254355
 * come : 0.002568311266351602
 * way : 0.0032772430419908947
 * 
 */

/**
 * @class Main
 */
public class Prog5_Group9 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		if (args.length < 2) {
			System.err.println("Error: Incorrect number of arguments\nArguments:\n");
			System.err.println("\t<stop file> <graph file>");
			System.exit(1);
		}
		Stop stops = new Stop(args[0]);
		GraphBuilder builder = new GraphBuilder(stops, args[1]);
		DirectedGraph<Site> graph = builder.graph();
		PageRank ranker = new PageRank(graph, PageRank.DEFAULT_ITERATIONS, 10);
		ranker.run();
		Set<Site> maxSet = getMaxSet(graph, 10);
		for (Site site: maxSet) {
			System.out.println(site.getName() + " : " + site.getRank());
		}
	}
	
	private static Set<Site> getMaxSet(DirectedGraph<Site> graph, int count) {
		Set<Site> sites = new HashSet<>(graph.vertexSet());
		Set<Site> maxSet = new HashSet<>();
		for (int i = 0; i < count; ++i) {
			Site max = null;
			for (Site site : sites) {
				if (max == null)
					max = site;
				else if (site.getRank() > max.getRank())
					max = site;
			}
			maxSet.add(max);
			sites.remove(max);
		}
		return maxSet;
	}

}
