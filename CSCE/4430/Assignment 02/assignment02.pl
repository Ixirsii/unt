/*
 * Ryan Porterfield
 * CSCE 4430 Fall 2016
 * Assignment 02
 */
c:-['assignment02.pro'].

go:-forall(share(S,[]), show(S)).

show(Xs):-intersperse(Xs,' ',Ys),atomic_list_concat(Ys,S),writeln(S).

/*
Excuse me [sir|madam].
Do you have a [second|minute|hour] to talk about our lord and savior [Cthulhu|Sauron|The Doctor]
*/

person('sir').
person('madam').
person('child').
p-->{person(P)},[P].

time('second').
time('minute').
time('hour').
time('day').
t-->{time(T)},[T].

savior('Cthulhu').
savior('Sauron').
savior('The Doctor').
savior('Trigger').
savior('Captain Kirk').
savior('No Face').
s-->{savior(S)},[S].

intersperse(Xs,B,Rs):-intersperse(Xs,B,Rs,[]).
intersperse([],_)-->[].
intersperse([X|Xs],B)-->[X],[B],intersperse(Xs,B).

share-->
  ['Excuse me'],p,['. Do you have a'],t,['to talk about our lord and savior'],s,['?\n'].