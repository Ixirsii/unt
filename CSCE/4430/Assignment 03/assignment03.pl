/*
 * Ryan Porterfield
 * CSCE 4430 Fall 2016
 * Assignment 03
 *
 * To run the program, open it in swipl and call the run/0 function.
 *
 * I wasn't able to get vertices with no outgoing links to split their rank
 * with every other vertex, so the numbers are off a little bit. Everything
 * else works though.
 *
 * ?- run.
 * [0.027645934714267257,0.32419916505504714,0.2892056539331537,0.03296369665389087,0.06821411653244908,0.03296369665389087,0.013636363636363636,0.013636363636363636,0.013636363636363636,0.013636363636363636,0.013636363636363636]
 * true.
 */

% Facts

vertex(a).
vertex(b).
vertex(c).
vertex(d).
vertex(e).
vertex(f).
vertex(g).
vertex(h).
vertex(i).
vertex(j).
vertex(k).

edge(d, a).
edge(d, b).
edge(b, c).
edge(c, b).
edge(e, d).
edge(e, b).
edge(e, f).
edge(f, e).
edge(f, b).
edge(g, b).
edge(g, e).
edge(h, b).
edge(h, e).
edge(i, b).
edge(i, e).
edge(j, e).
edge(k, e).

% Rules

c:-['assignment03.pl'].

get_rank(V, R):-
  vertex(V),
  rank(V, R).

init_ranks:-
  findall(Value, vertex(Value), Vertices),
  num_vertices(N),
  Initial is 1/N,
  maplist(set_rank(Initial), Vertices).

list_sum([], 0).
list_sum([H|T], Sum) :-
   list_sum(T, Rest),
   Sum is H + Rest.

outgoing(Vertex, Count):-
  findall(Value, edge(Vertex, Value), Edges),
  length(Edges, Count).

num_vertices(N):-
  findall(Value, vertex(Value), Vertices),
  length(Vertices, N).

run:-
  init_ranks,
  run_loop(30),
  findall(Value, vertex(Value), Vertices),
  maplist(get_rank, Vertices, Ranks),
  writeln(Ranks).

run_loop(Count):-
  forall(upto(1, Count, V), update_ranks).

send_rank(V, R):-
  get_rank(V, Rank),
  outgoing(V, Count),
  R is Rank / Count.

set_rank(R, V):-
  vertex(V),
  retractall(rank(V, _)),
  assertz(rank(V, R)).

update_rank(V):-
  findall(Value, edge(Value, V), Edges),
  maplist(send_rank, Edges, Ranks),
  list_sum(Ranks, RankSum),
  num_vertices(N),
  NewRank is (0.15/N)+(0.85*RankSum),
  set_rank(NewRank, V).

update_ranks:-
  findall(Value, vertex(Value), Vertices),
  maplist(update_rank, Vertices).

upto(Low, High, Low):- Low =< High.
upto(Low, High, Var):-
    Inc is Low+1,
    Inc =< High,
    upto(Inc, High, Var).

% EOF
