/* ************************************************************************** *
 * ************************************************************************** *
 * Copyright (c) 2016, Ryan Porterfield                                       *
 * All rights reserved.                                                       *
 *                                                                            *
 * Redistribution and use in source and binary forms, with or without         *
 * modification, are permitted provided that the following conditions         *
 * are met:                                                                   *
 *                                                                            *
 *     1. Redistributions of source code must retain the above copyright      *
 *     notice, this list of conditions and the following disclaimer.          *
 *                                                                            *
 *     2. Redistributions in binary form must reproduce the above copyright   *
 *     notice, this list of conditions and the following disclaimer in the    *
 *     documentation and/or other materials provided with the distribution.   *
 *                                                                            *
 *     3. Neither the name of the copyright holder nor the names of its       *
 *     contributors may be used to endorse or promote products derived from   *
 *     this software without specific prior written permission.               *
 *                                                                            *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS    *
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED      *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A            *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT         *
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED   *
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR     *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF     *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING       *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS         *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.               *
 * ************************************************************************** *
 * ************************************************************************** */

/* Final ranks after running the algorithm:
 *
 * A: 0.0300299385459852
 * B: 0.379366978177349
 * C: 0.338418790338391
 * D: 0.0385731174342839
 * E: 0.0798220889879918
 * F: 0.038573117434242
 * G: 0.0159568588876443
 * H: 0.0159568588876443
 * I: 0.0159568588876443
 * J: 0.0159568588876443
 * K: 0.0159568588876443
 */

class Site : Equatable {
    /*! Likelihood that a web surfer will jump to a random page*/
    static let dampingFactor:Double = 0.85
    /*! List of all sites */
    static var allSites:[Site] = []

    /*! Name of the site */
    private let name:String
    /*! PageRank value */
    private var rank:Double
    /*! Links from the site to other sites */
    private var outgoingLinks:[Site]

    /*! \brief      Create a new Site
     *
     * \param name  Name of the site
     */
    init(name:String) {
        self.name = name
        self.rank = 0
        self.outgoingLinks = []
        Site.allSites.append(self)
    }
  
    /*! \brief      Set the initial page rank of every site
     */
    static public func initRank() {
        let initialRank:Double = 1 / Double(allSites.count)
        for site in allSites {
            site.setRank(rank: initialRank)
        }
    }
  
    /*! \brief      Update the PageRank of every site
     */
    static public func updateRanks() {
        for site in allSites {
            var recievedRank:Double = 0;
            for s in allSites {
                recievedRank += s.sendRank(site: site)
            }
            let newRank:Double = ((1 - dampingFactor) / Double(allSites.count)) + (dampingFactor * recievedRank)
            site.setRank(rank: newRank)
        }
    }
  
    /*! \brief      Check if 2 sites are equal
     *
     * Uses the name of the site to check if it's the same as another site.
     * \param lhs   Left hand side of the equality check
     * \param rhs   Right hand side of the equality check
     * \return      true if the sites are the same, otherwise false
     */
    static public func ==(lhs:Site, rhs:Site) -> Bool {
        return lhs.name == rhs.name
    }
  
    /*! \brief      Add an outgoing link to the site
     *
     * \param site  A site which is linked to from this site
     */
    public func addOutgoingLink(site:Site) {
        self.outgoingLinks.append(site)
    }
  
    /*! \brief      Add multiple outgoing links to the site
     *
     * \param sites An array of sites which are linked to from this site
     */
    public func addOutgoingLinks(sites:[Site]) {
        self.outgoingLinks += sites
    }
  
    /*! \brief      Get the site's name
     *
     * \return      the site's name
     */
    public func getName() -> String {
        return name
    }
  
    /*! \brief      Get the site's PageRank
     *
     * \return      the site's PageRank
     */
    public func getRank() -> Double {
        return rank
    }
  
    /*! \brief      Get the rank that this site will send to another site
     *
     * \param site  A site in the collection
     * \return      0 if site is this site or if this site has outgoing links,
     *              but does not link to site. If this site does link to site,
     *              returns this site's PageRank / # out-going links.
     */
    private func sendRank(site:Site) -> Double {
        var send:Double = 0
        if site == self {
            send =  0
        } else if outgoingLinks.isEmpty {
            send = rank / Double(Site.allSites.count)
        } else if outgoingLinks.contains(site) {
            send = rank / Double(outgoingLinks.count)
        }
        return send
    }
  
    /*! \brief      Set a new PageRank
     *
     * \param rank  New PageRank for this site
     */
    private func setRank(rank:Double) {
        self.rank = rank
    }
}

// Initialize sites
let a = Site(name: "A")
let b = Site(name: "B")
let c = Site(name: "C")
let d = Site(name: "D")
let e = Site(name: "E")
let f = Site(name: "F")
let g = Site(name: "G")
let h = Site(name: "H")
let i = Site(name: "I")
let j = Site(name: "J")
let k = Site(name: "K")
// Add links to sites
b.addOutgoingLink(site: c)
c.addOutgoingLink(site: b)
d.addOutgoingLinks(sites: [a, b])
e.addOutgoingLinks(sites: [b, d, f])
f.addOutgoingLinks(sites: [b, e])
g.addOutgoingLinks(sites: [b, e])
h.addOutgoingLinks(sites: [b, e])
i.addOutgoingLinks(sites: [b, e])
j.addOutgoingLink(site: e)
k.addOutgoingLink(site: e)
// Initialize PageRanks
Site.initRank()
// Update PageRanks
for i in 0...29 {
    Site.updateRanks()
}
// Print final PageRanks
for site in Site.allSites {
    print("\(site.getName()): \(site.getRank())")
}
