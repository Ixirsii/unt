;;;; Ryan Porterfield
;;;; Lab 15 - Q Learning

;;; ************************************************************************ ;;;
;;;                                Functions                                 ;;;
;;; ************************************************************************ ;;;

(defun get-cell (arr coords)
  (if (and (listp coords) (equal (length coords) 2))
    (aref arr (first coords) (second coords))
    nil))

(defun get-num (arr row)
  (remove nil (_get-num arr row 0)))

(defun _get-num (arr row indx)
  (if (>= indx (second (array-dimensions arr))) nil
      (if (numberp (aref arr row indx))
          (cons (list row (aref arr row indx)) (_get-num arr row (1+ indx)))
          (_get-num arr row (1+ indx)))))

(defun get-q (row)
  (_get-q row 0))

(defun _get-q (row indx)
  (if (>= indx (second (array-dimensions q-array))) 0
    (max (aref q-array row indx) (_get-q row (1+ indx)))))

(defun get-moves (state)
  (list
    (if (= (mod state 3) 0) NIL (- state 1))
    (if (= (mod (+ state 1) 3) 0) NIL (+ state 1))
    (if (> (+ state 3) 5) NIL (+ state 3))
    (if (< (- state 3) 0) NIL (- state 3))))

(defun get-random (lst)
  (nth (random (length lst)) lst))

(defun get-start-state ()
  (let* ((arr-dims (array-dimensions table-array))
          (dimX (first arr-dims)))
    (random dimX)))

(defun initialize ()
  (setf  r .5)  ; r is your discount value
  (setf table-array (make-array '(6 4) :initial-element nil))
  (setf q-array (make-array  '(6 6) :initial-element 0))
  (setf r-array (make-array '(6 6) :initial-element 0)))

(defun initialize-r ()
  (setf (aref r-array 2 5) 100)
  (setf (aref r-array 4 5) 100))

(defun initialize-state (array-name)
  (loop for i from 0 to 5 do
    (let ((temp (get-moves i)))
      (setf (aref array-name i 0) (first temp))
      (setf (aref array-name i 1) (second temp))
      (setf (aref array-name i 2) (third temp))
      (setf (aref array-name i 3) (fourth temp)))))

(defun print-array (array)
  (let ((arr-dims (array-dimensions array)))
    (dotimes (row (first arr-dims))
      (dotimes (column (second arr-dims))
        (format t "~5,1f " (aref array row column)))
      (format t "~%"))))

(defun run (state)
  (cond ((equal state 5) nil)
        (t (let* ((next (get-random (get-num table-array state)))
                  (q-val (get-q (second next)))
                  (new-q (+ (aref r-array (first next) (second next)) (* r q-val))))
          (setf (aref q-array (first next) (second next)) new-q)
          (run (second next))))))

;;; ************************************************************************ ;;;
;;;                                   EOF                                    ;;;
;;; ************************************************************************ ;;;