;;; Ryan Porterfield
;;; Assignment 07 - Adventure Game Pt. II
;;; Copyright (c) 2016, Ryan Porterfield
;;; All rights reserved.
;;;
;;; Redistribution and use in source and binary forms, with or without
;;; modification, are permitted provided that the following conditions
;;; are met:
;;;
;;;     1. Redistributions of source code must retain the above copyright
;;;     notice, this list of conditions and the following disclaimer.
;;;
;;;     2. Redistributions in binary form must reproduce the above copyright
;;;     notice, this list of conditions and the following disclaimer in the
;;;     documentation and/or other materials provided with the distribution.
;;;
;;;     3. Neither the name of the copyright holder nor the names of its
;;;     contributors may be used to endorse or promote products derived from
;;;     this software without specific prior written permission.
;;;
;;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
;;; IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
;;; TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
;;; PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
;;; HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
;;; SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
;;; TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
;;; PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
;;; LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
;;; NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
;;; SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


;;; ************************************************************************ ;;;
;;;                                Functions                                 ;;;
;;; ************************************************************************ ;;;


;; Get a property from a closure
(defun ask (object message)
  (funcall object message))


;; Entity closure
(defun entity (name description)
	(lambda (message)
	  (cond ((equal message 'name) name)
			    ((equal message 'description) description)))) 


;; Moveable entity closure
(defun moveable_entity (entity location)
	(lambda (message)
    (let ((name (ask entity 'name)))
      (cond ((equal message 'name) name)
            ((equal message 'description) (ask entity 'description))
            ((equal message 'location) location)
            ((equal message 'move) (lambda (newlocation)
              (send (eval location) 'delete_item name)
              (setq location newlocation)
              (send (eval location) 'add_item name)))))))


;; Initialize global variables
(defun initialize ()
  ;; People
  (setf albert
    (make_person 'albert "CSCE 3210 student" 'union '(money)))
  (setf mary
    (make_person 'mary "CSCE 3210 student" 'computerlab '(knowledge)))
  (setf player
    (make_person 'ryan "CSCE 3210 student" 'dorm nil))
  (setf swigger
    (make_person 'swigger "CSCE 3210 professor" 'library '(knowledge)))
  ;; Locations
  (setf audb
    (make_location 'audb "English building" nil '(Dormitory ComputerLab)))
  (setf bank
    (make_location 'bank "Currency exchange" '(money) '(union)))
  (setf dormitory
    (make_location 'dormitory "Student residence" '(id) '(audb union)))
  (setf computerlab
    (make_location 'computerlab "Publicly accessible computers" '(computers)
                   '(audb library)))
  (setf library
    (make_location 'library "Place where you can check out books" nil
                   '(ComputerLab)))
  (setf union
    (make_location 'union "Student services" nil '(dormitory bank)))
  ;; Items
  (setf money (make_item 'money "currency" 'bank))
  nil)


;; Inventory closure
(defun inventory (items)
  (lambda (message)
		(cond ((equal message 'inventory) items)
          ((equal message 'delete_item) (lambda (item)
            (setf items (delete item items))))
          ((equal message 'add_item) (lambda (item)
            (setq item (append items (list item))))))))


;; Item closure
(defun item (moveable_entity item_type)
	(lambda (message)
    (cond ((equal message 'name) (ask moveable_entity 'name))
          ((equal message 'description) (ask moveable_entity 'description))
          ((equal message 'location) (ask moveable_entity 'location))
          ((equal message 'item_type) item_type)
          ((equal message 'move)
            (lambda (newlocation)
              (send (eval moveable_entity) 'move newlocation))))))


;; Location closure
(defun location (entity inventory nearby)
  (lambda (message)
		(cond ((equal message 'name) (ask entity 'name))
          ((equal message 'description) (ask entity 'description))
          ((equal message 'nearby) nearby)
          ((equal message 'inventory) (ask inventory 'inventory))
          ((equal message 'delete_item)
            (lambda (item)
              (send inventory 'delete_item item)))
          ((equal message 'add_item)
            (lambda (item)
              (send inventory 'add_item item))))))


;;
(defun look_around ()
  (let ((l (player 'location)))
    (l 'inventory)))

;;
(defun make_item (name description location)
  (let* ((base_entity (entity name description))
         (base_move (moveable_entity base_entity location)))
    (item base_move nil)))


;;
(defun make_location (name description items nearby)
  (let* ((base_entity (entity name description))
         (base_inv (inventory items)))
    (location base_entity base_inv nearby)))


;;
(defun make_person (name description location items)
  (let* ((base_entity (entity name description))
         (base_move (moveable_entity base_entity location))
         (base_inv (inventory items)))
    (person base_move base_inv)))


;; Person closure
(defun person (moveable_entity inventory)
	(lambda (message)
	  (cond ((equal message 'name) (ask moveable_entity 'name))
          ((equal message 'description) (ask moveable_entity 'description))
          ((equal message 'location) (ask moveable_entity 'location))
          ((equal message 'inventory) (ask inventory 'inventory))
          ((equal message 'move-to)
            (lambda (newlocation)
              (send (eval moveable_entity) 'move newlocation)))
          )))


;;
(defun send (object message &rest args)
	(apply (ask object message) args))


;;; ************************************************************************ ;;;
;;;                                   EOF                                    ;;;
;;; ************************************************************************ ;;;
