;;;; Ryan Porterfield
;;;; Assignment 08 - Arrays
;;;; Copyright (c) 2016, Ryan Porterfield
;;;; All rights reserved.
;;;;
;;;; Redistribution and use in source and binary forms, with or without
;;;; modification, are permitted provided that the following conditions
;;;; are met:
;;;;
;;;;     1. Redistributions of source code must retain the above copyright
;;;;     notice, this list of conditions and the following disclaimer.
;;;;
;;;;     2. Redistributions in binary form must reproduce the above copyright
;;;;     notice, this list of conditions and the following disclaimer in the
;;;;     documentation and/or other materials provided with the distribution.
;;;;
;;;;     3. Neither the name of the copyright holder nor the names of its
;;;;     contributors may be used to endorse or promote products derived from
;;;;     this software without specific prior written permission.
;;;;
;;;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
;;;; IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
;;;; TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
;;;; PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
;;;; HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
;;;; SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
;;;; TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
;;;; PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
;;;; LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
;;;; NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
;;;; SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

;;; ************************************************************************ ;;;
;;;                                Functions                                 ;;;
;;; ************************************************************************ ;;;;; Finds the path with the best cost

;;; \brief  Add weighted edge(s) to a vertex
;;;
;;; This builds the graph of connected nodes
;;; \param location Vertex
;;; \param wbranches List of weighted branches
;;; \return NIL
(defun addwbranches (location wbranches)
  (setf (get (intern (string (write-to-string location))) 'connected-to) wbranches)
  nil)

;;; \brief  Convert the transitions matrix and Q matrix to a list
;;;
;;; Sets the property 'connected-to for every state to a list of pairs where
;;; each pair contains a transition from the current state, and the reward for
;;; that transition
;;; \param transitions Transition matrix
;;; \param q-matrix Q matrix
;;; \return NIL
;;; \sa _convert-to-list
(defun convert-to-list (transitions q-matrix)
  (_convert-to-list transitions q-matrix 0))

;;; \brief  Convert the transitions matrix and Q matrix to a list
;;;
;;; Recursively sets the property 'connected-to for every state to a list of
;;; pairs where each pair contains a transition from the current state, and the
;;; reward for that transition
;;; \param transitions Transition matrix
;;; \param q-matrix Q matrix
;;; \param indx Index (current state) used for recursion
;;; \return NIL
(defun _convert-to-list (transitions q-matrix indx)
  (cond ((>= indx (first (array-dimensions transitions))) nil)
        (t (addwbranches indx
    (get-weighted-branches indx (get-num transitions indx) q-matrix))
    (_convert-to-list transitions q-matrix (1+ indx)))))

;;; \brief Find the path with the best cost
;;;
;;; This function was provided by Ms. Swiggger
;;; \param paths - a list of paths from the start to the end vertices
;;; \return the path with the best cost
(defun cost (paths)
  (let ((bestpath nil)
        (temppath 0))
    (setf bestpath (cons (apply '+ (mapcar 'rest (first paths))) (first paths)))
    (setf paths (rest paths))
    (loop
      (if (null paths) (return (rest bestpath)))
      (if (> (setf temppath (apply '+ (mapcar 'rest (first paths))))
             (first bestpath))
        (setf bestpath (cons temppath (first paths))))
      (setf paths (rest paths)))))

;;; Run a cost first search of the graph
;;;
;;; \param start The vertex to start searching at
;;; \param goal The vertex to search for/get to
;;; \return the path with the best cost
(defun cost-first (start goal)
  (let ((current nil) (paths nil))
    (setq paths (list (list (cons start 0))))
    (loop
      (setq current (cost paths)) ; cost helper function
      (cond
        ((null paths) (return nil))
        ((match (first (first current)) goal)
          (return (reverse current)))
        (t (setq paths (append
          (remove current paths :test #'equal)
          (filter current paths))))))))

;;; \brief  Run an episode
;;;
;;; Transitions to a random state from the current state, updating the Q-matrix
;;; along the way until it reaches the goal state of 62.
;;; \param state Current state
;;; \return NIL
(defun episode (state)
  (cond ((equal state 62) nil)
        (t (let* ((next (get-random (get-num table-array state)))
                  (q-val (get-q next))
                  (new-q (+ (aref r-array state next) (* r q-val))))
          (setf (aref q-array state next) new-q)
          (episode next)))))

;;; \brief  Get a list of connected vertices
;;;
;;; \param path Current path
;;; \return a list of connected vertices
(defun expanpaths (path)
  (mapcar (lambda (nextpath) (cons nextpath path))
    (get (intern (string (write-to-string (first (first path))))) 'connected-to)))

;;; \brief  Filters out nodes which are already in the search path.
;;;
;;; Filters out nodes which are already in the search path.
;;; This prevents the search algorithms from running infinitely along the same
;;; path.
;;; \param current ;
;;; \param paths ;
;;; \return 
(defun filter (current paths)
  (let* ((search-paths nil)
      (next-paths (expanpaths current))
      (next-nodes (mapcar (lambda (elem) (first elem)) next-paths))
      (node (first next-nodes))
      (path (first next-paths))
      (contains (mapcar (lambda (sublist) (list-contains node sublist)) paths)))
    (loop
      (cond ((null node) (return search-paths))
        ((not (member 'T contains))
          (setq search-paths (append search-paths (list path)))))
      (setq next-nodes (rest next-nodes))
      (setq next-paths (rest next-paths))
      (setq node (first next-nodes))
      (setq path (first next-paths))
      (setq contains (mapcar (lambda (sublist)
          (list-contains node sublist)) paths)))))

;;; \brief  Get the value of a cell in a matrix
;;;
;;; \param arr A matrix
;;; \param coords A list of the x and y coordinates
;;; \return the value of arr at coords
(defun get-cell (arr coords)
  (if (and (listp coords) (equal (length coords) 2))
    (aref arr (first coords) (second coords))
    nil))

;;; \brief  Get a list of integers from [0, rows)
;;;
;;; \param table A matrix used to generate the range
;;; \return A list containing every integer from 0 (inclusive) to the number of
;;;         rows in the matrix (exclusive)
;;; \sa _get-episodes-list
(defun get-episodes-list (table)
  (_get-episodes-list (first (array-dimensions table)) 0))

;;; \brief  Recursive algorithm which generates a range from [0, rows)
;;;
;;; \param len The maximum value (exclusive) of the range
;;; \param indx The current index of recursion
;;; \return A list containing every integer from 0 (inclusive) to the number of
;;;         rows in the matrix (exclusive)
(defun _get-episodes-list (len indx)
  (if (>= indx (1- len)) nil (cons indx (_get-episodes-list len (1+ indx)))))

;;; \brief  Get a list of possible state transitions
;;;
;;; \param arr A matrix containing possible state transitions
;;; \param row Current state
;;; \return a list of possible moves from one state to the next
;;; \sa _get-num
(defun get-num (arr row)
  (remove nil (_get-num arr row 0)))

;;; \brief  Recrursive algorithm to get a list of possible state transitions
;;;
;;; \param arr A matrix containing possible state transitions
;;; \param row Current state
;;; \return a list of possible moves from one state to the next
(defun _get-num (arr row indx)
  (if (>= indx (second (array-dimensions arr))) nil
      (if (numberp (aref arr row indx))
          (cons (aref arr row indx) (_get-num arr row (1+ indx)))
          (_get-num arr row (1+ indx)))))

;;; \brief  Get the maximum Q value for a state transition
;;;
;;; \param row Current state
;;; \retrun the maximum Q value for a state transition
;;; \sa _get-q
(defun get-q (row)
  (_get-q row 0))

;;; \brief  Recursive function to get the maximum Q value for a state transition
;;;
;;; \param row Current state
;;; \param indx The current index of recursion
;;; \retrun the maximum Q value for a state transition
(defun _get-q (row indx)
  (if (>= indx (second (array-dimensions q-array))) 0
    (max (aref q-array row indx) (_get-q row (1+ indx)))))

;;; \brief  Get a list of possible state transitions
;;;
;;; \param state Current state
;;; \return a list of possible state transitions
(defun get-moves (state)
  (list
    (if (= (mod state 8) 0) NIL (1- state)) ; Left
    (if (= (mod (1+ state) 8) 0) NIL (1+ state)) ; Right
    (if (> (+ state 8) 63) NIL (+ state 8)) ; Up
    (if (< (- state 8) 0) NIL (- state 8)))) ; Down

;;; \brief  Get a random element from a list
;;;
;;; \param lst A list
;;; \return a random element from a list
(defun get-random (lst)
  (nth (random (length lst)) lst))

;;; \brief  Get possible state transitions and rewards as a list
;;;
;;; \param state Current state
;;; \param transitions Transition matrix
;;; \param q-matrix Q matrix
;;; \return a list of pairs of state transitions from the current state, and
;;;         the reward for that transition
(defun get-weighted-branches (state transitions q-matrix)
  (if (null transitions) nil
    (append (list (cons (first transitions)
                      (get-cell q-matrix (list state (first transitions)))))
            (get-weighted-branches state (rest transitions) q-matrix))))

;;; \brief  Initialize program data
;;;
;;; \return NIL
(defun initialize ()
  (setf  r .5)  ; r is your discount value
  (setf table-array (make-array '(64 4) :initial-element nil))
  (setf q-array (make-array  '(64 64) :initial-element 0))
  (setf r-array (make-array '(64 64) :initial-element 0))
  nil)

;;; \brief  Initialize the reward matrix
;;;
;;; HAS SIDE EFFECTS!
;;; Modifies an array in-place, adding the initial reward values for state
;;; transitions.
;;; \param r-array Reward matrix (assumed to be 64x64)
;;; \return NIL
(defun initialize-r (r-array)
  (setf (aref r-array 61 62) 100)
  (setf (aref r-array 54 62) 100)
  (setf (aref r-array 63 62) 100)
  (setf (aref r-array 0 8) -1)
  (setf (aref r-array 9 8) -1)
  (setf (aref r-array 16 8) -1)
  (setf (aref r-array 8 16) -1)
  (setf (aref r-array 17 16) -1)
  (setf (aref r-array 42 16) -1)
  (setf (aref r-array 16 24) -1)
  (setf (aref r-array 25 24) -1)
  (setf (aref r-array 32 24) -1)
  (setf (aref r-array 24 32) -1)
  (setf (aref r-array 33 32) -1)
  (setf (aref r-array 40 32) -1)
  (setf (aref r-array 32 40) -1)
  (setf (aref r-array 41 40) -1)
  (setf (aref r-array 48 40) -1)
  (setf (aref r-array 40 48) -1)
  (setf (aref r-array 49 48) -1)
  (setf (aref r-array 56 48) -1)
  (setf (aref r-array 48 56) -1)
  (setf (aref r-array 57 56) -1)
  (setf (aref r-array 43 44) -1)
  (setf (aref r-array 52 44) -1)
  (setf (aref r-array 45 44) -1)
  (setf (aref r-array 36 44) -1)
  (setf (aref r-array 44 45) -1)
  (setf (aref r-array 53 45) -1)
  (setf (aref r-array 46 45) -1)
  (setf (aref r-array 37 45) -1)
  (setf (aref r-array 45 46) -1)
  (setf (aref r-array 54 46) -1)
  (setf (aref r-array 47 46) -1)
  (setf (aref r-array 38 46) -1)
  (setf (aref r-array 46 47) -1)
  (setf (aref r-array 55 47) -1)
  (setf (aref r-array 39 47) -1)
  (setf (aref r-array 12 13) -5)
  (setf (aref r-array 21 13) -5)
  (setf (aref r-array 14 13) -5)
  (setf (aref r-array 5 13) -5)
  (setf (aref r-array 48 49) -5)
  (setf (aref r-array 57 49) -5)
  (setf (aref r-array 50 49) -5)
  (setf (aref r-array 41 49) -5)
  nil)

;;; \brief  Initialize the transition matrix
;;;
;;; \param transitions Transition matrix
;;; \return NIL
(defun initialize-state (transitions)
  (loop for i from 0 to (1- (first (array-dimensions transitions))) do
    (let ((temp (get-moves i)))
      (setf (aref transitions i 0) (first temp))
      (setf (aref transitions i 1) (second temp))
      (setf (aref transitions i 2) (third temp))
      (setf (aref transitions i 3) (fourth temp))))
  nil)

;;; \brief  Helper function to see if an element is in a list.
;;;
;;; This function calls member, but returns T instead of the element
;;; \param elem element being searched for
;;; \param lst A list
;;; \return T if lst contains elem
(defun list-contains (elem lst)
  (if (member elem lst) t nil))

;;; \brief  Helper function for equals
;;;
;;; \param element Element 1
;;; \param pattern Element 2
;;; \return T if element equals pattern, otherwise NIL
(defun match (element pattern)
  (equal element pattern))

;;; \brief  Print a matrix
;;;
;;; \param matrix A matrix
;;; \return NIL
(defun print-array (matrix)
  (let ((arr-dims (array-dimensions matrix)))
    (dotimes (row (first arr-dims))
      (dotimes (column (second arr-dims))
        (format t "~5,1f " (aref matrix row column)))
      (format t "~%"))))

;;; \brief  Run each episode in a random order
;;;
;;; \param episodes A range of episodes
;;; \return NIL
;;; \sa episode
(defun run (episodes)
  (if (null episodes) nil
    (let ((ep (get-random episodes)))
      (print "episode ") (princ ep)
      (episode ep)
      (convert-to-list table-array q-array)
      (print (cost-first ep 62))
      (run (remove ep episodes)))))

;;; ************************************************************************ ;;;
;;;                                   EOF                                    ;;;
;;; ************************************************************************ ;;;
