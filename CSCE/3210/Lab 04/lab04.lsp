;;; Ryan Porterfield
;;; Lab 04 - Recursion

(defun countdown (start)
  (if (<= start 0) nil (append (list start) (countdown (1- start)))))

(defun replicate (size elem)
  (if (<= size 0) nil (append (list elem) (replicate (1- size) elem))))

(defun double-each (lst)
  (if (null lst) nil (cons (* (first lst) 2) (double-each (rest lst)))))