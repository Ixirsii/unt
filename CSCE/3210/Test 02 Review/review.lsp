;;;; Ryan Porterfield
;;;; CSCE 3210 Fall 2016 Test 2 Review


(defun attach (ls1 ls2) 
  (if (null ls1) nil
    (append (attach2 (first ls1) ls2) (attach (rest ls1) ls2))))

(defun attach2 (elem lis)
  (if (null lis) nil
    (append (list (list elem (first lis))) (attach2 elem (rest lis)))))

(defun make-object (name location top)
  (lambda (message)
    (cond ((equal message 'name) name)
      ((equal message 'location) location)
      ((equal message 'top) top)
      ((equal message 'remove-top) (lambda ()
        (if (not (equal top nil)) (setf top 'clear))))
      ((equal message 'place-above) (lambda (above)
        (if (not (equal top nil)) (setf top above) nil)))
      ((equal message 'move-to) (lambda (destination)
        (if (send destination 'place-above nanme) (setf location destination) nil))))))