;;; Ryan Porterfield
;;; Lab 10 - Uniform Cost Search

(defun addwbranches (location wbranches)
  (setf (get location 'connected-to) wbranches))

(defun cost (paths)
  (let ((bestpath nil)
    (temppath 0))
    (setf bestpath (cons (apply '+ (mapcar 'rest (first paths))) (first paths)))
    (setf paths (rest paths))
    (loop
      (if (null paths) (return (rest bestpath)))
      (if (< (setf temppath (apply '+ (mapcar 'rest (first paths))))
             (first bestpath))
        (setf bestpath (cons temppath (first paths))))
      (setf paths (rest paths)))))

(defun cost-first (start goal)
  (let ((current nil) (paths nil) (i 0))
    (setq paths (list (list (cons start 0))))
    (loop
      (print 'paths) (princ paths)
      (setq current (cost paths)) ; cost helper function
      (cond
        ((null paths) (return (values i nil)))
        ((match (first (first current)) goal)
          (return (values
            (reverse current)
            (cons '(nodes expanded) i))))
        (t (setq paths (append
          (remove current paths :test #'equal)
          (expanpaths current)))))
      (setq i (1+ i)))))

(defun expanpaths (path) 
  (mapcar (lambda (nextpath) (cons nextpath path)) 
          (get (first (first path)) 'connected-to)))

 (defun match (element pattern)
       (equal element pattern))


(addwbranches 'denton '((acity . 70) (bcity . 50)))
(addwbranches 'acity '((ccity . 80) (dcity . 140)))
(addwbranches 'bcity '((ecity . 120)))
(addwbranches 'ccity '((fcity . 200)))
(addwbranches 'ecity '((gcity . 70) ( hcity . 120) (icity . 140)))
(addwbranches 'gcity '((jcity . 110) (kcity . 40)))
(addwbranches 'hcity '((lcity . 150) (mcity . 40)))
(addwbranches 'icity '((ncity . 20) (ocity . 50)))