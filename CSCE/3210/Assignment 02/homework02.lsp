;;; Ryan Porterfield
;;; Homework 02

;;
(defun where-is (a)
  (first (member 'waldo a))
)

;;
(defun gross-receipts (adults children)
  (+ (* adults 7) (* children 4))
)

;;
(defun list-len (a)
  (cond ((null a) 0) (t (1+ (list-len (rest a)))))
)

;;
(defun longer-list (a b)
  (cond ((not (listp a)) "Error: first argument is not a list")
    ((not (listp b)) "Error: second argument is not a list")
    ((> (list-len a) (list-len b)) a)
    ((> (list-len b) (list-len a)) b)
  )
)

(defun between (a b c)
  (cond ((and (> a b) (> b c)) t)
        ((and (> c b) (> b a)) t)
        (t NIL)
  )
)

(defun is-triangle (a b c)
  (if (and (<= a (+ b c)) (<= b (+ a c)) (<= c(+ a b))) t nil)
)
