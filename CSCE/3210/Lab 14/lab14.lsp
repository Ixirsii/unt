;;;; Ryan Porterfield
;;;; Lab 14

;;; ************************************************************************ ;;;
;;;                                Problem 1                                 ;;;
;;; ************************************************************************ ;;;

;;; Wrapper function for _elem
(defun elem (e arr)
  (_elem e arr 0))

;;; Checks to see if e is an element in arr
;;; Works recursively by incrementing indx each call
(defun _elem (e arr indx)
  (cond ((>= indx (first (array-dimensions arr))) nil)
        ((equal (aref arr indx) e) e)
        (t (_elem e arr (1+ indx)))))

;;; Wrapper function for _find_common
(defun find_common (arr1 arr2)
  (let ((sublist (_find_common arr1 arr2 0)))
    (make-array (length sublist) :initial-contents sublist)))

;;; Finds elements which are present in both arr1 and arr2
;;; Works recursively by incrementing indx1 each call
(defun _find_common (arr1 arr2 indx1)
  (cond ((>= indx1 (first (array-dimensions arr1))) nil)
        (t (remove nil (append (list (elem (aref arr1 indx1) arr2))
                               (_find_common arr1 arr2 (1+ indx1)))))))


;;; ************************************************************************ ;;;
;;;                                Problem 2                                 ;;;
;;; ************************************************************************ ;;;

;;; Remove duplicates from an array
(defun setify (arr)
  (remove-duplicates arr))

;;; ************************************************************************ ;;;
;;;                                Problem 3                                 ;;;
;;; ************************************************************************ ;;;

;;; Wrapper function for _arr_substitute
(defun arr_substitute (arr)
  (let ((sublist (_arr_substitute arr 0)))
    (make-array (array-dimensions arr) :initial-contents sublist)))

;;; Substitute all b in a matrix with g
;;; Recurses over the rows of the matrix calling _col_substitute on each row
(defun _arr_substitute (arr xindx)
  (cond ((>= xindx (first (array-dimensions arr))) nil)
        (t (remove nil (append (list (remove nil (_col_substitute arr xindx 0)))
                   (_arr_substitute arr (1+ xindx)))))))

;;; Substitute all b in a matrix with g
;;; Recurses over the columns of 1 row of the matrix
(defun _col_substitute (arr xindx yindx)
  (cond ((>= xindx (first (array-dimensions arr))) nil)
        ((>= yindx (second (array-dimensions arr))) nil)
        ((equal (aref arr xindx yindx) 'b)
          (append (list 'g) (_col_substitute arr xindx (1+ yindx))))
        (t (append (list (aref arr xindx yindx))
                   (_col_substitute arr xindx (1+ yindx))))))

;;; ************************************************************************ ;;;
;;;                                Problem 4                                 ;;;
;;; ************************************************************************ ;;;

;;; Wrapper function for _arr_count
(defun arr_count (elem arr)
  (_arr_count elem arr 0))

;;; Count all the times an element occurs in an array
(defun _arr_count (elem arr indx)
  (cond ((>= indx (first (array-dimensions arr))) 0)
        ((equal (aref arr indx) elem) (1+ (_arr_count elem arr (1+ indx))))
        (t (_arr_count elem arr (1+ indx)))))

;;; Count the number of times the letter a occurs in an array
(defun count_a (arr)
  (arr_count 'a arr))

;;; ************************************************************************ ;;;
;;;                                   EOF                                    ;;;
;;; ************************************************************************ ;;;