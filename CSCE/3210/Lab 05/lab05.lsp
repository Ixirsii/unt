;;; Ryan Porterfield
;;; Lab 05 - Recursion 2

(defun add-only-numbers (lst)
  (cond ((null lst) 0)
    ((numberp (first lst)) (+ (first lst) (add-only-numbers (rest lst))))
    (t (add-only-numbers (rest lst))))
)

(defun remove-vowels (lst)
  (setq vowels '(a e i o u))
  (cond ((null lst) nil)
    ((member (first lst) vowels) (remove-vowels (rest lst)))
    (t (cons (first lst) (remove-vowels (rest lst)))))
)

(defun index (lst atm)
  (cond ((null lst) nil)
  ((eq atm (first lst)) 1)
  ((numberp (index (rest lst) atm)) (+ 1 (index (rest lst) atm)))
  (t nil))
)

(defun remove-by-index (lst index)
  (cond ((null lst) nil)
    ((= 1 index) (remove-by-index (rest lst) (- index 1)))
    (t (cons (first lst) (remove-by-index (rest lst) (- index 1)))))
)

(defun remove-from-list (input rmv)
  (cond ((null rmv) input)
    (t (remove-from-list (remove-by-index input (first rmv)) (rest rmv))))
)
