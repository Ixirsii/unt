;;; Ryan Porterfield
;;; Lab 07

;; Subtract 2 from each element in a list
(defun sub2 (input)
  (mapcar
    (lambda (x) (- x 2))
    input))

;; Get information about students with a specified major
(defun select (major year input)
  (remove nil 
    (mapcar
      (lambda (student)
        (if (eq major (second student))
          (list (first student) (- year (third student)))))
      input)))
