;;; Ryan Porterfield
;;; Lab 11

;; Get a property from a closure
(defun ask (object message)
  (funcall object message))

;; Initialize program data
(defun initialize ()
	(setf john (make-person 'john 'secretary 30 'companyx))
	(setf june (make-person 'june 'manager  nil 'companyx))
  (setf companyx (make-company 'companyx 'parking '(june john)))
  (setf companyz (make-company 'companyz 'health-care nil)))

;; Create a new Company closure
(defun make-company (name perks employee-list)
  (lambda (message)
		(cond ((equal message 'name) name)
			((equal message 'perks) perks)
      ((equal message 'employee) 
        (lambda (arg)
          (let ((emp arg)) 
            (if (equal (ask (eval emp) 'work-for) name)
              (print "yes") (print "no")))))
      ((equal message 'employee-list) employee-list)
      ((equal message 'delete-emp)
        (lambda (emp)
          (setf employee-list (delete emp employee-list))))
      ((equal message 'add-emp)
        (lambda (emp)
          (setq employee-list (append employee-list (list emp))))))))

;; Create a new person closure
(defun make-person (name job age company)
	(lambda (message)
	  (cond ((equal message 'name) name)
			((equal message 'job) job)
      ((equal message 'age) age)
			((equal message 'work-for) company)
			((equal message 'perks)
        (let ((perks nil))
          (if (equal job 'manager) (setf perks (cons 'car perks)))
          (append perks (list (ask (eval company) 'perks)))))
      ((equal message 'move)
        (lambda (newcompany)
          (send (eval company) 'delete-emp name)
          (setq company newcompany)
          (send (eval company) 'add-emp name)))))) 

;; Helper function to reload file in interpreter
(defun reload ()
  (load "lab10.lsp"))

(defun send (object message &rest args)
	(apply (ask object message) args))

