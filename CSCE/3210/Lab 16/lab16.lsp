;;;; Ryan Porterfield
;;;; Lab 16 - Hash Tables

(setf city
 '((denton  ((acity . 70) (bcity . 50)))
  (acity  ((ccity . 80) (dcity . 140) (denton . 70)))
  (bcity ((ecity . 120) (denton . 50)))
  (ccity ((fcity . 200) (acity . 80)))
  (ecity ((bcity . 120) (gcity . 70) ( hcity . 120) (icity . 140)))
  (gcity ((ecity . 70) (jcity . 110) (kcity . 40)))
  (hcity ((ecity . 120) (lcity . 150) (mcity . 40)))
  (icity ((ecity . 140) (ncity . 20) (ocity . 50)))))

;;; ************************************************************************ ;;;
;;;                                Functions                                 ;;;
;;; ************************************************************************ ;;;

(defun distance (table c1 c2)
  (_distance (gethash c1 table) c2))

(defun _distance (neighbors city)
  (let ((neighbor (first neighbors)))
    (cond ((null neighbors) nil)
          ((equal (first neighbor) city) (rest neighbor))
          (t (_distance (rest neighbors) city)))))

;;; Populate a hash table from a list of key-value pairs
(defun fill-city (table cities)
  (cond ((null cities) nil)
        (t (let* ((entry (first cities))
                  (key (first entry))) 
              (setf (gethash key table) (second entry))
              (fill-city table (rest cities))))))

;;; Get the neighbors of a city
(defun get-neighbors (table city)
  (nth-value 0 (gethash city table)))

(defun within-radius (table city radius)
  (_within-radius (gethash city table) city radius))

(defun _within-radius (neighbors city radius)
  (cond ((null neighbors) nil)
        (t (let* ((neighbor (first neighbors))
                  (city (first neighbor))
                  (distance (rest neighbor)))
              (cond ((<= distance radius) (append (list city)
                      (_within-radius (rest neighbors) city radius)))
                    (t (_within-radius (rest neighbors) city radius)))))))

;;; ************************************************************************ ;;;
;;;                                   EOF                                    ;;;
;;; ************************************************************************ ;;;