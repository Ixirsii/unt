;;; Ryan Porterfield
;;; Homework 03

;; Replace all occurences of an element in a list
(defun my-replace (input repl)
  (cond
    ((null input) input)
    ((eq (first input) (first repl))
      (cons (first (rest repl)) (my-replace (rest input) repl)))
    (t (cons (first input) (my-replace (rest input) repl))))
)

;; Sum the elements of two lists
(defun add-lists (nums1 nums2)
  (cond
    ((and (null nums1) (null nums2)) nil)
    ((null nums1) (cons (first nums2) (add-lists nums1 (rest nums2))))
    ((null nums2) (cons (first nums1) (add-lists (rest nums1) nums2)))
    (t (cons (+ (first nums1) (first nums2))
                (add-lists (rest nums1) (rest nums2)))))
)

;; Get information about students with a selected major
(defun select (major year students)
  (cond ((null students) nil)
  ((eq (first (rest (first students))) major) 
    (cons
      (list (first (first students)) (- year (first (rest (rest (first students))))))
      (select major year (rest students))))
  (t (select major year (rest students))))
)
