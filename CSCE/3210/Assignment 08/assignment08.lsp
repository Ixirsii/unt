;;; Ryan Porterfield
;;; Assignment 07 - Adventure Game Pt. II
;;; Copyright (c) 2016, Ryan Porterfield
;;; All rights reserved.
;;;
;;; Redistribution and use in source and binary forms, with or without
;;; modification, are permitted provided that the following conditions
;;; are met:
;;;
;;;     1. Redistributions of source code must retain the above copyright
;;;     notice, this list of conditions and the following disclaimer.
;;;
;;;     2. Redistributions in binary form must reproduce the above copyright
;;;     notice, this list of conditions and the following disclaimer in the
;;;     documentation and/or other materials provided with the distribution.
;;;
;;;     3. Neither the name of the copyright holder nor the names of its
;;;     contributors may be used to endorse or promote products derived from
;;;     this software without specific prior written permission.
;;;
;;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
;;; IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
;;; TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
;;; PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
;;; HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
;;; SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
;;; TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
;;; PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
;;; LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
;;; NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
;;; SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


;;; ************************************************************************ ;;;
;;;                                   Data                                   ;;;
;;; ************************************************************************ ;;;


(setf *network*
  '((Cause (true false)                 ; can take on values true/false
    ()                                  ; has no parents
    (0.98 0.02))
  (Effect1 (true false)       
    (Cause)                             ; has Cause as its only parent
    ((true) 0.2 0.8)                    ; these 4 numbers represent the CPT
    ((false) 0.7 0.3))                  ; (note that each row adds up to 1)
  (Effect2 (true false)
    (Effect1)
    ((true) 0.8 0.2)    
    ((false) 0.4 0.6))))


;;; ************************************************************************ ;;;
;;;                                Functions                                 ;;;
;;; ************************************************************************ ;;;



;;; ************************************************************************ ;;;
;;;                                   EOF                                    ;;;
;;; ************************************************************************ ;;;