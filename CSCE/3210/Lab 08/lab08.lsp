;;; Ryan Porterfield
;;; lab 08

;; Set properties of a car
(defun make-model (car money cars)
  (setf (get car 'cost) money)
  (cons car cars))

(defun create()
  (setf cars (make-model 'toyota 30000 nil))
  (setf cars (make-model 'ford 27000 cars))
  (setf cars (make-model 'gmc 32000 cars))
  (setf cars (make-model 'nissan 29000 cars))
  (setf cars (make-model 'lexus 35000 cars)))

(defun get-cheapest (cars)
  (cond
    ((null (rest cars)) (first cars))
    ((< (get (first cars) 'cost) (get (get-cheapest (rest cars)) 'cost)) (first cars))
    (t (get-cheapest (rest cars)))))
