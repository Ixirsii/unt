;;; Ryan Porterfield
;;; Lab 09

(defun addcity (location branches)
  (setf (get location 'connected-to) branches))

(defun breadth-first (start goal)
  (let ((breadth (lambda (current paths)
    (append (rest paths) (filter current paths))
    )))
    (tree-search start goal breadth)))
            
(defun list-contains (elem lst)
  (if (member elem lst) t nil))

(defun depth-first (start goal)
  (let ((depth (lambda (current paths)
    (append (filter current paths) (rest paths)))))
    (tree-search start goal depth)))

(defun filter (current paths)
  (let* ((search-paths nil)
      (next-paths (expanpaths current))
      (next-nodes (mapcar (lambda (elem) (first elem)) next-paths))
      (node (first next-nodes))
      (path (first next-paths))
      (contains (mapcar (lambda (sublist) (list-contains node sublist)) paths)))
    (loop
      (cond ((null node) (return search-paths))
        ((not (member 'T contains))
          (setq search-paths (append search-paths (list path)))))
      (setq next-nodes (rest next-nodes))
      (setq next-paths (rest next-paths))
      (setq node (first next-nodes))
      (setq path (first next-paths))
      (setq contains (mapcar (lambda (sublist)
          (list-contains node sublist)) paths)))))

(defun expanpaths (path)
  (mapcar (lambda (nextpath) (cons nextpath path))
          (get (car path) 'connected-to)))

(defun match (element pattern)
  (equal element pattern))

(defun tree-search (start goal search-fun)
  (let* ((paths (list (list start)))
         (current paths)
         (i 0))
    (loop
      (setq current (first paths))
      (print 'paths) (princ paths)
      (print 'current) (princ current)
      (cond ((null paths) (return (values i nil)))
            ((match (first current) goal) (return (values i (reverse current))))
            (t (setq paths (funcall search-fun current paths))))
      (setq i (1+ i)))))

; **************************************************************************** ;
;                                Initialization                                ;
; **************************************************************************** ;

(addcity 'denton '(acity bcity))
(addcity 'acity '(ccity dcity denton))
(addcity 'bcity '(ecity))
(addcity 'ccity '(fcity))
(addcity 'ecity '(gcity hcity icity))
(addcity 'gcity '(jcity kcity))
(addcity 'hcity '(lcity mcity))
(addcity 'icity '(ncity ocity))

