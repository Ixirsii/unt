;;; Ryan Porterfield
;;; Assignment 05

; **************************************************************************** ;
;                                  Functions                                   ;
; **************************************************************************** ;

;; Add edge(s) to a vertex with weight
;; This builds the graph of connected nodes
(defun addwbranches (location wbranches)
	(setf (get location 'connected-to) wbranches))

;; Run a breadth first search of the graph
;; Parameters:
;; start - The vertex to start searching at
;; goal - The vertex to search for/get to
(defun breadth-first (start goal)
  (let ((breadth (lambda (current paths)
    (append (rest paths) (filter current paths))
    )))
    (tree-search start goal breadth)))
            
;; Finds the path with the best cost
;; This function was provided by Ms. Swiggger
;; Parameters:
;; paths - a list of paths from the start to the end vertices
(defun cost (paths)
  (let ((bestpath nil)
    (temppath 0))
    (setf bestpath (cons (apply '+ (mapcar 'rest (first paths))) (first paths)))
    (setf paths (rest paths))
    (loop
      (if (null paths) (return (rest bestpath)))
      (if (< (setf temppath (apply '+ (mapcar 'rest (first paths))))
             (first bestpath))
        (setf bestpath (cons temppath (first paths))))
      (setf paths (rest paths)))))

;; Run a cost first search of the graph
;; Parameters:
;; start - The vertex to start searching at
;; goal - The vertex to search for/get to
(defun cost-first (start goal)
  (let ((current nil) (paths nil) (i 0))
    (setq paths (list (list (cons start 0))))
    (loop
      (setq current (cost paths)) ; cost helper function
      (cond
        ((null paths) (return (values i nil)))
        ((match (first (first current)) goal)
          (return (values
            (reverse current)
            (cons '(nodes expanded) i)
            (cons '(path length) (length current))
            (cons 'cost (getcost current)))))
        (t (setq paths (append
          (remove current paths :test #'equal)
          ;(expanpaths current)))))
          (filter current paths)))))
      (setq i (1+ i)))))

;; Run a depth first search of the graph
;; Parameters:
;; start - The vertex to start searching at
;; goal - The vertex to search for/get to
(defun depth-first (start goal)
  (let ((depth (lambda (current paths)
    (append (filter current paths) (rest paths)))))
    (tree-search start goal depth)))

;; Filters out nodes which are already in the search path.
;; This prevents the search algorithms from running infinitely along the same
;; path
(defun filter (current paths)
  (let* ((search-paths nil)
      (next-paths (expanpaths current))
      (next-nodes (mapcar (lambda (elem) (first elem)) next-paths))
      (node (first next-nodes))
      (path (first next-paths))
      (contains (mapcar (lambda (sublist) (list-contains node sublist)) paths)))
    (loop
      (cond ((null node) (return search-paths))
        ((not (member 'T contains))
          (setq search-paths (append search-paths (list path)))))
      (setq next-nodes (rest next-nodes))
      (setq next-paths (rest next-paths))
      (setq node (first next-nodes))
      (setq path (first next-paths))
      (setq contains (mapcar (lambda (sublist)
          (list-contains node sublist)) paths)))))

;; Get a list of connected vertices
(defun expanpaths (path)
  (mapcar (lambda (nextpath) (cons nextpath path))
    (get (first (first path)) 'connected-to)))

;; Get the cost of a given path
(defun getcost (path)
  (apply '+ (mapcar 'rest path)))

;; Helper function to see if an element is in a list.
;; This function calls member, but returns T instead of the element
(defun list-contains (elem lst)
  (if (member elem lst) t nil))

;; Helper function for equals
(defun match (element pattern)
  (equal element pattern))

;; Generic tree search algorithm for breadth and depth first
;; This algorithm is the common code between breadth and depth first searches,
;; but requires an additional function as a parameter to build the list of
;; vertices to search.
;; Parameters:
;; start - The vertex to start searching at
;; goal - The vertex to search for/get to
;; search-fun - a function which builds the list of paths to search
(defun tree-search (start goal search-fun)
  (let* ((paths (list (list (cons start 0)))) (current paths) (i 0))
    (loop
      (setq current (first paths))
      (print "current") (princ current)
      (print "paths") (princ paths)
      (cond ((null paths) (return (values i nil)))
        ((match (first (first current)) goal)
          (return (values
            (reverse current)
            (cons '(nodes expanded) i)
            (cons '(path length) (length current))
            (cons '(cost) (getcost current)))))
        (t (setq paths (funcall search-fun current paths))))
      (setq i (1+ i)))))

; **************************************************************************** ;
;                                Initialization                                ;
; **************************************************************************** ;

; I should have done this differently.
; a = 0, b = 1, c = 2, ..., h = 7
; to build the graph. Each node has an associated cost in the form (NODE . COST)
(addwbranches 'aa '(         (ba . 2) (ab . 1)))
(addwbranches 'ab '((aa . 1) (bb . 1) (ac . 1)))
(addwbranches 'ac '((ab . 1) (bc . 1) (ad . 1)))
(addwbranches 'ad '((ac . 1) (bd . 1) (ae . 1)))
(addwbranches 'ae '((ad . 1) (be . 1) (af . 1)))
(addwbranches 'af '((ae . 1) (bf . 3) (ag . 1)))
(addwbranches 'ag '((af . 1) (bg . 1) (ah . 1)))
(addwbranches 'ah '((ag . 1) (bh . 1)         ))

(addwbranches 'ba '(         (ca . 2) (bb . 1) (aa . 1)))
(addwbranches 'bb '((ba . 2) (cb . 1) (bc . 1) (ab . 1)))
(addwbranches 'bc '((bb . 1) (cc . 1) (bd . 1) (ac . 1)))
(addwbranches 'bd '((bc . 1) (cd . 1) (be . 1) (ad . 1)))
(addwbranches 'be '((bd . 1) (ce . 1) (bf . 3) (ae . 1)))
(addwbranches 'bf '((be . 1) (cf . 1) (bg . 1) (af . 1)))
(addwbranches 'bg '((bf . 3) (cg . 1) (bh . 1) (ag . 1)))
(addwbranches 'bh '((bg . 1) (ch . 1)          (ah . 1)))

(addwbranches 'ca '(         (da . 2) (cb . 1) (ba . 2)))
(addwbranches 'cb '((ca . 2) (db . 1) (cc . 1) (bb . 1)))
(addwbranches 'cc '((cb . 1) (dc . 1) (cd . 1) (bc . 1)))
(addwbranches 'cd '((cc . 1) (dd . 1) (ce . 1) (bd . 1)))
(addwbranches 'ce '((cd . 1) (de . 1) (cf . 1) (be . 1)))
(addwbranches 'cf '((ce . 1) (df . 1) (cg . 1) (bf . 3)))
(addwbranches 'cg '((cf . 1) (dg . 1) (ch . 1) (bg . 1)))
(addwbranches 'ch '((cg . 1) (dh . 1)          (bh . 1)))

(addwbranches 'da '(         (ea . 2) (db . 1) (ca . 2)))
(addwbranches 'db '((da . 2) (eb . 1) (dc . 1) (cb . 1)))
(addwbranches 'dc '((db . 1) (ec . 1) (dd . 1) (cc . 1)))
(addwbranches 'dd '((dc . 1) (ed . 1) (de . 1) (cd . 1)))
(addwbranches 'de '((dd . 1) (ee . 1) (df . 1) (ce . 1)))
(addwbranches 'df '((de . 1) (ef . 1) (dg . 1) (cf . 1)))
(addwbranches 'dg '((df . 3) (eg . 1) (dh . 1) (cg . 1)))
(addwbranches 'dh '((dg . 1) (eh . 1)          (ch . 1)))

(addwbranches 'ea '(         (fa . 2) (eb . 1) (da . 2)))
(addwbranches 'eb '((ea . 2) (fb . 1) (ec . 1) (db . 1)))
(addwbranches 'ec '((eb . 1) (fc . 1) (ed . 1) (dc . 1)))
(addwbranches 'ed '((ec . 1) (fd . 1) (ee . 1) (dd . 1)))
(addwbranches 'ee '((ed . 1) (fe . 2) (ef . 1) (de . 1)))
(addwbranches 'ef '((ee . 1) (ff . 2) (eg . 1) (df . 1)))
(addwbranches 'eg '((ef . 3) (fg . 2) (eh . 1) (dg . 1)))
(addwbranches 'eh '((eg . 1) (fh . 2)          (dh . 1)))

(addwbranches 'fa '(         (ga . 2) (fb . 1) (ea . 2)))
(addwbranches 'fb '((fa . 2) (gb . 3) (fc . 1) (eb . 1)))
(addwbranches 'fc '((fb . 1) (gc . 1) (fd . 1) (ec . 1)))
(addwbranches 'fd '((fc . 1) (gd . 1) (fe . 2) (ed . 1)))
(addwbranches 'fe '((fd . 1) (ge . 1) (ff . 2) (ee . 1)))
(addwbranches 'ff '((fe . 2) (gf . 1) (fg . 2) (ef . 1)))
(addwbranches 'fg '((ff . 2) (gg . 1) (fh . 2) (eg . 1)))
(addwbranches 'fh '((fg . 2) (gh . 1)          (eh . 1)))

(addwbranches 'ga '(         (ha . 2) (gb . 3) (fa . 2)))
(addwbranches 'gb '((ga . 2) (hb . 1) (gc . 1) (fb . 1)))
(addwbranches 'gc '((gb . 3) (hc . 1) (gd . 1) (fc . 1)))
(addwbranches 'gd '((gc . 1) (hd . 1) (ge . 1) (fd . 1)))
(addwbranches 'ge '((gd . 1) (he . 1) (gf . 1) (fe . 2)))
(addwbranches 'gf '((ge . 1) (hf . 1) (gg . 1) (ff . 2)))
(addwbranches 'gg '((gf . 3) (hg . 1) (gh . 1) (fg . 2)))
(addwbranches 'gh '((gg . 1) (hh . 1)          (fh . 2)))

(addwbranches 'ha '(         (hb . 1) (ga . 2)))
(addwbranches 'hb '((ha . 2) (hc . 1) (gb . 3)))
(addwbranches 'hc '((hb . 1) (hd . 1) (gc . 1)))
(addwbranches 'hd '((hc . 1) (he . 1) (gd . 1)))
(addwbranches 'he '((hd . 1) (hf . 1) (ge . 1)))
(addwbranches 'hf '((he . 1) (hg . 1) (gf . 1)))
(addwbranches 'hg '((hf . 3) (hh . 1) (gg . 1)))
(addwbranches 'hh '((hg . 1)          (gh . 1)))

; **************************************************************************** ;
;                                     EOF                                      ;
; **************************************************************************** ;
