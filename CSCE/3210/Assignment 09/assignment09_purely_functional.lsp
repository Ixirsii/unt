;;;; Ryan Porterfield
;;;; Assignment 08 - Arrays
;;;; Copyright (c) 2016, Ryan Porterfield
;;;; All rights reserved.
;;;;
;;;; Redistribution and use in source and binary forms, with or without
;;;; modification, are permitted provided that the following conditions
;;;; are met:
;;;;
;;;;     1. Redistributions of source code must retain the above copyright
;;;;     notice, this list of conditions and the following disclaimer.
;;;;
;;;;     2. Redistributions in binary form must reproduce the above copyright
;;;;     notice, this list of conditions and the following disclaimer in the
;;;;     documentation and/or other materials provided with the distribution.
;;;;
;;;;     3. Neither the name of the copyright holder nor the names of its
;;;;     contributors may be used to endorse or promote products derived from
;;;;     this software without specific prior written permission.
;;;;
;;;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
;;;; IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
;;;; TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
;;;; PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
;;;; HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
;;;; SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
;;;; TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
;;;; PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
;;;; LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
;;;; NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
;;;; SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

;;; ************************************************************************ ;;;
;;;                                   Data                                   ;;;
;;; ************************************************************************ ;;;

;;; Unplayed cell
(setf blank " ")

;;; List of valid player characters
(setf players '("X" "O"))

;;; Size of the board
(setf size '(3 3))

;;; Row win
(setf test0 '(("X" "X" "X") (" " " " " ") (" " " " " ")))
;;; Column win
(setf test1 '((" " "X" " ") (" " "X" " ") (" " "X" " ")))
;;; Diagonal win #1
(setf test2 '(("X" " " " ") (" " "X" " ") (" " " " "X")))
;;; Diagonal win #2
(setf test3 '((" " " " "X") (" " "X" " ") ("X" " " " ")))
;;; No wins
(setf test4 '((" " "X" " ") ("X" " " " ") (" " " " "X")))
;;; Tie
(setf test5 '(("O" "X" "O") ("X" "O" "X") ("X" "O" "X")))

;;; ************************************************************************ ;;;
;;;                                Functions                                 ;;;
;;; ************************************************************************ ;;;

;;; Wrapper function for _create
;;;
;;; Calls _create to initialize a new, empty board with dimensions size.
;;; Turns the initial output of _create into an array.
;;; \param  size A list in the form (dimX dimY) where dimX, dimY are the x and
;;;         y dimensions respectively of the array.
;;; \return an array of size with every element initialized to " ".
;;; \sa _create
(defun create (size)
  (_create size 0))

;;; Initialize a new, empty board with dimensions size.
;;;
;;; \param  size A list in the form (dimX dimY) where dimX, dimY are the x and
;;;         y dimensions respectively of the array.
;;; \param  indx Counter used for recursion.
;;; \return a list of size with every element initialized to " ".
(defun _create (size indx)
  (if (equal (first size) indx) nil
    (append (list (init_row (second size))) (_create size (1+ indx)))))

;;; Wrapper function for _has_tie
;;;
;;; Check if the board completed with a tie
;;; \param  board The current game state
;;; \param  player A game character (probably from \c players)
(defun has_tie (board players)
  (if (has_won board players) nil (_has_tie board)))

;;; Check if the board completed with a tie
;;;
;;; \param  board The current game state
(defun _has_tie (board)
  (if (null board) t
    (let ((row (first board)))
      (and (not (equal (first row) blank))
            (if (null (rest row)) (_has_tie (rest board))
              (_has_tie (append (list (rest row)) (rest board))))))))

;;; Check if any players have won
;;;
;;; \param  board The current game state
;;; \param  player A game character (probably from \c players)
(defun has_won (board players)
  (cond ((null players) nil)
        ((player_has_won board (first players)) 
          (format t "Congratulations! ~A has won!" (first players)) t)
        (t (has_won board (rest players)))))

;;; Wrapper function for _has_won_col
;;;
;;; Checks to see if player has won by matching a given column.
;;; \param  board The current game state
;;; \param  col The column of the board being checked
;;; \param  player A game character (probably from \c players)
;;; \return T if player has matched \c col, otherwise nil.
;;; \sa     _has_won_col
(defun has_won_col (board col player)
    (_has_won_col board col player))

;;; Check to see if a player has won by matching a column
;;;
;;; \param  board The current game state
;;; \param  col The column of the board being checked
;;; \param  player A game character (probably from \c players)
;;; \return T if player has matched \c col, otherwise nil.
(defun _has_won_col (board col player)
  (cond ((null board) t)
        (t (and (equal (nth col board) player)
                (_has_won_col (rest board) col player)))))

;;; Wrapper function for _has_won_cols
;;;
;;; Calls _has_won_col on every column of the board to check if player has won
;;; by matching any column.
;;; \param  board The current game state
;;; \param  player A game character (probably from \c players)
;;; \return T if player has matched any column, otherwise nil.
;;; \sa     _has_won_cols
(defun has_won_cols (board player)
  (_has_won_cols board player 0))

;;; Check if player has won by matching any column.
;;;
;;; \param  board The current game state
;;; \param  player A game character (probably from \c players)
;;; \param  indx Index used for recursion
;;; \return T if player has matched any column, otherwise nil.
(defun _has_won_cols (board player indx)
  (if (equal (len (first board)) indx) nil
    (or (has_won_col board indx player)
         (_has_won_cols board player (1+ indx)))))

;;; Wrapper function for _has_won_dia
;;;
;;; Calls _has_won_dia on both possible diagonals to check if the player has won
;;; by matching either diagonal.
;;; \param  board The current game state
;;; \param  player A game character (probably from \c players)
;;; \return T if player has matched either diagonal, otherwise nil.
;;; \sa     _has_won_dia
(defun has_won_dia (board player)
    (or (_has_won_dia board #'1+ player 0)
         (_has_won_dia board #'1- player (1- (second size)))))

;;; Check if the player has won by matching a given diagonal.
;;;
;;; \param  board The current game state
;;; \param  fun A function applied to \c yindx to determine whether \c yindx
;;;         gets incremented or decremented
;;; \param  player A game character (probably from \c players)
;;; \param  yindx Column index used for recursion
;;; \return T if player matched a given diagonal, otherwise nil.
(defun _has_won_dia (board fun player yindx)
  (if (or (null board) (equal yindx (second size)) (< yindx 0)) t
        (and (equal (nth yindx (first board)) player)
        (_has_won_dia (rest board) fun player (funcall fun yindx)))))

;;; Checks to see if player has won by matching a given row
;;;
;;; Checks to see if player has won by matching a given row
;;; \param  row The row of the board being checked
;;; \param  player A game character (probably from \c players)
;;; \return T if player has matched \c row, otherwise nil.
(defun has_won_row (row player)
    (if (null row) t
    (and (equal (first row) player)
         (_has_won_row (rest row) player))))

;;; Check if player has won by matching any row.
;;;
;;; \param  board The current game state
;;; \param  player A game character (probably from \c players)
;;; \return T if player has matched any row, otherwise nil.
(defun has_won_rows (board player)
  (if (null board) nil
    (or (has_won_row (first board) player)
         (_has_won_rows (rest board) player))))

;;; Wrapper function for _init_row
;;;
;;; Calls _init_row to get a list of blanks used to initialize the board
;;; \param  len The length of the row
;;; \return A list of length \c len containing all \c blank elements.
(defun init_row (len)
  (_init_row 0 len))

;;; Get a list of blanks used to initialize the board
;;;
;;; \param  len The length of the row
;;; \return A list of length \c len containing all \c blank elements.
(defun _init_row (indx len)
  (cond ((equal indx len) nil)
        (t (append (list blank) (_init_row (1+ indx) len)))))

;;; Main loop for the game
;;;
;;; Checks for winners or a tie, if none are found then players are polled for
;;; input and the game loops.
;;; \param  board The current game state
;;; \param  size A list in the form (dimX dimY) where dimX, dimY are the x and
;;;         y dimensions respectively of the array.
;;; \param  players A list of valid player characters
;;; \return a string announcing the winner, or a tie.
(defun interface(board size players)
  (let ((x (first players))
        (o (second players)))
    (cond ((has_won board size players) nil)
          ((has_tie board size players) (format t "There was a tie~%~%"))
          (t (turn x players board size)
              (cond ((player_has_won board size x)
                      (format t "Congratulations! ~A has won!~%~%" x))
                    ((has_tie board size players) (format t "There was a tie~%~%"))
                    (t (turn o players board size)
                      (interface board size players)))))))

;;; Check to see if a player has won
;;;
;;; \param  board The current game state
;;; \param  size A list in the form (dimX dimY) where dimX, dimY are the x and
;;;         y dimensions respectively of the array.
;;; \param  player A game character (probably from \c players)
;;; \return T if player has won the game, otherwise nil.
(defun player_has_won (board player)
  (if (null players) nil (or
    (has_won_cols board player)
    (has_won_dia board player)
    (has_won_rows board player))))

;;; Place a player on the board
;;;
;;; \param  player A game character (probably from \c players)
;;; \param  board The current game state
;;; \param  x Row of the board to place \c player in
;;; \param  y Column of the board to place \c player in
;;; \return new state of the board after placing \c player
(defun player_move (player players board x y)
  (cond ((not (find player players :test #'string=))
          "Error: Invalid player character")
        ((not (equal (nth y (nth x board)) blank))
          "Error: Must play on a blank tile")
        (t (replace_by_index board x (replace_by_index (nth x board) y player)))))

;;; Prompt a player for input
;;;
;;; \param  player A game character (probably from \c players)
;;; \param  bounds A list in the form (dimX dimY) where dimX, dimY are the x and
;;;         y dimensions respectively of the array.
;;; \return user input
(defun prompt_input (player bounds)
  (format t "Player ~A's turn. Please enter (x y): " player)
  (let* ((input (read)))
    (cond ((not (and (listp input) (equal (length input) 2)))
            (format t "Error: Input must be in the form (x y) ~%~%")
            (prompt_input player bounds))
          ((or (< (first input) 0)
               (< (second input) 0)
               (>= (first input) (first bounds))
               (>= (second input) (second bounds))) 
            (format t "Error: Input must be in the range [~A, ~A), [~A, ~A)~%~%"
                    0 (first bounds) 0 (second bounds))
            (prompt_input player bounds))
          (t input))))

;;; Replace an element in an array by the index of the element
;;;
;;; \param  lst A list of elements
;;; \param  indx The index of the element to replace
;;; \param  elem The new element replacing the current element
;;; \return A new list identical to the old list, but with one element replaced.
(defun replace_by_index (lst indx elem)
  (cond
    ((null lst) nil)
    ((or (> indx (length lst)) (< indx 0)) lst)
    ((equal indx 0) (cons elem (rest lst)))
    (t (cons (first lst) (replace_by_index (rest lst) (1- indx) elem)))))

;;; Prompts a user for input and places the move on the board
;;;
;;; \param  player A game character (probably from \c players)
;;; \param  players A list of valid player characters
;;; \param  board The current game state
;;; \param  size A list in the form (dimX dimY) where dimX, dimY are the x and
;;;         y dimensions respectively of the array.
(defun turn (player players board size)
  (let* ((input (prompt_input player size))
         (x (first input))
         (y (second input)))
    (player_move player players board x y)
    (pprint board)
    (format t "~%")))


;;; ************************************************************************ ;;;
;;;                                   EOF                                    ;;;
;;; ************************************************************************ ;;;