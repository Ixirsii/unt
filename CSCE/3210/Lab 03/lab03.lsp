;;; Ryan Porterfield
;;; Lab 03

(defun data-type (elem)
  (cond ((numberp elem) 'number)
        ((atom elem) 'atom)
        ((listp elem) 'list)
        (t "unknown")))

(defun report-victory (score)
  (cond ((> score 0) 'i-won)
        ((equal score 0) 'tie)
        (t 'you-won)))

(defun asc-sqr (a b c)
  (cond ((equal (* a a) b) 'square)
        ((equal (+ b c) a) 'times)
        (t 'nothing)))