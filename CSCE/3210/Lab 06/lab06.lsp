;;; Ryan Porterfield
;;; Lab 05 - Recursion 2

;; Remove vowels from a list of whitespace separated characters
(defun remove-vowels (lst)
  (let ((vowels '(a e i o u)))
    (cond ((null lst) nil)
      ((member (first lst) vowels) (remove-vowels (rest lst)))
      (t (cons (first lst) (remove-vowels (rest lst))))))
)

;; Copy the elements from a list
(defun replicate (item size)
  (let
    ((copy nil))
      (dotimes (x size copy)
        (setf copy (cons item copy))))
)

;; Sum the numbers in the top level of a list
(defun add-only-numbers (input)
  (let
    ((sum 0))
      (dolist (e input sum)
        (if (numberp e) (setf sum (+ sum e)))))
)
