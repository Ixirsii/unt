;;; Ryan Porterfield
;;; Assignment 06 - Adventure Game Pt. I
;;; Copyright (c) 2016, Ryan Porterfield
;;; All rights reserved.
;;;
;;; Redistribution and use in source and binary forms, with or without
;;; modification, are permitted provided that the following conditions
;;; are met:
;;;
;;;     1. Redistributions of source code must retain the above copyright
;;;     notice, this list of conditions and the following disclaimer.
;;;
;;;     2. Redistributions in binary form must reproduce the above copyright
;;;     notice, this list of conditions and the following disclaimer in the
;;;     documentation and/or other materials provided with the distribution.
;;;
;;;     3. Neither the name of the copyright holder nor the names of its
;;;     contributors may be used to endorse or promote products derived from
;;;     this software without specific prior written permission.
;;;
;;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
;;; IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
;;; TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
;;; PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
;;; HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
;;; SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
;;; TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
;;; PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
;;; LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
;;; NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
;;; SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


;;; ************************************************************************ ;;;
;;;                                Functions                                 ;;;
;;; ************************************************************************ ;;;


(setf *test-net*
  '((Cause (true false)
           ()
           (0.98 0.02))
    (Effect1 (true false)
             (Cause)
             ((true) 0.7 0.3)
             ((false) 0.5 0.5))
    (Effect2 (true false)
             (effect1)
             ((true) 0.8 0.2)
              ((false) 0.4 0.6))))


; This function gets all the parents that are in the network
(defun get-all-parents (bn)
  (mapcar #'third bn))


;  This function returns the cpt values for a particular variable.  Check the
;  network and make sure you understand those cpt vlues.  
(defun get-cpt (var bn)
  (nthcdr 3 (assoc var bn)))


; This function returns the parents (or nil) for a particular variable
(defun get-parents (var bn)
  (third (assoc var bn)))


;;;This function gets the values (true, false) for a particular variable
(defun get-values (var bn)
  (second (assoc var bn)))


;;; This function gets all the high-level variables from network.
(defun get-vars (bn)
  (mapcar #'first bn))


;this function gets all the variables with parents (or no parents)
(defun get-vars-with-parents (bn)
  (let ((vars (get-vars bn))
        (parents (get-all-parents bn)))
    (mapcar #'cons vars parents)))


;; look up values for the given vars and list them in same order
;; - used as a helper to access appropriate CPT values
(defun list-vals-for (vars vars-with-vals)
  (if (null vars) nil
    (cons (second (assoc (first vars) vars-with-vals))
      (list-vals-for (rest vars) vars-with-vals))))


;; vars-with-vals is a list of the form ((var-1 val-1) ... (var-n val-n))
;; returns nil if var doesn't appear in vars-with-vals
(defun get-val (var vars-with-vals)
    (let ((var-val-pair (assoc var vars-with-vals)))
          (if var-val-pair (second var-val-pair) nil)))


;; This function returns a SPECIFIC cpt value, given a variable, a value for
;; that variable, and a list of current variables & values.  
;; assumes all parents have values specified in vars-with-vals
(defun prob-given-parents (var val vars-with-vals bn)
  (let* ((var-data (rest (assoc var bn)))
         (column (position val (get-values var bn)))
         (parents (second var-data))
         (parent-vals (list-vals-for parents vars-with-vals))
         (cpt (nthcdr 2 var-data))
         (cpt-row (if (null (rest cpt)) ; if only one row
         (first cpt)       ; return it, else find matching row
         (rest (assoc parent-vals cpt :test #'equal)))))
    (nth column cpt-row)))


;; Given a bayes net, this code 
;; returns a list of vars where parents always appear before their children
(defun topological-sort-vars (bn)
  (let* ((topology (get-vars-with-parents bn))
         (ordered-nodes nil))
    (loop
      (if (null topology) (return (reverse ordered-nodes)))
      (dolist (node-data topology)
      (let ((node (first node-data))
            (node-parents (rest node-data)))
        (if (null node-parents)
          (progn
            (push node ordered-nodes)
            (setf topology
              (remove nil
              (mapcar #'(lambda (l) (remove node l))
              topology)))
            (return))))))))


;;; ************************************************************************ ;;;
;;;                                   EOF                                    ;;;
;;; ************************************************************************ ;;;
