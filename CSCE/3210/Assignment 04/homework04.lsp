;;; Ryan Porterfield
;;; Program 04

;; Calculate the profit from a list of department revenues
(defun profit (revenues)
  (cond ((null revenues) 0)
    (t (let ((rev (first revenues)))
         (+
           (- (first (rest rev)) (first (rest (rest rev))))
           (profit (rest revenues))))))
)

;; Count the occurences of 'interesting' elements
(defun int-count (input interesting)
  (cond ((null input) 0)
    ((member (first input) interesting)
      (1+ (int-count (rest input) interesting)))
    (t (int-count (rest input) interesting)))
)

