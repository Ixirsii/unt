#|
A Bayes-net has the form (<var-1-data> ... <var-k-data>)

Each element of the list has the form
(<var-name> <list-of-values>
	    <list-of-parent-names>
	    <cpt-row-1> ... <cpt-row-N>)

A cpt-row has the form (<prob-of-val-1> ... <prob-of-val-n>) if the list of
parents is empty, or (<cpt-key> cond-prob-of-val-1 ... cond-prob-of-val-n),
if the list of parents is non-empty.  n is the number of values the
corresponding variable can take on.  <cpt-key> has the form
(<val-1> ... <val-m>) where m is the number of parents and each <val-j>
is a possible value of the jth parent.  Each cpt is essentially a table
with N rows labelled by the combination of parent values and columns
corresponding to values of the given variable.  The ordering of the
columns corresponds to the ordering of the values in the given
<list-of-values>.  
|#

(setf *test-net*
      '((Cause (true false)          ; can take on values true/false
	       ()                    ; has no parents
	       (0.98 0.02))            ; P(Cause = true) = P(Cause = false) = 0.5
	(Effect1 (true false)       
		 (Cause)             ; has Cause as its only parent
		 ((true) 0.7 0.3)    ; these 4 numbers represent the CPT
		 ((false) 0.5 0.5))  ; (note that each row adds up to 1)
	(Effect2 (true false)
		 (Effect1)
		 ((true) 0.8 0.2)    
		 ((false) 0.4 0.6)
        
      )))


(setf *network*
  '((Cause (true false)                 ; can take on values true/false
    ()                                  ; has no parents
    (0.98 0.02))
  (Effect1 (true false)       
    (Cause)                             ; has Cause as its only parent
    ((true) 0.2 0.8)                    ; these 4 numbers represent the CPT
    ((false) 0.7 0.3))                  ; (note that each row adds up to 1)
  (Effect2 (true false)
    (Effect1)
    ((true) 0.8 0.2)    
    ((false) 0.4 0.6))))