;;; Ryan Porterfield
;;; Lab 09

(defun addcity (location branches)
  (setf (get location 'connected-to) branches))

(addcity 'denton '(acity bcity))
(addcity 'acity '(ccity dcity))
(addcity 'bcity '(ecity))
(addcity 'ccity '(fcity))
(addcity 'ecity '(gcity hcity icity))
(addcity 'gcity '(jcity kcity))
(addcity 'hcity '(lcity mcity))
(addcity 'icity '(ncity ocity))

(defun match (element pattern)
  (equal element pattern))

(defun expanpaths (path)
  (mapcar (lambda (nextpath) (cons nextpath path))
          (get (car path) 'connected-to)))

(defun depth-first (start goal)
  (let* ((paths (list (list start)))
         (current paths)
         (i 0))
    (loop
      (setq current (first paths))
      (print 'paths) (princ paths)
      (print 'current) (princ current)
      (cond ((null paths) (return (values i nil)))
            ((match (first current) goal) (return (values i (reverse current))))
            (t (setq paths (append (expanpaths current) (rest paths)))))
      (setq i (1+ i)))))

(defun breadth-first (start goal)
  (let* ((paths (list (list start)))
         (current paths)
         (i 0))
    (loop
      (setq current (first paths))
      (print 'paths) (princ paths)
      (print 'current) (princ current)
      (cond ((null paths) (return (values i nil)))
            ((match (first current) goal) (return (values i (reverse current))))
            (t (setq paths (append (rest paths) (expanpaths current)))))
      (setq i (1+ i)))))
