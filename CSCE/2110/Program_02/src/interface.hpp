/** *********************************************************************** **
 ** *********************************************************************** **
 **                                                                         **
 ** @file           interface.hpp                                           **
 ** @brief          Provides a generic interface for interacting with a     **
 **                 @link HashMap @endlink                                  **
 **                                                                         **
 ** @author         Ryan Porterfield <RyanPorterfield@my.unt.edu>           **
 ** @copyright      BSD 3 Clause (See COPYING file)                         **
 **                                                                         **
 ** *********************************************************************** **
 ** *********************************************************************** **/


#ifndef HASHMAP_INTERFACE_H
#define HASHMAP_INTERFACE_H

#include <cstring>
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <tuple>
#include <unordered_map>

typedef std::tuple<uint32_t, std::string> gkey_t;
typedef std::tuple<std::string, std::string> sval_t;

uint32_t str_to_u (std::string s);

class GKeyEqual {
  public:
    bool operator ()(const gkey_t &a, const gkey_t &b) const
    {
        bool first = std::get<0> (a) == std::get<0> (b);
        bool second = std::get<1> (a) == std::get<1> (b);
        return first && second;
    }
};

class GKeyHash {
  public:
    std::size_t operator ()(const gkey_t key) const
    {
        std::hash<uint32_t> h0;
        std::hash<std::string> h1;
        return h0 (std::get<0> (key)) * h1 (std::get<1> (key));
    }
};

typedef std::unordered_map<gkey_t, unsigned char, GKeyHash, GKeyEqual> gmap_t;
typedef std::unordered_map<uint32_t, sval_t> smap_t;

/** @class          MapInterface shell.hpp "src/shell.hpp"
 *  @brief          Interface that defines behavior for interacting with a
 *                  @link HashMap @endlink
 */
class MapInterface
{
  public:

    /** @brief          Default destructor
     *
     */
    virtual ~MapInterface () {}

    virtual void add (std::stringstream &lstream) const = 0;

    virtual void clear () const = 0;

    virtual void print () const = 0;

    virtual void remove (std::stringstream &lstream) const = 0;

  protected:
    /** @brief      Attempts to get the next token from the user's input.
     *
     *  @details    Uses the stringstream passed to get the next whitespace
     *              separated token from the user's input.  If the end of the
     *              input was reached it throws an out_of_range exception.
     *  @param lstream
     *              A stringstream that the next token should be read from.
     *  @throws std::out_of_range
     *              if the end of the stream was reached.
     *  @return     The next token in the stream.
     */
    std::string get_next (std::stringstream &lstream) const
    {
        std::string cmd;
        lstream >> cmd;
        if (not lstream or "" == cmd)
            throw std::out_of_range ("Reached end of input");
        return cmd;
    }
};


/** @class          StudentInterface shell.hpp "src/shell.hpp"
 *  @brief          An implementation of @link MapInterface @endlink for the
 *                  student table.
 */
class GradeInterface : public MapInterface
{
  public:

    /** @brief
     *
     */
    GradeInterface (gmap_t *map) : map_ (map) {}

    /** @brief
     *
     */
    virtual ~GradeInterface () {}

    /** @brief
     *
     */
    virtual void add (std::stringstream &lstream) const
    {
        gkey_t key = get_key (lstream);
        unsigned char grade = get_value (lstream);
        if (contains (key)){
            std::cout << "Warning: Existing grade for " << std::get<0> (key);
            std::cout << std::endl;
            return;
        }
        (*map_)[key] = grade;
    }

    /** @brief
     *
     */
    virtual void clear () const
    {
        map_->clear ();
    }

    /** @brief
     *
     */
    virtual bool contains (gkey_t key) const
    {
        try {
            map_->at (key);
            return true;
        } catch (std::out_of_range e) {
            return false;
        }
    }

    virtual void print () const
    {
        std::cout << "{ ";
        for (auto it = map_->begin (); it != map_->end (); ++it) {
            std::cout << "(" << std::get<0> (it->first) << ", ";
            std::cout << std::get<1> (it->first) << ", " << it->second << ")";
            auto it0 = it;
            if (++it0 != map_->end ())
                std::cout << ", ";
        }
        std::cout << " }" << std::endl;
    }

    virtual void remove (std::stringstream &lstream) const
    {
        gkey_t key = get_key (lstream);
        if (not contains (key)) {
            std::cout << "Warning: No grade for " << std::get<0> (key);
            std::cout << std::endl;
            return;
        }
        map_->erase (key);
    }

  private:

    /** @brief      The map that the interface is interacting with
     *
     */
    gmap_t *map_;

    /** @brief
     *
     */
    gkey_t get_key (std::stringstream &lstream) const
    {
        std::string id_str = get_next (lstream);
        std::string course = get_next (lstream);
        uint32_t id = str_to_u (id_str);
        gkey_t key (id, course);
        return key;
    }

    /** @brief
     *
     */
    unsigned char get_value (std::stringstream &lstream) const
    {
        std::string grade_str = get_next (lstream);
        return (unsigned char) grade_str.c_str()[0];
    }
};


/** @class          StudentInterface shell.hpp "src/shell.hpp"
 *  @brief          An implementation of @link MapInterface @endlink for the
 *                  student table.
 */
class StudentInterface : public MapInterface
{
  public:

    /** @brief      Create a new StudentInterface to interact with a table of
     *              students.
     */
    StudentInterface (smap_t *map) : map_ (map) {}

    /** @brief      Destructor
     *
     */
    virtual ~StudentInterface () {}

    /** @brief
     *
     */
    virtual void add (std::stringstream &lstream) const
    {
        uint32_t key = get_key (lstream);
        if (contains (key)){
            std::cout << "Warning: Existing student ID: " << key << std::endl;
            return;
        }
        sval_t name = get_value (lstream);
        (*map_)[key] = name;
    }

    /** @brief
     *
     */
    virtual void clear () const
    {
        map_->clear ();
    }

    /** @brief
     *
     */
    virtual bool contains (uint32_t key) const
    {
        try {
            map_->at (key);
            return true;
        } catch (std::out_of_range e) {
            return false;
        }
    }

    virtual void print () const
    {
        std::cout << "{ ";
        for (auto it = map_->begin (); it != map_->end (); ++it) {
            std::cout << "(" << it->first << ", ";
            std::cout << std::get<0> (it->second) << ", ";
            std::cout << std::get<1> (it->second) << ")";
            auto it0 = it;
            if (++it0 != map_->end ())
                std::cout << ", ";
        }
        std::cout << " }" << std::endl;
    }

    virtual void remove (std::stringstream &lstream) const
    {
        uint32_t key = get_key (lstream);
        if (not contains (key)) {
            std::cout << "Warning: No student with ID: " << key << std::endl;
            return;
        }
        map_->erase (key);
    }

  private:
    /**
     *
     */
    smap_t *map_;

    /** @brief
     *
     */
    uint32_t get_key (std::stringstream &lstream) const
    {
        std::string id_str = get_next (lstream);
        return str_to_u (id_str);
    }

    /** @brief
     *
     */
    sval_t get_value (std::stringstream &lstream) const
    {
        std::string first = get_next (lstream);
        std::string last = get_next (lstream);
        sval_t val (first, last);
        return val;
    }
};


#endif // HASHMAP_INTERFACE_H


/** *********************************************************************** **
 **                                   EOF                                   **
 ** *********************************************************************** **/
