/** *********************************************************************** **
 ** *********************************************************************** **
 **                                                                         **
 ** @file           shell.h                                                 **
 ** @brief          Provides an interactive shell for interfacing with a    **
 **                 @link HashMap @endlink                                  **
 **                                                                         **
 ** @author         Ryan Porterfield <RyanPorterfield@my.unt.edu>           **
 ** @copyright      BSD 3 Clause (See COPYING file)                         **
 **                                                                         **
 ** *********************************************************************** **
 ** *********************************************************************** **/


#ifndef HASHMAP_SHELL_H
#define HASHMAP_SHELL_H


#include <cerrno>
#include <cstdlib>
#include <cstdio>
#include <functional>
#include <iostream>
#include <limits>
#include <sstream>
#include <stdexcept>
#include <string>
#include <unordered_map>
#include "interface.hpp"


/** @class          Shell shell.hpp "src/shell.hpp"
 *  @brief          A simple shell that processes and reacts to user input.
 *
 *  @details        Get's user input, parses it, and executes commands in a
 *                  loop until a quit command is given. This class provides an
 *                  interactive way to interact with a @link HashMap @endlink
 *                  and see changes and modifications in real time.
 */
class Shell
{
  public:
    /** @brief      Constant array/list of all commands the user can use. 
     * 
     */
    static const std::string COMMANDS[6];

    /** @brief      Create a new instance of a shell.
     *
     */
    Shell ()
        : prompt_ ("table> "), running_ (true),
        g_interface_ (new GradeInterface (&grade_map_)),
        s_interface_ (new StudentInterface (&student_map_))
    {}

    /** @brief      Destroy an instance of a shell.
     *
     */
    virtual ~Shell ()
    {
        delete g_interface_;
        delete s_interface_;
    }

    /** @brief      Run the shell
     *
     *  @details    Main shell loop. Print the prompt, get input from the user,
     *              and execute commands until the user quits the program.
     */
    void run ()
    {
        while (running_) {
            std::string cmd;
            std::cout << prompt_;
            std::getline (std::cin, cmd) ;
            std::stringstream lstream (cmd);
            exec_cmd (lstream);
        }
    }

  private:
    /** The "prompt" that is printed before we get input from the user */
    std::string     prompt_;
    /** @c true while the shell is running or @c false when it's stopping */
    bool            running_;
    /**  */
    gmap_t          grade_map_;
    /**  */
    smap_t          student_map_;
    /**  */
    MapInterface    *g_interface_;
    /**  */
    MapInterface    *s_interface_;

    /** @brief      Parse user input and execute commnads.
     *
     *  @details    Parses commands input by the user and checks if the command
     *              is valid. If it is we execute a command based on the user's
     *              input.
     *  @param cmd  The command input by the user.
     */
    void exec_cmd (std::stringstream &lstream)
    {
        std::string in0;
        try {
            in0 = get_next (lstream);
        } catch (std::out_of_range e) {
            print_help ();
        }
        try {
            MapInterface *interface = parse_table (in0);
            std::string cmd = get_next (lstream);
            interface_cmd (cmd, lstream, interface);
        } catch (std::out_of_range e) {
            noninterface_cmd (in0, lstream);
        }
    }

    /** @brief      Attempts to get the next token from the user's input.
     *
     *  @details    Uses the stringstream passed to get the next whitespace
     *              separated token from the user's input.  If the end of the
     *              input was reached it throws an out_of_range exception.
     *  @param lstream
     *              A stringstream that the next token should be read from.
     *  @throws std::out_of_range
     *              if the end of the stream was reached.
     *  @return     The next token in the stream.
     */
    std::string get_next (std::stringstream &lstream)
    {
        if (not lstream)
            throw std::out_of_range ("Reached end of input");
        std::string cmd;
        lstream >> cmd;
        cmd = str_tolower (cmd);
        return cmd;
    }

    /**
     *
     */
    void interface_cmd (std::string cmd, std::stringstream &lstream,
                        const MapInterface *interface)
    {
        if (COMMANDS[0] == cmd)
            interface->add (lstream);
        else if (COMMANDS[1] == cmd)
            interface->clear ();
        else if (COMMANDS[2] == cmd)
            interface->remove (lstream);
        else if (COMMANDS[3] == cmd)
            interface->print ();
        else if (COMMANDS[5] == cmd)
            print_help ();
        else
            no_cmd (lstream);
    }

    /** @brief      Prints an error message for an improper command then prints
     *              the help dialouge.
     *
     *  @param cmd  The full string of the command.
     */
    void no_cmd (std::stringstream &lstream)
    {
        std::fprintf (stderr, "Error: Invalid command: %s\n",
                lstream.str ().c_str ());
        print_help ();
    }

    /** @brief      Executes a command that is not specific to a table.
     *
     */
    void noninterface_cmd (std::string cmd, std::stringstream &lstream)
    {
        if (COMMANDS[4] == cmd)
            running_ = false;
        else if (COMMANDS[5] == cmd)
            print_help ();
        else
            no_cmd (lstream);
    }

    /** @brief      Extracts a table name from the stringstream and returns a
     *              MapInterface.
     *
     *  @details    Attempts to get a table name from the input stream using
     *              @link get_next @endlink
     */
    MapInterface* parse_table (std::string table)
    {
        if ("grades" == table)
            return g_interface_;
        else if ("students" == table)
            return s_interface_;
        else
            throw std::out_of_range ("Invalid table: " + table);
    }

    /** @brief          Print list of commands that can be executed on the
     *                  grades table.
     */
    void print_grades_help ()
    {
        std::string g_str = "grades ";
        std::cout << g_str << COMMANDS[0] << " <student id> <class> <grade>";
        std::cout << std::endl << "\t\t- Add grade for <student id> in <class>";
        std::cout << std::endl << g_str << COMMANDS[1] << std::endl;
        std::cout << "\t\t- Remove every grade grom the gradebook" << std::endl;
        std::cout << g_str << COMMANDS[2] << " <student id> <course>";
        std::cout << std::endl << "\t\t- Remove the grade for <student id> in" \
            " <course>" << std::endl;
        std::cout << g_str << COMMANDS[3] << std::endl;
        std::cout << "\t\t- Prints every grade for every course every student" \
            " is in." << std::endl;
    }

    /** @brief      Print a list of valid commands and what they do.
     *
     */
    void print_help ()
    {
        std::cout << std::endl << "COMMAND:\tHELP:" << std::endl;
        print_grades_help ();
        print_students_help ();
        std::cout << COMMANDS[4] << "\t\t- Terminate the program" << std::endl;
        std::cout << COMMANDS[5] << "\t\t- Print a help message" << std::endl;
        std::cout << std::endl;
    }

    /** @brief          Print list of commands that can be executed on the
     *                  students table.
     */
    void print_students_help ()
    {
        std::string s_str = "students ";
        std::cout << s_str << COMMANDS[0] << " <student id> <first name> " \
            "<last name>" << std::endl;
        std::cout << "\t\t- Add <first name> <last name> with <student id>";
        std::cout << std::endl << s_str << COMMANDS[1] << std::endl;
        std::cout << "\t\t- Remove every student from the roster";
        std::cout << std::endl << s_str << COMMANDS[2] << " <student id>";
        std::cout << std::endl << "\t\t- Remove the student with <student id>";
        std::cout << std::endl << s_str << COMMANDS[3] << std::endl;
        std::cout << "\t\t- Prints every student in the roster" << std::endl;
    }

    /** @brief      Remove an element from the @link HashTable @endlink
     *
     *  @return     @c true if the element was successfully removed, otherwise
     *              @c false.
     */
    bool remove ()
    {
        bool flag = false;
        return flag;
    }

    /** @brief      Search the @link HashTable @endlink for an element.
     *
     *  @return     @c true if the element is in the table, otherwise @c false.
     */
    bool search ()
    {
        return false;
    }

    /** @brief      Converts a string to all lowercase
     *
     *  @param s    String being converted to lowercase
     *  @return     An all lowercase version of parameter s
     */
    std::string str_tolower (std::string s)
    {
        std::string l;
        for (unsigned int i = 0; i < s.size (); ++i)
            l += tolower (s[i]);
        return l;
    }
};


#endif // HASHMAP_SHELL_H


/** *********************************************************************** **
 **                                   EOF                                   **
 ** *********************************************************************** **/
