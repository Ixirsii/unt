/** *********************************************************************** **
 ** *********************************************************************** **
 **                                                                         **
 ** @file           shell.h                                                 **
 ** @brief          Provides an interactive shell for interfacing with a    **
 **                 @link HashTable @endlink                                **
 **                                                                         **
 ** @author         Ryan Porterfield <RyanPorterfield@my.unt.edu>           **
 ** @copyright      BSD 3 Clause (See COPYING file)                         **
 **                                                                         **
 ** *********************************************************************** **
 ** *********************************************************************** **/


#ifndef HASH_SHELL_H
#define HASH_SHELL_H


#include <cerrno>
#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <limits>
#include <sstream>
#include <string>

#include "shell.hpp"


const std::string Shell::COMMANDS[6] = {"add", "clear", "delete", "display", 
    "quit", "help"};

/** @brief      Converts a string to an unsigned int.
 *
 *  @details    A c++ wrapper function around the C function strtol which
 *              safely converts a character array to a long int.
 *  @param s    The string being converted to unsigned int.
 *  @throws     std::out_of_range if the string can not be converted
 *  @return     An unsigned int representation of the string.
 */
unsigned str_to_u (std::string s)
{
    char *end;
    char const *cs = s.c_str ();
    unsigned v;
    v = strtol (cs, &end, 0);
    if (ERANGE == errno || *cs == '\0' || *end not_eq '\0') {
        std::fprintf (stderr, "ERROR: could not convert \"%s\" to int\n",
                      cs);
        throw std::out_of_range (s + " cannot be converted to unsigned");
    }
    return v;
}

int
main (void)
{
    Shell table_shell;
    table_shell.run ();
    return EXIT_SUCCESS;
}

#endif // HASH_SHELL_H


/** *********************************************************************** **
 **                                   EOF                                   **
 ** *********************************************************************** **/
