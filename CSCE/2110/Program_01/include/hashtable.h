/** *********************************************************************** **
 ** *********************************************************************** **
 **                                                                         **
 ** @file           hashtable.h                                             **
 ** @brief          Simple hash table class implementation for integers     **
 **                                                                         **
 ** @author         Ryan Porterfield <RyanPorterfield@my.unt.edu>           **
 ** @copyright      BSD 3 Clause (See COPYING file)                         **
 ** @version        1.0.0:2014-09-24                                        **
 **                                                                         **
 ** *********************************************************************** **
 ** *********************************************************************** **/


#ifndef HASHTABLE_H
#define HASHTABLE_H

#include <iostream>
#include <set>


/** @class          HashTable hashtable.h "include/hashtable.h"
 *  @brief          A hash table for integers.
 *
 *  @details        A simple hash table implementation that stores integers.
 *                  Provides an O(1) lookup time provided there are no
 *                  collisions but as the table size is only 7 collisions are
 *                  almost guaranteed. The hashing function used is:
 *                  h(k) = (k * k) % b
 *                  where b is the bucket size (7). Collisions are handled with
 *                  std::set.
 */
class HashTable
{
protected:
    /** @class      Node hashtable.h "include/hashtable.h"
     *  @brief      A bucket object for a key-value pair to be inserted in
     *              the hash table.
     *
     */
    class Node
    {
    public:
        Node (int k, int v)
            : k_ (k), v_ (v), next_ (NULL)
        {}

        virtual ~Node ()
        {}

        Node& get_next ()
        {
            return *next_;
        }

        int get_key ()
        {
            return k_;
        }

        int get_value ()
        {
            return v_;
        }

        bool has_next ()
        {
            return NULL not_eq next_;
        }

        void set_next (Node *next)
        {
            next_ = next;
        }

    private:
        int k_;
        int v_;
        Node *next_;
    };

public:
    /** @brief      Create a new empty HashTable.
     * 
     */
    HashTable ()
        : size_ (0)
    {
        for (int i = 0; i < bucket_size_; ++i)
            bucket_[i] = NULL;
    }

    /** @brief      Create a new HashTable with elements from init.
     * 
     *
     *  @param init An array of integers whose elements will be added to the
     *              new HashTable.
     */
    HashTable (int init[])
        : size_ (0)
    {
        for (int i = 0; i < bucket_size_; ++i)
            bucket_[i] = NULL;
        int len = sizeof (init) / sizeof (int);
        for (int i = 0; i < len; ++i)
            add (init[i]);
    }

    /** @brief      Create a new HashTable with elements from init.
     * 
     *  @param init An existing HashTable whose elements will be added to the
     *              new HashTable.
     */
    HashTable (HashTable &init)
        : size_ (0)
    {
        for (int i = 0; i < bucket_size_; ++i)
            bucket_[i] = NULL;
        std::set<int> v = init.values ();
        std::set<int>::iterator it;
        for (it = v.begin (); it not_eq v.end (); ++it)
            add (*it);
    }

    /** @brief      Clear all elements from the table before destroying it.
     * 
     */
    ~HashTable ()
    {
        clear ();
    }

    /** @brief      Add an element to the table.
     *
     *  @param v    The element being added to the table.
     *  @return     @c true if the element was successfully added otherwise
     *              @c false.
     */
    bool add (int v)
    {
        if (contains (v))
            return false;
        int k = hash (v);
        Node *n = new Node (k, v);
        int i = index (k);
        if (NULL == bucket_[i])
            add_first_bucket (i, n);
        else
            add_bucket (i, n);
        ++size_;
        return true;
    }

    /** @brief      Remove all elements from the HashTable.
     * 
     */
    void clear ()
    {
        for (int i = 0; i < bucket_size_; ++i) {
            Node *cur = bucket_[i];
            if (NULL == cur)
                continue;
            while (cur->has_next ()) {
                Node *del = cur;
                cur = &cur->get_next ();
                delete del;
            }
            delete cur;
            bucket_[i] = NULL;
        }
    }

    /** @brief      Check if the table contains an element.
     *
     *  @param v    The element being checked.
     *  @return     @c true if the element is in the table, otherwise @c false.
     */
    bool contains (int v)
    {
        int k = hash (v);
        int i = index (k);
        if (NULL == bucket_[i])
            return false;
        Node *cur = bucket_[i];
        while (cur->has_next ()) {
            if (cur->get_value () == v)
                return true;
            cur = &cur->get_next ();
        }
        /* Check last Node in bucket */
        if (cur->get_value () == v)
            return true;
        return false;
    }

    /** @brief      Get the number of elements in the HashTable.
     *
     *  @return     The number of elements in the table.
     */
    int get_size ()
    {
        return size_;
    }

    /** @brief      Print a formatted string of all elements in the table.
     * 
     */
    void print ()
    {
        std::cout << "Printing table" << std::endl;
        for (int i = 0; i < bucket_size_; ++i) {
            Node *cur = bucket_[i];
            std::cout << "[ ";
            if (NULL not_eq cur) {
                while (cur->has_next ()) {
                    std::cout << cur->get_value () << ", ";
                    cur = &cur->get_next ();
                }
                std::cout << cur->get_value ();
            }
            std::cout << " ]";
            if (i < (bucket_size_ - 1))
                std::cout << ", ";
        }
        std::cout << std::endl;
    }

    /** @brief      Remove an element from the table.
     * 
     *
     *  @param v    The element being removed from the table.
     *  @return     @c true if the element was successfully removed otherwise
     *              @c false.
     */
    bool remove (int v)
    {
        if (not contains (v))
            return false;
        int k = hash (v);
        int i = index (k);
        Node *cur = bucket_[i];
        if (cur->get_value () == v)
            remove_first_bucket (i);
        else
            remove_bucket (i, v);
        --size_;
        return true;
    }

    /** @brief      Get a set containing every element in the table.
     * 
     * @return      A std::set containing every element in the table.
     */
    std::set<int> values ()
    {
        std::set<int> values;
        for (int i = 0; i < bucket_size_; ++i) {
            Node *cur = bucket_[i];
            while (cur->has_next ()) {
                values.insert (cur->get_value ());
                cur = &cur->get_next ();
            }
            values.insert (cur->get_value ());
        }
        return values;
    }

protected:
    /** How many buckets in the HashTable */
    const static int    bucket_size_ = 7;
    /** How many elements are in the table */
    int                 size_;
    /** Array of sets that store elements in the table and handle collisions */
    Node                *bucket_[bucket_size_];

    /** @brief      Get a hash of the element.
     *
     *  @details    Hash an integer by squaring it
     *  @return     A hash of the integer
     */
    int
    hash (int v)
    {
        return v * v;
    }

    /** @brief      Get the bucket index to place the key-value pair in.
     *
     *  @param k    The hashed key of the value being added to the table.
     *  @return     A number between 0 and @link bucket_size_ @endlink
     */
    int index (int k)
    {
        return k % bucket_size_;
    }

private:

    /** @brief      Add a node to the last position of bucket_[index]
     *
     *  @param i    Index in @link bucket_ @endlink the node is being stored
     *              in.
     *  @param n    The @link Node @endlink being stored.
     */
    void add_bucket (int i, Node *n)
    {
        Node *cur = bucket_[i];
        while (cur->has_next ()) {
            cur = &cur->get_next ();
        }
        cur->set_next (n);
    }

    /** @brief      Add a node to the first position of bucket_[index]
     *
     *  @param i    Index in @link bucket_ @endlink the node is being stored
     *              in.
     *  @param n    The @link Node @endlink being stored.
     */
    void add_first_bucket (int i, Node *n)
    {
        bucket_[i] = n;
    }

    /** @brief      Remove a node from an arbitrary position in bucket_[index]
     *
     *  @param i    Index in @link bucket_ @endlink the node is being remvoed
     *              from.
     *  @param k    The key of the value being removed.
     */
    void remove_bucket (int i, int v)
    {
        Node *cur = bucket_[i];
        Node *del = NULL, *prev = NULL;
        /*
         * It might be useful to break this off into it's own function
         * eventually
         */
        while (cur->has_next ()) {
            if (cur->get_value () == v) {
                del = cur;
                cur = &cur->get_next ();
                break;
            }
            prev = cur;
            cur = &cur->get_next ();
        }
        /*
         * We assume that the last Node is the one we want to delete since we
         * already checked to make sure the value was in the table.
         */
        if (NULL == del) {
            del = cur;
            cur = NULL;
        }
        prev->set_next (cur);
        delete del;
    }

    /** @brief      Remove the first node from bucket_[index]
     *
     *  @param i    Index in @link bucket_ @endlink the node is being remvoed
     *              from.
     */
    void remove_first_bucket (int i)
    {
        Node *del = bucket_[i];
        if (del->has_next ())
            bucket_[i] = &del->get_next ();
        else
            bucket_[i] = NULL;
        delete del;
    }
};


#endif // HASHTABLE_H


/** *********************************************************************** **
 **                                   EOF                                   **
 ** *********************************************************************** **/
