/** *********************************************************************** **
 ** *********************************************************************** **
 **                                                                         **
 ** @file           shell.h                                                 **
 ** @brief          Provides an interactive shell for interfacing with a    **
 **                 @link HashTable @endlink                                **
 **                                                                         **
 ** @author         Ryan Porterfield <RyanPorterfield@my.unt.edu>           **
 ** @copyright      BSD 3 Clause (See COPYING file)                         **
 **                                                                         **
 ** *********************************************************************** **
 ** *********************************************************************** **/


#ifndef HASH_SHELL_H
#define HASH_SHELL_H


#include <cerrno>
#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <limits>
#include <sstream>
#include <string>
#include <hashtable.h>


/** @class          Shell shell.h "include/shell.h"
 *  @brief          A simple shell that processes and reacts to user input.
 *
 *  @details        Get's user input, parses it, and executes commands in a
 *                  loop until a quit command is given. This class provides an
 *                  interactive way to interact with a @link HashTable @endlink
 *                  and see changes and modifications in real time.
 *
 */
class Shell
{
public:
    /** @brief      Constant array/list of all commands the user can use. 
     * 
     */
    static const std::string COMMANDS[7];

    /** @brief      Create a new instance of a shell.
     *
     */
    Shell ()
        : prompt_ (">>> "), running_ (true)
    {}

    /** @brief      Destroy an instance of a shell.
     *
     */
    virtual ~Shell ()
    {}

    /** @brief      Run the shell
     *
     *  @details    Main shell loop. Print the prompt, get input from the user,
     *              and execute commands until the user quits the program.
     */
    void run ()
    {
        while (running_) {
            std::string cmd;
            std::cout << prompt_;                                                  
            std::cin >> cmd;
            parse_cmd (cmd);
        }
    }

protected:
    /** @brief      Add a number to the @link HashTable @endlink
     *
     *  @return     @c true if a number was successfully added to the table,
     *              otherwise returns @c false
     */
    bool add ()
    {
        bool flag = false;
        long int v;
        if (get_int (v)) {
            if (table_.add (v))
                flag = true;
            else
                std::fprintf (stderr, "WARNING: %ld already in table\n", v);
        }
        return flag;
    }

    /** @brief      Calls @link HashTable::clear @endlink
     *
     */
    void clear ()
    {
        table_.clear ();
    }

    /** @brief      Gets an integer as input from stdin.
     *
     *  @details    Reads input from stdin and attempts to parse it as an
     *              integer using strtol from the standard C library. Error
     *              checking is done using errno.
     *  @param v    A pointer to the integer being set. We set the integer
     *              value as a parameter passed by reference so we can return a
     *              status.
     *  @return     @c true if a number was successfully parsed, otherwise @c
     *              false.
     */
    bool get_int (long int &v)
    {
        std::string v_str;
        std::cin >> v_str;
        char *end;
        char const *s = v_str.c_str ();
        v = strtol (s, &end, 0);
        if (ERANGE == errno || *s == '\0' || *end not_eq '\0') {
            std::fprintf (stderr, "ERROR: could not convert \"%s\" to int\n",
                    s);
            return false;
        }
        return true;
    }

    /** @brief      Parse user input and execute commnads.
     *
     *  @details    Parses commands input by the user and checks if the command
     *              is valid. If it is we execute a command based on the user's
     *              input.
     *  @param cmd  The command input by the user.
     */
    void parse_cmd (std::string cmd)
    {
        cmd = str_tolower (cmd);
        if (COMMANDS[0] == cmd)
            add ();
        else if (COMMANDS[1] == cmd)
            clear ();
        else if (COMMANDS[2] == cmd)
            remove ();
        else if (COMMANDS[3] == cmd)
            search ();
        else if (COMMANDS[4] == cmd)
            table_.print ();
        else if (COMMANDS[5] == cmd)
            running_ = false;
        else if (COMMANDS[6] == cmd)
            print_help ();
        else {
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            std::cout << "Unrecognized command - " << cmd << std::endl;
            print_help ();
        }
    }

    /** @brief      Print a list of valid commands and what they do.
     *
     */
    void print_help ()
    {
        std::cout << std::endl << "COMMAND:\t\tHELP:" << std::endl;
        std::cout << COMMANDS[0] << " <n>\t\t\tAdds n to the table";
        std::cout << std::endl << COMMANDS[1];
        std::cout << "\t\t\tRemoves every element from the table" << std::endl;
        std::cout << COMMANDS[2] << " <n>\t\tRemoves n from the table";
        std::cout << std::endl;
        std::cout << COMMANDS[3] << " <n>\t\tChecks if n is in the table";
        std::cout << std::endl;
        std::cout << COMMANDS[4] << "\t\t\tPrints the table" << std::endl;
        std::cout << COMMANDS[5] << "\t\t\tTerminate the program" << std::endl;
        std::cout << COMMANDS[6] << "\t\t\tPrint a list of commands";
        std::cout << std::endl;
    }

    /** @brief      Search the @link HashTable @endlink for an element.
     *
     *  @return     @c true if the element is in the table, otherwise @c false.
     */
    bool search ()
    {
        long int v;
        if (get_int (v)) {
            if (table_.contains (v))
                std::cout << "true" << std::endl;
            else
                std::cout << "false" << std::endl;
            return true;
        }
        return false;
    }

    /** @brief      Remove an element from the @link HashTable @endlink
     *
     *  @return     @c true if the element was successfully removed, otherwise
     *              @c false.
     */
    bool remove ()
    {
        bool flag = false;
        long int v;
        if (get_int (v)) {
            if (table_.remove (v))
                flag = true;        /* v was successfully removed from table */
            else
                std::fprintf (stderr, "WARNING: %ld not in table\n", v);
        }
        return flag;
    }

private:
    /** The "prompt" that is printed before we get input from the user */
    std::string     prompt_;
    /** @c true while the shell is running or @c false when it's stopping */
    bool            running_;
    /** @link HashTable @endlink that stores all the integer values */
    HashTable       table_;

    /** @brief      Converts a string to all lowercase
     *
     *  @param s    String being converted to lowercase
     *  @return     An all lowercase version of parameter s
     */
    std::string str_tolower (std::string s)
    {
        std::string l;
        for (unsigned int i = 0; i < s.size (); ++i)
            l += tolower (s[i]);
        return l;
    }
};


#endif // HASH_SHELL_H


/** *********************************************************************** **
 **                                   EOF                                   **
 ** *********************************************************************** **/
