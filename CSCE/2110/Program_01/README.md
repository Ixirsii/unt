HashTable
=========
Author:         Ryan Porterfield <Ryan AT RyanPorterfield DOT com>  
Date Created:   2014-09-10


About
-----
This program provides a simple hash table implementation for integer  
values. The hash function `h(k) = k^2` collides when a positive and  
negative form of some integer are inserted, ie. -3 and 3.  However,  
this hash function was provided as part of the assignment.  
  
Additionally the backing table has a fixed size of 7 buckets.
  
The table provides add, remove, and contains function as expected from a  
collection as well as a print fuction to print a formatted version of  
the table to stdout.


Usage
-----
To compile just run `make`.  
To run the program execute the generated _hash_ executable ie. `./hash`


Error Checking
--------------
The shell utility provides the following error checking:

1.  Checks that the command input is valid and prints a help message if  
    not.
2.  Checks that values added to, removed from, and searched for are  
    actually integers, and prints an error message if the value  
    provided cannot be parsed.
3.  Supports values from INT\_MIN to INT\_MAX
4.  If the input value starts with 0, ie. 010 the value is parsed in base 8.
5.  If the input value starts with 0x or 0X, ie 0x10 the value is parsed  
    in base 16.


License
-------
Copyright (c) Ryan Porterfield 2014  
All rights reserved.  
  
Redistribution and use in source and binary forms, with or without  
modification, are permitted provided that the following conditions  
are met:  

1.  Redistributions of source code must retain the above copyright  
    notice, this list of conditions and the following disclaimer.  
2.  Redistributions in binary form must reproduce the above copyright  
    notice, this list of conditions and the following disclaimer in the   
    documentation and/or other materials provided with the distribution.  
3.  Neither the name of the copyright holder nor the names of its  
    contributors may be used to endorse or promote products derived from  
    this software without specific prior written permission.  

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS  
IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED  
TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A  
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT  
HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,  
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED  
TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR  
PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF  
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING  
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS  
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.  
