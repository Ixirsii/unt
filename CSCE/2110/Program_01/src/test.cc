/** *********************************************************************** **
 ** *********************************************************************** **
 **                                                                         **
 ** @file                                                                   **
 ** @brief          Simple tests to make sure @link HashTable @endlink      **
 **                 functions properly.                                     **
 **                                                                         **
 ** @author         Ryan Porterfield <RyanPorterfield@my.unt.edu>           **
 ** @copyright      BSD 3 Clause (See COPYING file)                         **
 **                                                                         **
 ** *********************************************************************** **
 ** *********************************************************************** **/


#include <iostream>
#include <hashtable.h>


/*
 * Function that adds [0..9] to a hash table and prints the state of the table.
 */
void
test_add (HashTable t)
{
    for (int i = 0; i < 10; ++i)
        t.add (i);
    t.print ();
}


/*
 * Function that checks if 2 numbers are in the table, and prints the results.
 */
void
test_contains (HashTable t)
{
    std::cout << "Table ";
    std::cout << (t.contains (8) ? "contains" : "doesn't contain");
    std::cout << " 8" << std::endl;
    std::cout << "Table ";
    std::cout << (t.contains (6) ? "contains" : "doesn't contain");
    std::cout << " 6" << std::endl;
}


/*
 * Function that removes a number from the table and prints the state of the
 * table.
 */
void
test_remove (HashTable t)
{
    t.remove (6);
    t.print ();
}


/** *********************************************************************** **
 **                                   EOF                                   **
 ** *********************************************************************** **/
