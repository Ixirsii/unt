/** *********************************************************************** **
 ** *********************************************************************** **
 **                                                                         **
 ** @file           main.cc                                                 **
 ** @brief          Start a @link Shell @endlink to interact with a         **
 **                 @link HashTable @endlink                                **
 **                                                                         **
 ** @details        Creates and runs an @link Shell @endlink so the user    **
 **                 can interact with a @link HashTable @endlink which      **
 **                 stores integers.                                        **
 **                 The shell does the following error checking:            **
 **                                                                         **
 **                     1)  Checks that the command input is valid and      **
 **                         prints a help message if not.                   **
 **                     2)  Checks that values added to, removed from,      **
 **                         and searched for are actually integers.         **
 **                     3)  Supports values from INT_MIN to INT_MAX         **
 **                     4)  If the input value starts with 0, ie. 010       **
 **                         the value is parsed as a base 8 integer.        **
 **                     5)  If the input value starts with 0x or 0X,        **
 **                         ie 0x10 the value is parsed in base 16.         **
 **                                                                         **
 ** @author         Ryan Porterfield <RyanPorterfield@my.unt.edu>           **
 ** @copyright      BSD 3 Clause (See COPYING file)                         **
 **                                                                         **
 ** *********************************************************************** **
 ** *********************************************************************** **/


#include <iostream>
#include <string>
#include <stdlib.h>
#include <hashtable.h>
#include <shell.h>


/** @brief Array of all valid @link Shell @endlink commands.
 *
 */
const std::string Shell::COMMANDS[] = {
    "add",      /**< Add an integer to the table if it isn't already present */
    "clear",    /**< Removes all values from the table */
    "delete",   /**< Removes a specified value from the table */
    "search",   /**< Checks to see if the specified value is in the table */
    "show",     /**< Prints a nicely formated version of the table to stdout */
    "quit",     /**< Exit the program */
    "help"      /**< Print a help message with all valid commands */
};


int
main (void)
{
    Shell sh;
    sh.run ();
    return EXIT_SUCCESS;
}


/** *********************************************************************** **
 **                                   EOF                                   **
 ** *********************************************************************** **/
